<?php
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@my', dirname(dirname(__DIR__)) . '/my');
Yii::setAlias('@event', dirname(dirname(__DIR__)) . '/event');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');
