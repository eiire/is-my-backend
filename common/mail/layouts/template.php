<?php
use yii\helpers\Html;
use yii\helpers\Url;
$this->beginPage(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>" />
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody(); ?>
    <table cellpadding="0" cellspacing="0" width="100%" height="auto" style="border-collapse:collapse;">
        <tbody>
            <tr>
                <td bgcolor="#f6f7fb" style="padding: 80px;">
                    <table width="650" align="center" cellpadding="0" cellspacing="0" height="auto" style="border-collapse:collapse;">
                        <tbody>
                            <tr>
                                <td align="center" style="padding-bottom:20px;">
                                    <img src="https://my.is1c.ru/img/logo.png" alt="">
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table style="border:1px solid #c8c8c8;border-radius:20px 0 20px 0;" cellpadding="0" cellspacing="0" width="650" height="auto" align="center" bgcolor="#ffffff">
                        <tbody>
                            <tr>
                                <td style="padding:30px 25px 20px 25px;">
                                    <?php if ($title) : ?>
                                    <p style="font-weight:bold;line-height:21px;padding:0;margin:0;margin-bottom:20px;"><font color="#000000" face="Arial" size="3"><?=$title; ?></font></p>
                                    <?php endif; ?>
                                    <p style="font-weight:normal;line-height:21px;padding:0;margin:0;"><font color="#000000" face="Arial" size="3"><?=$content; ?></font></p>
                                    <table cellpadding="0" cellspacing="0" width="100%" height="auto" style="border-collapse:collapse;margin-top:15px;">
                                        <tbody>
                                            <tr>
                                                <td><span><font color="#999999" size="2" face="Arial">С уважением, Ваш ИнфоСофт</font></span></td>
                                                <td align="right"><span><font color="#999999" size="2" face="Arial">+7 (383) 211-27-27</span></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <?php if (isset($links) && $links != false) : ?>
                    <table width="650" align="center" cellpadding="0" cellspacing="0" height="auto" style="border-collapse:collapse;">
                        <tbody>
                            <tr>
                                <td align="center" style="padding-top:15px;padding-left:25px;padding-right:25px;">
                                    <a href="https://is1c.ru" target="_blank" style="text-decoration:none;cursor:pointer;" rel="noopener noreferrer"><font color="#333e8d" size="3" face="Arial">is1c.ru</font></a>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <a href="https://my.is1c.ru" target="_blank" style="text-decoration:none;cursor:pointer;" rel="noopener noreferrer"><font color="#333e8d" size="3" face="Arial">my.is1c.ru</font></a>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <a href="mailto:my@is1c.ru" target="_blank" style="text-decoration:none;cursor:pointer;" rel="noopener noreferrer"><font color="#333e8d" size="3" face="Arial">my@is1c.ru</font></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <?php endif; ?>
                </td>
            </tr>
        </tbody>
    </table>
    <?php $this->endBody(); ?>
    </body>
</html>
<?php $this->endPage(); ?>