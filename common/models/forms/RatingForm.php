<?php
namespace common\models\forms;

use Yii;
use yii\base\Model;

class RatingForm extends Model {
    public $rating;
    public $number;
    public $date;
    public $message;

    public function rules() {
        return [
            [['rating', 'number', 'date', 'message'], 'string']
        ];
    }
}