<?php
namespace common\models\helpers;

class FormatHelper {
    /**
     * Удаляет повторяющиеся первые символы из строки
     * 
     * @param string $str строка
     * @param string $symbol символ для удаления
     * 
     * @return string
     */
    static function number($str, $symbol) {
        $s = substr($str, 0, 1);
        if ($s == $symbol) {
            $str = substr($str, 1, strlen($str));
            $str = self::number($str, $symbol);
        }
        return $str;
    }

    /**
     * Форматирует размер
     * 
     * @param int $size
     * 
     * @return string 
     */
    static function size($size) {
        $size_name = ['Байт', 'КБ', 'МБ', 'ГБ', 'ТБ'];
        return $size ? round($size / pow(1024, ($i = floor(log($size, 1024)))), 2) . ' ' . $size_name[$i] : '0 Байт';
    }

    /**
     * Обрезает строку с начала
     * 
     * @param string $str строка
     * @param int $lenght длина строки
     * 
     * @return string
     */
    static function phone($str, $lenght = 10) {
        $str = preg_replace("/[^0-9]/", '', $str);
        return substr($str, strlen($str) - $lenght);
    }

    /**
     * Подстовляет нужное окончание существительных после числительных
     * 
     * @param int $number числительное
     * @param array $words существительные
     * 
     * @return string
     */
    static function word($number, $words = []) {
        $cases = [2, 0, 1, 1, 1, 2];
        return $words[($number % 100 > 4 && $number % 100 < 20) ? 2 : $cases[min($number % 10, 5)]];
    }

    /**
     * Подстовляет нужное окончание слова "час" в зависимости от значения
     * 
     * @param int $hour кол-во часов
     * 
     * @return string
     */
    static function hour($hour) {
        return self::word($hour, ['час', 'часа', 'часов']);
    }

    /**
     * Подстовляет нужное окончание слова "день" в зависимости от значения
     * 
     * @param int $day кол-во дней
     * 
     * @return string
     */
    static function day($day) {
        return self::word($day, ['день', 'дня', 'дней']);
    }

    /**
     * Заменяет указанную подстроку в строке
     * 
     * @param string $subject Строка для замены
     * @param array $replace Заменяемые значения
     * 
     * @return string
     */
    static function replace($subject, $replace) {
        $keys = array_keys($replace);
        foreach ($keys as $key) {
            $subject = preg_replace('/(' . $key . ')/iu', $replace[$key], $subject);
        }
        return $subject;
    }

    /**
     * Форматирует число в сумму с разделением
     * 
     * @param int $value число для форматирования
     * @param string $postfix символ валюты
     * 
     * @return string
     */
    static function money($value, $postfix = '&#8381;') {
        $result = number_format($value, 0, '.', ' ');
        if ($postfix) $result .= $postfix;
        return $result;
    }
}