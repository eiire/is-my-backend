<?php
namespace common\models\helpers;

class NumbersHelper {
    /**
     * Генерирует заданное кол-во неповторяющихся чисел
     * 
     * @param int $count - кол-во чисел
     * @param array $range - диапазон чисел
     * 
     * @return array
     */
    static function generateNumbers($count, $range = [1, 99]) {
        do {
            $numbers = [];
            for ($i = 0; $i < $count; $i++) {
                $numbers[] = rand($range[0], $range[1]);
            }
        } while (count(array_unique($numbers)) != $count);
        return $numbers;
    }
}