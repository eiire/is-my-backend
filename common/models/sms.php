<?php
namespace common\models;

use Yii;
use common\models\Request;

class SMS {
    private $url = 'https://sms.ru/sms/send';
    private $token;

    function __construct() {
        $this->token = Yii::$app->params['sms'];
    }

    /**
     * Отправляет SMS сообщение
     * 
     * @param string $to Номер телефона
     * @param string $message Текст сообщения
     * 
     * @return bool|string Возвращает true, в случаи успеха, или код ошибки
     */
    public function sendSms($to, $message) {
        if (YII_DEBUG) return true;
        $params = http_build_query([
            'api_id' => $this->token,
            'to' => $to,
            'msg' => $message,
            'json' => 1
        ]);

        $request = new Request($this->url . '?' . $params);
        $request->execute();
        $response = json_decode($request->getResponse());

        if ($response->status == 'OK') return true;
        return false;
    }
}