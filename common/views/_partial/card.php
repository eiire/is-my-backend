<?php
use yii\helpers\Url;
$class = ($disable) ? 'card card-disable' : 'card';
?>
<div class="card-wrapper block">
    <?php if (!$disable) : ?> 
        <a href="<?=Url::to($route); ?>">
    <?php endif; ?>
        <div class="<?=$class; ?>">
            <div class="card-title"><?=$title; ?></div>
            <div class="card-circle"></div>
            <?php if ($icon) : ?>
            <div class="card-icon">
                <i class="<?=$icon; ?>"></i>
            </div>
            <?php endif; ?>
        </div>
    <?php if (!$disable) : ?> 
    </a>
    <?php endif; ?>
</div>