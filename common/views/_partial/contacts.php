<?php
$is = 'ИнфоСофт';
$phone = '+7 (383) 211–27–27';
$web = '<a href="https://is1c.ru/">www.is1c.ru</a>';
$email = '<a href="mailto:info@is1c.ru">info@is1c.ru</a>';
?>
<?php if ($home) : ?>
<div class="contacts">
    <div class="row">
        <div class="col-12 col-sm">
            <span><?=date('Y'); ?> &copy; <?=$is; ?></span>
        </div>
        <div class="col-12 col-sm-auto">
            <span><?=$web; ?></span>
            <span><?=$email; ?></span>
            <span><?=$phone; ?></span>
        </div>
    </div>
</div>
<?php else: ?>
<div class="contacts">
    <p><?=$web; ?></p>
    <p><?=$email; ?></p>
    <p><?=$phone; ?></p>
    <p><?=date('Y'); ?> &copy; <?=$is; ?></p>
</div>
<?php endif; ?>