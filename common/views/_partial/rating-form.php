<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
$title = ($title) ? $title : 'Оцените качество услуги';
?>
<div id="rating-modal" class="modal" style="display:none;">
    <div class="modal-title"><?=$title; ?></div>
    <?php $active_form = ActiveForm::begin([
        'enableClientValidation' => false,
        'fieldConfig' => ['options' => ['tag' => false]]
    ]); ?>
    <fieldset class="rating m-t-10">
        <input type="radio" id="star5" name="rating" value="5">
        <label class="full" for="star5" title="Отлично"></label>
        <input type="radio" id="star4" name="rating" value="4">
        <label class="full" for="star4" title="Хорошо"></label>
        <input type="radio" id="star3" name="rating" value="3">
        <label class="full" for="star3" title="Удовлетворительно"></label>
        <input type="radio" id="star2" name="rating" value="2">
        <label class="full" for="star2" title="Неудовлетворительно"></label>
        <input type="radio" id="star1" name="rating" value="1">
        <label class="full" for="star1" title="Плохо"></label>
    </fieldset>
    <div class="clearfix"></div>
    <div id="rating-message" class="text-primary m-l-20" style="visibility:hidden;">Оставьте, пожалуйста, комментарий</div>
    <div class="input-group p-l-20 p-r-20">
    <?php
        echo $active_form
            ->field($form, 'rating')
            ->input('hidden')
            ->label(false);

        echo $active_form
            ->field($form, 'number')
            ->input('hidden')
            ->label(false);

        echo $active_form
            ->field($form, 'date')
            ->input('hidden')
            ->label(false);

        echo $active_form
            ->field($form, 'message')
            ->textarea(['rows' => 3])
            ->label('Комментарий');
    ?>
    </div>
    <div class="row modal-control">
        <div class="col-6">
            <?=Html::submitButton('Оценить', ['id' => 'rating-btn', 'class' => 'btn btn-primary modal-btn', 'disabled' => true]); ?>
        </div>
        <div class="col-6 text-right">
            <a href="javascript:;" class="btn btn-light modal-btn" data-rating-close>Отмена</a>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>