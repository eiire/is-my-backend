<?php
namespace common\widgets;

use yii\base\Widget;
use yii\helpers\Html;

class Alert extends Widget {
    public $type;
    public $message;
    public $close;

    public function run() {
        $class = 'alert alert-' . $this->type;
        $inner = $this->message;
        if ($this->close) $inner .= Html::button('&times;', ['class' => 'alert-close']);
        $alert = Html::tag('div', $inner, ['class' => $class]);
        return $alert;
    }
}