<?php
namespace common\widgets;

use yii\base\Widget;
use yii\helpers\Url;
use yii\helpers\Html;

class Modal extends Widget {
    public $id;
    public $title;
    public $message;
    public $button;
    public $close;
    public $form;

    public function run() {
        $inner = '';
        if (isset($this->title)) $inner .= Html::tag('div', $this->title, ['class' => 'modal-title']);
        if (isset($this->message)) $inner .= Html::tag('div', $this->message, ['class' => 'modal-desc']);

        $buttons = '';
        if (isset($this->button)) {
            $button = Html::a($this->button['label'], Url::to($this->button['route']), ['class' => isset($this->button['class']) ? $this->button['class'] : 'btn btn-primary']);
            $buttons .= Html::tag('div', $button, ['class' => 'col']);
        }

        if (isset($this->form)) {
            $inner .= Html::beginForm($this->form['action'], 'GET', $this->form['options']);
            $inner .= Html::hiddenInput($this->form['input']['name'], $this->form['input']['value'], $this->form['input']['options']);

            $button = Html::submitButton($this->form['button']['label'], ['class' => isset($this->form['button']['class']) ? $this->form['button']['class'] : 'btn btn-primary']);
            $buttons .= Html::tag('div', $button, ['class' => 'col']);
        }

        $close = Html::button(isset($this->close) ? $this->close : 'Отмена', ['class' => 'btn btn-light', 'data-fancybox-close' => true]);
        $buttons .= Html::tag('div', $close, ['class' => 'col text-right']);

        if (isset($this->button) || isset($this->close) || isset($this->form)) $inner .= Html::tag('div', $buttons, ['class' => 'row modal-control']);
        if (isset($this->form)) $inner .= Html::endForm();
        $modal = Html::tag('div', $inner, ['id' => $this->id, 'class' => 'modal', 'style' => 'display:none;']);

        return $modal;
    }
}