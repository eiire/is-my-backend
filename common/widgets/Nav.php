<?php
namespace common\widgets;

use Yii;
use yii\base\Widget;
use yii\helpers\Url;

class Nav extends Widget {
    public $ul_class;
    public $set_active;
    public $items;

    protected function setActive($route) {
        $_route = explode('/', $route[0]);
        return $this->set_active && Yii::$app->controller->id == $_route[0] && Yii::$app->controller->action->id == $_route[1] && count($route) == 1;
    }

    protected function createItem($label, $route, $icon = false, $class = false) {
        $item = '<li';

        $style = '';
        if ($class) $style .= $class;
        if ($this->setActive($route)) $style .= ' active';
        if ($style != '') $style = ' class="' . trim($style) . '"';

        $item .= $style . '>';
        if (isset($route['modal'])) {
            $item .= '<a href="javasctipt:;" data-fancybox data-modal="' . $route['modal'] . '" data-src= "' . $route['src'] . '"';
            if (isset($route['data'])) $item .= ' data-' . $route['data']['name'] . '="' . $route['data']['value'] . '"';
            $item .= '>';
        } elseif ($route) {
            $item .= '<a href="' . Url::to($route) . '">';
        } else {
            $item .= '<span>';
        }
        if ($icon) {
            $item .= '<i class="' . $icon. '"></i> ';
        }
        $item .= $label;
        if ($route) {
            $item .= '</a>';
        } else {
            $item .= '</span>';
        }
        return $item;
    }

    public function run() {
        $item = $this->items;
        $nav = '<ul class="' . $this->ul_class . '">';
        for ($i = 0; $i < count($this->items); $i++) {
            if (isset($item[$i]['line'])) {
                $nav .= '<div class="nav-line"></div>';
            } elseif (!$item[$i]['hidden']) {
                $nav .= $this->createItem($item[$i]['label'], $item[$i]['route'], $item[$i]['icon'], $item[$i]['class']);
                if (isset($item[$i]['items'])) {
                    $nav .= '<ul>';
                    $sub_item = $item[$i]['items'];
                    for ($j = 0; $j < count($sub_item); $j++) {
                        $nav .= $this->createItem($sub_item[$j]['label'], $sub_item[$j]['route'], $sub_item[$j]['icon'], $item[$j]['class']);
                        $nav .= '</li>';
                    }
                    $nav .= '</ul>';
                }
                $nav .= '</li>';
            }
        }
        $nav .= '</ul>';
        return $nav;
    }
}