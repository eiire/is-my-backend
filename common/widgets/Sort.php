<?php
namespace common\widgets;

use yii\base\Widget;
use yii\helpers\Url;

class Sort extends Widget {
    private $sort;
    public $style;
    public $items;

    public function init() {
        if (isset($_GET['sort'])) {
            $this->sort = $_GET['sort'];
        }
        parent::init();
    }

    public function run() {
        $class = ($this->style) ? ' class="' . $this->style .'"' : '';
        $widget = '<ul' . $class . '>';

        foreach ($this->items as $item) {
            if ($this->sort == $item['sort']) {
                $widget .= '<li>' . $item['title'] . '</li>';
            } else {
                $widget .= '<li><a href="' . Url::current(['sort' => $item['sort']]);
                if ($item['anchor']) $widget .= ' #' . $item['anchor'];
                $widget .= '" class="btn btn-link">' . $item['title'] . '</a></li>';
            }
        }

        $widget .= '</ul>';
        return $widget;
    }
}