<?php
namespace common\widgets;

use Yii;
use yii\base\Widget;
use yii\helpers\Url;

class WeekNav extends Widget {
    public function run() {
        $week = ($_GET['week']) ? (int) $_GET['week'] : 1;

        $prev = ($week <= 1) ? 0 : $week - 1;
        $next = ($week >= 12) ? 0 : $week + 1;
        
        $nav = '<div class="row m-b-10">';
        $nav .= '<div class="col-6">';
        if ($prev != 0) {
            $nav .= '<a href="' . Url::to(['specialist/report', 'stage' => 2, 'week' => $prev]) . '"><i class="fas fa-long-arrow-alt-left"></i> Предыдущая неделя</a>';
        }
        $nav .= '</div>';

        if ($next != 0) {
            $nav .= '<div class="col-6 text-right">';
            $nav .= '<a href="' . Url::to(['specialist/report', 'stage' => 2, 'week' => $next]) . '">Следующая неделя <i class="fas fa-long-arrow-alt-right"></i></a>';
            $nav .= '</div>';
        }
        $nav .= '</div>';
        return $nav;
    }
}