<?php
namespace console\controllers;

use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\helpers\Console;
use my\models\users\Users;
use my\models\users\UsersCode;
use my\models\Client;
use common\models\Request;
use common\models\helpers\FormatHelper;

class CommandController extends Controller {
    private function arrayToStr($array) {
        $str = "\n";
        foreach ($array as $key => $value) {
            $str .= (is_array($array[$key])) ? $this->arrayToStr($array[$key]) : $key . ': ' . $value . "\n";
        }
        return $str;
    }

    public function actionUser($user) {
        $type = (filter_var($user, FILTER_VALIDATE_EMAIL)) ? 'email' : 'code';

        if ($type == 'email') {
            $users = Users::find()
                ->where(['login' => $user])
                ->asArray()
                ->one();

            $codes = UsersCode::find()
                ->where(['user_id' => $users['id']])
                ->asArray()
                ->all();
        } elseif ($type == 'code') {
            $codes = UsersCode::find()
                ->where(['code' => $user])
                ->asArray()
                ->all();

            $user_ids = [];
            if (is_array($codes)) {
                foreach ($codes as $code) {
                    $user_ids[] = $code['user_id'];
                }
            } else {
                $user_ids = $codes['user_id'];
            }

            $users = Users::find()
                ->where(['id' => $user_ids])
                ->asArray()
                ->all();
        }

        if ($users && $codes) $result = array_merge($users, $codes);

        if ($result) {
            $this->stdout($this->arrayToStr($result));
        } else {
            $this->stdout("Пользователь не найден\n");
        }

        return ExitCode::OK;
    }

    public function actionGeneratePassword($password) {
        $this->stdout(hash('sha256', Yii::$app->params['salt'] . $password));
        return ExitCode::OK;
    }

    public function actionCodesUpdate() {
        $users = UsersCode::find()->all();

        foreach ($users as $user) {
            $client = new Client();
            $client->id = $user->code;
            $details = $client->sendRequest(Client::UPP, Client::CLIENT);
            $details->full_name = urldecode($details->full_name);

            $company = FormatHelper::replace($details->full_name, [
                'Индивидуальный предприниматель' => 'ИП',
                'Общество с ограниченной ответственностью' => 'ООО'
            ]);

            UsersCode::updateAll([
                'name' => $company,
                'inn' => $details->inn,
                'cashbox' => $details->kassa,
            ], [
                'user_id' => $user->user_id,
                'code' => $user->code
            ]);
        }
    }
}