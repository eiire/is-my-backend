<?php
namespace event\assets;

use yii\web\AssetBundle;

class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700',
        'https://use.fontawesome.com/releases/v5.3.1/css/all.css',
        'css/common.css',
        'css/style.css'
    ];

    public $js = [
        'js/common.js',
        'js/script.js'
    ];
}