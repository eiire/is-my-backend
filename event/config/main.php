<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-event',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'event\controllers',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-event',
        ],
        'session' => [
                'name' => 'advanced-event',
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=event',
            'username' => 'root',
            'password' => 'Gfhjkm_root_mysql_2018',
            'charset' => 'utf8',
            // Schema cache options (for production environment)
            'enableSchemaCache' => true,
            'schemaCacheDuration' => 3600,
            'schemaCache' => 'cache',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '' => 'site/index',
                '<alias:signin|signup|confirm>' => 'auth/<alias>',
                '<alias:favorites|feedback|history|note|questions|rating|result|timeline|video|welcome>' => 'site/<alias>',
                '<alias:privacy>' => 'pages/<alias>',
                'dashboard' => 'dashboard/index',
            ],
        ],
    ],
    'defaultRoute' => 'auth/signin',
    'params' => $params,
];
