<?php
namespace event\controllers;

use Yii;
use yii\web\Controller;
use yii\db\Query;
use event\models\Event;
use event\models\users\User;
use event\models\users\Users;
use event\models\db\Events;
use event\models\db\Sections;
use event\models\db\Schedule;
use event\models\db\Favorites;
use event\models\db\Questions;
use event\models\db\QuestionsAnswers;
use event\models\db\Likes;
use event\models\db\Rating;
use event\models\db\FeedbackQuestions;
use event\models\db\FeedbackAnswers;
use event\models\helpers\FileHelper;
use common\models\helpers\FormatHelper;

class ApiController extends Controller {
    private $user_id;
    private $event_id;
    private $section_id;

    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    public function beforeAction($action) {
        Yii::$app->response->getHeaders()->set('Access-Control-Allow-Origin', '*');

        $this->enableCsrfValidation = false;
        $this->layout = false;

        $this->user_id = 1;
        $this->event_id = 4;
        $this->section_id = 6;

        return parent::beforeAction($action);
    }

    public function actionEvent() {
        $event = Events::find()
            ->select(['id', 'title', 'desc', 'date_start', 'date_end', 'location'])
            ->where(['id' => $this->event_id])
            ->asArray()
            ->one();

        $event['date_start'] = date('d.m.Y', strtotime($event['date_start']));
        $event['date_end'] = date('d.m.Y', strtotime($event['date_end']));

        $result = [
            'event' => $event
        ];

        $result = json_encode($result, JSON_UNESCAPED_UNICODE);

        return $this->render('api', [
            'result' => $result
        ]);
    }

    public function actionSchedule() {
        $sections = Sections::find()
            ->select(['id', 'title'])
            ->where(['event_id' => $this->event_id])
            ->asArray()
            ->all();

        $schedule = Schedule::find()
            ->where(['event_id' => $this->event_id])
            ->asArray()
            ->all();

        $schedule_ids = [];
        foreach ($schedule as $row) {
            $schedule_ids[] = $row['id'];
        }

        // Доклады в избранном у пользователя
        $favorites = Favorites::find()
            ->select('schedule_id')
            ->where(['event_id' => $this->event_id, 'user_id' => $this->user_id])
            ->asArray()
            ->all();

        $favorites_result = [];
        foreach ($favorites as $row) {
            $favorites_result[$row['schedule_id']] = true;
        }

        // Доклады с оценкой пользователя
        $rating = Rating::find()
            ->select(['schedule_id', 'rating'])
            ->where(['schedule_id' => $schedule_ids, 'user_id' => $this->user_id])
            ->asArray()
            ->all();

        $rating_result = [];
        foreach ($rating as $row) {
            $rating_result[$row['schedule_id']] = $row['rating'];
        }

        // Средняя оценка докладов
        $average = Rating::find()
            ->select(['AVG(rating)', 'schedule_id'])
            ->where(['schedule_id' => $schedule_ids])
            ->groupBy('schedule_id')
            ->asArray()
            ->all();

        $average_result = [];
        foreach ($average as $row) {
            $average_result[$row['schedule_id']] = round($row['AVG(rating)'], 1);
        }

        $schedule_formated = [];
        foreach ($schedule as &$row) {
            $row['rating'] = ($rating_result[$row['id']]) ? $rating_result[$row['id']] : false;
            $row['rating_available'] = (!isset($rating_result[$row['id']]) && time() >= strtotime($row['time_start'])) ? true : false;
            $row['average_rating'] = ($average_result[$row['id']]) ? $average_result[$row['id']] : false;

            // Вопросы можно задвать после начала доклада и час после завершения
            $row['questions_available'] = (time() >= strtotime($row['time_start']) && time() <= strtotime($row['time_end']) + 60*60) ? true : false;

            $row['favorite'] = ($favorites_result[$row['id']]) ? true : false;

            $row['date'] = date('d.m.Y', strtotime($row['date']));
            $row['time_start'] = date('H:i', strtotime($row['time_start']));
            $row['time_end'] = date('H:i', strtotime($row['time_end']));
            $row['today'] = (date('Ymd') == date('Ymd', strtotime($row['date'])));
            $row['started'] = (time() >= strtotime($row['time_start'])); // !!! Добавить проверку на дату

            $photo = FileHelper::search($row['id'], 'upload/event/' . $row['event_id'] . '/speakers', ['jpg', 'png']);
            $row['photo'] = $photo ? $photo : '';

            $duration = strtotime($row['time_end']) - strtotime($row['time_start']);
            if ($duration > 60*60) {
                $row['duration'] = gmdate('G', (int) $duration) . ' ' . FormatHelper::word(gmdate('G', (int) $duration), ['час', 'часа', 'часов']);
                if (gmdate('i', (int) $duration) != '00') {
                    $row['duration'] .= ' ' . gmdate('i', (int) $duration) . ' ' . FormatHelper::word(intval(gmdate('i', (int) $duration)), ['минута', 'минуты', 'минут']);
                }
            } else {
                $row['duration'] = gmdate('i', (int) $duration) . ' ' . FormatHelper::word($duration, ['минута', 'минуты', 'минут']);
            }

            unset($row['user_id']);
            $schedule_formated[$row['section_id']][$row['id']] = $row;
        }

        $schedule_result = [];
        foreach ($sections as $section) {
            $schedule_result[] = [
                'title' => $section['title'],
                'schedule' => $schedule_formated[$section['id']]
            ];
        }

        $result = [
            'schedule' => $schedule_result,
            'sections' => count($schedule_result)
        ];

        $result = json_encode($result, JSON_UNESCAPED_UNICODE);

        return $this->render('api', [
            'result' => $result
        ]);
    }

    public function actionFavorites() {
        $favorites = Favorites::find()
            ->select('schedule_id')
            ->where(['event_id' => $this->event_id, 'user_id' => $this->user_id])
            ->asArray()
            ->all();

        $favorites_ids = [];
        foreach ($favorites as $favorite) {
            $favorites_ids[] = $favorite['schedule_id'];
        }

        $result = [
            'favorites' => $favorites_ids
        ];

        $result = json_encode($result, JSON_UNESCAPED_UNICODE);

        return $this->render('api', [
            'result' => $result
        ]);
    }

    public function actionAddToFavorite() {
        $id = Yii::$app->request->get('id');

        if ($id) {
            $data = [
                'user_id' => $this->user_id,
                'event_id' => $this->event_id,
                'schedule_id' => (int) $id
            ];

            $favorite = Favorites::find()
                ->select('id')
                ->where($data)
                ->one();

            if ($favorite) {
                Favorites::deleteAll($data);
                $result = ['result' => false];
            } else {
                (new Query())->createCommand()
                    ->insert('favorites', $data)
                    ->execute();

                $result = ['result' => true];
            }
        }

        if ($result) {
            $result = json_encode($result, JSON_UNESCAPED_UNICODE);

            return $this->render('api', [
                'result' => $result
            ]);
        }

        throw new \yii\web\NotFoundHttpException();
    }

    public function actionQuestions() {
        $id = (int) Yii::$app->request->get('id');

        $questions = Questions::find()
            ->where(['schedule_id' => $id])
            ->orderBy(['date' => SORT_DESC])
            ->asArray()
            ->all();

        $user_ids = [];
        $question_ids = [];
        foreach ($questions as $question) {
            $user_ids[] = $question['user_id'];
            $question_ids[] = $question['id'];
        }

        $users = Users::find()
            ->select(['id', 'name'])
            ->where(['id' => $user_ids])
            ->asArray()
            ->all();

        $users_result = [];
        foreach ($users as $user) {
            $users_result[$user['id']] = $user['name']; 
        }

        // Получаем список вопросов, добавленных в избранное текущим пользователем
        $likes = Likes::find()
            ->select('question_id')
            ->where(['schedule_id' => $id, 'user_id' => $this->user_id])
            ->asArray()
            ->all();

        $likes_result = [];
        foreach ($likes as $like) {
            $likes_result[$like['question_id']] = true;
        }

        // Получаем кол-во лайков
        $likes_count = Likes::find()
            ->select(['COUNT(1)', 'question_id'])
            ->where(['schedule_id' => $id])
            ->groupBy('question_id')
            ->asArray()
            ->all();

        $likes_count_result = [];
        $total_likes = 0;
        foreach ($likes_count as $like) {
            $likes_count_result[$like['question_id']] = $like['COUNT(1)'];
            $total_likes += $like['COUNT(1)'];
        }

        // Получим ответы к вопросам
        $answers = QuestionsAnswers::find()
            ->where(['question_id' => $question_ids])
            ->asArray()
            ->all();

        $answers_result = [];
        foreach ($answers as $answer) {
            $answers_result[$answer['question_id']] = $answer['answer'];
        }

        foreach ($questions as &$question) {
            $question['author'] = $users_result[$question['user_id']];
            $question['liked'] = ($likes_result[$question['id']]) ? true : false;
            $question['likes'] = ($likes_count_result[$question['id']] > 0) ? $likes_count_result[$question['id']] : 0;
            $question['answer'] = ($answers_result[$question['id']]) ? $answers_result[$question['id']] : false;
        }

        $count = count($questions);

        if (Yii::$app->request->get('type') == 'count') {
            $result = [
                'count' => $count,
                'likes' => $total_likes
            ];
        } else {
            $result = [
                'questions' => $questions,
                'count' => $count,
                'likes' => $total_likes
            ];
        }

        $result = json_encode($result, JSON_UNESCAPED_UNICODE);

        return $this->render('api', [
            'result' => $result
        ]);
    }

    public function actionAddQuestion() {
        $schedule_id = (int) Yii::$app->request->get('id');
        $question = htmlspecialchars(trim(Yii::$app->request->get('question')));

        $query = (new Query())->createCommand()
            ->insert('questions', [
                'schedule_id' => $schedule_id,
                'user_id' => $this->user_id,
                'question' => $question
            ])
            ->execute();

        $result = ($query) ? true : false;

        $result = [
            'result' => $result
        ];

        $result = json_encode($result, JSON_UNESCAPED_UNICODE);

        return $this->render('api', [
            'result' => $result
        ]);
    }

    public function actionLikeQuestion() {
        $schedule_id = (int) Yii::$app->request->get('schedule');
        $question_id = (int) Yii::$app->request->get('question');

        $query = (new Query())->createCommand()
            ->insert('likes', [
                'user_id' => $this->user_id,
                'schedule_id' => $schedule_id,
                'question_id' => $question_id
            ])
            ->execute();

        $result = ($query) ? true : false;

        $result = [
            'result' => $result
        ];

        $result = json_encode($result, JSON_UNESCAPED_UNICODE);

        return $this->render('api', [
            'result' => $result
        ]);
    }

    public function actionFeedbackQuestions() {
        $answers = FeedbackAnswers::find()
            ->where(['user_id' => $this->user_id, 'event_id' => $this->event_id])
            ->count();

        if ($answers == 0) {
            $questions = FeedbackQuestions::find()
                ->select(['id', 'question', 'choice', 'type'])
                ->where(['event_id' => $this->event_id])
                ->asArray()
                ->all();

            foreach ($questions as &$question) {
                if ($question['choice']) $question['choice'] = explode(';', $question['choice']);
            }
        } else {
            $questions = false;
        }

        $result = [
            'feedback' => $questions
        ];

        $result = json_encode($result, JSON_UNESCAPED_UNICODE);

        return $this->render('api', [
            'result' => $result
        ]);
    }

    public function actionFeedbackAnswers() {
        $json = Yii::$app->request->post('json');

        if ($json) {
            $count = FeedbackAnswers::find()
                ->where(['user_id' => $this->user_id, 'event_id' => $this->event_id])
                ->count();

            if ($count == 0) {
                $answers = json_decode($json);

                $data = [];
                foreach ($answers->answers as $key => $value) {
                    $data[] = [
                        'user_id' => $this->user_id,
                        'event_id' => $this->event_id,
                        'question_id' => $key,
                        'answer' => $value,
                    ];
                }
                
                $query = (new Query())->createCommand()
                    ->batchInsert('feedback_answers', ['user_id', 'event_id', 'question_id', 'answer'], $data)
                    ->execute();

                $result = ['result' => $query ? true : false];
            } else {
                $result = ['result' => false];
            }
        } else {
            $result = ['result' => false];
        }

        $result = json_encode($result, JSON_UNESCAPED_UNICODE);

        return $this->render('api', [
            'result' => $result
        ]);
    }

    public function actionRating() {
        $id = (int) Yii::$app->request->get('id');
        $rating = (int) Yii::$app->request->get('rating');
        $message = Yii::$app->request->get('message') ? Yii::$app->request->get('message') : NULL;

        if ($id && $rating) {
            $count = Rating::find()
                ->where(['user_id' => $this->user_id, 'schedule_id' => $id])
                ->count();

            if ($count == 0) {
                $query = (new Query())->createCommand()
                    ->insert('rating', [
                        'user_id' => $this->user_id,
                        'schedule_id' => $id,
                        'rating' => $rating,
                        'message' => $message
                    ])
                    ->execute();

                $result = ['result' => $query ? true : false];
            } else {
                $result = ['result' => false];
            }
        } else {
            $result = ['result' => false];
        }

        $result = json_encode($result, JSON_UNESCAPED_UNICODE);

        return $this->render('api', [
            'result' => $result
        ]);
    }

    public function actionContact() {
        $message = htmlspecialchars(trim(Yii::$app->request->post('message')));

        if ($message) {
            $query = (new Query())->createCommand()
                ->insert('contacts', [
                    'user_id' => $this->user_id,
                    'message' => $message
                ])
                ->execute();

            $result = ['result' => $query ? true : false];
        } else {
            $result = ['result' => false];
        }

        $result = json_encode($result, JSON_UNESCAPED_UNICODE);

        return $this->render('api', [
            'result' => $result
        ]);
    }
}