<?php
namespace event\controllers;

use Yii;
use yii\web\Controller;
use yii\db\Query;
use yii\helpers\Url;
use yii\base\Security;
use yii\web\Cookie;
use event\models\Event;
use event\models\users\User;
use event\models\users\Users;
use event\models\users\UsersConfirm;
use event\models\users\UsersAccess;
use event\models\db\Events;
use event\models\db\LotteryUsers;
use event\models\forms\SigninForm;
use event\models\forms\SignupForm;
use event\models\forms\PhoneConfirmForm;
use common\models\helpers\FormatHelper;
use common\models\helpers\NumbersHelper;
use common\models\sms;

class AuthController extends Controller {
    private $user;

    public function beforeAction($action) {
        $this->user = new User();

        if ($this->user->haveAccess()) return $this->redirect(['site/index']);

        return parent::beforeAction($action);
    }

    public function actionSignin() {
        $form = new SigninForm();
        $access = false;

        if ($form->load(Yii::$app->request->post())) {
            $this->user->login = FormatHelper::phone($form->login);
            $login = $this->user->login;

            if (!$form->login) {
                $alert = [
                    'type' => 'danger',
                    'message' => 'Все поля обязательны для заполнения'
                ];
            } elseif ($user_id = $this->user->userExist()) {
                $access = true;
                $confirm = false;

                if (Yii::$app->request->cookies->has('trace')) {
                    $date = (new \DateTime('-1 week'))->format('Y-m-d H:i:s');
                    $trace = htmlspecialchars(Yii::$app->request->cookies->get('trace')->value);

                    $confirm = UsersConfirm::find()
                        ->select('user_id')
                        ->where(['user_id' => $user_id, 'trace' => $trace, 'enter' => 1])
                        ->andWhere(['>=', 'date', $date])
                        ->count();

                    $confirm = ($confirm) ? true : false;
                }

                if (!$confirm) {
                    $code = $this->user->generateCode(10000, 99999);

                    // Отправляем смс
                    $sms_phone = '7' . $login;
                    $sms_message = 'ИнфоСофт код подтверждения: ' . $code;
                    $sms = (new SMS())->sendSms($sms_phone, $sms_message);

                    if ($sms) {
                        UsersConfirm::deleteAll(['user_id' => $user_id]);

                        $trace = (new Security())->generateRandomString();
                        $query = (new Query())->createCommand()
                            ->insert('users_confirm', [
                                'user_id' => $user_id,
                                'code' => $code,
                                'trace' => $trace
                            ])
                            ->execute();

                        if ($query) {
                            Yii::$app->response->cookies->add(
                                new Cookie([
                                    'name' => 'trace',
                                    'value' => $trace,
                                    'expire' => (new \DateTime('+1 week'))->getTimestamp()
                                ])
                            );
                        }

                        return $this->redirect(['auth/confirm']);
                    }
                }
            } else {
                $alert = [
                    'type' => 'danger',
                    'message' => 'Номер телефона указан неверно'
                ];
            }

            $this->user->saveHistory([
                'login' => $login,
                'success' => $access
            ]);
        }

        if ($access) {
            $this->user->createSession($user_id);
            return $this->redirect(['site/index']);
        }
    
        if (isset($_GET['response'])) {
            if ($_GET['response'] == 'fail') {
                $alert = [
                    'type' => 'warning',
                    'message' => 'Время сеанса истекло'
                ];
            }
        }

        $event = Event::todayEvent();
        $signup = true;

        return $this->render('signin', [
            'signup' => $signup,
            'form' => $form,
            'alert' => $alert
        ]);
    }

    public function actionSignup() {
        $form = new SignupForm();
        $show_form = true;

        $event = Event::todayEvent();
        if (!$event) return $this->redirect(['auth/signin']);

        if ($form->load(Yii::$app->request->post())) {
            $login = FormatHelper::phone(trim($form->login));
            $email = htmlspecialchars(trim($form->email));
            $name = htmlspecialchars(trim($form->name));
            $this->user->login = $login;

            if (!$form->login || !$form->name || !$form->email) {
                $alert = [
                    'type' => 'danger',
                    'message' => 'Заполните все поля'
                ];
            } elseif (!$this->user->phoneValidate($login)) {
                $alert = [
                    'type' => 'danger',
                    'message' => 'Введен некорректный номер телефона'
                ];
            } elseif (!$this->user->emailValidate($email)) {
                $alert = [
                    'type' => 'danger',
                    'message' => 'Введен некорректный адрес электронной почты'
                ];
            } elseif (!$this->user->captchaValidate($_POST['g-recaptcha-response'])) {
               $alert = [
                   'type' => 'danger',
                   'message' => 'Валидация не пройдена. Попробуйте еще раз'
               ];
            } else {
                $user_id = false;

                if ($this->user->userExist()) { // Если пользователь найден
                    $user = Users::find()
                        ->select('id')
                        ->where(['login' => $login])
                        ->one();

                    $user_id = $user->id;

                    $users_access = UsersAccess::find()
                        ->where(['event_id' => $event, 'user_id' => $user_id])
                        ->all();

                    if (!$users_access) {
                        (new Query())->createCommand()
                            ->insert('users_access', ['user_id' => $user_id, 'event_id' => $event->id])
                            ->execute();
                    }
                } else {
                    (new Query())->createCommand()
                        ->insert('users', [
                            'login' => $login,
                            'name' => $name,
                            'email' => $email,
                            'confirm' => 0,
                            'role' => 1
                        ])
                        ->execute();

                    $user_id = Yii::$app->db->getLastInsertID();

                    (new Query())->createCommand()
                        ->insert('users_access', ['user_id' => $user_id, 'event_id' => $event->id])
                        ->execute();
                }

                $code = $this->user->generateCode(10000, 99999);
                $trace = (new Security())->generateRandomString();
                $expire = (new \DateTime('+1 week'))->getTimestamp();

                Yii::$app->response->cookies->add(
                    new Cookie([
                        'name' => 'trace',
                        'value' => $trace,
                        'expire' => $expire
                    ])
                );

                // Отправляем смс
                $sms_phone = '7' . $login;
                $sms_message = 'ИнфоСофт код подтверждения: ' . $code;
                $sms = (new SMS())->sendSms($sms_phone, $sms_message);

                if ($sms) {
                    $query = (new Query())->createCommand()
                        ->insert('users_confirm', [
                            'user_id' => $user_id,
                            'code' => $code,
                            'trace' => $trace,
                            'enter' => 0,
                            'date' => date('Y-m-d H:i:s')
                        ])
                        ->execute();

                    // Лотерея
                    if ($query) {
                        $lottery = LotteryUsers::find()
                            ->select('number')
                            ->orderBy(['number' => SORT_DESC])
                            ->one();

                        $lottery_number = ($lottery) ? $lottery->number + 1 : 1;

                        (new Query())->createCommand()
                            ->insert('lottery_users', ['user_id' => $user_id, 'number' => $lottery_number])
                            ->execute();
                    }

                    return $this->redirect(['auth/confirm']);
                } else {
                    $alert = [
                        'type' => 'danger',
                        'message' => 'Произошла ошибка, попробуйте еще раз'
                    ];
                }
            }
        }

        return $this->render('signup', [
            'event' => $event,
            'form' => $form,
            'show_form' => $show_form,
            'alert' => $alert
        ]);
    }

    public function actionConfirm() {
        $trace = htmlspecialchars(Yii::$app->request->cookies->get('trace')->value);
        if (!$trace) return $this->redirect(['auth/signin']);

        $date = (new \DateTime('-15 minutes'))->format('Y-m-d H:i:s');

        $query = UsersConfirm::find()
            ->select('date')
            ->where(['trace' => $trace, 'enter' => 0])
            ->andWhere(['>=', 'date', $date])
            ->one();

        if (!$query) return $this->redirect(['auth/signin', 'response' => 'fail']);

        $timer = (time() - strtotime($query->date) >= 60) ? '05' : '59';

        $form = new PhoneConfirmForm();
        if ($form->load(Yii::$app->request->post())) {
            if (!$form->code) {
                $alert = [
                    'type' => 'danger',
                    'message' => 'Введите код подтверждения'
                ];
            } else {
                $code = (int) $form->code;
                $code = substr($code, 0, 5);

                $query = UsersConfirm::find()
                    ->select(['id', 'user_id'])
                    ->where(['code' => $code, 'enter' => 0, 'trace' => $trace])
                    ->andWhere(['>=', 'date', $date])
                    ->one();

                if ($query) {
                    (new Query())->createCommand()
                        ->update('users', ['confirm' => 1], ['id' => $query->user_id])
                        ->execute();

                    (new Query())->createCommand()
                        ->update('users_confirm', [
                            'enter' => 1,
                            'code' => NULL,
                            'date' => date('Y-m-d H:i:s', time())
                        ], ['id' => $query->id])
                        ->execute();

                    if ($this->user->createSession($query->user_id)) return $this->redirect(['site/index']);
                } else {
                    $alert = [
                        'type' => 'danger',
                        'message' => 'Указанный код не действителен или срок действия истек'
                    ];
                }
            }
        } else {
            $alert = [
                'type' => 'warning',
                'message' => 'На указанный вами номер была отправлена SMS с кодом подтверждения'
            ];
        }

        if ($_GET['response']) {
            switch ($_GET['response']) {
                case 'success':
                    $alert = [
                        'type' => 'success',
                        'message' => 'Код подтверждения отправлен повторно',
                    ];
                    break;
                case 'fail':
                    $alert = [
                        'type' => 'danger',
                        'message' => 'Произошла ошибка, попробуйте еще раз',
                    ];
                    break;
            }
        }

        return $this->render('confirm', [
            'form' => $form,
            'timer' => $timer,
            'alert' => $alert
        ]);
    }
}