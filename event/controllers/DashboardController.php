<?php
namespace event\controllers;

use Yii;
use yii\web\Controller;
use yii\db\Query;
use yii\data\ActiveDataProvider;
use event\models\users\User;
use event\models\users\Users;
use event\models\users\UsersAccess;
use event\models\users\UsersSession;
use event\models\db\Events;
use event\models\db\Schedule;
use event\models\db\Questions;
use event\models\db\Likes;
use event\models\db\LotteryUsers;
use event\models\db\LotteryNumbers;
use event\models\forms\dashboard\AddUserForm;
use event\models\forms\dashboard\UsersForm;
use event\models\forms\dashboard\QuestionsForm;
use common\models\helpers\FormatHelper;

class DashboardController extends Controller {
    private $user;

    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    public function beforeAction($action) {
        $this->user = new User();

        if (!$this->user->permission(2)) throw new \yii\web\NotFoundHttpException();

        return parent::beforeAction($action);
    }

    public function actionIndex() {
        return $this->render('index');
    }

    public function actionAddUser() {
        $form = new AddUserForm();

        $date = date('Y-m-d') . ' 00:00:00';
        $events = Events::find()
            ->select(['id', 'title'])
            ->where(['active' => 1])
            ->andWhere(['>=', 'date_end', $date])
            ->all();

        $events_result = [];
        foreach ($events as $event) {
            $events_result[$event->id] = $event->title;
        }

        if ($form->load(Yii::$app->request->post())) {
            if (!$form->event || !$form->phone) {
                $alert = [
                    'type' => 'danger',
                    'message' => 'Заполните обязательные поля',
                    'close' => true
                ];
            } elseif ($form->email && !$this->user->emailValidate($form->email)) {
                $alert = [
                    'type' => 'danger',
                    'message' => 'Email указан неверно',
                    'close' => true
                ];
            } else {
                $event = (int) $form->event;
                $login = trim(FormatHelper::phone($form->phone));
                $email = trim($form->email);
                $name = trim($form->name);

                $user = Users::find()
                    ->select('id')
                    ->where(['login' => $login])
                    ->one();

                if ($user) {
                    $user_id = $user->id;

                    $access = UsersAccess::find()
                        ->where(['event_id' => $event, 'user_id' => $user_id])
                        ->all();

                    if (!$access) {
                        (new Query())->createCommand()
                            ->insert('users_access', ['user_id' => $user_id, 'event_id' => $event])
                            ->execute();

                        $alert = [
                            'type' => 'success',
                            'message' => 'Пользователю добавлен доступ к конференции',
                            'close' => true
                        ];
                    } else {
                        $alert = [
                            'type' => 'warning',
                            'message' => 'Пользователь уже зарегистрирован',
                            'close' => true
                        ];
                    }
                } else {
                    (new Query())->createCommand()
                        ->insert('users', [
                            'login' => $login,
                            'email' => $email,
                            'name' => $name,
                            'confirm' => 1,
                            'role' => 1
                        ])
                        ->execute();

                    $user_id = Yii::$app->db->getLastInsertID();

                    (new Query())->createCommand()
                        ->insert('users_access', ['user_id' => $user_id, 'event_id' => $event])
                        ->execute();

                    $alert = [
                        'type' => 'success',
                        'message' => 'Пользователь успешно зарегистрирован',
                        'close' => true
                    ];
                }

                if ($form->lottery) {
                    $lottery_exist = LotteryUsers::find()
                        ->select('id')
                        ->where(['id' => $user_id])
                        ->one();

                    if (!$lottery_exist) {
                        $lottery = LotteryUsers::find()
                            ->select('number')
                            ->orderBy(['number' => SORT_DESC])
                            ->one();

                        $lottery_number = ($lottery) ? $lottery->number + 1 : 1;

                        (new Query())->createCommand()
                            ->insert('lottery_users', ['user_id' => $user_id, 'number' => $lottery_number])
                            ->execute();
                    }
                }

                $form->phone = NULL;
                $form->name = NULL;
                $form->email = NULL;
            }
        }

        return $this->render('add-user', [
            'event' => $events_result,
            'form' => $form,
            'alert' => $alert
        ]);
    }

    public function actionUsers() {
        $form = new UsersForm();

        $events = Events::find()
            ->select(['id', 'title'])
            ->all();

        $events_result[0] = false;
        foreach ($events as $event) {
            $events_result[$event->id] = $event->title;
        }

        if (Yii::$app->request->get('event')) {
            $event_id = (int) Yii::$app->request->get('event');
            $show_form = false;

            $users_access = UsersAccess::find()
                ->select('user_id')
                ->where(['event_id' => $event_id])
                ->all();

            $users_ids = [];
            foreach ($users_access as $user) {
                $user_ids[] = $user->user_id;
            }

            $users = Users::find()
                ->where(['id' => $user_ids, 'role' => [0, 1]]);
        } else {
            $users = Users::find()
                ->where(['role' => [0, 1]]);
        }

        $provider = new ActiveDataProvider([
            'query' => $users
        ]);

        return $this->render('users', [
            'events' => $events_result,
            'provider' => $provider,
            'form' => $form,
        ]);
    }

    public function actionQuestions() {
        if (!Yii::$app->request->get('event') && !Yii::$app->request->get('schedule')) {
            $form = new QuestionsForm();
            $show_form = true;

            $event = Events::find()
                ->select(['id', 'title'])
                ->where(['>=', 'date_end', date('Y-m-d')])
                ->orderBy(['date_start' => SORT_ASC])
                ->all();

            $event_result['false'] = '';
            for ($i = 0; $i < count($event); $i++) {
                $event_result[$event[$i]->id] = $event[$i]->title;
            }
        } else {
            $show_form = false;
            $event_id = (int) Yii::$app->request->get('event');
            $schedule_id = (int) Yii::$app->request->get('schedule');

            $schedule = Schedule::find()
                ->where(['speaker', 'title'])
                ->where(['event_id' => $event_id, 'id' => $schedule_id])
                ->asArray()
                ->one();

            $questions = Questions::find()
                ->where(['schedule_id' => $schedule_id])
                ->orderBy(['date' => SORT_DESC])
                ->asArray()
                ->all();

            // Сохраняем в массив id пользователей, оставивших вопрос
            $users_ids = [];
            for ($i = 0; $i < count($questions); $i++) {
                $users_ids[] = $questions[$i]['user_id'];
            }

            // Извлекаем информацию о пользователя, оставивших вопрос
            $users = Users::find()
                ->select(['id', 'login', 'name'])
                ->where(['id' => $users_ids])
                ->asArray()
                ->all();

            $users_array = [];
            for ($i = 0; $i < count($users); $i++) {
                $users_array[$users[$i]['id']] = $users[$i];
            }

            $likes_count = Likes::find()
                ->select(['COUNT(1)', 'question_id'])
                ->where(['schedule_id' => $schedule_id])
                ->groupBy('question_id')
                ->asArray()
                ->all();

            $likes_count_array = [];
            for ($i = 0; $i < count($likes_count); $i++) {
                $likes_count_array[$likes_count[$i]['question_id']] = $likes_count[$i]['COUNT(1)'];
            }

            for ($i = 0; $i < count($questions); $i++) {
                $questions[$i]['author'] = $users_array[$questions[$i]['user_id']]['name'];
                $questions[$i]['like_count'] = ($likes_count_array[$questions[$i]['id']] > 0) ? $likes_count_array[$questions[$i]['id']] : 0;
            }
        }

        // Сортировка по лайкам
        if (isset($_GET['sort']) && $_GET['sort'] == 'like') array_multisort(array_column($questions, 'like_count'), SORT_DESC, $questions);

        return $this->render('questions', [
            'form' => $form,
            'show_form' => $show_form,
            'event' => $event_result,
            'questions' => $questions,
            'schedule' => $schedule
        ]);
    }

    public function actionOnline() {
        $date = new \DateTime('-10 minutes');

        $sessions = UsersSession::find()
            ->select('user_id')
            ->where(['>', 'date', $date->format('Y-m-d H:i:s')])
            ->all();

        $user_ids = [];
        foreach ($sessions as $session) {
            $user_ids[] = $session->user_id;
        }

        $users = Users::find()
            ->select(['id', 'login', 'name'])
            ->where(['id' => $user_ids])
            ->andWhere(['confirm' => 1]);

        $provider = new ActiveDataProvider([
            'query' => $users,
        ]);

        return $this->render('online', [
            'result' => $provider
        ]);
    }

    public function actionLottery() {
        $min = (Yii::$app->request->get('min')) ? Yii::$app->request->get('min') : 1;
        $max = (Yii::$app->request->get('max')) ? Yii::$app->request->get('max') : 99;

        // Получаем выпавшие номера
        $numbers = LotteryNumbers::find()
            ->select('number')
            ->orderBy(['id' => SORT_DESC])
            ->limit(3)
            ->all();

        $number = 0;
        for ($i = 1; $i <= 1000; $i++) {
            $number = rand($min, $max);

            $count = LotteryNumbers::find()
                ->where(['number' => $number])
                ->count();

            if ($count == 0) break;
        }

        if ($count == 0) {
            (new Query())->createCommand()
                ->insert('lottery_numbers', ['number' => $number])
                ->execute();

            (new Query())->createCommand()
                ->update('lottery_users', ['exist' => 1], ['number' => $number])
                ->execute();
        } else {
            $number = 'NaN';
        }

        $winners = LotteryUsers::find()
            ->select(['user_id'])
            ->where(['win' => 0, 'number' => $number])
            ->groupBy('user_id')
            ->asArray()
            ->all();

        $winners_array = [];
        foreach ($winners as $winner) {
            $winners_array[] = $winner['user_id'];
        }

        $winners_names = Users::find()
            ->select('name')
            ->where(['id' => $winners_array])
            ->asArray()
            ->all();

        (new Query())->createCommand()
            ->update('lottery_users', ['win' => 1], ['user_id' => $winners_array])
            ->execute();

        $winners_text = (count($winners_array) > 1) ? 'Победители' : 'Победитель';

        return $this->render('lottery', [
            'number' => $number,
            'numbers' => $numbers,
            'winners_count' => $winners_count,
            'winners_names' => $winners_names,
            'winners_text' => $winners_text,
        ]);
    }
}