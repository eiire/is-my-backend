<?php
namespace event\controllers;

use Yii;
use yii\web\Controller;
use yii\db\Query;
use yii\helpers\Url;
use event\models\Event;
use event\models\users\User;
use event\models\users\Users;
use event\models\users\UsersConfirm;
use event\models\users\UsersSession;
use event\models\users\UsersHistory;
use event\models\users\UsersRecovery;
use event\models\users\UsersAccess;
use event\models\db\Events;
use event\models\db\Favorites;
use event\models\db\Likes;
use event\models\db\Schedule;
use event\models\db\QuestionsSettings;
use event\models\db\LotteryUsers;
use event\models\helpers\FileHelper;
use common\models\helpers\FormatHelper;
use common\models\sms;
use event\models\is1c2020;

class HandlerController extends Controller {
    private $user;

    public function beforeAction($action) {
        $this->user = new User();
        $this->enableCsrfValidation = false;

        return parent::beforeAction($action);
    }

    public function actionHandler() {
        $this->layout = false;
        $method = Yii::$app->request->get('method');
        $result = false;

        if ($method == 'sms') {
            if (!Yii::$app->request->cookies->has('trace')) return $this->redirect(['auth/confirm', 'response' => 'fail']);
            $trace = htmlspecialchars(Yii::$app->request->cookies->get('trace')->value);

            $date = (new \DateTime('-50 seconds'))->format('Y-m-d H:i:s');

            $query = UsersConfirm::find()
                ->select('user_id')
                ->where(['trace' => $trace])
                ->andWhere(['<', 'date', $date])
                ->one();

            if (!$query) return $this->redirect(['auth/confirm', 'response' => 'fail']);

            $query = Users::find()
                ->select(['id', 'login'])
                ->where(['id' => $query->user_id])
                ->one();

            if (!$query) return $this->redirect(['auth/confirm', 'response' => 'fail']);
            
            $code = $this->user->generateCode(10000, 99999);

            $sms_phone = '7' . $query->login;
            $sms_message = 'ИнфоСофт код подтверждения: ' . $code;
            $sms = (new SMS())->sendSms($sms_phone, $sms_message);

            if ($sms) {
                (new Query())->createCommand()
                    ->update('users_confirm', ['code' => $code, 'date' => date('Y-m-d H:i:s')], ['user_id' => $query->id, 'trace' => $trace])
                    ->execute();

                return $this->redirect(['auth/confirm', 'response' => 'success']);
            } else {
                return $this->redirect(['auth/confirm', 'response' => 'fail']);
            }

            $result = true;
        } elseif ($method == 'add-users') {
            $json = json_decode($_POST['vars']);
            $event_id = $json->EventNum;
            $users = $json->Users;

            $phones = [];
            foreach ($users as $user) {
                $phones[] = FormatHelper::phone($user->Num);
            }

            $exist_users = Users::find()
                ->select(['id', 'login'])
                ->where(['login' => $phones])
                ->all();

            // Зарегистрированные пользователи
            $exist_ids = [];
            $exist_phones = [];
            foreach ($exist_users as $user) {
                $exist_ids[] = $user->id;
                $exist_phones[] = $user->login;
            }

            $exist_access = UsersAccess::find()
                ->select('user_id')
                ->where(['event_id' => $event_id, 'user_id' => $exist_ids])
                ->all();

            // Пользователи уже имеющие доступ к конференции
            $exist_access_ids = [];
            foreach ($exist_access as $user) {
                $exist_access_ids[] = $user->user_id;
            }

            $users_to_insert = [];
            foreach ($users as $user) {
                if (!in_array($user->Num, $exist_phones) && $user->User != '' && $user->Num != '') {
                    $users_to_insert[] = [
                        'login' => FormatHelper::phone($user->Num),
                        'name' => htmlspecialchars($user->User),
                        'email' => ($user->Email != '' && $this->user->emailValidate($user->Email)) ? htmlspecialchars($user->Email) : NULL,
                        'role' => 1
                    ];
                }
            }

            (new Query())->createCommand()
                ->batchInsert('users', ['login', 'name', 'email', 'role'], $users_to_insert)
                ->execute();

            // Зарегистрированные пользователи, без доступа к конференции
            $users_to_access = [];
            foreach ($exist_ids as $user) {
                if (!in_array($user, $exist_access_ids)) {
                    $users_to_access[$user] = [
                        'user_id' => $user,
                        'event_id' => $event_id,
                        'active' => 1
                    ];
                }
            }

            $inserted_users = Users::find()
                ->select('id')
                ->where(['login' => $phones])
                ->all();

            foreach ($inserted_users as $user) {
                if (!in_array($user->id, $exist_access_ids)) {
                    $users_to_access[$user->id] = [
                        'user_id' => $user->id,
                        'event_id' => $event_id,
                        'active' => 1
                    ];
                }
            }

            (new Query())->createCommand()
                ->batchInsert('users_access', ['user_id', 'event_id', 'active'], $users_to_access)
                ->execute();

            $result = 'true';
        } elseif ($method == 'is1c2020') {
            $name = htmlspecialchars(trim($_GET['name']));
            $surname = htmlspecialchars(trim($_GET['surname']));
            $company = htmlspecialchars(trim($_GET['company']));
            $position = htmlspecialchars(trim($_GET['position']));
            $city = htmlspecialchars(trim($_GET['city']));
            $section = htmlspecialchars(trim($_GET['section']));
            $email = htmlspecialchars(trim($_GET['email']));
            $phone = htmlspecialchars(trim($_GET['phone']));
            $ip = htmlspecialchars(trim($_GET['ip']));

            if ($name && $surname && $company && $position && $city && $email && $phone) {
                $exist = is1c2020::find()
                    ->select('id')
                    ->where(['email' => $email])
                    ->orWhere(['phone' => $phone])
                    ->one();

                if (!$exist) {
                    $query = (new Query())->createCommand()
                        ->insert('is1c2020', [
                            'name' => $name,
                            'surname' => $surname,
                            'company' => $company,
                            'position' => $position,
                            'city' => $city,
                            'section' =>$section,
                            'email' => $email,
                            'phone' => $phone,
                            'ip' => $ip
                        ])
                        ->execute();

                    if ($query) {
                        $text = 'Здравствуйте, ' . $name . '!<br><br>Вы успешно зарегистрированы как участник конференции IS1C2020.';
                        Yii::$app->mailer->compose('layouts/template', ['content' => $text, 'links' => false])
                            ->setFrom('my@is1c.ru')
                            ->setTo($email)
                            ->setSubject('Регистрация на IS1C2020')
                            ->send();

                        $result = 'true';
                    } else {
                        $result = 'false';
                    }
                } else {
                    $result = 'exist';
                }
            } else {
                $result = 'false';
            }
        }

        if ($result) {
            return $this->render('handler', [
                'result' => $result
            ]);
        }

        throw new \yii\web\NotFoundHttpException();
    }

    public function actionEvent() {
        $this->layout = false;
        $method = Yii::$app->request->get('method');
        $result = false;

        // Добавить/удалить доклад из избранного
        if ($method == 'schedule-favorite') {
            $event = Yii::$app->request->get('event');
            $schedule = Yii::$app->request->get('schedule');

            if ($event && $schedule) {
                $data = [
                    'user_id' => $this->user->getID(),
                    'event_id' => (int) $event,
                    'schedule_id' => (int) $schedule
                ];

                $favorite = Favorites::find()
                    ->select('id')
                    ->where($data)
                    ->one();

                if ($favorite) {
                    Favorites::deleteAll($data);
                    $result = 'delete';
                } else {
                    (new Query())->createCommand()
                        ->insert('favorites', $data)
                        ->execute();
                    $result = 'add';
                }
            }
        // Поставить/Убрать лайк вопросу
        } elseif ($method == 'question-like') {
            $schedule = Yii::$app->request->get('schedule');
            $question = Yii::$app->request->get('question');

            if ($schedule && $question) {
                $data = [
                    'user_id' => $this->user->getID(),
                    'schedule_id' => (int) $schedule,
                    'question_id' => (int) $question
                ];

                $like = Likes::find()
                    ->select('id')
                    ->where($data)
                    ->one();

                if ($like) {
                    Likes::deleteAll($data);
                    $result = 'delete';
                } else {
                    (new Query())->createCommand()
                        ->insert('likes', $data)
                        ->execute();

                    $result = 'add';
                }
            }
        } elseif ($method == 'questions-autoupdate') {
            $value = QuestionsSettings::find()
                ->where(['id' => 1])
                ->one();

            $new_value = ($value->value == 0) ? 1 : 0;

            (new Query())->createCommand()
                ->update('questions_settings', ['value' => $new_value], ['id' => 1])
                ->execute();

            $result = ($new_value) ? 'on' : 'off';
        } elseif ($method == 'questions-current') {
            $id = (int) Yii::$app->request->get('id');

            $query = (new Query())->createCommand()
                ->update('questions_settings', ['value' => $id], ['id' => 2])
                ->execute();

            $result = ($query) ? 'ok' : 'fail';
        // Получить доклады для конференции
        } elseif ($method == 'get-questions') {
            $access = ($this->user->permission(2)) ? true : false;
            $event = (int) Yii::$app->request->get('event');

            if ($access && $event) {
                $questions = Schedule::find()
                    ->select(['id', 'title'])
                    ->where(['event_id' => $event])
                    ->andWhere(['not', ['speaker' => NULL]])
                    ->asArray()
                    ->all();

                $json = json_encode($questions);
                $result = $json;
            }
        // Скачать файл
        } elseif ($method == 'download') {
            $file_name = Yii::$app->request->get('file');
            $file_type = Yii::$app->request->get('type');
            $event_id = Yii::$app->request->get('event');
            
            if ($file_name && $file_type) {
                if ($file_type == 'doc') {
                    $format = ['pdf', 'pptx'];
                 } elseif ($file_type == 'video') {
                    $format = ['avi', 'mkv', 'mp4'];
                } elseif ($file_type == 'photo') {
                    $format = ['jpg', 'png', 'zip', 'rar'];
                }

                $event = Events::find()
                    ->select(['date_start', 'date_end'])
                    ->where(['id' => $event_id])
                    ->one();

                $count = UsersHistory::find()
                    ->where(['login' => $this->user->getLogin(), 'success' => 1])
                    ->andWhere(['>=', 'date', $event->date_start])
                    ->andWhere(['<=', 'date', $event->date_end])
                    ->count();

                if ($count > 0) {
                    $file = FileHelper::search($file_name, 'upload/event/' . $event_id . '/' . $file_type, $format, false);
                    return Yii::$app->response->SendFile($file);
                }
            } else {
                $result = false;
            }
        } elseif ($method == 'event-list') {
            $confs = Events::find()
                ->select(['id', 'title'])
                ->where(['active' => 1])
                ->andWhere(['>=', 'date_start', date('Y-m-d', time())])
                ->asArray()
                ->all();

            $confs_result = [];
            foreach ($confs as $event) {
                $confs_result['events'][] = [
                    'id' => $event['id'],
                    'title' => rawurlencode($event['title']),
                ];
            }

            $result = json_encode($confs_result, JSON_UNESCAPED_UNICODE);
        } elseif ($method == 'event') {
            $id = (int) Yii::$app->request->get('id');

            if ($id) {
                $access = UsersAccess::find()
                    ->select('id')
                    ->where(['user_id' => $this->user->getID(), 'event_id' => $id])
                    ->one();

                if ($access) {
                    UsersAccess::updateAll(['active' => 0], ['user_id' => $this->user->getID()]);
                    UsersAccess::updateAll(['active' => 1], ['user_id' => $this->user->getID(), 'event_id' => $id]);
                    return $this->redirect(['site/index']);
                } else {
                    return $this->redirect(['site/history', 'response' => 'fail']);
                }
            }
        }

        if ($result) {
            return $this->render('handler', [
                'result' => $result
            ]);
        }

        throw new \yii\web\NotFoundHttpException();
    }

    public function actionSocial() {
        $code = Yii::$app->request->get('code');
        $type = Yii::$app->request->get('type');
        $auth = Yii::$app->request->get('auth');
        $types = ['vk', 'fb'];

        if (!$type || !in_array($type, $types)) throw new \yii\web\NotFoundHttpException();

        if ($auth) {
            $params = [
                'client_id' => Yii::$app->params[$type]['client_id'],
                'redirect_uri' => Yii::$app->params[$type]['redirect_uri'],
                'response_type' => 'code',
                'scope' => 'email'
            ];

            if ($type == 'vk') {
                $params['v'] = Yii::$app->params['vk']['version'];
                $url = 'https://oauth.vk.com/authorize?';
            } elseif ($type == 'fb') {
                $url = 'https://www.facebook.com/' . Yii::$app->params['fb']['version'] . '/dialog/oauth?';
            }

            return $this->redirect($url . http_build_query($params));
        }

        $params = [
            'client_id' => Yii::$app->params[$type]['client_id'],
            'client_secret' => Yii::$app->params[$type]['client_secret'],
            'redirect_uri' => Yii::$app->params[$type]['redirect_uri'],
            'code' => $code
        ];

        $url = ($type == 'vk') ? 'https://oauth.vk.com/access_token?' : 'https://graph.facebook.com/' . Yii::$app->params['fb']['version'] . '/oauth/access_token?';
        $json = file_get_contents($url . http_build_query($params));
        $json = json_decode($json);

        if ($type == 'vk') {
            $params = [
                'access_token' => $json->access_token,
                'user_ids' => $json->user_id,
                'v' => Yii::$app->params['vk']['version']
            ];

            $url = 'https://api.vk.com/method/users.get?' . http_build_query($params);
            $json_user = file_get_contents($url);
            $json_user = json_decode($json_user);

            $login = $json_user->response[0]->id;
            $email = $json->email;
            $name = $json_user->response[0]->last_name . ' ' . $json_user->response[0]->first_name;
        } elseif ($type == 'fb') {
            $params = [
                'access_token' => $json->access_token,
                'fields' => 'id,name,email'
            ];

            $url = 'https://graph.facebook.com/' . Yii::$app->params['fb']['version'] . '/me?' . http_build_query($params);

            $json_user = file_get_contents($url);
            $json_user = json_decode($json_user);

            $login = $json_user->id;
            $email = $json_user->email;
            $name = $json_user->name;
        }

        $user = Users::find()
            ->select('id')
            ->where(['OR', ['login' => $login], ['email' => $email]])
            ->one();

        $event = Event::todayEvent();

        if ($user) {
            $user_id = $user->id;
        } else {
            $role = ($event) ? 1 : 0;

            (new Query())->createCommand()
                ->insert('users', [
                    'login' => $login,
                    'email' => $email,
                    'name' => $name,
                    'source' => $type,
                    'role' => $role,
                    'confirm' => 1
                ])
                ->execute();

            $user_id = Yii::$app->db->getLastInsertID();
            if ($event) {
                (new Query())->createCommand()
                    ->insert('users_access', ['user_id' => $user_id, 'event_id' => $event->id])
                    ->execute();

                // Лотерея
                $lottery = LotteryUsers::find()
                    ->select('number')
                    ->orderBy(['number' => SORT_DESC])
                    ->one();

                $lottery_number = ($lottery) ? $lottery->number + 1 : 1;

                (new Query())->createCommand()
                    ->insert('lottery_users', ['user_id' => $user_id, 'number' => $lottery_number])
                    ->execute();
            }
        }

        if ($this->user->createSession($user_id)) return $this->redirect(['site/index']);

        throw new \yii\web\NotFoundHttpException();
    }
}