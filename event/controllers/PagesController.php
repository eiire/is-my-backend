<?php
namespace event\controllers;

use yii\web\Controller;

class PagesController extends Controller {
    public function actionPrivacy() {
        return $this->render('privacy');
    }
}