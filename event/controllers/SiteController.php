<?php
namespace event\controllers;

use Yii;
use yii\web\Controller;
use yii\db\Query;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\data\Pagination;
use event\models\Event;
use event\models\users\User;
use event\models\users\Users;
use event\models\users\UsersHistory;
use event\models\users\UsersAccess;
use event\models\db\Events;
use event\models\db\Likes;
use event\models\db\Notes;
use event\models\db\Rating;
use event\models\db\Schedule;
use event\models\db\Sections;
use event\models\db\Questions;
use event\models\db\Favorites;
use event\models\db\LotteryUsers;
use event\models\db\LotteryNumbers;
use event\models\db\FeedbackAnswers;
use event\models\db\QuestionsAnswers;
use event\models\db\FeedbackQuestions;
use event\models\db\Hidden;
use common\models\forms\RatingForm;
use event\models\forms\NoteForm;
use event\models\forms\FeedbackForm;
use event\models\forms\QuestionsForm;
use event\models\forms\QuestionsAnswerForm;
use event\models\helpers\FileHelper;
use common\models\helpers\FormatHelper;

class SiteController extends Controller {
    private $user;
    private $event_id;

    // Сохранить и загрузить файл
    private function saveFile($str, $name, $download = true) {
        $file_name = time() . '_' . $name . '.csv';
        $file_path = __DIR__ . '/../web/upload/tmp/';

        $file = fopen($file_path . $file_name, 'w+');
        fwrite($file, $str);
        fclose($file);
        if ($download) Yii::$app->response->SendFile($file_path . $file_name);
    }

    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    public function beforeAction($action) {
        parent::beforeAction($action);

        $this->user = new User();

        if (isset($_GET['logout'])) {
            $this->user->logOut();
            return $this->redirect(['auth/signin']);
        } elseif (!$this->user->haveAccess($action->id)) {
            return $this->redirect(['auth/signin']);
        }

        $access = UsersAccess::find()
            ->select(['event_id', 'active'])
            ->where(['user_id' => $this->user->getID()])
            ->asArray()
            ->all();

        if ((!$access || $this->user->permission(0)) && $action->id != 'welcome') {
            return $this->redirect(['site/welcome']);
        }

        if (count($access) > 1) {
            $active_events = [];
            foreach ($access as $row) {
                if ($row['active']) $active_events[] = $row['event_id'];
            }

            if (count($active_events) > 1) {
                $event_id = max($active_events);
                UsersAccess::updateAll(['active' => 0], ['and', ['user_id' => $this->user->getID()] , ['<>', 'event_id', $event_id]]);
                $this->event_id = $event_id;
            } else {
                $this->event_id = $active_events[0];
            }

        } else {
            $this->event_id = $access[0]['event_id'];
        }


        return true;
    }

    public function actionIndex() {
        $id = $this->event_id;

        $event = Events::find()
            ->where(['id' => $id, 'active' => 1])
            ->asArray()
            ->one();

        $event['desc'] = Event::formatDesc($event['desc'], $event, $this->user);
        $event['date'] = (date('dmY', strtotime($event['date_start'])) == date('dmY', strtotime($event['date_end']))) ? date('d.m.Y', strtotime($event['date_start'])) : date('d.m.Y', strtotime($event['date_start'])) . ' &ndash; ' . date('d.m.Y', strtotime($event['date_end']));

        $schedule = Schedule::find()
            ->where(['event_id' => $id])
            ->orderBy(['date' => SORT_ASC, 'time_start' => SORT_ASC])
            ->asArray()
            ->all();

        // ID всех докладов концеренции
        $schedule_ids = [];
        foreach ($schedule as $row) {
            $schedule_ids[] = $row['id'];
        }

        // Доклады в избранном у пользователя
        $favorites = Favorites::find()
            ->select('schedule_id')
            ->where(['event_id' => $id, 'user_id' => $this->user->getID()])
            ->asArray()
            ->all();

        $favorites_result = [];
        foreach ($favorites as $row) {
            $favorites_result[$row['schedule_id']] = true;
        }

        // Доклады с оценкой пользователя
        $rating = Rating::find()
            ->select(['schedule_id', 'rating'])
            ->where(['schedule_id' => $schedule_ids, 'user_id' => $this->user->getID()])
            ->asArray()
            ->all();

        $rating_result = [];
        foreach ($rating as $row) {
            $rating_result[$row['schedule_id']] = $row['rating'];
        }

        // Средняя оценка докладов
        $average = Rating::find()
            ->select(['AVG(rating)', 'schedule_id'])
            ->where(['schedule_id' => $schedule_ids])
            ->groupBy('schedule_id')
            ->asArray()
            ->all();

        $average_result = [];
        foreach ($average as $row) {
            $average_result[$row['schedule_id']] = round($row['AVG(rating)'], 1);
        }

        // Кол-во входов пользователя во время конференции
        $history = UsersHistory::find()
            ->where(['login' => $this->user->getLogin(), 'success' => 1])
            ->andWhere(['>=', 'date', $event['date_start']])
            ->andWhere(['<=', 'date', $event['date_end']])
            ->count();

        $lottery = false;
        $schedule_result = [];
        foreach ($schedule as &$item) {
            if (!$lottery && $item['type'] == 3) $lottery = true;

            $_start = date('d.m.Y', strtotime($item['date'])) . ' ' . date('H:i:s', strtotime($item['time_start']));
            $_end = date('d.m.Y', strtotime($item['date'])) . ' ' . date('H:i:s', strtotime($item['time_end']));

            $item['photo'] = FileHelper::search($item['id'], 'upload/event/' . $item['event_id'] . '/speakers', ['jpg', 'png']);
            $item['favorite'] = ($favorites_result[$item['id']]) ? true : false;
            $item['start'] = (time() > strtotime($_start)) ? true : false;
            $item['end'] = (time() > strtotime($_end)) ? true : false;
            $item['show_rating'] = (time() > strtotime($_start) && !in_array($item['id'], array_keys($rating_result))) ? true : false;
            $item['average'] = ($average_result[$item['id']]) ? $average_result[$item['id']] : false;
            $item['this_speaker'] = ($item['user_id'] == $this->user->getID());
            $item['desc'] = FormatHelper::replace($item['desc'], ["\n" => '<br>']);

            // Если пользователь заходил во время конференции
            if ($history > 0) {
                $download = [];
                // Поиск презентации
                if (FileHelper::search($item['id'], 'upload/event/' . $item['event_id'] . '/doc', ['pdf', 'pptx'])) {
                    $download[] = [
                        'label' => 'Скачать презентацию',
                        'route' => ['handler/event', 'method' => 'download', 'file' => $item['id'], 'type' => 'doc', 'event' => $item['event_id']]
                    ];
                }

                // Поиск фото
                if (FileHelper::search($item['id'], 'upload/event/' . $item['event_id'] . '/photo', ['jpg', 'png', 'zip', 'rar'])) {
                    $download[] = [
                        'label' => 'Скачать фотографии доклада',
                        'route' => ['handler/event', 'method' => 'download', 'file' => $item['id'], 'type' => 'photo', 'event' => $item['event_id']]
                    ];
                }

                // Поиск видео
                if (FileHelper::search($item['id'], 'upload/event/' . $item['event_id'] . '/video', ['avi', 'mkv', 'mp4'])) {
                    $download[] = [
                        'label' => 'Смотреть видео доклада',
                        'route' => ['site/video', 'id' => $item['id']]
                    ];
                }

                $item['download'] = $download;
            }

            $key = str_replace('-', '', $item['date']);
            $schedule_result[$key]['schedule'][$item['section_id']][] = $item;
        }

        $sections = Sections::find()
            ->where(['event_id' => $id])
            ->asArray()
            ->all();

        $sections_result = [];
        foreach ($sections as $section) {
            $sections_result[$section['id']] = [
                'id' => $section['id'],
                'title' => $section['title']
            ];
        }

        // Добавляем к расписанию сецкии для каждого дня
        foreach ($schedule_result as $day => $item) {
            $day_sections = array_keys($item['schedule']);
            foreach ($day_sections as $day_section) {
                $schedule_result[$day]['sections'][$sections_result[$day_section]['id']] = [
                    'id' => $sections_result[$day_section]['id'],
                    'title' => $sections_result[$day_section]['title']
                ];
            }

            // Сортируем секции
            $item['sections'] = ksort($schedule_result[$day]['sections']);
            $item['schedule'] = ksort($schedule_result[$day]['schedule']);
        }

        $days = array_keys($schedule_result);
        $days_result = [];
        for($i = 1; $i <= count($days); $i++) {
            $days_result[$days[$i - 1]] = [
                'id' => 'day' . $i,
                'title' => 'День ' . $i,
                'active' => ($days[$i - 1] == date('Ymd', time()))
            ];
        }

        // Ищем активный (текущий) день конференции
        $exist_active_day = false;
        foreach ($days_result as $day) {
            if ($day['active']) {
                $exist_active_day = true;
                break;
            }
        }
        if (!$exist_active_day) $days_result[$days[0]]['active'] = true;

        if ($lottery) {
            $lottery_numbers = LotteryUsers::find()
                ->where(['user_id' => $this->user->getID()])
                ->orderBy(['number' => SORT_ASC])
                ->asArray()
                ->all();

            foreach ($schedule_result as &$day) {
                foreach($day['schedule'] as &$section) {
                    foreach ($section as &$item) {
                        if ($item['type'] == '3') {
                            foreach ($lottery_numbers as $number) {
                                $item['lottery'][] = [
                                    'number' => $number['number'],
                                    'exist' => $number['exist']
                                ];
                            }
                        }
                    }
                }
            }
        }

        $form_rating = new RatingForm();
        if ($form_rating->load(Yii::$app->request->post())) {
            $count = Rating::find()
                ->where(['user_id' => $this->user->getID(), 'schedule_id' => $form_rating->number])
                ->count();

            if ($count == 0) {
                $query = (new Query())->createCommand()
                    ->insert('rating', [
                        'user_id' => $this->user->getID(),
                        'schedule_id' => $form_rating->number,
                        'rating' => $form_rating->rating,
                        'message' => $form_rating->message
                    ])
                    ->execute();

                $rating_status = ($query) ? 'rating-success' : 'rating-fail';
            } else {
                $rating_status = 'rating-fail';
            }

            return $this->redirect(['site/index']);
        }

        if ($_GET['response']) {
            switch ($_GET['response']) {
                case 'feedback-success':
                    $alert = [
                        'type' => 'success',
                        'message' => 'Спасибо! Ваша анкета успешно сохранена',
                        'close' => true
                    ];
                    break;
                case 'feedback-fail':
                    $alert = [
                        'type' => 'danger',
                        'message' => 'Произошла ошибка при сохранении анкеты',
                        'close' => true
                    ];
                    break;
                case 'rating-success':
                    $alert = [
                        'type' => 'success',
                        'message' => 'Оценка доклада успешно сохранена',
                        'close' => true
                    ];
                    break;
                case 'rating-fail':
                    $alert = [
                        'type' => 'danger',
                        'message' => 'Произошла ошибка при сохранении оценки',
                        'close' => true
                    ];
                    break;
            }
        }

        return $this->render('index', [
            'id' => $id,
            'event' => $event,
            'schedule' => $schedule_result,
            'days' => $days_result,
            'form_rating' => $form_rating,
            'alert' => $alert
        ]);
    }

    public function actionFavorites() {
        $id = $this->event_id;

        $event = Events::find()
            ->select('title')
            ->where(['id' => $id])
            ->asArray()
            ->one();

        $favorites = Favorites::find()
            ->select('schedule_id')
            ->where(['event_id' => $id, 'user_id' => $this->user->getID()])
            ->asArray()
            ->all();

        $favorites_ids = [];
        foreach ($favorites as $favorite) {
            $favorites_ids[] = $favorite['schedule_id'];
        }

        $schedule = Schedule::find()
            ->where(['event_id' => $id, 'id' => $favorites_ids])
            ->orderBy(['date' => SORT_ASC, 'time_start' => SORT_ASC])
            ->asArray()
            ->all();

        $schedule_result = [];
        foreach ($schedule as &$item) {
            $item['photo'] = FileHelper::search($item['id'], 'upload/event/' . $item['event_id'] . '/speakers', ['jpg', 'png']);
            $item['favorite'] = true;

            $key = str_replace('-', '', $item['date']);
            $schedule_result[$key][$item['section_id']][] = $item;
        }

        $days = array_keys($schedule_result);
        if (count($days) > 1) {
            $days_result = [];
            foreach ($days as $day) {
                $days_result[$day] = date('d.m.Y', strtotime($day));
            }
        } else {
            $days_result = false;
        }

        $sections = Sections::find()
            ->select(['id', 'title'])
            ->where(['event_id' => $id])
            ->asArray()
            ->all();

        $sections_result = [];
        foreach ($sections as $section) {
            $sections_result[$section['id']] = $section['title'];
        }

        if (!$favorites) {
            $alert = [
                'type' => 'warning',
                'message' => 'Вы еще не добавили доклады в избранное'
            ];
        }

        return $this->render('favorites', [
            'id' => $id,
            'event' => $event,
            'favorites' => $favorites,
            'schedule' => $schedule_result,
            'sections' => $sections_result,
            'days' => $days_result,
            'alert' => $alert
        ]);
    }

    public function actionQuestions() {
        $id = $this->event_id;
        $schedule_id = (int) Yii::$app->request->get('schedule');

        if (!$schedule_id) throw new \yii\web\NotFoundHttpException();

        $event = Events::find()
            ->select(['title', 'date_end'])
            ->where(['id' => $id])
            ->one();

        // Получаем информацию о докладе
        $schedule = Schedule::find()
            ->where([
                'event_id' => $id,
                'id' => $schedule_id,
                'type' => 2,
            ])
            ->asArray()
            ->one();

        if (!$schedule) throw new \yii\web\NotFoundHttpException();

        $start = (time() > strtotime($schedule['time_start'])) ? true : false; // Начался ли доклад
        $end = (time() > strtotime($schedule['time_end'])) ? true : false; // Закончился ли доклад

        $form = new QuestionsForm();
        $form_answer = new QuestionsAnswerForm();

        // Получаем список вопросов к текущему докладу
        $questions = Questions::find()
            ->where(['schedule_id' => $schedule['id']])
            ->orderBy(['date' => SORT_DESC])
            ->asArray()
            ->all();

        // Сохраняем в массив id пользователей, оставивших вопрос и id вопроса
        $users_ids = [];
        $question_ids = [];
        foreach ($questions as $question) {
            $users_ids[] = $question['user_id'];
            $question_ids[] = $question['id'];
        }

        // Извлекаем информацию о пользователя, оставивших вопрос
        $users = Users::find()
            ->select(['id', 'login', 'name'])
            ->where(['id' => $users_ids])
            ->asArray()
            ->all();

        $users_result = [];
        foreach ($users as $user) {
            $users_result[$user['id']] = $user;
        }

        // Получаем список вопросов, добавленных в избранное текущим пользователем
        $likes = Likes::find()
            ->select('question_id')
            ->where(['schedule_id' => $schedule['id'], 'user_id' => $this->user->getID()])
            ->asArray()
            ->all();

        $likes_result = [];
        foreach ($likes as $like) {
            $likes_result[$like['question_id']] = true;
        }

        // Получаем кол-во лайков
        $likes_count = Likes::find()
            ->select(['COUNT(1)', 'question_id'])
            ->where(['schedule_id' => $schedule['id']])
            ->groupBy('question_id')
            ->asArray()
            ->all();

        $likes_count_result = [];
        foreach ($likes_count as $like) {
            $likes_count_result[$like['question_id']] = $like['COUNT(1)'];
        }

        // Получим ответы к вопросам
        $answers = QuestionsAnswers::find()
            ->where(['question_id' => $question_ids])
            ->asArray()
            ->all();

        $answers_result = [];
        foreach ($answers as $answer) {
            $answers_result[$answer['question_id']] = $answer['answer'];
        }

        // Добавляем данные к вопросу
        foreach ($questions as &$question) {
            $question['author'] = $users_result[$question['user_id']]['name'];
            $question['login'] = $users_result[$question['user_id']]['login'];
            $question['like'] = ($likes_result[$question['id']]) ? true : false;
            $question['like_count'] = ($likes_count_result[$question['id']] > 0) ? $likes_count_result[$question['id']] : 0;
            $question['can_answer'] = ($schedule['user_id'] == $this->user->getID()) ? true : false;
            $question['answer'] = ($answers_result[$question['user_id']]) ? $answers_result[$question['user_id']] : false;
        }

        // Сортировка по лайкам
        if (isset($_GET['sort']) && $_GET['sort'] == 'like') array_multisort(array_column($questions, 'like_count'), SORT_DESC, $questions);

        if ($form->load(Yii::$app->request->post())) {
            if (!$form->id || !$form->schedule) {
                $alert = [
                    'type' => 'danger',
                    'message' => 'Произошла ошибка, попробуйте еще раз',
                    'close' => true
                ];
            } elseif (!$form->question) {
                $alert = [
                    'type' => 'danger',
                    'message' => 'Заполните поле "Вопрос"',
                    'close' => true
                ];
            } else {
                $event_id = (int) trim($form->id);
                $schedule_id = (int) trim($form->schedule);
                $question = htmlspecialchars(trim($form->question));

                $query = (new Query())->createCommand()
                    ->insert('questions', [
                        'schedule_id' => $schedule_id,
                        'user_id' => $this->user->getID(),
                        'question' => $question
                    ])
                    ->execute();
                if ($query) {
                    return $this->redirect(['site/questions', 'schedule' => $schedule_id, 'response' => 'success']);
                } else {
                    return $this->redirect(['site/questions', 'schedule' => $schedule_id, 'response' => 'fail']);
                }
            }
            $form->question = NULL;
        }

        if ($form_answer->load(Yii::$app->request->post())) {
            if (!$form_answer->id) {
                $alert = [
                    'type' => 'danger',
                    'message' => 'Произошла ошибка, попробуйте еще раз',
                    'close' => true
                ];
            } elseif (!$form_answer->answer) {
                $alert = [
                    'type' => 'danger',
                    'message' => 'Ответ пустой',
                    'close' => true
                ];
            } else {
                $question_id = (int) $form_answer->id;
                $answer = trim($form_answer->answer);

                $query = (new Query())->createCommand()
                    ->insert('questions_answers', ['question_id' => $question_id, 'answer' => $answer])
                    ->execute();

                if ($query) {
                    return $this->redirect(['site/questions', 'speaker' => $speaker, 'response' => 'answer-success']);
                } else {
                    return $this->redirect(['site/questions', 'speaker' => $speaker, 'response' => 'answer-fail']);
                }
            }
        }

        // Вопросы можно задавать после начала доклада и час после завершения доклада
        $date_end = date('d.m.Y', strtotime($schedule['date'])) . ' ' . date('H:i', strtotime($schedule['time_end']));
        $show_form = ($start && strtotime($date_end) + 3600 > time()) ? true : false;

        if ($_GET['response']) {
            switch ($_GET['response']) {
                case 'success':
                    $alert = [
                        'type' => 'success',
                        'message' => 'Ваш вопрос успешно сохранен',
                        'close' => true
                    ];
                    break;
                case 'fail':
                    $alert = [
                        'type' => 'danger',
                        'message' => 'Произошла ошибка при сохранении вопроса',
                        'close' => true
                    ];
                    break;
                case 'answer-success':
                    $alert = [
                        'type' => 'success',
                        'message' => 'Ваш ответ успешно сохранен',
                        'close' => true
                    ];
                    break;
                case 'answer-fail':
                    $alert = [
                        'type' => 'danger',
                        'message' => 'Произошла ошибка при сохранении ответа',
                        'close' => true
                    ];
                    break;
            }
        }

        return $this->render('questions', [
            'event' => $event,
            'id' => $id,
            'schedule' => $schedule,
            'schedule_id' => $schedule_id,
            'questions' => $questions,
            'form' => $form,
            'form_answer' => $form_answer,
            'show_form' => $show_form,
            'alert' => $alert
        ]);
    }

    public function actionFeedback() {
        $id = $this->event_id;

        $event = Events::find()
            ->select('title')
            ->where(['id' => $id])
            ->asArray()
            ->one();

        $feedback = FeedbackAnswers::find()
            ->where(['event_id' => $id, 'user_id' => $this->user->getID()])
            ->one();

        if (!$feedback) {
            $daytime_questions = FeedbackQuestions::find()
                ->select('date')
                ->where(['event_id' => $id])
                ->distinct()
                ->count();

            $where = ($daytime_questions > 1) ? ['event_id' => $id, 'date' => date('Y-m-d')] : ['event_id' => $id];

            $questions = FeedbackQuestions::find()
                ->where($where)
                ->asArray()
                ->all();

            foreach ($questions as &$question) {
                if ($question['type'] == 'C' || $question['type'] == 'R') {
                    $question['choice'] = explode(';', $question['choice']);
                }
            }

            $form = new FeedbackForm();

            if ($form->load(Yii::$app->request->post())) {
                $answers = $form->answer;
                $question_ids = array_keys($form->answer);

                $all_empty = true;
                foreach ($answers as &$answer) {
                    if ($all_empty && !empty($answer)) $all_empty = false;
                    if (is_array($answer)) $answer = implode(';', $answer);
                }

                if ($all_empty) {
                    $alert = [
                        'type' => 'danger',
                        'message' => 'Заполните хотя бы одно поле',
                        'close' => true
                    ];
                } else {
                    $count = FeedbackAnswers::find()
                        ->where(['event_id' => $this->event_id, 'user_id' => $this->user->getID()])
                        ->count();

                    if ($count == 0) {
                        $data = [];
                        for ($i = 0; $i < count($question_ids); $i++) {
                            if (!empty($answers[$question_ids[$i]])) {
                                $data[] = [
                                    'user_id' => $this->user->getID(),
                                    'event_id' => $id,
                                    'question_id' => $question_ids[$i],
                                    'answer' => $answers[$question_ids[$i]]
                                ];
                            }
                        }

                        $query = (new Query())->createCommand()
                            ->batchInsert('feedback_answers', ['user_id', 'event_id', 'question_id', 'answer'], $data)
                            ->execute();

                        $status = ($query) ? 'feedback-success' : 'feedback-fail';
                        return $this->redirect(['site/index', 'response' => $status]);
                    } else {
                        return $this->redirect(['site/index', 'response' => 'feedback-fail']);
                    }
                }
            }
        }

        return $this->render('feedback', [
            'form' => $form,
            'title' => $event['title'],
            'questions' => $questions,
            'alert' => $alert
        ]);
    }

    public function actionNote() {
        $id = (int) Yii::$app->request->get('schedule');
        $form = new NoteForm();

        $schedule = Schedule::find()
            ->select('title')
            ->where(['id' => $id])
            ->asArray()
            ->one();

        $note = Notes::find()
            ->select('note')
            ->where(['user_id' => $this->user->getID(), 'schedule_id' => $id])
            ->asArray()
            ->one();

        if ($form->load(Yii::$app->request->post())) {
            if (!$form->note) {
                $alert = [
                    'type' => 'danger',
                    'message' => 'Заметка пустая',
                    'close' => true
                ];
            } else {
                if ($note) {
                    $query = (new Query())->createCommand()
                        ->update('notes', ['note' => $form->note], ['user_id' => $this->user->getID(), 'schedule_id' => $id])
                        ->execute();
                } else {
                    $query = (new Query())->createCommand()
                        ->insert('notes', [
                            'user_id' => $this->user->getID(),
                            'schedule_id' => $id,
                            'note' => $form->note
                        ])
                        ->execute();
                }

                if ($query) {
                    return $this->redirect(['site/note', 'schedule' => $id, 'response' => 'success']);
                } else {
                    return $this->redirect(['site/note', 'schedule' => $id, 'response' => 'fail']);
                }
            }
        }

        if ($_GET['response']) {
            switch ($_GET['response']) {
                case 'success':
                    $alert = [
                        'type' => 'success',
                        'message' => 'Заметка успешно сохранена',
                        'close' => true
                    ];
                    break;
                case 'fail':
                    $alert = [
                        'type' => 'danger',
                        'message' => 'Произошла ошибка при сохранении заметки, попробуйте еще раз',
                        'close' => true
                    ];
                    break;
            }
        }

        return $this->render('note', [
            'form' => $form,
            'note' => $note['note'],
            'schedule' => $schedule,
            'alert' => $alert
        ]);
    }

    public function actionTimeline() {
        $id = $this->event_id;

        $sections = Sections::find()
            ->where(['event_id' => $id])
            ->asArray()
            ->all();

        $section_result = [];
        foreach ($sections as $section) {
            $section_result[$section['id']] = $section['title'];
        }

        $favorites = Favorites::find()
            ->select('schedule_id')
            ->where(['event_id' => $id, 'user_id' => $this->user->getID()])
            ->asArray()
            ->all();

        $favorites_result = [];
        foreach ($favorites as $row) {
            $favorites_result[$row['schedule_id']] = true;
        }

        $date = date('Y-m-d', time());
        $time = date('H:i:s', time());

        $schedule = Schedule::find()
            ->where(['event_id' => $id, 'type' => 2, 'date' => $date])
            ->andWhere(['>=', 'time_start', $time])
            ->orderBy(['date' => SORT_ASC, 'time_start' => SORT_ASC])
            ->asArray()
            ->all();

        $schedule_result = [];
        foreach ($schedule as &$item) {
            $time = date('H:i', strtotime($item['time_start']));
            $item['section'] = $section_result[$item['section_id']];
            $item['end'] = date('H:i', strtotime($item['time_end']));
            $item['favorite'] = ($favorites_result[$item['id']]) ? true : false;
            $schedule_result[$time][] = $item;
        }

        return $this->render('timeline', [
            'schedule' => $schedule_result
        ]);
    }

    public function actionHistory() {
        $id = $this->event_id;

        $user_events = UsersAccess::find()
            ->select('event_id')
            ->where(['user_id' => $this->user->getID()])
            ->asArray()
            ->all();

        $ids = [];
        foreach ($user_events as $event) {
            $ids[] = $event['event_id'];
        }

        $events = Events::find()
            ->where(['id' => $ids, 'active' => 1])
            ->orderBy(['date_start' => SORT_DESC])
            ->asArray()
            ->all();

        $events_result = [];
        foreach ($events as &$event) {
            $date_start = date('d.m.Y', strtotime($event['date_start']));
            $date_end = date('d.m.Y', strtotime($event['date_end']));
            $event['date'] = ($date_start == $date_end) ? $date_start : $date_start . ' &ndash; ' . $date_end;

            $events_result[] = $event;
        }

        if ($_GET['response']) {
            switch ($_GET['response']) {
                case 'fail':
                    $alert = [
                        'type' => 'danger',
                        'message' => 'Произошла ошибка',
                        'close' => true
                    ];
                    break;
            }
        }

        return $this->render('history', [
            'events' => $events_result,
            'alert' => $alert
        ]);
    }

    public function actionVideo() {
        $id = (int) Yii::$app->request->get('id');

        $schedule = Schedule::find()
            ->select(['event_id', 'speaker', 'title'])
            ->where(['id' => $id])
            ->asArray()
            ->one();

        $video = FileHelper::search($id, 'upload/event/' . $schedule['event_id'] . '/video', ['avi', 'mkv', 'mp4']);

        if (!$schedule || !$video) throw new \yii\web\NotFoundHttpException();

        return $this->render('video', [
            'schedule' => $schedule,
            'video' => $video
        ]);
    }

    public function actionRating() {
        $id = (int) Yii::$app->request->get('id');

        $schedule = Schedule::find()
            ->select('user_id')
            ->where(['id' => $id])
            ->asArray()
            ->one();

        if (!$schedule || $schedule['user_id'] != $this->user->getID()) throw new \yii\web\NotFoundHttpException();

        $rating = Rating::find()
            ->select(['rating', 'message'])
            ->where(['schedule_id' => $id])
            ->asArray()
            ->all();

        $average = Rating::find()
            ->select('AVG(rating), schedule_id')
            ->where(['schedule_id' => $id])
            ->groupBy('schedule_id')
            ->asArray()
            ->all();

        $result = [];
        foreach ($rating as $row) {
            $result[] = [
                'rating' => $row['rating'],
                'message' => $row['message'],
                'attribute' => ($row['message']) ? 'message' : 'all'
            ];
        }

        $average = round($average[0]['AVG(rating)'], 2);
        $count = count($rating);

        return $this->render('rating', [
            'rating' => $result,
            'average' => $average,
            'count' => $count
        ]);
    }

    public function actionWelcome() {
        $events = Events::find()
            ->select(['title', 'desc', 'date_start'])
            ->where(['access' => 1, 'active' => 1])
            ->asArray()
            ->all();

        $events_result = [];
        foreach ($events as &$event) {
            $type = (time() > strtotime($event['date_start'])) ? 'ended' : 'future';
            $event['desc'] = mb_substr(strip_tags($event['desc']), 0, 200) . '...';
            $events_result[$type][] = $event;
        }

        return $this->render('welcome', [
            'user' => $this->user,
            'events' => $events_result
        ]);
    }

    public function actionResult() {
        $event_id = 7;

        $users = Users::find()
            ->select(['id', 'login', 'name', 'email'])
            ->where(['role' => 1, 'confirm' => 1])
            ->all();

        $users_result = [];
        $users_result_2 = [];
        $users_logins = [];
        foreach ($users as $user) {
            $users_result[$user->id] = [
                'login' => $user->login,
                'name' => $user->name,
                'email' => $user->email
            ];

            $users_result_2[$user->login] = [
                'name' => $user->name,
                'email' => $user->email
            ];

            $users_logins[] = $user->login;
        }

        // Анкета
        $questions = FeedbackQuestions::find()
            ->select(['id', 'question', 'date'])
            ->where(['event_id' => $event_id])
            ->all();

        $questions_result = [];
        foreach ($questions as $question) {
            $questions_result[$question->id] = [
                'question' => $question->question,
                'date' => date('d.m.Y', strtotime($question->date))
            ];
        }

        $answers = FeedbackAnswers::find()
            ->where(['event_id' => $event_id])
            ->all();

        $result = [];
        foreach ($answers as $answer) {
            $result[] = [
                'login' => $users_result[$answer->user_id]['login'],
                'name' => $users_result[$answer->user_id]['name'],
                'email' => $users_result[$answer->user_id]['email'],
                'question' => $questions_result[$answer->question_id]['question'],
                'answer' => $answer->answer,
                'date' => $questions_result[$answer->question_id]['date']
            ];
        }

        $csv = "\xEF\xBB\xBF"; // BOM
        $csv .= "Номер телефона;Участник;Email;Вопрос;Ответ;Дата\n";
        foreach ($result as $value) {
            $csv .= $value['login'] . ';';
            $csv .= $value['name'] . ';';
            $csv .= $value['email'] . ';';
            $csv .= $value['question'] . ';';
            $csv .= $value['answer'] . ';';
            $csv .= $value['date'] . ';';
            $csv .= "\n";
        }

        $this->saveFile($csv, 'feedback', false);

        // Оценка докладов
        $schedule = Schedule::find()
            ->select(['id', 'speaker', 'title'])
            ->where(['event_id' => $event_id])
            ->all();

        $schedule_result = [];
        $speaker_ids = [];
        foreach ($schedule as $item) {
            $schedule_result[$item->id] = [
                'speaker' => $item->speaker,
                'title' => $item->title
            ];
            $speaker_ids[] = $item->id;
        }

        $rating = Rating::find()
            ->where(['schedule_id' => $speaker_ids])
            ->all();

        $result = [];
        foreach ($rating as $value) {
            $result[] = [
                'login' => $users_result[$value->user_id]['login'],
                'name' => $users_result[$value->user_id]['name'],
                'email' => $users_result[$value->user_id]['email'],
                'speaker' => $schedule_result[$value->schedule_id]['speaker'],
                'title' => $schedule_result[$value->schedule_id]['title'],
                'rating' => $value->rating,
                'message' => $value->message
            ];
        }

        $csv = "\xEF\xBB\xBF"; // BOM
        $csv .= "Номер телефона;Участник;Email;Спикер;Доклад;Оценка;Комментарий\n";
        foreach ($result as $value) {
            $csv .= $value['login'] . ';';
            $csv .= $value['name'] . ';';
            $csv .= $value['email'] . ';';
            $csv .= $value['speaker'] . ';';
            $csv .= $value['title'] . ';';
            $csv .= $value['rating'] . ';';
            $csv .= $value['message'] . ';';
            $csv .= "\n";
        }

        $this->saveFile($csv, 'rating', false);

        // Входы
        $event = Events::find()
            ->select(['date_start', 'date_end'])
            ->where(['id' => $event_id])
            ->one();

        $users_history = UsersHistory::find()
            ->select(['login'])
            ->where(['login' => $users_logins, 'success' => 1])
            ->andWhere(['>=', 'date', $event->date_start])
            ->andWhere(['<=', 'date', $event->date_end])
            ->distinct()
            ->all();

        $result = [];
        foreach ($users_history as $value) {
            $result[] = [
                'login' => $value->login,
                'name' => $users_result_2[$value->login]['name'],
                'email' => $users_result_2[$value->login]['email'],
            ];
        }

        $csv = "\xEF\xBB\xBF"; // BOM
        $csv .= "Номер телефона;Участник;Email\n";
        foreach ($result as $value) {
            $csv .= $value['login'] . ';';
            $csv .= $value['name'] . ';';
            $csv .= $value['email'] . ';';
            $csv .= "\n";
        }

        $this->saveFile($csv, 'entry', false);

        // Вопросы к докладам
        $questions = Questions::find()
            ->where(['schedule_id' => $speaker_ids])
            ->all();

        $speaker_ids = [];
        foreach ($questions as $question) {
            $speaker_ids[] = $question->schedule_id;
        }

        $schedule = Schedule::find()
            ->select(['id', 'speaker', 'title'])
            ->where(['id' => $speaker_ids])
            ->all();

        $speakers = [];
        foreach ($schedule as $item) {
            $speakers[$item->id] = [
                'speaker' => $item->speaker,
                'title' => $item->title,
            ];
        }

        $result = [];
        foreach ($questions as $question) {
            $result[] = [
                'login' => $users_result[$question->user_id]['login'],
                'name' => $users_result[$question->user_id]['name'],
                'email' => $users_result[$question->user_id]['email'],
                'speaker' => $speakers[$question->schedule_id]['speaker'],
                'title' => $speakers[$question->schedule_id]['title'],
                'question' => $question->question,
                'date' => date('d.m.Y H:i:s', strtotime($question->date))
            ];
        }

        $csv = "\xEF\xBB\xBF"; // BOM
        $csv .= "Номер телефона;Участник;Email;Докладчик;Тема доклада;Вопрос;Дата\n";
        foreach ($result as $value) {
            $csv .= $value['login'] . ';';
            $csv .= $value['name'] . ';';
            $csv .= $value['email'] . ';';
            $csv .= $value['speaker'] . ';';
            $csv .= $value['title'] . ';';
            $csv .= $value['question'] . ';';
            $csv .= $value['date'] . ';';
            $csv .= "\n";
        }

        $this->saveFile($csv, 'questions', false);

        return $this->render('result', [
            'result' => true
        ]);
    }
}