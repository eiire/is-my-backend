<?php
namespace event\models;

use Yii;
use yii\base\Model;
use event\models\db\Events;

class Event extends Model {
    /**
     * Форматирует описание
     * 
     * @param string $desc описание
     * @param array $event данные конференции
     * @param object $user данные пользователя
     * 
     * @return string
     */
    static function formatDesc($desc, $event, $user) {
        $replace = [
            '{{ login }}' => $user->getLogin(),
            '{{ name }}' => $user->getName(),
            '{{ email }}' => $user->getEmail(),
            '{{ title }}' => $event['title'],
            '{{ location }}' => $event['location'],
            '{{ url }}' => $event['url'],
            '{{ date.start }}' => date('d.m.Y', strtotime($event['date_start'])),
            '{{ date.end }}' => date('d.m.Y', strtotime($event['date_end'])),
            '{{ time.start }}' => date('H:i', strtotime($event['date_start'])),
            '{{ time.end }}' => date('H:i', strtotime($event['date_end'])),
            "\n" => '<br>'
        ];

        $keys = array_keys($replace);
        foreach ($keys as $key) {
            $desc = preg_replace('/(' . $key . ')/iu', $replace[$key], $desc);
        }

        return $desc;
    }

    /**
     * Возвращает id ближайшей конференции
     * 
     * @return int|bool
     */
    static function nextEvent() {
        $event = Events::find()
            ->select('id')
            ->where(['=>', 'date_start', date('Y-m-d') + '00:00:00'])
            ->orderBy(['date_start' => SORT_ASC])
            ->one();

        if (!$event) return false;
        return $event->id;
    }

    /**
     * Возвращает сегодняшнуюю конференцию
     * 
     * @return bool
     */
    static function todayEvent() {
        $event = Events::find()
            ->where(['id' => 9])
            ->orderBy(['date_start' => SORT_ASC])
            ->one();

        if (!$event) return false;
        return $event;
    }
}