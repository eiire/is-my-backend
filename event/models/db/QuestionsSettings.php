<?php
namespace event\models\db;

use Yii;
use yii\db\ActiveRecord;

/**
 * Таблица с настройками вопросов с автообновлением 
 * 
 * @var int $id - уникальный идентификатор записи
 * @var int $value - значение
 *
 * id: 0
 * value:
 * 0 - остановить автообновление
 * 1 - не -//-
 * 
 * id: 1
 * value - id доклада для автообновления
 */
class QuestionsSettings extends ActiveRecord {}