<?php
namespace event\models\db;

use Yii;
use yii\db\ActiveRecord;

/**
 * Таблица с расписанием мероприятий
 * 
 * @var int $id - уникальный идентификатор записи
 * @var int $event_id - id конференции из таблицы event_list
 * @var int $section_id - id секции из таблицы event_sections
 * @var int $type - тип элемента расписания:
*                    1 - Организационное мероприятие без спикера (перерыв, обед)
*                    2 - Доклад со спикером
*                    3 - Лотерея
 * @var varchar $speaker - Спекер
 * @var varchar $title - Название элемента расписания
 * @var text $desc - Описание элемента расписания
 * @var varchar $company - Компания спикера
 * @var varchar $position - Должность спикера
 * @var date $date - Дата начала
 * @var time $time_start - Время начала
 * @var time $time_end - Время конца
 */
class Schedule extends ActiveRecord {}