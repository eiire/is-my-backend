<?php
namespace event\models\forms;

use Yii;
use yii\base\Model;

class FeedbackForm extends Model {
    public $answer;

    public function rules() {
        return [
            [['answer'], 'required'],
        ];
    }
}