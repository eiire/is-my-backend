<?php
namespace event\models\forms;

use Yii;
use yii\base\Model;

class NoteForm extends Model {
    public $note;

    public function rules() {
        return [
            [['note'], 'required'],
        ];
    }
}