<?php
namespace event\models\forms;

use Yii;
use yii\base\Model;

class QuestionsAnswerForm extends Model {
    public $id;
    public $answer;

    public function rules() {
        return [
            [['answer', 'id'], 'required']
        ];
    }
}