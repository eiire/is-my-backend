<?php
namespace event\models\forms;

use Yii;
use yii\base\Model;

class QuestionsForm extends Model {
    public $id, $schedule, $question;

    public function rules() {
        return [
            [['id', 'schedule', 'question'], 'required'],
        ];
    }
}