<?php
namespace event\models\forms;

use Yii;
use yii\base\Model;

class SigninForm extends Model {
    public $login;

    public function rules() {
        return [
            [['login'], 'required']
        ];
    }
}