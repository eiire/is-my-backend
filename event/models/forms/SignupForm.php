<?php
namespace event\models\forms;

use Yii;
use yii\base\Model;

class SignupForm extends Model {
    public $login;
    public $email;
    public $name;

    public function rules() {
        return [
            [['login', 'name', 'email'], 'required'],
        ];
    }
}