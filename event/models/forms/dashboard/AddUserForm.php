<?php
namespace event\models\forms\dashboard;

use Yii;
use yii\base\Model;

class AddUserForm extends Model {
    public $event;
    public $phone;
    public $email;
    public $name;
    public $lottery;

    public function rules() {
        return [
            [['event', 'phone', 'email', 'name', 'lottery'], 'string'],
        ];
    }
}