<?php
namespace event\models\forms\dashboard;

use Yii;
use yii\base\Model;

class QuestionsForm extends Model {
    public $event;
    public $schedule;

    public function rules() {
        return [
            [['event'], 'required'],
            [['schedule'], 'string']
        ];
    }
}
