<?php
namespace event\models\forms\dashboard;

use Yii;
use yii\base\Model;

class UsersForm extends Model {
    public $event;

    public function rules() {
        return [
            [['event'], 'string'],
        ];
    }
}