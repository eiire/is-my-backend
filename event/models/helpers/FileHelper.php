<?php
namespace event\models\helpers;

use Yii;

class FileHelper {
    /**
    * Поиск файла
    * 
    * @param int $id имя файла
    * @param string $dir папка для поиска
    * @param array $format формат файла
    * 
    * @return string|false ссылка на файл или false
    */
    static function search($name, $dir, $format, $url = true) {
        $file_name = $dir . '/' . $name;
        for ($i = 0; $i < count($format); $i++) {
            if (file_exists(__DIR__ . '/../../web/' . $file_name . '.' . $format[$i])) {
                if ($url) {
                    $file = Yii::$app->params['url'] . '/' . $file_name . '.' . $format[$i];
                } else {
                    $file = __DIR__ . '/../../web/' . $file_name . '.' . $format[$i];
                }
                return $file;
            }
        }
        return false;
    }

    /**
     * Записывает текст в файл и скачивае его
     * 
     * @param string $str текст для записи
     * @param string $name имя файла
     * @param bool $download скачать файл
     * @param string $dir папка для сохранения файла
     */
    static function create($str, $name, $download = true, $dir = 'upload/tmp/') {
        $file_path = __DIR__ . '/../../web/' . $dir . $name;
        $file = fopen($file_path, 'w+');
        fwrite($file, $str);
        fclose($file);
        if ($download) Yii::$app->response->SendFile($file_path);
    }
}