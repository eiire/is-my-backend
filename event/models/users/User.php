<?php
namespace event\models\users;

use Yii;
use yii\helpers\Url;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\base\Security;
use yii\base\Model;
use event\models\users\Users;
use event\models\users\UsersHistory;
use event\models\users\UsersConfirm;
use event\models\users\UsersSession;

class User extends Model {
    public $login;
    protected $user;
    protected $session;

    public function init() {
        if (Yii::$app->request->cookies->has('access')) {
            $token = htmlspecialchars(Yii::$app->request->cookies->get('access')->value);
            $this->session = UsersSession::find()
                ->select(['id', 'user_id'])
                ->where(['token' => $token])
                ->one();

            $users = Users::find()
                ->where(['id' => $this->session->user_id])
                ->one();
        }

        $this->user = ($token && $users) ? $users : false;
        parent::init();
    }

    /**
     * Получить id текущего пользователя
     * 
     * @return int|bool
     */
    public function getID() {
        return $this->session->user_id;
    }

    /**
     * Получить логин текущего пользователя
     * 
     * @return string
     */
    public function getLogin() {
        return $this->user->login;
    }

    /**
     * Получить token текущего пользователя
     * 
     * @return string
     */
    public function getToken() {
        return (Yii::$app->request->cookies->has('access')) ? htmlspecialchars(Yii::$app->request->cookies->get('access')->value) : false;
    }

    /**
     * Получить email текущего пользователя
     * 
     * @return string
     */
    public function getEmail() {
        return $this->user->email;
    }

    /**
     * Получить имя пользователя
     * 
     * @return string
     */
    public function getName() {
        return $this->user->name;
    }

    /**
     * Получить id сессии
     * 
     * @return int
     */
    public function getSessionID() {
        return $this->session->id;
    }

    /**
     * Получить роль пользователя
     * 
     * @return int
     */
    public function getRole() {
        return $this->user->role;
    }

    /**
     * Подтвержден ли пользователь
     * 
     * @return bool
     */
    public function getConfirm() {
        return (bool) $this->user->confirm;
    }

    /**
     * Проверка авторизации
     *
     * @param string $action - текущее представление
     * @param array $exception - представления, для которых нужно исключить проверку прав доступа (доступ к ним будет постоянно разрешен)
     *
     * @return bool
     */
    public function haveAccess($action = NULL, $exception = []) {
        if (!in_array($action, $exception)) {
            $token = $this->getToken();

            if (!$token || !$this->user) return false;

            (new Query())->createCommand()
                ->update('users_session',
                    ['date' => date('Y-m-d H:i:s')],
                    ['id' => $this->getSessionID()]
                )
                ->execute();
        }

        return true;
    }


    /**
     * Проверяет права доступа у текущего пользователя
     * 0 - Незарегистрированный пользователь, авторизованный через соц.сети
     * 1 - Зарегистрированный пользователь
     * 
     * @param int|array минимальные права доступа
     * 
     * @return bool
     */
    public function permission($role) {
        if (is_array($role)) return in_array($this->getRole(), $role);
        if ($this->getRole() == $role) return true;
        return false;
    }

    /**
     * Создает сессию пользователя
     * 
     * @param int $user_id - id пользователя
     * 
     * @return bool
     */
    public function createSession($user_id) {
        $token = (new Security())->generateRandomString();

        $result = (new Query())->createCommand()
            ->insert('users_session', [
                'user_id' => (int) $user_id,
                'token' => $token,
            ])
            ->execute();

        $id = Yii::$app->db->getLastInsertID();

        if (Yii::$app->request->cookies->has('access')) Yii::$app->response->cookies->remove('access');

        Yii::$app->response->cookies->add(new \yii\web\Cookie([
            'name' => 'access',
            'value' => $token,
            'expire' => (new \DateTime('+1 week'))->getTimestamp()
        ]));

        return ($result) ? $id : false;
    }

    /**
     * Выход пользователя из профиля
     * 
     * @return bool
     */
    public function logOut() {
        $token = $this->getToken();
        if ($token) {
            UsersSession::deleteAll(['token' => $token]);
        }

        Yii::$app->response->cookies->remove('access');
        return true;
    }

    /**
     * Существует ли пользователь с таким логином
     * Возвращает id пользователя или false
     *
     * @return bool
     */
    public function userExist() {
        $user = Users::find()
            ->select('id')
            ->where(['login' => $this->login])
            ->one();

        return ($user) ? $user->id : false;
    }

    /**
     * Возвращает id конференций, к которым иимеет доступ пользователь
     * 
     * @return array
     */
    public function userEvents() {
        $query = UsersAccess::find()
            ->select('event_id')
            ->where(['user_id' => $this->getID()])
            ->asArray()
            ->all();

        return ($query) ? $query : false;
    }

    /**
     * Валидация email'a
     * 
     * @return bool
     */
    public function emailValidate($email) {
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) return true;
        return false;
    }

    /**
     * Валидация номера телефона
     * 
     * @return bool
     */
    public function phoneValidate($phone, $length = 10) {
        $str = str_replace([' ', '-', '(', ')', '+7', '+'], '', $phone);
        $nums = preg_replace('/[^0-9]/', '', $str);
        return (strlen($nums) == strlen($str) && strlen($nums) == $length) ? true : false;
    }

    /**
     * Сохранить историю входа
     * 
     * @param array $params Параметры входа
     * 
     * @return bool
     */
    public function saveHistory($params = []) {
        $query = (new Query())->createCommand()
            ->insert('users_history', [
                'login' => $params['login'],
                'ip' => $this->getIp(),
                'browser' => $this->getBrowser(),
                'success' => $params['success'],
            ])
            ->execute();

        return $query;
    }

    /**
     * Генерирует уникальный код подтверждения
     * 
     * @param int $min Минимальный символ
     * @param int $max Максимальный символ
     * 
     * @return int
     */
    public function generateCode($min, $max) {
        $continue = false;
        do {
            $code = random_int($min, $max);

            $count = UsersConfirm::find()
                ->where(['code' => $code])
                ->count();

            if ($count == 0) $continue = true;
        } while (!$continue);

        return $code;
    }

    /**
     * Получить IP-адрес пользователя
     * 
     * @return string
     */
    public function getIp() {
        $ip = explode(':', $_SERVER['HTTP_X_FORWARDED_FOR'])[0];
        if (!empty($ip)) return $ip;
        return $_SERVER['REMOTE_ADDR'];
    }

    /**
     * Получить браузер пользователя
     * 
     * @return string
     */
    public function getBrowser() {
        $user_agent = $_SERVER['HTTP_USER_AGENT'];
        if (strpos($user_agent, 'MSIE') !== false) return 'Internet Explorer';
        elseif (strpos($user_agent, 'OPR') !== false) return 'Opera';
        elseif (strpos($user_agent, 'YaBrowser') !== false) return 'Yandex Browser';
        elseif (strpos($user_agent, 'Vivaldi') !== false) return 'Vivaldi';
        elseif (strpos($user_agent, 'Maxthon') !== false) return 'Maxthon';
        elseif (strpos($user_agent, 'Edge') !== false) return 'Microsoft Edge';
        elseif (strpos($user_agent, 'Firefox') !== false) return 'Mozilla Firefox';
        elseif (strpos($user_agent, 'Chrome') !== false) return 'Google Chrome';
        elseif (strpos($user_agent, 'Safari') !== false) return 'Safari';
        else return 'Unknown';
    }

    /**
     * Получить страну пользователя
     * 
     * @return string|bool
     */
    public function getCountry() {
        $api = 'http://ip-api.com/json/';
        $json = json_decode(file_get_contents($api . $this->getIp()));
        if (isset($json->status) == 'fail') return false;
        return $json->country;
    }

    /**
     * Проверка капчи
     * 
     * @return bool
     */
    public function captchaValidate($recaptcha_response) {
        $query_url = 'https://www.google.com/recaptcha/api/siteverify';
        $query_data = http_build_query(['secret' => Yii::$app->params['recaptcha']['secret'], 'response' => $recaptcha_response, 'remoteip' => $this->getIp()]);

        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_POST => 1,
            CURLOPT_HEADER => 0,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_SSL_VERIFYPEER => 1,
            CURLOPT_URL => $query_url,
            CURLOPT_POSTFIELDS => $query_data,
        ]);

        $result = curl_exec($curl);
        curl_close($curl);

        $json = json_decode($result);
        return $json->success;
    }
}