<?php
use yii\helpers\Url;
?>
<div class="block">
    <a href="<?=Url::to(['site/details', 'id' => $result['id']]); ?>" class="h2"><?=$result['title']; ?></a>
    <div class="row text-lighten mt-2">
        <div class="col-12 col-md-auto">
            <i class="fas fa-map-marker-alt text-primary mr-2"></i> <span><?=$result['location']; ?></span>
        </div>
        <div class="col-12 col-md-auto">
            <i class="far fa-calendar-alt text-primary mr-2"></i> <?=date('d.m.Y', strtotime($result['date_start'])); ?>
        </div>
    </div>
    <div class="mt-2">
        <?=$result['desc']; ?>
    </div>
</div>