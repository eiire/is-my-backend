<div data-tab-content="<?=$result['attribute']; ?>" class="item m-b-10">
    <div class="row">
        <div class="col-12 col-md-auto">
            <div class="item-group">
                <div class="item-label">Оценка</div>
                <div class="item-desc"><?=$result['rating']; ?></div>
            </div>
        </div>
        <div class="col-12 col-md">
            <div class="item-group">
                <div class="item-label">Комментарий</div>
                <div class="item-desc"><?=$result['message']; ?></div>
            </div>
        </div>
    </div>
</div>