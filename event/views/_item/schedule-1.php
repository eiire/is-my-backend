<?php
// Организационное мероприятие без спикера (перерыв, обед)
use yii\helpers\Url; 
?>
<div class="item m-b-10">
    <div class="row no-gutters">
        <div class="col-auto">
            <div class="h3"><i class="far fa-clock text-primary"></i> <?=date('H:i', strtotime($result['time_start'])); ?><?php if ($result['time_end']) : ?> &ndash; <?=date('H:i', strtotime($result['time_end'])); ?><?php endif; ?></div>
        </div>
        <?php if ($result['start'] && !$result['end']) : ?>
        <div class="col-auto m-l-5" data-balloon="Идет сейчас">
            <i class="fas fa-circle fa-xs text-success"></i>
        </div>
        <?php endif; ?>
    </div>
    <div class="h2"><?=$result['title']; ?></div>
    <?=$result['desc']; ?>
</div>