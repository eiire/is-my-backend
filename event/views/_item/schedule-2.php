<?php
// Доклад со спикером
use yii\helpers\Url;
use common\widgets\Nav;
?>
<div class="item m-b-10">
    <div class="row no-gutters">
        <div class="col-12 col-md-auto">
            <?php if ($result['photo']) : ?>
            <img src="<?=$result['photo']; ?>" class="event-photo" alt="">
            <?php endif; ?>
        </div>
        <div class="col-12 col-md">
            <div class="row no-gutters">
                <div class="col-auto">
                <div class="h3"><i class="far fa-clock text-primary"></i> <?=date('H:i', strtotime($result['time_start'])); ?><?php if ($result['time_end']) : ?> &ndash; <?=date('H:i', strtotime($result['time_end'])); ?><?php endif; ?></div>
                </div>
                <?php if ($result['start'] && !$result['end']) : ?>
                <div class="col-auto m-l-5" data-balloon="Идет сейчас">
                    <i class="fas fa-circle fa-xs text-success"></i>
                </div>
                <?php endif; ?>
            </div>
            <div class="event-title h2">
                <?=$result['title']; ?>
                <?php if ($result['average']) : ?>
                <?php if ($result['this_speaker']) : ?>
                <a href="<?=Url::to(['site/rating', 'id' => $result['id']]); ?>"><span data-balloon="Средняя оценка" class="m-l-5 text-lighten"><?=$result['average']; ?></a>
                <?php elseif ($result['start']) : ?>
                <span data-balloon="Средняя оценка" class="m-l-5 text-lighten"><?=$result['average']; ?>
                <?php endif; ?>
                <?php endif; ?>
            </div>
            <div class="row m-b-5">
                <div class="col-12 col-md">
                    <?php if ($result['speaker']) : ?>
                    <p class="text-lighten m-b-0"><i class="fas fa-microphone text-primary m-r-5"></i> <?=$result['speaker']; ?>
                    <?php 
                        if ($result['company'] || $result['position']) echo '(';
                        if ($result['company']) echo $result['company'];
                        if ($result['company'] && $result['position']) echo ' &ndash; ';
                        if ($result['position']) echo $result['position'];
                        if ($result['company'] || $result['position']) echo ')';
                    ?>
                    </p>
                    <?php endif; ?>
                </div>
                <div class="col-auto">
                    <a href="<?=Url::to(['site/note', 'schedule' => $result['id']]); ?>" class="btn btn-link">Заметки</a>
                </div>
                <?php if ($result['start']) : ?>
                <div class="col-auto">
                    <a href="<?=Url::to(['site/questions', 'schedule' => $result['id']]); ?>" class="btn btn-link">Вопросы</a>
                </div>
                <?php endif; ?>
                <?php if ($result['show_rating']) : ?>
                <div class="col-auto">
                    <button data-fancybox data-src="#rating-modal" data-modal="true" data-rating-id="<?=$result['id']; ?>" class="btn btn-link">Оценить доклад</button>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <?php if ($result['desc']) : ?>
        <div class="item-details"><?=$result['desc']; ?></div>
        <button class="item-more"><i class="fas fa-chevron-down"></i></button>
    <?php endif; ?>
    <div class="event-favorite">
        <div class="row no-gutters">
            <?php if ($result['download']) : ?>
            <div class="col-auto">
                <div class="nav-list-wrapper m-r-20"><i class="fas fa-save fa-lg"></i></div>
                <?php
                    echo Nav::Widget([
                        'ul_class' => 'nav-list m-r-5',
                        'set_active' => false,
                        'items' => $result['download']
                    ]);
                ?>
            </div>
            <?php endif; ?>
            <div class="col-auto">
                <?php if ($result['favorite']) : ?>
                <span data-balloon="Удалить из избранного" data-favorite="yes" data-event="<?=$result['event_id']; ?>" data-schedule="<?=$result['id']; ?>"><i class="fas fa-star fa-lg"></i></span>
                <?php else : ?>
                <span data-balloon="Добавить в избранное" data-favorite="no" data-event="<?=$result['event_id']; ?>" data-schedule="<?=$result['id']; ?>"><i class="far fa-star fa-lg"></i></span>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>