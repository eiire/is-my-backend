<?php
// Лотерия
use yii\helpers\Url;
?>
<div class="item m-b-10">
    <div class="row no-gutters">
        <div class="col-auto">
            <div class="h3"><i class="far fa-clock text-primary"></i> <?=date('H:i', strtotime($result['time_start'])); if ($result['time_end']) : ?> &ndash; <?=date('H:i', strtotime($result['time_end'])); endif; ?></div>
        </div>
        <?php if ($result['start'] && !$result['end']) : ?>
        <div class="col-auto m-l-5" data-balloon="Идет сейчас">
            <i class="fas fa-circle fa-xs text-success"></i>
        </div>
        <?php endif; ?>
    </div>
    <div class="h2"><?=$result['title']; ?></div>
    <?php if (isset($result['lottery'])) : ?>
    <div class="lottery m-t-5 m-b-5">
        <?php foreach ($result['lottery'] as $number) : ?>
            <div class="lottery-item<?php if ($number['exist'] == '1') echo ' lottery-item-disabled' ?>"><?=$number['number']; ?></div>
        <?php endforeach; ?>
    </div>
    <?php endif; ?>
</div>