<?php
use yii\helpers\Url;
?>
<div class="item mb-4">
    <div class="h2"><?=$result['title']; ?></div>
    <div class="row mb-2">
        <div class="col-12 col-md text-lighten ">
            <p class="mb-0">
                <i class="fas fa-microphone text-primary m-r-5"></i> <?=$result['speaker']; ?> 
                <?php 
                    if ($result['company'] || $result['position']) echo '(';
                    if ($result['company']) echo $result['company'];
                    if ($result['company'] && $result['position']) echo ' &ndash; ';
                    if ($result['position']) echo $result['position'];
                    if ($result['company'] || $result['position']) echo ')';
                ?>
            </p>
            <p class="mb-0"><i class="fas fa-bullhorn text-primary m-r-5"></i> <?=$result['section']; ?></p>
            <p class="mb-0"><i class="far fa-clock text-primary m-r-5"></i> до <?=$result['end']; ?></p>
        </div>
    </div>
    <?php if ($result['desc']) : ?>
        <div class="item-details"><?=$result['desc']; ?></div>
        <button class="item-more"><i class="fas fa-chevron-down"></i></button>
    <?php endif; ?>
    <div class="event-favorite">
        <?php if ($result['favorite']) : ?>
        <span data-balloon="Удалить из избранного" data-favorite="yes" data-event="<?=$result['event_id']; ?>" data-schedule="<?=$result['id']; ?>"><i class="fas fa-star fa-lg"></i></span>
        <?php else : ?>
        <span data-balloon="Добавить в избранное" data-favorite="no" data-event="<?=$result['event_id']; ?>" data-schedule="<?=$result['id']; ?>"><i class="far fa-star fa-lg"></i></span>
        <?php endif; ?>
    </div>
</div>