<?php
use common\widgets\Nav;
?>
<div class="sidebar">
    <div class="logo-wrapper">
        <a href="/">
            <div class="logo"></div>
        </a>
    </div>
    <?php
        echo Nav::Widget([
            'ul_class' => 'nav',
            'set_active' => true,
            'items' => [
                [
                    'label' => 'Программа',
                    'icon' => 'fas fa-bullhorn',
                    'route' => ['site/index']
                ],
                [
                    'label' => 'Избранное',
                    'icon' => 'fas fa-star',
                    'route' => ['site/favorites']
                ],
                [
                    'label' => 'Далее',
                    'icon' => 'fas fa-clock',
                    'route' => ['site/timeline']
                ],
                [
                    'label' => 'Анкета',
                    'icon' => 'fas fa-pen',
                    'route' => ['site/feedback']
                ],
                [
                    'label' => 'Посещенные',
                    'icon' => 'fas fa-history',
                    'route' => ['site/history']
                ],
                [
                    'label' => 'Выход',
                    'icon' => 'fas fa-sign-out-alt',
                    'route' => ['site/index', 'logout' => true],
                ]
            ]
        ]);
        echo $this->render('@common/views/_partial/contacts');
    ?>
</div>