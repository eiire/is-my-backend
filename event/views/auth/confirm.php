<?php
$this->title = 'Авторизация';
use yii\widgets\ActiveForm;
use common\widgets\Alert;
use yii\helpers\Html;
use yii\helpers\Url;

$this->registerJsFile('https://www.google.com/recaptcha/api.js');
?>
<div class="login-page">
    <div class="login-container">
        <div class="logo-wrapper">
            <div class="logo"></div>
        </div>
        <div class="login-block">
            <h2 class="text-center light">Вход в аккаунт</h2>
            <?php if ($alert) echo Alert::Widget(['type' => $alert['type'], 'message' => $alert['message']]); ?>
            <?php
                $active_form = ActiveForm::begin([
                    'id' => 'login-form',
                    'enableClientValidation' => false,
                    'options' => ['autocomplete' => 'off'],
                ]); 
            ?>
                <div class="input-group">
                    <?=$active_form
                        ->field($form, 'code')
                        ->input('text', ['placeholder' => 'Введите код подтверждения', 'maxlength' => 5, 'autofocus' => 'autofocus', 'inputmode' => 'numeric', 'pattern' => '[0-9]*', 'autocomplete' => 'one-time-code'])
                        ->label('Код подтверждения');
                    ?>
                </div>
                <div class="login-recovery">
                    <span class="text-lighten countdown">Отправить еще раз через 00:<span data-countdown><?=$timer; ?></span></span>
                    <a href="<?=Url::to(['handler/handler', 'method' => 'sms']); ?>" class="send d-none">Отправить еще раз</a>
                </div>
                <?=Html::submitButton('Войти', ['class' => 'btn btn-primary login-btn']); ?>
                <div class="divider"><span>или</span></div>
                <?=Html::a('Зарегистрироваться', ['auth/signup'], ['class' => 'btn btn-light login-btn']); ?>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>