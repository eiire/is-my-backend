<?php
$this->title = 'Авторизация';
use yii\widgets\ActiveForm;
use common\widgets\Alert;
use yii\helpers\Html;
use yii\helpers\Url;

$this->registerJsFile('https://www.google.com/recaptcha/api.js');
?>
<div class="login-page">
    <div class="login-container">
        <div class="logo-wrapper">
            <div class="logo"></div>
        </div>
        <div class="login-block">
            <h2 class="text-center light">Вход в аккаунт</h2>
            <?php if ($alert) echo Alert::Widget(['type' => $alert['type'], 'message' => $alert['message']]); ?>
            <?php
                $active_form = ActiveForm::begin([
                    'id' => 'login-form',
                    'enableClientValidation' => false
                ]); 
            ?>
                <div class="input-group">
                    <?=$active_form
                        ->field($form, 'login')
                        ->input('text', ['placeholder' => 'Введите ваш номер', 'data-mask-phone' => true, 'autofocus' => 'autofocus'])
                        ->label('Номер телефона');
                    ?>
                </div>
                <?=Html::submitButton('Войти', ['class' => 'btn btn-primary login-btn']); ?>
            <?php ActiveForm::end(); ?>
            <?php 
                if ($signup) :
                    echo Html::a('Зарегистрироваться', ['auth/signup'], ['class' => 'btn btn-light mt-2 login-btn']);
                endif;
            ?>
            <div class="divider"><span>или</span></div>
            <h3 class="text-center">Войти с помощью</h3>
            <div class="row no-gutters">
                <div class="col pr-2">
                    <?=Html::a('<i class="fab fa-vk"></i> ВКонтакте', ['handler/social', 'type' => 'vk', 'auth' => true], ['class' => 'btn auth-btn auth-vk']); ?>
                </div>
                <div class="col pl-2">
                    <?=Html::a('<i class="fab fa-facebook-f"></i> Facebook', ['handler/social', 'type' => 'fb', 'auth' => true], ['class' => 'btn auth-btn auth-fb']); ?>
                </div>
            </div>
        </div>
    </div>
</div>