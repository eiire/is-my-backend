<?php
$this->title = 'Регистрация';
use yii\widgets\ActiveForm;
use common\widgets\Alert;
use yii\helpers\Html;
use yii\helpers\Url;

$this->registerJsFile('https://www.google.com/recaptcha/api.js');
?>
<div class="login-page">
    <div class="login-container">
        <div class="logo-wrapper">
            <div class="logo"></div>
        </div>
        <div class="login-block">
            <h2 class="text-center light mb-0"><?=$this->title; ?></h2>
            <?php if ($event) : ?><p class="text-center"><?=$event->title; ?></p><?php endif; ?>
            <?php if ($alert) echo Alert::Widget(['type' => $alert['type'], 'message' => $alert['message']]);
                if ($show_form) :
                $active_form = ActiveForm::begin([
                    'id' => 'login-form',
                    'enableClientValidation' => false,
                    'options' => ['autocomplete' => 'off']
                ]);
            ?>
                <div class="input-group">
                    <?=$active_form
                        ->field($form, 'login')
                        ->input('text', ['placeholder' => 'Введите ваш номер', 'data-mask-phone' => true, 'autofocus' => 'autofocus'])
                        ->label('Номер телефона');
                    ?>
                </div>
                <div class="input-group">
                    <?=$active_form
                        ->field($form, 'email')
                        ->input('text', ['placeholder' => 'Введите ваш email'])
                        ->label('Email');
                    ?>
                </div>
                <div class="input-group">
                    <?=$active_form
                        ->field($form, 'name')
                        ->input('text', ['placeholder' => 'Введите ваше ФИО'])
                        ->label('ФИО');
                    ?>
                </div>
                <div class="input-group mb-0">
                    <div class="g-recaptcha" data-sitekey="<?=Yii::$app->params['recaptcha']['public']; ?>"></div>
                </div>
                <label class="text-lighten">Нажимая на кнопку, вы даете согласие на обработку своих <a href="<?=Url::to(['pages/privacy']); ?>" target="_blank">персональных данных</a>.</label>
                <?=Html::submitButton('Зарегистрироваться', ['class' => 'btn btn-primary login-btn']); ?>
                <div class="divider"><span>или</span></div>
                <?=Html::a('Войти', ['auth/signin'], ['class' => 'btn btn-light login-btn']); ?>
            <?php ActiveForm::end(); ?>
            <?php endif; ?>
        </div>
    </div>
</div>