<?php
$this->title = 'Добавить участника';
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use common\widgets\Alert;
?>
<div class="main">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-10 offset-md-1">
                <?=$this->render('@event/views/_partial/head', ['title' => $this->title, 'url' => 'dashboard/index']); ?>
                <div class="block">
                    <?php if ($alert) echo Alert::Widget(['type' => $alert['type'], 'message' => $alert['message'], 'close' => $alert['close']]); ?>
                    <?php
                        $active_form = ActiveForm::begin([
                            'enableClientValidation' => false,
                            'options' => ['autocomplete' => 'off'],
                        ]);
                    ?>
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="input-group">
                                    <?=$active_form
                                        ->field($form, 'event')
                                        ->dropDownList($event, ['required' => true])
                                        ->label('Конференция *');
                                    ?>
                                </div>
                                <div class="input-group">
                                    <?=$active_form
                                        ->field($form, 'phone')
                                        ->input('text', ['required' => true, 'data-mask-phone' => true])
                                        ->label('Номер телефона *');
                                    ?>
                                </div>
                                <div class="input-group">
                                    <?=$active_form
                                        ->field($form, 'email')
                                        ->input('text')
                                        ->label('Email');
                                    ?>
                                </div>
                                <div class="input-group">
                                    <?=$active_form
                                        ->field($form, 'name')
                                        ->input('text')
                                        ->label('Участник');
                                    ?>
                                </div>
                                <div class="input-group">
                                    <?=$active_form
                                        ->field($form, 'lottery')
                                        ->checkbox([
                                            'label' => 'Пользователь участвует в лотерее',
                                            'checked' => true
                                        ]);
                                    ?>
                                </div>
                                <div class="text-right">
                                    <?=Html::submitButton('Добавить', ['class' => 'btn btn-primary']); ?>
                                </div>
                                <small>* &mdash; поля обязательные для заполнения</small>
                            </div>
                        </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>