<?php
$this->title = 'Личный кабинет';
use common\widgets\Alert;
use yii\helpers\Url;
use yii\helpers\Html;
?>
<div class="main">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-10 offset-md-1">
                <?=$this->render('@event/views/_partial/head', ['title' => 'Dashboard', 'url' => 'dashboard/index']); ?>
                <div class="row">
                    <div class="col-12 col-md-6 col-lg-4">
                        <?=$this->render('@common/views/_partial/card', [
                            'title' => 'Добавить участника',
                            'route' => ['dashboard/add-user']
                        ]); ?>
                    </div>
                    <div class="col-12 col-md-6 col-lg-4">
                        <?=$this->render('@common/views/_partial/card', [
                            'title' => 'Участники',
                            'route' => ['dashboard/users']
                        ]); ?>
                    </div>
                    <div class="col-12 col-md-6 col-lg-4">
                        <?=$this->render('@common/views/_partial/card', [
                            'title' => 'Вопросы',
                            'route' => ['dashboard/questions']
                        ]); ?>
                    </div>
                    <div class="col-12 col-md-6 col-lg-4">
                        <?=$this->render('@common/views/_partial/card', [
                            'title' => 'Пользователи онлайн',
                            'route' => ['dashboard/online']
                        ]); ?>
                    </div>
                    <div class="col-12 col-md-6 col-lg-4">
                        <?=$this->render('@common/views/_partial/card', [
                            'title' => 'Лотерея',
                            'route' => ['dashboard/lottery']
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>