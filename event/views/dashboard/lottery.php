<?php
$this->title = 'Лотерея';
$this->registerJsFile('/js/countUp.js', ['position' => $this::POS_HEAD]);
?>
<div class="lottery-body">
<div class="row">
    <div class="container-fluid">
        <div class="row">
            <div class="col-8 offset-2">
                <div class="lottery-block">
                    <div id="number" class="lottery-number"></div>
                </div>
            </div>
            <div class="col-2">
                <div class="lottery-candidate d-none">
                    <?php if ($winners_count[1] || $winners_count[2] || $winners_count[3]) : ?>
                    <div class="lottery-candidate-title">Претендетны</div>
                    <?php endif; ?>
                    <ul class="lottery-candidate-list">
                        <?php if ($winners_count[3]) : ?><li>3 совпадения: <?=$winners_count[3]; ?></li><?php endif; ?>
                        <?php if ($winners_count[2]) : ?><li>2 совпадения: <?=$winners_count[2]; ?></li><?php endif; ?>
                        <?php if ($winners_count[1]) : ?><li>1 совпадение: <?=$winners_count[1]; ?></li><?php endif; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if ($winners_names) : ?>
<div class="lottery-winners">
    <div class="lottery-winners-wrap">
        <div class="lottery-winners-title"><?=$winners_text; ?></div>
        <ul class="lottery-winners-list">
            <?php foreach ($winners_names as $winner) : ?>
            <li><?=$winner['name']; ?></li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>
<?php endif; ?>
<div class="lottery-numbers">
    <ul class="lottery-numbers-list">
        <?php foreach ($numbers as $row) : ?>
        <li><?=$row['number']; ?></li>
        <?php endforeach; ?>
    </ul>
</div>
</div>
<script src="js/jquery.min.js"></script>
<script src="js/countUp.js"></script>
<script>
var counter = new CountUp(document.getElementById('number'), 1, <?=$number; ?>, '', 4);
counter.start(function() {
    $('.lottery-candidate').removeClass('d-none');
    <?php if ($winners_names) : ?>
    $('.lottery-block').css('opacity', 0);
    $('.lottery-winners').css('opacity', 1);
    <?php endif; ?>
});
</script>
