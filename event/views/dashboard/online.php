<?php
$this->title = 'Пользователи онлайн';
use app\widgets\Alert;
use yii\helpers\Url;
use yii\grid\GridView;
?>
<div class="main">
    <div class="container">
        <div class="col-12 col-md-10 offset-md-1">
            <?=$this->render('@event/views/_partial/head', ['title' => $this->title, 'url' => 'dashboard/index']); ?>
            <div class="block">
                <?php if ($response) echo Alert::Widget(['type' => $response['type'], 'message' => $response['message'], 'close' => $response['close']]); ?>
                <h2>Пользователи онлайн <span class="h3 text-lighten" data-balloon="Всего онлайн" data-balloon-pos="up"><?=$result->getCount(); ?></span></h2>
                <?=GridView::widget([
                    'dataProvider' => $result,
                    'columns' => [
                        [
                            'attribute' => 'id',
                            'label' => 'ID',
                            'enableSorting' => false,
                        ],
                        [
                            'attribute' => 'login',
                            'label' => 'Телефон',
                            'enableSorting' => false,
                        ],
                        [
                            'attribute' => 'name',
                            'label' => 'Участник',
                            'enableSorting' => false,
                        ]
                    ],
                    'tableOptions' => [
                        'class' => 'table-striped m-b-20'
                    ],
                    'summary' => false,
                ]); ?>
            </div>
        </div>
    </div>
</div>
