<?php
$this->title = 'Вопросы';
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use common\widgets\Alert;
use common\widgets\Sort;
use yii\helpers\Url;
$this->registerJsFile('/js/dashboard.js');
$url = ($url) ? $url : 'dashboard/questions';
?>
<div class="main">
    <div class="container">
        <div class="col-12 col-md-10 offset-md-1">
            <?php if ($show_form) : ?>
            <?=$this->render('@event/views/_partial/head', ['title' => 'Вопросы', 'url' => 'dashboard/index']); ?>
            <div class="block">
                <?php if ($response) echo Alert::Widget(['type' => $response['type'], 'message' => $response['message'], 'close' => $response['close']]); ?>
                <h2>Вопросы к докладам</h2>
                <?php
                    $active_form = ActiveForm::begin([
                        'method' => 'GET',
                        'enableClientValidation' => false,
                        'options' => ['autocomplete' => 'off', 'enctype' => 'multipart/form-data'],
                    ]);
                ?>
                <div class="row">
                    <div class="col-12 col-md-6">
                        <div class="input-group">
                            <?=$active_form
                                ->field($form, 'event')
                                ->dropDownList($event, ['name' => 'event'])
                                ->label('Конференция');
                            ?>
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="input-group">
                            <?=$active_form
                                ->field($form, 'schedule')
                                ->dropDownList([], ['name' => 'schedule'])
                                ->label('Доклад');
                            ?>
                        </div>
                    </div>
                </div>
                <div class="text-right">
                    <?=Html::submitButton('Продолжить', ['class' => 'btn btn-primary']); ?>
                </div>
                <?php
                    ActiveForm::end();
                    else :
                ?>
                <div class="logo-wrapper">
                    <a href="<?=Url::to([$url]); ?>">
                        <div class="logo"></div>
                    </a>
                </div>
                <?php if (!$hide_wrap) : ?>
                <div class="block">
                    <h2><i class="fas fa-microphone text-primary m-r-5"></i> <?=$schedule['speaker']; ?> &ndash; <?=$schedule['title']; ?></h2>
                    <?php if (!$hide_sort) : ?>
                    <div class="m-b-5 text-right">
                        <strong>Сортировать:</strong>
                        <?=Sort::Widget([
                            'style' => 'sort',
                            'items' => [
                                [
                                    'sort' => '',
                                    'title' => 'По времени'
                                ],
                                [
                                    'sort' => 'like',
                                    'title' => 'По лайкам'
                                ]
                            ]
                        ]); ?>
                    </div>
                    <?php endif; ?>
                    <?php if ($questions) : ?>
                    <?php for ($i = 0; $i < count($questions); $i++) : ?>
                        <div class="item m-b-10">
                            <div class="row m-b-5">
                                <div class="col">
                                    <i class="fas fa-user text-primary"></i> <?=$questions[$i]['author']; ?>
                                </div>
                                <div class="col-auto text-primary">
                                <i class="fas fa-thumbs-up fa-lg"></i> <?=$questions[$i]['like_count']; ?>
                                </div>
                            </div>
                            <p><?=$questions[$i]['question']; ?></p>
                        </div>
                    <?php endfor; ?>
                    <?php else : ?>
                        <p>Вопросов нет</p>
                    <?php endif; ?>
                </div>
                <?php endif; ?>
                <script>
                    setTimeout(function() { window.location.reload() }, 10000);
                </script>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
