<?php
$this->title = 'Участники';
use yii\widgets\ActiveForm;
use yii\widgets\ListView;
use yii\helpers\Html;
use common\widgets\Alert;
use yii\grid\GridView;
?>
<div class="main">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-10 offset-md-1">
                <?=$this->render('@event/views/_partial/head', ['title' => $this->title, 'url' => 'dashboard/index']); ?>
                <div class="block">
                    <?php if ($alert) echo Alert::Widget(['type' => $alert['type'], 'message' => $alert['message'], 'close' => $alert['close']]); ?>
                    <?php
                        $active_form = ActiveForm::begin([
                            'method' => 'get',
                            'enableClientValidation' => false,
                            'options' => ['autocomplete' => 'off']
                        ]);
                    ?>
                    <div class="row no-gutters mt-3">
                        <div class="col-12 col-md-4">
                            <div class="input-group">
                                <?=$active_form
                                    ->field($form, 'event')
                                    ->dropDownList($events, ['name' => 'event'])
                                    ->label(false);
                                ?>
                            </div>
                        </div>
                        <div class="col ml-3">
                            <?=Html::submitButton('Выбрать', ['class' => 'btn btn-primary btn-medium']); ?>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                    <?=GridView::widget([
                            'dataProvider' => $provider,
                        ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>