<?php
use yii\helpers\Url;
if ($exception->statusCode == 404) {
    $this->title = 'Страница не найдена';
    $error = 'Страница не найдена';
} else {
    $this->title = 'Ошибка';
    $error = 'Произошла ошибка';
}
?>
<div class="error-page">
    <div class="error-container">
        <div class="error-code"><?=$exception->statusCode; ?></div>
        <div class="error-desc"><?=$error; ?></div>
        <div class="error-action"><a href="<?=Url::to(['site/index']); ?>">Вернуться на главную</a></div>
    </div>
</div>