<?php
$this->title = 'Избранное - ' . $event['title'];
use common\widgets\Alert;
use yii\helpers\Url;
?>
<div class="main">
    <div class="container">
        <div class="d-block d-lg-none">
            <?=$this->render('@event/views/_partial/sidebar', ['nav' => 'event']); ?>
        </div>
        <div class="row">
            <div class="col-lg-2">
                <?=$this->render('@event/views/_partial/sidebar', ['nav' => 'event']); ?>
            </div>
            <div class="col-12 col-lg-10">
                <div class="block">
                    <h1 class="event-title mb-2">Избранное</h1>
                    <?php if ($alert) echo Alert::Widget(['type' => $alert['type'], 'message' => $alert['message'], 'close' => $alert['close']]); ?>
                    <?php foreach ($schedule as $day_key => $day_value) : ?>
                        <?php if ($days) : ?>
                            <div class="pb-4">
                                <h3 class="mb-1"><i class="far fa-calendar-alt text-primary"></i> <?=$days[$day_key]; ?></h3>
                        <?php endif; ?>
                        <?php foreach ($day_value as $section_key => $section_value) : ?>
                            <div class="h4 mb-2"><i class="fas fa-bullhorn text-primary"></i> <?=$sections[$section_key]; ?></div>
                            <?php foreach ($section_value as $value) : ?>
                                <?=$this->render('@event/views/_item/schedule-' . $value['type'], ['result' => $value, 'tab' => $day_key]); ?>
                            <?php endforeach; // $section_value as $value ?>
                        <?php endforeach; // $day_value['schedule'] as $section_key => $section_value ?>
                        <?php if ($days) : ?></div><?php endif; ?>
                    <?php endforeach; // $schedule as $day_key => $day_value ?>
                </div>
            </div>
        </div>
    </div>
</div>