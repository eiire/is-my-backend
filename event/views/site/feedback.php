<?php
$this->title = 'Анкета - ' . $title;
use common\widgets\Alert;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
?>
<div class="main">
    <div class="container">
        <div class="d-block d-lg-none">
            <?=$this->render('@event/views/_partial/sidebar', ['nav' => 'event']); ?>
        </div>
        <div class="row">
            <div class="col-lg-2">
                <?=$this->render('@event/views/_partial/sidebar', ['nav' => 'event']); ?>
            </div>
            <div class="col-12 col-lg-10">
                <div class="block">
                    <?php if ($alert) echo Alert::Widget(['type' => $alert['type'], 'message' => $alert['message'], 'close' => $alert['close']]); ?>
                    <?php if ($questions) : ?>
                    <h1 class="event-title">Анкета</h1>
                    <?php
                        $active_form = ActiveForm::begin([
                            'enableClientValidation' => false,
                            'options' => ['autocomplete' => 'off'],
                        ]);

                        for ($i = 0; $i < count($questions); $i++) :
                            $field_label = $questions[$i]['question'];
                            $field_id = $questions[$i]['id'];
                    ?>
                    <div class="row">
                        <div class="col-12 col-lg-8">
                            <div class="input-group">
                            <?php
                                switch ($questions[$i]['type']) {
                                    // Input
                                    case 'I':
                                        echo $active_form
                                            ->field($form, 'answer[' . $field_id . ']')
                                            ->input('text')
                                            ->label($field_label);
                                        break;
                                    // Textarea
                                    case 'T':
                                    case 'TEXT':
                                        echo $active_form
                                            ->field($form, 'answer[' . $field_id . ']')
                                            ->textarea(['rows' => 5])
                                            ->label($field_label);
                                        break;
                                    // Checkbox
                                    case 'C':
                                        echo $active_form
                                            ->field($form, 'answer[' . $field_id . ']')
                                            ->checkboxList($questions[$i]['choice'], [
                                                'item' => function ($index, $label, $name, $checked, $value) use ($i) {
                                                    return '<input type="checkbox" id="checkbox-' . $i . $index . '" name="' . $name . '" value="' . $label . '" class="checkbox">
                                                            <label for="checkbox-' . $i . $index . '">' . $label . '</label>';
                                                }
                                            ])
                                            ->label($field_label);
                                        break;
                                    // Radio button
                                    case 'R':
                                        echo $active_form
                                            ->field($form, 'answer[' . $field_id . ']')
                                            ->radioList($questions[$i]['choice'], [
                                                'item' => function ($index, $label, $name, $checked, $value) use ($i) {
                                                    return '<input type="radio" id="radio-' . $i . $index . '" name="' . $name . '" value="' . $label . '" class="radio">
                                                            <label for="radio-' . $i . $index . '">' . $label . '</label>';
                                                }
                                            ])
                                            ->label($field_label);
                                        break;
                                }
                            ?>
                            </div>
                        </div>
                    </div>
                    <?php endfor; ?>
                    <div class="row">
                        <div class="col-12 col-lg-8 text-right">
                            <?=Html::submitButton('Отправить', ['class' => 'btn btn-primary']); ?>
                        </div>
                    </div>
                    <?php
                        ActiveForm::end();
                        else :
                    ?>
                    <div class="text-center">
                        <i class="fas fa-check-circle fa-7x text-success m-t-10"></i>
                        <div class="h3 m-t-10 m-b-5">В данный момент для вас нет активных анкет</div>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>