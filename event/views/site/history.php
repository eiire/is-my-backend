<?php
$this->title = 'История конференций';
use yii\widgets\ActiveForm;
use common\widgets\Alert;
use common\widgets\Sort;
use yii\helpers\Url;
use yii\helpers\Html;
?>
<div class="main">
    <div class="container">
        <div class="d-block d-lg-none">
            <?=$this->render('@event/views/_partial/sidebar', ['nav' => 'event']); ?>
        </div>
        <div class="row">
            <div class="col-lg-2">
                <?=$this->render('@event/views/_partial/sidebar', ['nav' => 'event']); ?>
            </div>
            <div class="col-12 col-lg-10">
                <div class="block">
                <h1>Посещенные конференции</h1>
                <?php if ($alert) echo Alert::Widget(['type' => $alert['type'], 'message' => $alert['message'], 'close' => $alert['close']]); ?>
                <?php foreach ($events as $event) : ?>
                    <div class="item mb-3">
                        <a href="<?=Url::to(['handler/event', 'method' => 'event', 'id' => $event['id']]) ?>" class="h2"><?=$event['title']; ?></a>
                        <div class="mt-1 mb-2 text-lighten"><div class="far fa-calendar-alt text-primary"></div> <?=$event['date']; ?></div>
                    </div>
                <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</div>