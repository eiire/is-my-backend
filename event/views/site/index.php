<?php
$this->title = $event['title'];
use common\widgets\Alert;
use yii\helpers\Url;
?>
<div class="main">
    <div class="container">
        <div class="d-block d-lg-none">
            <?=$this->render('@event/views/_partial/sidebar', ['nav' => 'event']); ?>
        </div>
        <div class="row">
            <div class="col-lg-2">
                <?=$this->render('@event/views/_partial/sidebar', ['nav' => 'event']); ?>
            </div>
            <div class="col-12 col-lg-10">
                <div class="block">
                    <?php if ($alert) echo Alert::Widget(['type' => $alert['type'], 'message' => $alert['message'], 'close' => $alert['close']]); ?>
                    <h1 class="event-title"><?=$event['title']; ?></h1>
                    <div class="row text-lighten">
                        <?php if ($event['location']) : ?>
                        <div class="col-12 col-md-auto">
                            <i class="fas fa-map-marker-alt text-primary m-r-5"></i> <span><?=$event['location']; ?></span>
                        </div>
                        <?php endif; ?>
                        <div class="col-12 col-md-auto">
                            <i class="far fa-calendar-alt text-primary m-r-5"></i> <?=$event['date']; ?>
                        </div>
                    </div>
                    <div class="m-t-10"><?=$event['desc']; ?></div>
                    <?php if ($schedule) : ?>
                    <h2 class="event-title mt-4">Программа конференции</h2>
                    <?php if (count($days) > 1) : ?>
                        <ul class="tab-nav my-2">
                        <?php foreach ($days as $day) : ?>
                            <li class="tab-nav-item<?=$day['active'] ? ' active' : ''; ?>" data-tab="<?=$day['id']; ?>"><?=$day['title'];?></li>
                        <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                    <?php foreach ($schedule as $day_key => $day_value) : // Цикл по дням ?>
                        <div <?=$days[$day_key]['active'] ? '' : ' class="d-none" '; ?>data-tab-content="<?=$days[$day_key]['id']; ?>">
                            <?php if (count($day_value['sections']) > 1) : ?>
                                <div class="row no-gutters">
                                    <div class="col-auto">
                                        <h3>Секции:</h3>
                                    </div>
                                    <div class="col">
                                    <?php foreach ($day_value['sections'] as $day_section) : // Цикл по секциям в дне ?>
                                        <a href="#section-<?=$day_section['id']; ?>" class="btn btn-link m-l-10"><?=$day_section['title']; ?></a>
                                    <?php endforeach; ?>
                                    </div>
                                </div>
                            <?php endif; // count($day_value['sections']) > 1 ?>
                            <?php foreach ($day_value['schedule'] as $section_key => $section_value) : ?>
                                <h3 id="section-<?=$day_value['sections'][$section_key]['id']; ?>" class="mt-3"><i class="fas fa-bullhorn text-primary"></i> <?=$day_value['sections'][$section_key]['title']; ?></h3>
                                <?php foreach ($section_value as $value) : ?>
                                    <?=$this->render('@event/views/_item/schedule-' . $value['type'], ['result' => $value, 'tab' => $day_key]); ?>
                                <?php endforeach; ?>
                            <?php endforeach; // $day_value['schedule'] as $section_key => $section_value ?>
                        </div>
                        <?php endforeach; // $schedule as $day_key => $day_value ?>
                    <?php endif; // $schedule ?>
                    <?=$this->render('@common/views/_partial/rating-form', ['form' => $form_rating, 'title' => 'Оцените доклад']); ?>
                </div>
            </div>
        </div>
    </div>
</div>