<?php
$this->title = 'Вопросы';
use yii\widgets\ActiveForm;
use common\widgets\Alert;
use common\widgets\Sort;
use yii\helpers\Url;
use yii\helpers\Html;
?>
<div class="main">
    <div class="container">
        <div class="d-block d-lg-none">
            <?=$this->render('@event/views/_partial/sidebar', ['nav' => 'event']); ?>
        </div>
        <div class="row">
            <div class="col-lg-2">
                <?=$this->render('@event/views/_partial/sidebar', ['nav' => 'event']); ?>
            </div>
            <div class="col-12 col-lg-10">
                <div class="block">
                    <?php if ($alert) echo Alert::Widget(['type' => $alert['type'], 'message' => $alert['message'], 'close' => $alert['close']]); ?>
                    <h1 class="event-title">Заметки</h1>
                    <h3 class="m-b-15"><?=$schedule['title']; ?></h3>
                    <?php
                        $active_form = ActiveForm::begin([
                            'enableClientValidation' => false,
                            'options' => ['autocomplete' => 'off', 'enctype' => 'multipart/form-data'],
                        ]);
                    ?>
                    <div class="row">
                        <div class="col">
                            <div class="input-group">
                                <?=$active_form
                                    ->field($form, 'note')
                                    ->textarea(['rows' => 10, 'value' => $note])
                                    ->label(false);
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="row m-b-20">
                        <div class="col text-right">
                        <?=Html::submitButton('Сохранить', ['class' => 'btn btn-primary']); ?>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>