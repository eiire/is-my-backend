<?php
$this->title = 'Вопросы';
use yii\widgets\ActiveForm;
use common\widgets\Alert;
use common\widgets\Sort;
use common\widgets\Answer;
use yii\helpers\Url;
use yii\helpers\Html;
?>
<div class="main">
    <div class="container">
        <div class="d-block d-lg-none">
            <?=$this->render('@event/views/_partial/sidebar', ['nav' => 'event']); ?>
        </div>
        <div class="row">
            <div class="col-lg-2">
                <?=$this->render('@event/views/_partial/sidebar', ['nav' => 'event']); ?>
            </div>
            <div class="col-12 col-lg-10">
                <div class="block">
                    <?php if ($alert) echo Alert::Widget(['type' => $alert['type'], 'message' => $alert['message'], 'close' => $alert['close']]); ?>
                    <h1 class="event-title">Вопросы</h1>
                    <h3 class="m-b-15"><?=$schedule['title']; ?></h3>
                    <?php
                        if ($show_form) :
                            $active_form = ActiveForm::begin([
                                'enableClientValidation' => false,
                                'options' => ['autocomplete' => 'off'],
                            ]);
                    ?>
                    <div class="row">
                        <div class="col">
                            <div class="input-group">
                                <?=$active_form
                                    ->field($form, 'question')
                                    ->textarea(['rows' => 6])
                                    ->label('Ваш вопрос');
                                ?>
                                <?=$active_form
                                    ->field($form, 'id')
                                    ->input('hidden', ['value' => $id])
                                    ->label(false);
                                ?>
                                <?=$active_form
                                    ->field($form, 'schedule')
                                    ->input('hidden', ['value' => $schedule_id])
                                    ->label(false);
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="row m-b-20">
                        <div class="col text-right">
                        <?=Html::submitButton('Задать вопрос', ['class' => 'btn btn-primary']); ?>
                        </div>
                    </div>
                    <?php
                            ActiveForm::end();
                        endif;
                    ?>
                    <?php if ($questions) : ?>
                    <div class="row">
                        <div class="col">
                            <h2 id="questions" class="m-b-0">Список вопросов</h2>
                        </div>
                        <div class="col-auto p-t-5">
                            <strong>Сортировать:</strong>
                            <?=Sort::Widget([
                                'style' => 'sort m-b-10',
                                'items' => [
                                    [
                                        'sort' => '',
                                        'title' => 'По времени',
                                    ],
                                    [
                                        'sort' => 'like',
                                        'title' => 'По лайкам',
                                    ]
                                ]
                            ]); ?>
                        </div>
                    </div>
                    <?php for ($i = 0; $i < count($questions); $i++) : ?>
                        <div class="item m-b-10">
                            <div class="row m-b-5">
                                <div class="col">
                                    <i class="fas fa-user text-primary"></i> <?=$questions[$i]['author']; ?>
                                </div>
                                <div class="col-auto text-primary">
                                    <span id="likes"><?=$questions[$i]['like_count']; ?></span>
                                    <span id="liked" <?php if (!$questions[$i]['like']) : ?>class="d-none"<?php endif; ?>><i class="fas fa-thumbs-up fa-lg"></i></span>
                                    <span data-balloon="Мне нравится" <?php if ($questions[$i]['like']) : ?>class="d-none"<?php endif; ?> data-like data-schedule="<?=$schedule_id; ?>" data-question="<?=$questions[$i]['id']; ?>"><i class="far fa-thumbs-up fa-lg"></i></span>
                                </div>
                            </div>
                            <p><?=$questions[$i]['question']; ?></p>
                            <?php if ($questions[$i]['answer']) : ?>
                            <strong>Ответ спикера:</strong>
                            <p><?=$questions[$i]['answer']; ?></p>
                            <?php endif; ?>
                            <?php if (!$questions[$i]['answer'] && $questions[$i]['can_answer']) : ?>
                            <button data-answer data-fancybox data-src="#answer-modal" data-question="<?=$questions[$i]['question']; ?>" data-id="<?=$questions[$i]['id']; ?>" class="btn btn-link m-b-5">Ответить</button>
                            <?=$this->render('@event/views/_partial/answer-form', ['id' => 'answer-modal', 'form' => $form_answer]); ?>
                            <?php endif; ?>
                        </div>
                    <?php
                            endfor;
                        else:
                    ?>
                        <p>Нет вопросов</p>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="btn-fixed" data-page-update><i class="fas fa-redo-alt"></i></div>
</div>