<?php
$this->title = 'Оценки';
?>
<div class="main">
    <div class="container">
        <div class="d-block d-lg-none">
            <?=$this->render('@event/views/_partial/sidebar', ['nav' => 'event']); ?>
        </div>
        <div class="row">
            <div class="col-lg-2">
                <?=$this->render('@event/views/_partial/sidebar', ['nav' => 'event']); ?>
            </div>
            <div class="col-12 col-lg-10">
                <div class="block">
                    <h1 class="event-title">Оценки</h1>
                    <div class="row">
                        <div class="col">
                            <p class="m-b-0">Средняя оценка: <?=$average; ?></p>
                            <p>Всего оценок: <?=$count; ?></p>
                        </div>
                        <div class="col-auto">
                            <ul class="tab-nav p-t-10">
                                <li class="tab-nav-item active" data-tab="all">Все</li>
                                <li class="tab-nav-item" data-tab="message">Только с комментариями</li>
                            </ul>
                        </div>
                    </div>
                    <?php
                        foreach ($rating as $row) :
                            echo $this->render('@common/views/_partial/rating', ['result' => $row]);
                        endforeach;
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>