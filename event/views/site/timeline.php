<?php
$this->title = $event['title'];
use common\widgets\Alert;
use yii\helpers\Url;
?>
<div class="main">
    <div class="container">
        <div class="d-block d-lg-none">
            <?=$this->render('@event/views/_partial/sidebar', ['nav' => 'event']); ?>
        </div>
        <div class="row">
            <div class="col-lg-2">
                <?=$this->render('@event/views/_partial/sidebar', ['nav' => 'event']); ?>
            </div>
            <div class="col-12 col-lg-10">
                <div class="block">
                    <h1 class="mb-3">Будущие доклады</h1>
                    <?php 
                        if ($schedule) :
                        foreach ($schedule as $time => $items) :
                    ?>
                        <div class="h3 mb-2">
                            <i class="far fa-clock text-primary"></i> <?=$time; ?>
                        </div>
                    <?php
                            foreach ($items as $item) {
                                echo $this->render('@event/views/_item/timeline', ['result' => $item]);
                            }
                        endforeach;
                        else:
                    ?>
                    <p>Сегодня нет докладов</p>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>