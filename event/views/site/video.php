<?php
$this->title = 'Видео - ' . $details->title;
use common\widgets\Alert;
?>
<div class="main">
    <div class="container">
        <div class="d-block d-lg-none">
            <?=$this->render('@event/views/_partial/sidebar', ['nav' => 'event']); ?>
        </div>
        <div class="row">
            <div class="col-lg-2">
                <?=$this->render('@event/views/_partial/sidebar', ['nav' => 'event']); ?>
            </div>
            <div class="col-12 col-lg-10">
                <div class="block">
                    <?php if ($alert) echo Alert::Widget(['type' => $alert['type'], 'message' => $alert['message'], 'close' => $alert['close']]); ?>
                    <h1 class="event-title">Видео</h1>
                    <h3 class="m-b-15"><?=$schedule['speaker']; ?> &ndash; <?=$schedule['title']; ?></h3>
                    <div class="text-center m-t-10">
                        <video src="<?=$video; ?>" controls></video>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>