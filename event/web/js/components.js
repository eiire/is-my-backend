new Vue({
    el: '#phone-confirm',
    data: {
        resendBtnVisible: false,
        phoneBtnDisabled: true,
        codeBtnDisabled: true,
        timerVisible: false,
        currentPhone: null,
        inProgress: false,
        interval: null,
        response: null,
        status: null,
        enable: null,
        phone: null,
        class: null,
        code: null,
        time: 59,
        max: 5,
    },
    mounted: function() {
        this.currentPhone = this.$el.attributes.phone.value;
        this.phone = this.currentPhone;
        this.enable = this.$el.attributes.enable.value;
    },
    methods: {
        phoneConfirm: function() {
            this.inProgress = true;
            this.timer();
            this.sendCode();
        },
        codeConfirm: function() {
            this.codeBtn = true;
            this.checkCode();
        },
        resendCode: function() {
            this.timer();
            this.sendCode();
        },
        checkCode: async function() {
            if (this.code != this.currentPhone) {
                return await fetch('http://my.local/handler/auth?method=check-code&code=' + this.code)
                    .then(response => response.json())
                    .then(response => {
                        if (response.success == true) {
                            this.status = 'Ваш номер подтвержден';
                            this.resendBtnVisible = false;
                            this.timerVisible = false;
                            this.inProgress = false;
                            this.time = 59;
                        } else {
                            this.status = 'Код введен неверно';
                        }
                    });
            } else {
                return false;
            }
        },
        sendCode: async function() {
            if (this.phone) {
                return await fetch('http://my.local/handler/auth?method=send-code&phone=' + this.phone)
                    .then(response => response.json())
                    .then(response => {
                        if (response.success == true) {
                            this.status = 'Код отправлен';
                            this.time = 59;
                            this.timerVisible = true;
                            this.resendBtnVisible = false;
                        } else {
                            this.status = 'Произошла ошибка при отправке кода';
                        }
                    })
            }
            return false;
        },
        timer: function() {
            this.interval = setTimeout(() => {
                this.timerVisible = true;
                if (this.time > 0) {
                    this.time -=1;
                    if (this.time < 10) this.time = '0' + parseInt(this.time);
                    this.timer();
                } else {
                    this.timerVisible = false;
                    this.resendBtnVisible = true;
                }
            }, 1000);
        }
    },
    computed: {
        phoneLength: function() {
          if (this.phone && this.phone.length == 10 && this.phone != this.currentPhone) return true;
          return false;
        },
        codeLength: function() {
            if (this.code && this.code.length == this.max) return true;
            return false;
        },
        
    },
    watch: {
        phone() {
            console.log(this);
            this.phoneBtnDisabled = !this.phoneLength;
        },
        code() {
            this.codeBtnDisabled = !this.codeLength;
        }
    }
})