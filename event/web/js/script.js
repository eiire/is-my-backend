$(document).ready(function() {
    $('.navbar-btn').click(function() {
        var left = ($('.sidebar').position().left != 0) ? 0 : -250;
        $('.sidebar').css('left', left + 'px');
    });

    $(window).resize(function() {
        if ($(window).width() > 768) $('.sidebar').removeAttr('style');
    });

    if ($(window).width() < 768) {
        $('.nav-list-wrapper').click(function() {
            $('.nav-list').css('visibility', 'hidden');
            $('.nav-list').css('opacity', '0');
            var list = $(this).next();
            if (list.css('visibility') == 'hidden') {
                list.css('visibility', 'visible');
                list.css('opacity', '1');
            } else {
                list.css('visibility', 'hidden');
                list.css('opacity', '0');
            }
        });
    }

    $('.alert-close').click(function() {
        $(this).parent().hide();
    });

    $('.collapse-header').click(function() {
        $('.collapse-body').slideUp();
        if ($(this).next().is(':hidden')) {
            $(this).next().slideToggle();
        }
    });

    $('.file-upload input[type=file]').change(function() {
        var filename = $(this).val().replace(/.*\\/, '');
        $('.file-upload input[type=text]').val(filename);
    });

    $('.item-more').click(function() {
        $('.item-details').slideUp();
        $('.item-more').html('<i class="fas fa-chevron-down"></i>');
        if ($(this).prev().is(':hidden')) {
            $(this).prev().slideToggle();
            $(this).html('<i class="fas fa-chevron-up"></i>');
        }
    });

    $('.schedule-item').click(function() {
        if (!$(this).hasClass('schedule-disable')) {
            $('.schedule-item').removeClass('schedule-active');
            $(this).addClass('schedule-active');
            $('#specialisttimeform-guid').val($(this).data('guid'));
            $('#specialisttimeform-hour').val($(this).data('hour'));
            $('#specialisttimeform-date').val($(this).data('date'));
            $('#specialisttimeform-btn').removeAttr('disabled');
        }
    });

    $('[data-rating-id]').click(function() {
        $('#ratingform-number').val($(this).data('rating-id'));
        $('#ratingform-date').val($(this).data('rating-date'));
    });

    $('[data-rating-close]').click(function() {
        $('#rating-message').css('visibility', 'hidden');
        $('[name=rating]').prop('checked', false);
        $('#ratingform-message').val('');
        $('#ratingform-rating').val('');
        $.fancybox.close();
    });

    function ratingControlState(disabled) {
        var visibility = (disabled == true) ? 'visible' : 'hidden';
        $('#rating-btn').prop('disabled', disabled);
        $('#rating-message').css('visibility', visibility);
    }

    $('.rating input').on('change', function() {
        console.log('change');
        $('#ratingform-rating').val($(this).val());
        var rating = $('#ratingform-rating').val();
        var message = $('#ratingform-message').val();
        if (rating == 5) {
            ratingControlState(false);
        } else if (message.length < 5) {
            ratingControlState(true);
        } else {
            ratingControlState(false);
        }
    });

    $('#ratingform-message').keyup(function() {
        var rating = $('#ratingform-rating').val();
        var message = $(this).val();
        if (rating != 5 && message.length < 5) {
            ratingControlState(true);
        } else {
            ratingControlState(false)
        }
    });

    $('[data-favorite]').click(function() {
        if ($(this).data('favorite') == 'yes') {
            $(this).html('<i class="far fa-star fa-lg"></i>');
            $(this).data('favorite', 'no');
            $(this).attr('data-balloon', 'Добавить в избранное');
        } else {
            $(this).html('<i class="fas fa-star fa-lg"></i>');
            $(this).data('favorite', 'yes');
            $(this).attr('data-balloon', 'Удалить из избранного');
        }
        $.ajax({
            type: 'GET',
            url: '/handler/event',
            data: {
                method: 'schedule-favorite',
                event: $(this).data('event'),
                schedule: $(this).data('schedule')
            },
            success: function(data) {
                var value = (data == 'add') ? 'yes' : 'no';
                $(this).data('favorite', value);
            }
        })
    });

    $('[data-like]').click(function() {
        var icon = $(this);
        var new_icon = icon.prev('#liked');
        var likes = new_icon.prev('#likes');
        var likes_count = parseInt(likes.html()) + 1;
        likes.html(likes_count);
        icon.hide();
        new_icon.removeClass('d-none');
        $.ajax({
            type: 'GET',
            url: '/handler/event',
            data: {
                method: 'question-like',
                schedule: icon.data('schedule'),
                question: icon.data('question')
            }
        })
    });

    if ($('[data-countdown]').length) {
        var interval = setInterval(function() {
            value = $('[data-countdown]').text();
            value -= 1;
            if (value < 10) value = '0' + parseInt(value);
            $('[data-countdown]').text(value);
            if (value == '00') {
                clearInterval(interval);
                $('.countdown').addClass('d-none');
                $('.send').removeClass('d-none');
            }
        }, 1000);
    }

    $('.lottery-item').click(function() {
        if ($(this).hasClass('lottery-item-active')) {
            $(this).removeClass('lottery-item-active');
        } else {
            $(this).addClass('lottery-item-active');
        }
    });

    $('[data-answer]').click(function() {
        $('#questionsanswerform-question').text($(this).data('question'));
        $('#questionsanswerform-id').val($(this).data('id'));
    });

    $('[data-tab]').click(function() {
        console.log('click');
        $('[data-tab]').removeClass('active');
        $(this).addClass('active');
        if ($(this).data('tab') != 'all') {
            $('[data-tab-content]').addClass('d-none');
            $('[data-tab-content=' + $(this).data('tab') + ']').removeClass('d-none');
        } else {
            $('[data-tab-content]').removeClass('d-none');
        }
    });

    $('[data-subscribe]').click(function() {
        $('[data-subscribe]').addClass('btn-success');
        $('[data-subscribe]').attr('disabled', true);
        $('[data-subscribe]').text('Вы подписаны');
        $.ajax({
            type: 'GET',
            url: '/handler/event',
            data: {
                method: 'subscribe',
            }
        })
    });

    $('[data-page-update]').click(function() {
        document.location.reload();
    });

    $('[data-period]').datepicker();

    var periodStart = $('[data-period-start]').datepicker({
        endDate: new Date()
    });

    var periodEnd = $('[data-period-end]').datepicker({
        endDate: new Date()
    });

    periodStart.on('pick.datepicker', function () {
        if (periodStart.datepicker('getDate') > periodEnd.datepicker('getDate')) {
            periodEnd.datepicker('setDate', periodStart.datepicker('getDate'));
        }
        periodEnd.datepicker('setStartDate', periodStart.datepicker('getDate'));
    });

    periodEnd.on('pick.datepicker', function () {
        if (periodEnd.datepicker('getDate') < periodStart.datepicker('getDate')) {
            periodStart.datepicker('setDate', periodEnd.datepicker('getDate'));
        }
        periodStart.datepicker('setEndDate', periodEnd.datepicker('getDate'));
    });

    $('[data-mask-phone]').inputmask('+7 (999) 999-9999');
    $('[data-mask-email]').inputmask({alias:'email'});
});