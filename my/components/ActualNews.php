<?php
namespace my\components;

use Yii;
use my\models\news\News;
use my\models\news\NewsViewed;

class ActualNews {
    private $user_id;
    private $viewed;

    public function __construct($user_id) {
        $this->user_id = $user_id;
    }

    public function getViewedNews() {
        $viewed = NewsViewed::find()
            ->select('news_id')
            ->where(['user_id' => $this->user_id])
            ->all();

        $viewed_ids = [];
        foreach ($viewed as $v) {
            $viewed_ids[] = $v->news_id;
        }

        return $viewed_ids;
    }

    public function getNews() {
        $viewed_ids = $this->getViewedNews();

        $news = News::find()
            ->where(['active' => 1])
            ->andWhere(['not in', 'id', $viewed_ids])
            ->asArray()
            ->all();

        return ($news) ? $news : false;
    }
}