<?php
namespace my\components;

use Yii;
use yii\base\Model;
use common\models\Request;

class Client extends Model {
    public $id;

    const SC = 'http://sc/sc/hs/sitein/';
    const UPP = 'http://server1c/upp_is/hs/sitein/';
    const UPP_IN = 'http://server1c/upp_is/hs/sitein/in/';
    const UA = 'http://server1c/UA/hs/exchange/';

    /**
     * Информация о клиенте
     */
    const CLIENT = 'info_contr';

    /**
     * Список всех групп компаний
     */
    const UNIQ_GROUP_COMPANY_NAMES = 'list_group';

    /**
     * Создание группы компании
     */
    const CREATE_COMPANY_GROUP = 'insert_group';

    /**
     * Переименование группы компании
     */
    const RENAME_COMPANY_GROUP = 'rename_group';

    /**
     * Изменение количества сессий в группах
     */
    const CHANGE_SESSIONS_COMPANY_GROUP = 'sessions_change';

    /**
     * Создание группы компании
     */
    const MOVE_DB = 'move_db_in_group';

    /**
     * Основная информация о базе
     */
    const INFO = 'info_db';

    /**
     * Список баз пользователя
     */
    const LIST = 'list_db';

    /**
     * История активности
     */
    const HISTORY = 'log_history';

    /**
     * Количестов входов в базу за месяц
     */
    const COUNT = 'day_history';

    /**
     * История обновлений
     */
    const UPDATE = 'update_history';

    /**
     * История бэкапов
     */
    const BACKUP = 'info_backup';

    /**
     * Активные сеансы
     */
    const SESSION = 'active_user';

    /**
     * Удаление сеанса
     */
    const REMOVE_SESSION = 'kill_active_user';

    /**
     * Создать бэкап
     */
    const CREATE_BACKUP = 'create_backup';

    /**
     * Удалить бэкап
     */
    const DELETE_BACKUP = 'delete_backup';

    /**
     * Обращения в ЛК
     */
    const CONS = 'list_cons';

    /**
     * Список конфигураций клиента
     */
    const SOFT = 'list_soft';

    /**
     * Новая заявка на линию консультации
     */
    const NEW_CONS = 'ins_cons';

    /**
     * Список счетов для оплаты
     */
    const PAYMENT = 'list_account';

    /**
     * Получить счет
     */
    const GET_ACCOUNT = 'print_account';

    /**
     * Сервисы ИТС
     */
    const SERVICE = 'list_ss';

    /**
     * Заявка на сервис ИТС
     */
    const NEW_SERVICE = 'ins_ss';

    /**
     * Список ПО и работ для заказа специалиста
     */
    const PLANNING = 'planning';

    /**
     * Заказ специалиста
     */
    const INS_PLANNING = 'ins_plan';

    /**
     * Специалисты для срочного заказа
     */
    const FORCE_PLANNING = 'info_duty_till';

    /**
     * Срочный заказ специалиста
     */
    const FORCE_INS_PLANNING = 'ins_plan_kassa';

    /**
     * Получить список документов
     */
    const DOCS = 'get_luvr';

    /**
     * Подписать документ
     */
    const DOC_SIGNATURE = 'sign_luvr';

    /**
     * Рейтинг
     */
    const RATING = 'ins_ocenka';

    /**
     * Информация о балансе пользователя обалка
     */
    const BUDGET = 'fin_oper';

    /**
     * Изменяет количество сессий
     */
    const SESSIONS_CHANGE = 'sessions_change';

    /**
     * Заморозить базу
     */
    const FROZEN = 'sessions_change';

    /**
     * Список актов
     */
    const ACT = 'list_act';

    /**
     * Скачать акт
     */
    const GET_ACT = 'print_act';

    /**
     * Скачать приложение
     */
    const GET_APPENDIX = 'print_trans_contr';

    /**
     * Получить список email'ов для уведомлений
     */
    const NOTICE_EMAILS = 'list_email';

    /**
     * Добавить email для уведомления
     */
    const ADD_NOTICE_EMAIL = 'insert_email';

    /**
     * Удалить email для уведомления
     */
    const DEL_NOTICE_EMAIL = 'delete_email';

    /**
     * Возвращает сумму по номеру заказа
     */
    const GET_SUM_BY_ORDER_NUM = 'getSumByOrderNum';

    /**
     * Получить данные для оценки работ
     */
    const DETAILS_JOB_RATING = 'list_for_assessment';

    /**
     * Создать базу
     */
    const CREATE_DB = 'create_db';

    /**
     * Удалить базу
     */
    const DELETE_DB = 'delete_db';

    /**
     * Получить список торгового оборудования
     */
    const LIST_REQUEST_TO = 'list_request_to';

    /**
     * Добавить новую заявку на торговое оборудование
     */
    const INS_REQUEST_TO = 'ins_request_to';


    /**
     * Далее сегменты рынка
     */

    /**
     * Бюджет
     */
    const SEGMENT_BUDGET = 'Бюджет';

    /**
     * КОРП
     */
    const SEGMENT_CORP = 'КОРП';

     /**
     * Средний бизнес
     */
    const SEGMENT_MEDIUM = 'СБ';

     /**
     * Малый бизнес
     */
    const SEGMENT_SMALL = 'МБ';

    /**
     * Ищет ошибки при запросе к HTTP сервису и отправляет сообщение на email
     * 
     * @param string $response отет HTTP сервиса
     * @param string $method метод к которому отправлен запрос
     * @param string $url URL на который отправлен запрос
     * @param string $type Тип запроса (GET|POST)
     * 
     * @return bool
     */
    private function checkErrors($response, $method, $url, $type) {
        $errors = ['HTTPСервис', 'ОбщийМодуль'];
        if (str_replace($errors, '', $response) != $response) {
            $message = 'Ошибка в HTTP сервисе: ' . $response . "\n";
            $message .= 'Метод: ' . $method . "\n";
            $message .= 'URL запроса: ' . $url . "\n";
            $message .= 'Тип запроса: ' . $type . "\n";
            Yii::error($message, 'mail');
            return true;
        }
        return false;
    }

    /**
     * Отправка запроса к серверу
     * 
     * @param string $method метод
     * @param array $params параметры
     * @param bool $json декодировать json
     * 
     * @return object
     */
    public function sendRequest($url, $method = false, $params = [], $json = true) {
        $url = $url . urlencode($this->id);
        if ($method) $url .= '/' . $method;
        if ($params) {
            $url .= '?';
            $url .= http_build_query($params);
        }

        $request = new Request($url);
        $request->execute();
        $result = $request->getResponse();

        $this->checkErrors($result, $method, $url, 'GET');
        if ($json) $result = json_decode($result);
        return $result;
    }

    /**
     * Отправка POST-запроса к серверу
     * 
     * @param string $method метод
     * @param array $params параметры
     * @param bool $json декодировать json
     * 
     * @return object
     */
    public function sendPostRequest($url, $method = false, $params = [], $json = true) {
        $query_url = $url . urlencode($this->id);
        if ($method) $query_url .= '/' . $method . '/?';
        $query_data = $params;


        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_POST => 1,
            CURLOPT_HEADER => 0,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_SSL_VERIFYPEER => 1,
            CURLOPT_URL => $query_url,
            CURLOPT_POSTFIELDS => $query_data,
        ]);
        $result = curl_exec($curl);
        curl_close($curl);

        $this->checkErrors($result, $method, $url, 'POST');

        if ($json) $result = json_decode($result);

        return $result;
    }

    /**
     * Проверяет является ли бэкап пользовательским
     * 
     * @param string $backup имя бэкапа
     * 
     * @return bool
     */
    public function userBackup($backup) {
        if (count(explode('-', $backup)) > 1) return true;
        return false;
    }

    /**
     * Возвращает кол-во пользовательких бэкапов в базе
     * 
     * @param string $db имя базы
     * 
     * @return bool|int
     */
    public function backupCount($db) {
        $result = $this->sendRequest(self::SC, self::BACKUP, [
            'name_db' => $db
        ]);

        if ($result->ErrorDescription) return false;

        $count = 0;
        foreach ($result->InfoDB as $row) {
            if ($this->userBackup($row->name)) $count++;
        }

        return $count;
    }

    /** Вернуть инфомрмацию о базе
     * 
     * @param string $db имя базы
     * 
     * @return object|bool
     */
    public function baseDetails($db) {
        $bases = $this->sendRequest(Client::SC, Client::LIST);
        foreach ($bases->ListDB as $base) {
            if ($base->db == $db) return $base;
        }
        return false;
    }

    /**
     * Существует ли база у этого пользователя
     * 
     * @param string $db имя базы
     * 
     * @return bool
     */
    public function baseExist($db) {
        $bases = $this->sendRequest(Client::SC, Client::LIST);
        foreach ($bases->ListDB as $base) {
            if ($base->db == $db) return true;
        }
        return false;
    }

    /**
     * Существуют ли базы в облаке
     * 
     * @return bool
     */
    public function cloudUser() {
        $bases = $this->sendRequest(self::SC, self::LIST);
        if (!$bases || $bases->ErrorDescription || !$bases->ListDB) return false;
        return true;
    }
}