<?php
namespace my\components;

use Yii;
use yii\helpers\Url;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\base\Security;
use yii\base\Model;
use my\models\users\Users;
use my\models\users\UsersRecovery;
use my\models\users\UsersHistory;
use my\models\users\UsersBlock;
use my\models\users\UsersConfirm;
use my\models\users\UsersSession;
use my\models\users\UsersAction;
use my\models\users\UsersAuth;

class User extends Model {
    public $login;
    public $password;
    protected $user;
    protected $session;

    public function init() {
        $token = Yii::$app->session->get('access');

        if ($token) {
            $this->session = UsersSession::find()
                ->select(['id', 'user_id'])
                ->where(['token' => $token])
                ->one();

            $users = Users::find()
                ->select(['users.*', 'users_code.*'])
                ->leftJoin('users_code', 'users.id = users_code.user_id')
                ->where(['users.id' => $this->session->user_id, 'users_code.active' => 1])
                ->one();
        }

        $this->user = ($token && $users) ? $users : false;
        parent::init();
    }

    /**
     * Получить id текущего пользователя
     * 
     * @return int|bool
     */
    public function getID() {
        return $this->session->user_id;
    }

    /**
     * Получить логин текущего пользователя
     * 
     * @return string
     */
    public function getLogin() {
        return $this->user->login;
    }

    /**
     * Получить id пользователя по логину
     * 
     * @return int|bool
     */
    public function getIdByLogin() {
        $query = Users::find()
            ->select('id')
            ->where(['login' => $this->login])
            ->one();

        if (!$query) return false;
        return $query->id;
    }

    /**
     * Получить token текущего пользователя
     * 
     * @return string
     */
    public function getToken() {
        return Yii::$app->session->get('access');
    }

    /**
     * Получить email текущего пользователя
     * 
     * @return string
     */
    public function getEmail() {
        return $this->user->login;
    }

    /**
     * Получить email пользователя по коду восстановления пароля
     * 
     * @return string|bool
     */
    public function getEmailByCode($code) {
        $code = trim($code);

        $query = UsersRecovery::find()
            ->select('user_id')
            ->where(['code' => $code])
            ->one();

        $query = Users::find()
            ->select('login')
            ->where(['id' => $query->user_id])
            ->one();

        if (!$query) return false;
        return $query->login;
    }

    /**
     * Получить код
     * 
     * @return string
     */
    public function getCode() {
        return $this->user->code;
    }

    /**
     * Получить телефон
     * 
     * @return string
     */
    public function getContactPhone() {
        return $this->user->contact_phone;
    }

    /**
     * Получить контактное лицо
     * 
     * @return string
     */
    public function getContactPerson() {
        return $this->user->contact_person;
    }

    /**
     * Получить название/имя пользователя
     * 
     * @return string
     */
    public function getName() {
        return $this->user->name;
    }

    /**
     * Получить ИНН
     * 
     * @return string
     */
    public function getInn() {
        return $this->user->inn;
    }

    /**
     * Получить сегмент рынка
     * 
     * @return string
     */
    public function getSegment() {
        return $this->user->segment;
    }

    /**
     * Получить информацию о наличии кассы
     * 
     * @return bool
     */
    public function getCashbox() {
        return (bool) $this->user->cashbox;
    }

    /**
     * Получить id сессии
     * 
     * @return int
     */
    public function getSessionID() {
        return $this->session->id;
    }

    /**
     * Получить роль пользователя
     * 
     * @return int
     */
    public function getRole() {
        return $this->user->role;
    }

    /**
     * Подтвержден ли пользователь
     * 
     * @return bool
     */
    public function getConfirm() {
        return (bool) $this->user->confirm;
    }

    /**
     * Получить телефон двухфакторной аутентификации
     * 
     * @return string
     */
    public function getPhone() {
        return $this->user->phone;
    }

    /**
     * Генерация пароля
     * 
     * @return string
     */
    public function passwordGenerate($password = NULL) {
        if (!$password) $password = $this->password;
        $password = hash('sha256', Yii::$app->params['salt'] . $password);
        return $password;
    }

    /**
     * Валидация пароля
     * Длина от 8 до 25, заглавная буква, цифра или символ
     * 
     * @return bool
     */
    public function passwordValidate() {
        $password = $this->password;
        $length = strlen($password);

        if ($length < 8 || $length > 25) {
            return false;
        } elseif (preg_match('/[A-Z0-9!@#$%&*]/', $password)) { // Заглавная буква, цифра или символы
            return true;
        }

        return false;
    }

    /**
     * Запрос на восстановление пароля
     * 
     * @return bool
     */
    public function passwordRecovery() {
        $user_id = $this->getIdByLogin();
        $code = (new Security())->generateRandomString();

        UsersRecovery::deleteAll(['user_id' => $user_id]);

        $date = new \DateTime('+1 day');

        (new Query())->createCommand()
            ->insert('users_recovery', [
                'user_id' => $user_id,
                'code' => $code,
                'expire' => $date->format('Y-m-d H:i:s')
            ])
            ->execute();

        return $code;
    }

    /**
     * Изменяет пароль для пользователя
     * 
     * @param bool $delete удалить запись о восстановлении пароля
     * 
     * @return bool
     */
    public function passwordChange($delete = true) {
        $password = $this->passwordGenerate();

        $query = (new Query())->createCommand()
            ->update('users', ['password' => $password], ['login' => $this->login])
            ->execute();

        if ($delete) {
            $query = (new Query())->createCommand()
                ->delete('users_recovery', ['user_id' => $this->getIdByLogin()])
                ->execute();
        }
            
        if (!$query) return false;
        return true;
    }

    /**
     * Проверить наличие пользователя с таким логином и паролем
     * 
     * @param string $password пароль
     * 
     * @return bool
     */
    public function passwordExist($password) {
        $password = $this->passwordGenerate($password);

        $count = Users::find()
            ->where(['login' => $this->login, 'password' => $password])
            ->count();

        if ($count == 0) return false;
        return true;
    }

    /**
     * Проверка авторизации
     *
     * @param string $action - текущее представление
     * @param array $exception - представления, для которых нужно исключить проверку прав доступа (доступ к ним будет постоянно разрешен)
     *
     * @return bool
     */
    public function haveAccess($action = NULL, $exception = []) {
        if (!in_array($action, $exception)) {
            if ($this->clearSessions()) {
                Yii::$app->session->setFlash('session_remove', 'true');
                return false;
            }

            $token = $this->getToken();
            if (!$token || !$this->user) return false;

            (new Query())->createCommand()
                ->update('users_session',
                    ['date' => date('Y-m-d H:i:s')],
                    ['token' => $token]
                )
                ->execute();
        }

        return true;
    }

    /**
     * Проверяет права доступа у текущего пользователя
     * 0 - Пользователь без возможности активных действий
     * 1 - Пользователь с возможностью активных действий
     * 
     * @param int|array минимальные права доступа
     * 
     * @return bool
     */
    public function permission($role) {
        if (is_array($role)) return in_array($this->getRole(), $role);
        if ($this->getRole() == $role) return true;
        return false;
    }

    /**
     * Удаляет устаревшие сессии
     * Возварщает true, если была удалена сессия текущего пользователя
     * 
     * @return bool
     */
    public function clearSessions() {
        $date = new \DateTime('-30 minutes');

        $query = UsersSession::find()
            ->where(['token' => $this->getToken()])
            ->andWhere(['<', 'date', $date->format('Y-m-d H:i:s')])
            ->one();

        UsersSession::deleteAll(['<', 'date', $date->format('Y-m-d H:i:s')]);

        return $query;
    }

    /**
     * Очищает сессии текущего пользователя
     * 
     * @return bool
     */
    public function clearUserSession() {
        $user_id = $this->getID();
        $token = $this->getToken();

        $count = UsersSession::find()
            ->where(['user_id' => $user_id])
            ->count();

        if ($count == 1) return true;

        $query = UsersSession::deleteAll('user_id = :user_id AND token <> :token', [':user_id' => $user_id, ':token' => $token]);
        return $query;
    }

    /**
     * Авторизация пользователя
     *
     * @param bool $check_pass - проверять ли введенный пароль
     *
     * @return bool
     */
    public function signIn() {
        $query = Users::find()
            ->select('id')
            ->where([
                'login' => $this->login,
                'password' => $this->passwordGenerate(),
                'confirm' => 1
            ])
            ->one();

        if (!$query) return false;

        $session = $this->createSession($query->id);

        return ($session) ? true : false;
    }

    /**
     * Создает сессию пользователя
     * 
     * @param int $user_id - id пользователя
     * 
     * @return bool
     */
    public function createSession($user_id) {
        $token = (new Security())->generateRandomString();

        $result = (new Query())->createCommand()
            ->insert('users_session', [
                'user_id' => (int) $user_id,
                'token' => $token
            ])
            ->execute();

        Yii::$app->session->set('access', $token);
        return $result;
    }

    /**
     * Выход пользователя из профиля
     * 
     * @return bool
     */
    public function logOut() {
        UsersSession::deleteAll(['token' => $this->getToken()]);
        Yii::$app->session->set('access', NULL);
        return true;
    }

    /**
     * Существует ли пользователь с таким логином
     *
     * @return bool
     */
    public function userExist() {
        $count = Users::find()
            ->where(['login' => $this->login])
            ->count();

        if ($count == 1) return true;
        return false;
    }

    /**
     * Валидация email'a
     * 
     * @return bool
     */
    public function emailValidate($email) {
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) return true;
        return false;
    }

    /**
     * Валидация номера телефона
     * 
     * @return bool
     */
    public function phoneValidate($phone, $length = 10) {
        $str = str_replace([' ', '-', '(', ')', '+7', '+'], '', $phone);
        $nums = preg_replace("/[^0-9]/", '', $str);
        return (strlen($nums) == strlen($str) && strlen($nums) == $length) ? true : false;
    }

    /**
     * Возвращает кол-во неудачных авторизаций за последние 5 минут
     * 
     * @return int
     */
    public function errorSignInCount() {
        $date = new \DateTime('-5 minutes');

        $count = UsersHistory::find()
            ->where(['ip' => $this->getIp(), 'success' => 0])
            ->andWhere(['>', 'date', $date->format('Y-m-d H:i:s')])
            ->count();

        return $count;
    }

    /**
     * Блокировка пользователя на 2 недели
     * 
     * @return bool
     */
    public function block() {
        $date = new \DateTime('+2 week');

        $query = (new Query())->createCommand()
            ->insert('users_block', [
                'ip' => $this->getIp(),
                'expire' => $date->format('Y-m-d H:i:s')
            ])
            ->execute();

        return $query;
    }

    /**
     * Проверяет, заблокирован пользователь или нет
     *
     * @return bool
    */
    public function blocked() {
        UsersBlock::deleteAll(['<', 'expire', date('Y-m-d H:i:s')]);

        $count = UsersBlock::find()
            ->where(['ip' => $this->getIp()])
            ->count();

        if ($count == 1) return true;
        return false;
    }

    /**
     * Сохранить историю входа
     * 
     * @param array $params Параметры входа
     * 
     * @return bool
     */
    public function saveHistory($params = []) {
        $query = (new Query())->createCommand()
            ->insert('users_history', [
                'login' => $params['login'],
                'ip' => $this->getIp(),
                'browser' => $this->getBrowser(),
                'success' => $params['success']
            ])
            ->execute();

        return $query;
    }

    /**
     * Сохранить действие пользователя
     * 
     * @return bool
     */
    public function saveAction($params = []) {
        $query = (new Query())->createCommand()
            ->insert('users_action', [
                'user_id' => $this->getID(),
                'type' => $params['type'],
                'details' => $params['details'],
                'ip' => $this->getIp()
            ])
            ->execute();

        return $query;
    }

    /**
     * Проверяет наличия кода для восстановления пароля
     * 
     * @param string $code
     * 
     * @return bool
     */
    public function recoveryCodeExist($code) {
        $code = trim($code);

        $count = UsersRecovery::find()
            ->where(['code' => $code])
            ->andWhere(['>', 'expire', date('Y-m-d H:i:s')])
            ->count();

        if ($count == 0) return false;
        return true;
    }

    /**
     * Генерирует уникальный код подтверждения
     * 
     * @param int $min Минимальный символ
     * @param int $max Максимальный символ
     * 
     * @return int
     */
    public function generateCode($min, $max) {
        $continue = false;
        do {
            $code = random_int($min, $max);

            $count = UsersConfirm::find()
                ->where(['code' => $code])
                ->count();

            if ($count == 0) $continue = true;
        } while (!$continue);

        return $code;
    }

    /**
     * Получить IP-адрес пользователя
     * 
     * @return string
     */
    public function getIp() {
        $ip = explode(':', $_SERVER['HTTP_X_FORWARDED_FOR'])[0];
        if (!empty($ip)) return $ip;
        return $_SERVER['REMOTE_ADDR'];
    }

    /**
     * Получить браузер пользователя
     * 
     * @return string
     */
    public function getBrowser() {
        $user_agent = $_SERVER['HTTP_USER_AGENT'];
        if (strpos($user_agent, 'MSIE') !== false) return 'Internet Explorer';
        elseif (strpos($user_agent, 'OPR') !== false) return 'Opera';
        elseif (strpos($user_agent, 'YaBrowser') !== false) return 'Yandex Browser';
        elseif (strpos($user_agent, 'Vivaldi') !== false) return 'Vivaldi';
        elseif (strpos($user_agent, 'Maxthon') !== false) return 'Maxthon';
        elseif (strpos($user_agent, 'Edge') !== false) return 'Microsoft Edge';
        elseif (strpos($user_agent, 'Firefox') !== false) return 'Mozilla Firefox';
        elseif (strpos($user_agent, 'Chrome') !== false) return 'Google Chrome';
        elseif (strpos($user_agent, 'Safari') !== false) return 'Safari';
        else return 'Unknown';
    }

    /**
     * Получить страну пользователя
     * 
     * @return string|bool
     */
    public function getCountry() {
        $api = 'http://ip-api.com/json/';
        $json = json_decode(file_get_contents($api . $this->getIp()));
        if (isset($json->status) == 'fail') return false;
        return $json->country;
    }

    /**
     * Проверка капчи
     * 
     * @return bool
     */
    public function captchaValidate($recaptcha_response) {
        $query_url = 'https://www.google.com/recaptcha/api/siteverify';
        $query_data = http_build_query(['secret' => Yii::$app->params['recaptcha']['secret'], 'response' => $recaptcha_response, 'remoteip' => $this->getIp()]);

        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_POST => 1,
            CURLOPT_HEADER => 0,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_SSL_VERIFYPEER => 1,
            CURLOPT_URL => $query_url,
            CURLOPT_POSTFIELDS => $query_data,
        ]);

        $result = curl_exec($curl);
        curl_close($curl);

        $json = json_decode($result);
        return $json->success;
    }
}