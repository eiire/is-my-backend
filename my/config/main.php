<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-my',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'my\controllers',
    'bootstrap' => ['log'],
    'modules' => [],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-my',
            'baseUrl'=> '',
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-my',
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=data',
            'username' => 'root',
            'password' => 'Gfhjkm_root_mysql_2018',
            'charset' => 'utf8',
            // Schema cache options (for production environment)
            'enableSchemaCache' => true,
            'schemaCacheDuration' => 3600,
            'schemaCache' => 'cache',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
                [
                    'class' => 'yii\log\EmailTarget',
                    'categories' => ['mail'],
                    'logVars' => [],
                    'message' => [
                        'from' => ['my@is1c.ru' => 'Личный кабинет ИнфоСофт'],
                        'to' => ['it@is1c.ru'],
                        'subject' => 'Ошибка в HTTP сервисе',
                    ],
                    'prefix' => function($message) {
                        return;
                    }
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => false,
            'htmlLayout' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'mail.is1c.ru',
                'username' => 'my@is1c.ru',
                'password' => 'Qaz123wsX456',
                'port' => '25',
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '' => 'site/index',
                '<alias:signin|signup|recovery|change|confirm>' => 'auth/<alias>',
                '<alias:action|history|settings|feedback>' => 'site/<alias>',
                '<alias:rating|unsubscribe|changelog/>' => 'pages/<alias>',
                'privacy-policy' => 'pages/privacy',
                'support' => 'support/index',
                'service' => 'service/index',
                'docs' => 'docs/index',
                'specialist' => 'specialist/index',
                'handler' => 'handler/handler',
                'cloud' => 'cloud/index',
                'stats' => 'stats/index',
                'sca' => 'sca/index',
            ],
        ],
    ],
    'params' => $params,
];
