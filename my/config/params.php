<?php
return [
    'url' => 'https://my.is1c.ru',
    'title' => 'ИнфоСофт',
    'email' => [
        'from' => 'my@is1c.ru',
        'support' => 'saas@is1c.ru',
        'admin' => 'it@is1c.ru',
        'saas' => 'saas@is1c.ru'
    ],
    'salt' => 'e79c3af8203f526c',
];