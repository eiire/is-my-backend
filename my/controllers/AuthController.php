<?php
namespace my\controllers;

use Yii;
use yii\web\Controller;
use yii\db\Query;
use yii\helpers\Url;
use yii\base\Security;
use my\components\Client;
use my\components\User;
use my\models\users\Users;
use my\models\users\UsersCode;
use my\models\users\UsersConfirm;
use my\models\event\EventList;
use my\models\event\EventLotteryUsers;
use my\models\forms\SigninForm;
use my\models\forms\SignupForm;
use my\models\forms\EmailForm;
use my\models\forms\PasswordForm;
use my\models\forms\PhoneConfirmForm;
use common\models\helpers\FormatHelper;
use common\models\helpers\NumbersHelper;
use common\models\Request;

class AuthController extends Controller {
    private $user;

    public function beforeAction($action) {
        $this->user = new User();
        if ($this->user->haveAccess()) {
            return $this->redirect(['site/index']);
        }

        return parent::beforeAction($action);
    }

    public function actionSignin() {
        $form = new SigninForm();
        $show_form = true;
        $show_captcha = false;
        $access = false;

        if ($this->user->blocked()) {
            $show_form = false;
            $alert = [
                'type' => 'danger',
                'message' => 'Доступ запрещен'
            ];
        } elseif (Yii::$app->session->getFlash('session_remove')) {
            $alert = [
                'type' => 'warning',
                'message' => 'Время действия сеанса истекло'
            ];
        } elseif ($form->load(Yii::$app->request->post())) {
            $this->user->login = trim($form->login);
            $this->user->password = trim($form->password);
            $login = $this->user->login;

            $signin_count = $this->user->errorSignInCount();
            if ($signin_count >= 3 && $signin_count < 10) {
                $show_captcha = true;
            } elseif ($signin_count >= 10) {
                $this->user->block();

                $text = 'Дата: ' . date('d.m.Y H:i') . '<br>';
                $text .= 'IP: ' . $this->user->getIp() . '<br>';
                $text .= 'Браузер: ' . $this->user->getBrowser();

                Yii::$app->mailer->compose()
                    ->setFrom(Yii::$app->params['email']['from'])
                    ->setTo(Yii::$app->params['email']['admin'])
                    ->setSubject('Личный кабинет ' . Yii::$app->params['title'] . ' - Блокировка пользователя')
                    ->setHtmlBody($text)
                    ->send();
            }

            if ($show_captcha) $captcha = $this->user->captchaValidate($_POST['g-recaptcha-response']);

            if (!$form->login || !$form->password) {
                $alert = [
                    'type' => 'danger',
                    'message' => 'Все поля обязательны для заполнения'
                ];
            } elseif ($show_captcha && !$captcha) {
                $alert = [
                    'type' => 'danger',
                    'message' => 'Валидация не пройдена. Попробуйте еще раз'
                ];
            } elseif ($this->user->signIn()) {
                $access = true;

                $this->user = new User(); // Получаем данные пользователя после успешной авторизации

                $legal_entities = UsersCode::find()
                    ->select(['active'])
                    ->where(['user_id' => $this->user->getID()])
                    ->all();

                $is_all_zero = true;
                foreach ($legal_entities as $legal_entity) {
                    if ($legal_entity->active == 1) {
                        $is_all_zero = false;
                        break;
                    }
                }

                if ($is_all_zero) UsersCode::updateAll(['active' => 1], ['user_id' => $this->user->getID()]);

                $login = $this->user->getLogin();

                $client = new Client();
                $client->id = $this->user->getCode();
                $details = $client->sendRequest(Client::UPP, Client::CLIENT);

                $company = NULL;
                if ($details->full_name) {
                    $details->full_name = urldecode($details->full_name);
                    $company = FormatHelper::replace($details->full_name, [
                        'Индивидуальный предприниматель' => 'ИП',
                        'Общество с ограниченной ответственностью' => 'ООО'
                    ]);
                }

                UsersCode::updateAll([
                    'name' => $company,
                    'segment' => $details->segment,
                    'inn' => $details->inn,
                    'cashbox' => $details->kassa,
                ], [
                    'user_id' => $this->user->getID(),
                    'code' => $this->user->getCode()
                ]);
            } else {
                $alert = [
                    'type' => 'danger',
                    'message' => 'Неверно введен логин или пароль'
                ];
                $form->password = NULL;
            }

            $this->user->saveHistory([
                'login' => $login,
                'success' => $access
            ]);
        }

        if ($access) return $this->redirect(['site/index']);

        if (isset($_GET['response']) && $_GET['response'] == 'success') {
            $alert = [
                'type' => 'success',
                'message' => 'Вы успешно зарегистрированы. Теперь вы можете войти, используя свой email и пароль'
            ];
        }

        $email = isset($_GET['email']) ? $_GET['email'] : NULL;

        return $this->render('signin', [
            'form' => $form,
            'email' => $email,
            'show_form' => $show_form,
            'show_captcha' => $show_captcha,
            'alert' => $alert
        ]);
    }

    public function actionSignup() {
        $form = new SignupForm();

        if ($form->load(Yii::$app->request->post())) {
            $login = htmlspecialchars(trim($form->login));
            $inn = htmlspecialchars(trim($form->inn));
            $password = htmlspecialchars(trim($form->password));
            $password_repeat = htmlspecialchars(trim($form->password_repeat));

            $this->user->login = $login;
            $this->user->password = $password;

            if (!$login || !$inn || !$password || !$password_repeat) {
                $alert = [
                    'type' => 'danger',
                    'message' => 'Все поля обязательны для заполнения'
                ];
            } elseif ($password != $password_repeat) {
                $alert = [
                    'type' => 'danger',
                    'message' => 'Пароли не совпадают'
                ];
            } elseif (!$this->user->passwordValidate()) {
                $alert = [
                    'type' => 'danger',
                    'message' => 'Пароль должен быть длиной более 8 символов и содержать хотя бы одну заглавную букву или символ'
                ];
            } elseif (!$this->user->emailValidate($this->user->login)) {
                $alert = [
                    'type' => 'danger',
                    'message' => 'Введен некорректный адрес электронной почты'
                ];
            } elseif (!$this->user->captchaValidate($_POST['g-recaptcha-response'])) {
                $alert = [
                    'type' => 'danger',
                    'message' => 'Валидация не пройдена. Попробуйте еще раз'
                ];
            } elseif ($this->user->userExist()) {
                $alert = [
                    'type' => 'danger',
                    'message' => 'Вы уже зарегистрированы. Войдите используя адрес вашей электронной почты и пароль'
                ];
            } else {
                $params = [
                    'email' => $login,
                    'inn' => $inn
                ];

                $request = new Request('http://sky.is1c.ru/site/hs/getInfo/GetPersonCodeByINNMail?' . http_build_query($params));
                $request->execute();
                $response = json_decode($request->getResponse());

                if ($response->result == 'false') {
                    $alert = [
                        'type' => 'danger',
                        'message' => 'Вас не удалось найти в нашей клиентской базе. Проверьте введенные данные, либо обратитесь к менеджеру по телефону<br>211-27-27'
                    ];
                } else {
                    $code = trim($response->result);
                    $password = $this->user->passwordGenerate();

                    (new Query())->createCommand()
                        ->insert('users', [
                            'login' => $login,
                            'password' => $password,
                            'confirm' => 1,
                            'role' => 1
                        ])
                        ->execute();

                    $user_id = Yii::$app->db->getLastInsertID();

                    if ($user_id) {
                        $query = (new Query())->createCommand()
                        ->insert('users_code', [
                            'user_id' => $user_id,
                            'code' => $code,
                            'inn' => $inn,
                            'active' => 1
                        ])
                        ->execute();
                    }

                    if ($user_id && $query) return $this->redirect(['auth/signin', 'email' => $login, 'response' => 'success']);

                    $alert = [
                        'type' => 'danger',
                        'message' => 'Произошла ошибка, попробуйте еще раз'
                    ];
                }
            }

            $form->password = NULL;
            $form->password_repeat = NULL;
        }

        return $this->render('signup', [
            'form' => $form,
            'alert' => $alert
        ]);
    }

    public function actionRecovery() {
        $form = new EmailForm();
        $show_form = true;

        if ($form->load(Yii::$app->request->post())) {
            $this->user->login = trim($form->email);
            if (!$form->email || !$this->user->emailValidate($this->user->login)) {
                $alert = [
                    'type' => 'danger',
                    'message' => 'Введен некорректный адрес электронной почты'
                ];
            } elseif (!$this->user->captchaValidate($_POST['g-recaptcha-response'])) {
                $alert = [
                    'type' => 'danger',
                    'message' => 'Валидация не пройдена. Попробуйте еще раз'
                ];
            } elseif (!$this->user->userExist()) {
                $alert = [
                    'type' => 'danger',
                    'message' => 'Пользователь не найден или аккаунт не подтвержден'
                ];
            } else {
                $code = $this->user->passwordRecovery();

                $url = Yii::$app->params['url'] . '/change?code=' . $code;
                $text = 'Для восстановления пароля перейдите по <a href="' . $url . '">ссылке</a>. Если вы не запрашивали восстановления пароля, проигнорируйте это письмо.<br><br>' . $url;

                Yii::$app->mailer->compose('layouts/template', ['content' => $text])
                    ->setFrom(Yii::$app->params['email']['from'])
                    ->setTo($this->user->login)
                    ->setSubject('ИнфоСофт - Восстановление пароля')
                    ->send();

                $alert = [
                    'type' => 'success',
                    'message' => 'На ваш email отправлена инструкция по восстановлению пароля'
                ];
                $show_form = false;
            }
        }

        $email = (isset($_GET['email'])) ? $_GET['email'] : NULL;

        return $this->render('recovery', [
            'form' => $form,
            'show_form' => $show_form,
            'email' => $email,
            'alert' => $alert
        ]);
    }

    public function actionChange() {
        $form = new PasswordForm();
        $show_form = true;

        if (isset($_GET['code'])) {
            if ($this->user->recoveryCodeExist($_GET['code'])) {
                if ($form->load(Yii::$app->request->post())) {
                    $this->user->password = trim($form->password);

                    if (!$form->password || !$form->password_repeat) {
                        $alert = [
                            'type' => 'danger',
                            'message' => 'Все поля обязательны для заполнения'
                        ];
                    } elseif ($form->password != $form->password_repeat) {
                        $alert = [
                            'type' => 'danger',
                            'message' => 'Пароли не совпадают'
                        ];
                    } elseif (!$this->user->passwordValidate()) {
                        $alert = [
                            'type' => 'danger',
                            'message' => 'Пароль должен быть длиной более 8 символов и содержать хотя бы одну заглавную букву или символ'
                        ];
                    } else {
                        $this->user->login = $this->user->getEmailByCode($_GET['code']);
                        if ($this->user->passwordChange()) {
                            (new Query())->createCommand()
                                ->update('users', ['confirm' => 1], ['login' => $this->user->login])
                                ->execute();

                            $alert = [
                                'type' => 'success',
                                'message' => 'Ваш пароль успешно обновлен'
                            ];
                        } else {
                            $alert = [
                                'type' => 'danger',
                                'message' => 'Произошла ошибка, попробуйте еще раз'
                            ];
                        }
                        $show_form = false;
                    }
                }
            } else {
                $alert = [
                    'type' => 'danger',
                    'message' => 'Указанный код не действителен'
                ];
                $show_form = false;
            }
        } else {
            return $this->redirect(['site/index']);
        }

        return $this->render('change', [
            'form' => $form,
            'show_form' => $show_form,
            'alert' => $alert
        ]);
    }
}