<?php
namespace my\controllers;

use Yii;
use yii\data\Pagination;
use common\models\Request;
use my\components\Client;
use my\models\forms\PeriodForm;
use my\models\forms\SupportForm;
use my\models\forms\SessionsForm;
use my\models\forms\PaymentForm;
use my\models\forms\IpAddressForm;
use my\models\forms\CreateBaseForm;
use my\models\forms\CreateCompanyGroupForm;
use common\models\helpers\FormatHelper;

class CloudController extends SiteController {
    public function actionIndex() {
        $result = $this->client->sendRequest(Client::SC, Client::LIST);
        $res_group_list = $this->client->sendRequest(Client::SC, Client::UNIQ_GROUP_COMPANY_NAMES);
        $new_client = false;
        $is_franch = false;
        $groups_with_dbases = [];
        $groups_with_dbases_full = [];
        $group_form = new CreateCompanyGroupForm();
        $form = new CreateBaseForm();
        $user_dbases_ids = [];

        if (!$result) {
            $response = [
                'type' => 'danger',
                'message' => 'Произошла ошибка при получении списка баз' 
            ];
        } elseif ($result->ErrorDescription) {
            $response = [
                'type' => 'danger',
                'message' => $result->ErrorDescription 
            ];
        } elseif (!$result->ListDB) {
            $response = [
                'type' => 'warning',
                'message' => 'У вас нет созданных баз в облаке' 
            ];
            $balance = false;
            $new_client = true;
        } elseif (!$res_group_list) {
            $response = [
                'type' => 'danger',
                'message' => 'Произошла ошибка при получении списка групп компаний'
            ];
        } elseif ($res_group_list->ErrorDescription) {
            $response = [
                'type' => 'danger',
                'message' => $res_group_list->ErrorDescription
            ];
        } else {
            $bases = $result->ListDB;

            $pagination = new Pagination([
                'totalCount' => count($bases),
                'defaultPageSize' => 10
            ]);

            $limit = $this->paginationLimit($pagination);

            $list_group = array_map(function ($item) {return urldecode($item);},
                (isset($res_group_list) && isset($res_group_list->list_group)) ? $res_group_list->list_group : []);
            $self_associated_list_group = [];

            foreach ($bases as $dbase) {
                $user_dbases_ids[$dbase->db] = urldecode($dbase->db);
            }

            $is_franch = $bases[0]->franch;
            $id = 1;
            for ($i = $pagination->offset; $i < $limit; $i++) {
                $base = $bases[$i];
                $base->id = $id;

                if (!$group && $base->group == true) $group = true;

                $id++;
                $base->release = ($base->release == 'НЕ ОБНОВЛЯТЬ') ? 'Доработанная' : $base->release;
                $base->max_size = ($base->count_sessions == 1) ? 5*pow(1024, 3) : 10*pow(1024, 3);
                $base->format_size = FormatHelper::size($base->size);
                $base->tariff = ($base->tarif) ? $base->tarif : false;
                $base->frozen_date = date('d.m.Y', strtotime($base->date_frozen));
                $base->frozen_type = ($base->frozen) ? 'defrost' : 'frozen';
                $base->company_group = $base->group;
                $base->test_drive = $base->test_drives;

                if ($base->frozen) {
                    $defrost_date = strtotime('+7 days', strtotime($base->date_frozen));
                    $base->defrost = (time() > $defrost_date);
                    $base->defrost_date = (time() > $defrost_date) ? false : date('d.m.Y', $defrost_date);
                }

                if ($base->max_size <= $base->size) {
                    $base->max_size = ceil($base->size / pow(1024, 3))*pow(1024, 3);
                }

                $base->free_size = $base->max_size - $base->size;
                $base->format_free_size = FormatHelper::size($base->free_size);

                if ($base->future_change) {
                    $base->update_date = date('d.m.Y', strtotime($base->future_change));
                    $base->update_count = $base->future_sessions;
                }

                $date = new \DateTime('-1 month');
                $base_activity = $this->client->sendRequest(Client::SC, Client::COUNT, [
                    'name_db' => $base->db,
                    'date_begin' => $date->format('Ymd'),
                    'date_end' => date('Ymd')
                ]);

                $activity_count = [];
                $activity_date = [];

                foreach ($base_activity->InfoDB->day_history as $row) {
                    array_push($activity_count, $row->count);
                    array_push($activity_date, date('"d.m.Y"', strtotime($row->date)));
                }

                $base->activity_count = implode(',', $activity_count);
                $base->activity_date = implode(',', $activity_date);
                $groups_with_dbases[urldecode($base->group_name)][] = $base;
            }

            foreach ($bases as $base) {
                $groups_with_dbases_full[urldecode($base->group_name)][] = $base;
                $dbase_nav_list_configs[$base->db] = $this->createBaseNav($base, $is_franch);
            }

            foreach ($list_group as $group) {
                $self_associated_list_group[$group] = $group;

                if (!in_array($group, array_keys($groups_with_dbases_full))) {
                    if ($first_empty_group === null) {
                        $first_empty_group = $group;
                    }
                    $groups_with_dbases[$group] = [];
                }
            }

            $balance_result = $this->client->sendRequest(Client::SC, Client::BUDGET);
            $balance = false;

            if ($balance_result) {
                if ($balance_result->balance != 0 && $balance_result->last_sum_pay != 0) {
                    $days = floor($balance_result->balance / $balance_result->last_sum_pay);
                } else {
                    $days = 0;
                }

                $balance = [
                    'current' => FormatHelper::money($balance_result->balance),
                    'days' => $days,
                    'days_title' => FormatHelper::day($days),
                    'negative' => ($balance_result->balance <= 0)
                ];
            }

            if ($form->load(Yii::$app->request->get())) {
                $dbase_id = htmlspecialchars(trim($form->base));

                foreach ($bases as $dbase) {
                    if ($dbase->db === $dbase_id) {
                        $dbase_for_display = $dbase;
                    }
                }

                $is_filter = true;
                $group_with_dbases = [urldecode($dbase_for_display->group_name) => [$dbase_for_display]];
            }

            if ($group_form->load(Yii::$app->request->get())) {
                $is_filter = true;
                $group_name = htmlspecialchars(trim($group_form->group_name));
                $group_with_dbases[$group_name] = $groups_with_dbases_full[$group_name] ? $groups_with_dbases_full[$group_name] : [];
            }
        }

        $base_list = [
            1 => '1С:Бухгалтерия ПРОФ',
            2 => '1С:Зарплата и управление персоналом',
            3 => '1С:Управление Нашей фирмой',
            4 => '1С:Управление торговлей',
            5 => '1С:Комплексная автоматизация',
            6 => '1С:Розница'
        ];

        if ($form->load(Yii::$app->request->post())) {
            $base_response = 'base-fail';
            $params = [
                'email' => $this->user->getEmail(),
                'config' => htmlspecialchars(trim($form->base)),
            ];

            if ($is_franch) {
                $params['group_name'] = rawurlencode(htmlspecialchars(trim($form->group_name)));
            }

            $result = $this->client->sendRequest(Client::SC, Client::CREATE_DB, $params);

            if ($result->create_db) {
                $this->user->saveAction([
                    'type' => 'Создана заявка на создание базы',
                    'details' => 'Заявка на создание новой базы ' . $base_list[$form->base] . ' создана успешно'
                ]);

                $base_response = 'base-success';
            }

            return $this->redirect(['cloud/index', 'response' => $base_response]);
        }

        if ($group_form->load(Yii::$app->request->post())) {
            $response = 'fail';
            $params = [
                'email' => $this->user->getEmail(),
                'group_name' => rawurlencode(htmlspecialchars(trim($group_form->group_name))),
            ];
            $response_company_group = $this->client->sendRequest(Client::SC, Client::CREATE_COMPANY_GROUP, $params);

            if ($response_company_group->insert_group) {
                $this->user->saveAction([
                    'type' => 'Создана заявка на создание группы компаний',
                    'details' => 'Заявка на создание новой группы компаний создана успешно'
                ]);
                $response = 'success-create-group';
            }

            return $this->redirect(['cloud/', 'response' => $response]);
        }

        if ($_GET['response']) {
            switch ($_GET['response']) {
                case 'frozen':
                    $response = [
                        'type' => 'success',
                        'message' => 'База успешно заморожена',
                        'close' => true
                    ];
                    break;
                case 'defrost':
                    $response = [
                        'type' => 'success',
                        'message' => 'База успешно разморожена',
                        'close' => true
                    ];
                    break;
                case 'fail':
                    $response = [
                        'type' => 'danger',
                        'message' => 'Произошла ошибка, попробуйти еще раз',
                        'close' => true
                    ];
                    break;
                case 'base-fail':
                    $response = [
                        'type' => 'warning',
                        'message' => 'В данный момент обрабатывается предыдущая заявка на создание базы',
                        'close' => true
                    ];
                    break;
                case 'base-success':
                    $response = [
                        'type' => 'success',
                        'message' => 'Заявка на создание базы успешно отправлена. База будет создана в течение 5-10 минут',
                        'close' => true
                    ];
                    break;
                case 'base-deleted':
                    $response = [
                        'type' => 'success',
                        'message' => 'База успешно удалена',
                        'close' => true
                    ];
                    break;
                case 'success-create-group':
                    $response = [
                        'type' => 'success',
                        'message' => 'Группа успешно создана',
                        'close' => true
                    ];
                    break;
                case 'success-rename-group':
                    $response = [
                        'type' => 'success',
                        'message' => 'Группа успешно переименована',
                        'close' => true
                    ];
                    break;
                case 'fail-same-name':
                    $response = [
                        'type' => 'danger',
                        'message' => 'Группа не была переименовона, так как название совпадает с предыдущим',
                        'close' => true
                    ];
                    break;
                case 'fail-empty-name':
                    $response = [
                        'type' => 'danger',
                        'message' => 'Группа не была переименовона, так как пустое название группы невозможно',
                        'close' => true
                    ];
                    break;
                case 'success-change-session-group':
                    $response = [
                        'type' => 'success',
                        'message' => 'Количество сеансов у ' . $_GET['changed_group'] . ' изменилось на ' . $_GET['numb_sessions'],
                        'close' => true
                    ];
                    break;
                case 'fail-incorrect-input-value':
                    $response = [
                        'type' => 'danger',
                        'message' => 'Значение ' . $_GET['numb_sessions'] . ' является некорректным!',
                        'close' => true
                    ];
                    break;
                case 'success-move-dbase':
                    $response = [
                        'type' => 'success',
                        'message' => 'База данных была успешно перемещена',
                        'close' => true
                    ];
                    break;
            }
        }

        $frozen_modal = [
            'frozen' => [
                'title' => 'Вы уверены что хотите заморозить базу?',
                'message' => '<div class="text-danger">Минимальный срок заморозки базы 7 дней.</div>При заморозке базы вход в нее будет недоступен.<br>При этом, стоимость составит 50% от стоимости тарифа.',
                'button' => 'Заморозить'
            ],
            'defrost' => [
                'title' => 'Вы уверены что хотите разморозить базу?',
                'message' => 'При разморозке базы вход в нее станет доступен.<br>При этом, стоимость будет равна 100% от стоимости тарифа.',
                'button' => 'Разморозить'
            ]
        ];

        $groups = array_map(function ($item) {return rawurlencode($item);}, isset($list_group) ? $list_group : []);
        $groups_with_keys = [];

        foreach ($groups as $group) {
            $groups_with_keys[$group] = urldecode($group);
        }

        if ($is_franch) {
            if ($is_filter) {
                $processing_dbases = $group_with_dbases;
            } else {
                $processing_dbases = $groups_with_dbases;
            }
        } else if (!empty($groups_with_dbases)) {
            $processing_dbases = array_merge(...array_values($groups_with_dbases));
        }

        return $this->render('index', [
            'result' => $processing_dbases,
            'first_empty_group' => $first_empty_group,
            'sum_sessions_groups' => array_reduce($groups_with_dbases_full, function ($sum, $group_with_dbases) {return $sum + $group_with_dbases[0]->count_sessions_group;}),
            'group' => $group,
            'groups' => $groups,
            'groups_with_keys' => $groups_with_keys,
            'pagination' => $pagination,
            'group_names' => $self_associated_list_group,
            'is_franch' => $is_franch,
            'is_filter' => $is_filter,
            'form' => $form,
            'group_form' => $group_form,
            'balance' => $balance,
            'frozen_modal' => $frozen_modal,
            'new_client' => $new_client,
            'base_list' => $base_list,
            'user_dbases_ids' => $user_dbases_ids,
            'response' => $response,
            'profile' => $this->profile,
            'dbase_nav_list_configs' => $dbase_nav_list_configs,
        ]);
    }

    public function actionSession() {
        $db = Yii::$app->request->get('db');
        $result = $this->client->sendRequest(Client::SC, Client::SESSION, [
            'name_db' => $db
        ]);

        if (!$result) {
            $response = [
                'type' => 'danger',
                'message' => 'Произошла ошибка при получении активных сеансов'
            ];
        } elseif ($result->ErrorDescription) {
            $response = [
                'type' => 'danger',
                'message' => $result->ErrorDescription
            ];
        } elseif (!$result->InfoDB) {
            $response = [
                'type' => 'warning',
                'message' => 'В базе нет активных сеансов'
            ];
        } else {
            $result = $result->InfoDB;
            $format_result = [];

            $app = [
                '1CV8C' => 'Тонкий клиент 1С',
                'BackgroundJob' => 'Фоновое задание',
                'WebClient' => 'Браузер'
            ];

            $pagination = new Pagination([
                'totalCount' => count($result),
                'defaultPageSize' => 25
            ]);

            $limit = $this->paginationLimit($pagination);

            for ($i = $pagination->offset; $i < $limit; $i++) {
                $format_result[$i] = [
                    'db' => $db,
                    'ns' => $result[$i]->ns,
                    'user' => $result[$i]->user,
                    'date' => date('d.m.Y H:i', strtotime($result[$i]->date_start)),
                    'app' => $app[$result[$i]->app],
                    'guid' => $result[$i]->GUID
                ];
            }
        }

        if ($_GET['response']) {
            switch ($_GET['response']) {
                case 'success':
                    $response = [
                        'type' => 'success',
                        'message' => 'Сеанс успешно завершен',
                        'close' => true
                    ];
                    break;
                case 'fail':
                    $response = [
                        'type' => 'danger',
                        'message' => 'Произошла ошибка при завершении сеанса',
                        'close' => true
                    ];
                    break;
            }
        }

        return $this->render('session', [
            'result' => $format_result,
            'result_count' => $pagination->totalCount,
            'response' => $response,
            'pagination' => $pagination,
            'profile' => $this->profile
        ]);
    }

    public function actionHistory() {
        $db = Yii::$app->request->get('db');
        $form = new PeriodForm();

        if (isset($_GET['period_start']) && isset($_GET['period_end'])) {
            $period_start = date('Ymd', strtotime($_GET['period_start']));
            $period_end = date('Ymd', strtotime($_GET['period_end']));
        } else {
            $date = new \DateTime('-1 month');
            $period_start = $date->format('Ymd');
            $period_end = date('Ymd');
        }

        $result = $this->client->sendRequest(Client::SC, Client::HISTORY, [
            'name_db' => $db,
            'date_begin' => $period_start,
            'date_end' => $period_end
        ]);

        if (!$result) {
            $response = [
                'type' => 'danger',
                'message' => 'Произошла ошибка при получении истории входов'
            ];
        } elseif ($result->ErrorDescription) {
            $response = [
                'type' => 'danger',
                'message' => $result->ErrorDescription
            ];
        } elseif (!$result->InfoDB->login_history) {
            $response = [
                'type' => 'warning',
                'message' => 'В указанный период вход в базу не осуществлялся'
            ];
        } else {
            $result = $result->InfoDB;
            $format_result = [];
            $login_history = $result->login_history;

            $pagination = new Pagination([
                'totalCount' => count($login_history),
                'defaultPageSize' => 25
            ]);

            $limit = $this->paginationLimit($pagination);

            for ($i = $pagination->offset; $i < $limit; $i++) {
                $format_result[$i] = [
                    [
                        'label' => 'Пользователь',
                        'value' => $login_history[$i]->user
                    ],
                    [
                        'label' => 'Дата',
                        'value' => date('d.m.Y H:i', strtotime($login_history[$i]->date))
                    ]
                ];
            }
        }

        if ($_GET['response']) {
            switch ($_GET['response']) {
                case 'success':
                    $response = [
                        'type' => 'success',
                        'message' => 'Файл успешно сформирован',
                        'close' => true
                    ];
                    break;
                case 'fail':
                    $response = [
                        'type' => 'danger',
                        'message' => 'Произошла ошибка. Попробуйте еще раз',
                        'close' => true
                    ];
                    break;
            }
        }

        $period_start = date('d.m.Y', strtotime($period_start));
        $period_end = date('d.m.Y', strtotime($period_end));

        return $this->render('history', [
            'db' => $db,
            'result' => $format_result,
            'period_start' => $period_start,
            'period_end' => $period_end,
            'form' => $form,
            'response' => $response,
            'pagination' => $pagination,
            'profile' => $this->profile
        ]);
    }

    public function actionUpdate() {
        $db = Yii::$app->request->get('db');
        $form = new PeriodForm();

        if (isset($_GET['period_start']) && isset($_GET['period_end'])) {
            $period_start = date('Ymd', strtotime($_GET['period_start']));
            $period_end = date('Ymd', strtotime($_GET['period_end']));
        } else {
            $date = new \DateTime('-1 month');
            $period_start = $date->format('Ymd');
            $period_end = date('Ymd');
        }

        $result = $this->client->sendRequest(Client::SC, Client::UPDATE, [
            'name_db' => $db,
            'date_begin' => $period_start,
            'date_end' => $period_end
        ]);

        if (!$result) {
            $response = [
                'type' => 'danger',
                'message' => 'Произошла ошибка при получении истории обновлений'
            ];
        } elseif ($result->ErrorDescription) {
            $response = [
                'type' => 'danger',
                'message' => $result->ErrorDescription
            ];
        } elseif (!$result->InfoDB->update_history) {
            $response = [
                'type' => 'warning',
                'message' => 'В указанный период обновление конфигурации не осуществлялось'
            ];
        } else {
            $result = $result->InfoDB;
            $update_history = $result->update_history;
            $format_result = [];

            $pagination = new Pagination([
                'totalCount' => count($update_history),
                'defaultPageSize' => 25
            ]);

            $limit = $this->paginationLimit($pagination);

            for ($i = $pagination->offset; $i < $limit; $i++) {
                $format_result[$i] = [
                    [
                        'label' => 'Версия',
                        'value' => ($update_history[$i]->release_config == 'НЕ ОБНОВЛЯТЬ') ? 'Доработанная' : $update_history[$i]->release_config
                    ],
                    [
                        'label' => 'Дата',
                        'value' => date('d.m.Y H:i', strtotime($update_history[$i]->date))
                    ]
                ];
            }
        }

        $period_start = date('d.m.Y', strtotime($period_start));
        $period_end = date('d.m.Y', strtotime($period_end));

        return $this->render('update', [
            'result' => $format_result,
            'period_start' => $period_start,
            'period_end' => $period_end,
            'form' => $form,
            'response' => $response,
            'pagination' => $pagination,
            'profile' => $this->profile
        ]);
    }

    public function actionBackup() {
        $db = Yii::$app->request->get('db');

        $result = $this->client->sendRequest(Client::SC, Client::BACKUP, [
            'name_db' => $db
        ]);

        if (!$result) {
            $response = [
                'type' => 'danger',
                'message' => 'Произошла ошибка при получении списка резервных копий'
            ];
        } elseif ($result->ErrorDescription) {
            $response = [
                'type' => 'danger',
                'message' => $result->ErrorDescription
            ];
        } else {
            $result = $result->InfoDB;
            $format_result = [];
            $backup_creating = false;

            for ($i = 0; $i < count($result); $i++) {
                $format_result[$i] = [
                    'db' => $db,
                    'id' => $i,
                    'label' => 'Имя резервной копии',
                    'name' => $result[$i]->name,
                    'condition' => ($result[$i]->condition == 'Успешно') ? 'Создан' : $result[$i]->condition,
                    'date' => date('d.m.Y H:i', strtotime($result[$i]->date)),
                    'size' => FormatHelper::size($result[$i]->size),
                    'user_backup' => $this->client->userBackup($result[$i]->name),
                    'delete' => ($this->client->userBackup($result[$i]->name) && $result[$i]->condition != 'Выполняется') ? true : false
                ];

                if (!$backup_creating && $result[$i]->condition == 'Выполняется') $backup_creating = true;
            }

            $days = (count($result) > 5) ? '30 дней' : '3 дня';

            $response = [
                'type' => 'warning',
                'message' => 'Вы можете самостоятельно создать 2 резервные копии базы. Время хранения копии ' . $days
            ];
        }

        $create_backup = ($this->client->backupCount($db) < 2 && !$backup_creating) ? true : false;

        if ($_GET['response']) {
            switch ($_GET['response']) {
                case 'create-success':
                    $response = [
                        'type' => 'success',
                        'message' => 'Запрос на создание резервной копии успешно отправлен. Копия будет создана в течении нескольких минут',
                        'close' => true
                    ];
                    break;
                case 'create-fail':
                    $response = [
                        'type' => 'danger',
                        'message' => 'Вы достигли лимита на создание резервных копий',
                        'close' => true
                    ];
                    break;
                case 'delete-success':
                    $response = [
                        'type' => 'success',
                        'message' => 'Резервная копия успешно удалена',
                        'close' => true
                    ];
                    break;
                case 'delete-fail':
                    $response = [
                        'type' => 'danger',
                        'message' => 'Удаление данной резервной копии невозможно',
                        'close' => true
                    ];
                    break;
            }
        }

        return $this->render('backup', [
            'db' => $db,
            'result' => $format_result,
            'create_backup' => $create_backup,
            'response' => $response,
            'profile' => $this->profile
        ]);
    }

    public function actionSupport() {
        $form = new SupportForm();
        $show_form = true;

        $result = $this->client->sendRequest(Client::SC, Client::LIST);

        if (!$result) {
            $show_form = false;
            $response = [
                'type' => 'warning',
                'message' => 'Произошла ошибка при получении данных'
            ];
        } elseif (!$result->ListDB) {
            $show_form = false;
            $response = [
                'type' => 'warning',
                'message' => 'У вас нет созданных баз в облаке'
            ];
        } else {
            foreach ($result->ListDB as $row) {
                if (count($result->ListDB) > 1) {
                    $bases['false'] = '';
                }
                $bases[$row->db] = $row->name;
            }
        }

        if ($form->load(Yii::$app->request->post())) {
            if (!$this->user->permission(1)) {
                $response = [
                    'type' => 'danger',
                    'message' => 'Недостаточно прав для выполнения операции',
                    'close' => true
                ];
            } elseif (!$form->message || $form->db == 'false') {
                $response = [
                    'type' => 'danger',
                    'message' => 'Заполните все поля'
                ];
            } else {
                $text = 'База: ' . $form->db . '<br>';
                $text .= 'Email: ' . $form->email . '<br>';
                $text .= $form->message;

                $mail = Yii::$app->mailer->compose()
                    ->setFrom(Yii::$app->params['email']['from'])
                    ->setTo(Yii::$app->params['email']['support'])
                    ->setSubject('Личный кабинет ' . Yii::$app->params['title'] . ' - Новое обращение в поддержку')
                    ->setHtmlBody($text)
                    ->send();

                if ($mail) {
                    $response = [
                        'type' => 'success',
                        'message' => 'Сообщение успешно отправлено'
                    ];
                    $show_form = false;
                } else {
                    $response = [
                        'type' => 'danger',
                        'message' => 'Произошла ошибка при отправке сообщения'
                    ];
                }
            }
        }

        return $this->render('support', [
            'bases' => $bases,
            'response' => $response,
            'user' => $this->user,
            'form' => $form,
            'show_form' => $show_form,
            'profile' => $this->profile
        ]);
    }

    public function actionSessionsChange() {
        $db = Yii::$app->request->get('db');

        $base = $this->client->baseDetails($db);
        if (!$base) throw new \yii\web\NotFoundHttpException();
        $change_available = !$base->group && !$base->test_drives;

        if (!$change_available) {
            $alert = [
                'type' => 'danger',
                'message' => 'Для данной базы невозможно изменить количество сеансов'
            ];
        } else {
            $bases = $this->client->sendRequest(Client::SC, Client::LIST);
            foreach ($bases->ListDB as $row) {
                if ($row->db == $db) {
                    $base = $row;
                    break;
                }
            }

            $future_change = false;
            if ($base->future_change) {
                $base->update_date = date('d.m.Y', strtotime($base->future_change));
                $base->update_count = $base->future_sessions;
                $future_change = true;
            }
        }

        $form = new SessionsForm();

        if ($form->load(Yii::$app->request->post())) {
            $count = (int) $form->count;

            if ($count <= 0) {
                $alert = [
                    'type' => 'danger',
                    'message' => 'Количество сеансов должно быть больше 0',
                    'close' => true
                ];
            } elseif ($count == $base->count_sessions) {
                $alert = [
                    'type' => 'warning',
                    'message' => 'У вас уже установлено данное количество сеансов',
                    'close' => true
                ];
            } elseif ($future_change) {
                $alert = [
                    'type' => 'danger',
                    'message' => 'Для изменения количества сеансов необходимо отменить запрос на уменьшение количества сеансов',
                    'close' => true
                ];
            } else {
                $response = $this->client->sendRequest(Client::SC, Client::SESSIONS_CHANGE, [
                    'name_db' => $db,
                    'count_sessions' => $count
                ]);

                if ($response->result == 'ОК') {
                    $response = 'success';

                    $this->user->saveAction([
                        'type' => 'Изменение количества сеансов',
                        'details' => 'Количество сеансов изменено с ' . $base->count_sessions . ' до ' . $count
                    ]);
                } else {
                    $response = 'fail';
                }
                return $this->redirect(['cloud/sessions-change', 'db' => $db, 'response' => $response]);
            }
        }

        if (isset($_GET['response'])) {
            switch ($_GET['response']) {
                case 'success':
                    $alert = [
                        'type' => 'success',
                        'message' => 'Количество сеансов успешно изменено',
                        'close' => true
                    ];
                    break;
                case 'canceled':
                    $alert = [
                        'type' => 'success',
                        'message' => 'Уменьшение количества сеансов отменено',
                        'close' => true
                    ];
                    break;
                case 'fail':
                    $alert = [
                        'type' => 'danger',
                        'message' => 'Произошла ошибка, попробуйте еще раз',
                        'close' => true
                    ];
                    break;
            }
        }

        return $this->render('sessions-change', [
            'base' => $base,
            'change_available' => $change_available,
            'future_change' => $future_change,
            'form' => $form,
            'alert' => $alert,
            'profile' => $this->profile
        ]);
    }

    public function actionPayment() {
        $form = new PaymentForm();

        $result = $this->client->sendRequest(Client::SC, Client::BUDGET, [
            'date_begin' => (new \DateTime('-3 month'))->format('Ymd'),
            'date_end' => date('Ymd')
        ]);

        $min_sum = ($result->last_sum_pay) ? $result->last_sum_pay*30 : 1200;

        $quick_pay = [$min_sum, $min_sum*3, $min_sum*6];
        $quick_pay_format = [];
        foreach ($quick_pay as $sum) {
            $quick_pay_format[] = [
                'value' => $sum,
                'format_value' => FormatHelper::money($sum)
            ];
        }

        $accounts = $this->client->sendRequest(Client::UPP , Client::PAYMENT);
        if ($accounts && !$accounts->ErrorDescription) {
            $unpaid_account = false;
            foreach ($accounts->list_account as $account) {
                if ($account->saas_dog) {
                    $unpaid_account = true;
                    break;
                }
            }
        }

        if ($form->load(Yii::$app->request->post())) {
            $sum = trim($form->sum);
            $method = trim($form->method);
            $account = trim($form->account);

            if (!$sum || !$method || !in_array($method, ['account', 'card'])) {
                $alert = [
                    'type' => 'danger',
                    'message' => 'Произошла ошибка, попробуйте еще раз',
                    'close' => true,
                ];
            } elseif ($min_sum > $sum) {
                $alert = [
                    'type' => 'danger',
                    'message' => 'Миниамльная сумма оплаты ' . $min_sum . '&#8381;',
                    'close' => true,
                ];
            } else {
                $params = [
                    'source' => 'saas',
                    'code' => $this->user->getCode(),
                    'sum' => $sum
                ];

                $request = new Request('http://sky.is1c.ru/site/hs/getInfo/getcheck?' . http_build_query($params));
                $request->execute();
                $response = json_decode($request->getResponse());

                if ($response->ErrorCode == 200) {
                    $account = $response->CheckNum;

                    if ($method == 'account') {
                        return $this->redirect(['cloud/payment', 'account' => $account]);
                    } elseif ($method == 'card') {
                        $params = [
                            'method' => 'cloud-payment',
                            'email' => $this->user->getEmail(),
                            'account' => $account
                        ];

                        return $this->redirect('https://is1c.ru/api?' . http_build_query($params));
                    }
                } else {
                    $alert = [
                        'type' => 'danger',
                        'message' => 'Произошла ошибка, попробуйте еще раз',
                        'close' => true,
                    ];
                }
            }
        }

        $account = Yii::$app->request->get('account');
        if ($account) {
            $request = new Request('http://sky.is1c.ru/site/hs/getInfo/' . $account . '?source=saas');
            $request->execute();
            $response = json_decode($request->getResponse());
            $code = trim(rawurldecode($response->code));

            if ($code != $this->user->getCode()) throw new \yii\web\NotFoundHttpException();

            $account_details = [
                'payer' => $response->payer,
                'sum' => FormatHelper::money($response->total_sum),
                'number' => $account,
                'date' => date('Ymd')
            ];

            $this->user->saveAction([
                'type' => 'Выписан счета',
                'details' => rawurldecode($account) . ';На сумму ' . $account_details['sum']
            ]);
        }

        $min_sum = FormatHelper::money($min_sum);

        return $this->render('payment', [
            'min_sum' => $min_sum,
            'quick_pay' => $quick_pay_format,
            'account' => $account_details,
            'unpaid_account' => $unpaid_account,
            'form' => $form,
            'alert' => $alert,
            'profile' => $this->profile
        ]);
    }

    public function actionPaymentHistory() {
        $form = new PeriodForm();
        if (isset($_GET['period_start']) && isset($_GET['period_end'])) {
            $period_start = date('Ymd', strtotime($_GET['period_start']));
            $period_end = date('Ymd', strtotime($_GET['period_end']));
        } else {
            $period_start = (new \DateTime('-1 month'))->format('Ymd');
            $period_end = date('Ymd');
        }

        $result = $this->client->sendRequest(Client::SC, Client::BUDGET, [
            'date_begin' => $period_start,
            'date_end' => $period_end,
        ]);

        if (!$result) {
            $response = [
                'type' => 'danger',
                'message' => 'Произошла ошибка при получении истории баланса'
            ];
        } elseif ($result->ErrorDescription) {
            $response = [
                'type' => 'danger',
                'message' => $result->ErrorDescription
            ];
        } elseif (!$result->move_money) {
            $response = [
                'type' => 'warning',
                'message' => 'Нет записей по истории изменения баланса'
            ];
        } else {
            $balance = FormatHelper::money($result->balance);
            $result = $result->move_money;
            $format_result = [];

            $pagination = new Pagination([
                'totalCount' => count($result),
                'defaultPageSize' => 25
            ]);

            $limit = $this->paginationLimit($pagination);
            $payment_history = [];
            foreach ($result as $row) {
                $payment_history[] = [
                    [
                        'label' => 'Сумма',
                        'value' => FormatHelper::money($row->summa)
                    ],
                    [
                        'label' => 'Дата',
                        'value' => date('d.m.Y', strtotime($row->date))
                    ],
                    [
                        'label' => 'Операция',
                        'value' => ($row->summa < 0) ? rawurldecode($row->move_type) : 'Пополнение баланса'
                    ],
                    [
                        'label' => 'База',
                        'value' => $row->db_name,
                    ],
                    [
                        'label' => 'Тариф',
                        'value' => rawurldecode($row->tarif)
                    ]
                ];
            }
        }

        $period_start = date('d.m.Y', strtotime($period_start));
        $period_end = date('d.m.Y', strtotime($period_end));

        return $this->render('payment-history', [
            'result' => $payment_history,
            'balance' => $balance,
            'period_start' => $period_start,
            'period_end' => $period_end,
            'pagination' => $pagination,
            'form' => $form,
            'alert' => $alert,
            'profile' => $this->profile
        ]);
    }

    public function actionPaymentResult() {
        $order = htmlspecialchars(Yii::$app->request->get('order'));
        if (!$order) return $this->redirect(['cloud/index']);

        $params = [
            'code' => $this->user->getCode(),
            'orderNum' => $order
        ];

        $request = new Request('http://sky.is1c.ru/site/hs/getInfo/getSumByOrderNum?' . http_build_query($params));
        $request->execute();
        $response = json_decode($request->getResponse());

        if ($response->result == 'false') return $this->redirect(['cloud/index']);
        $sum = FormatHelper::money($response->result);

        $this->user->saveAction([
            'type' => 'Пополнение баланса',
            'details' => 'На сумму ' . $sum
        ]);

        return $this->render('payment-result', [
            'sum' => $sum,
            'profile' => $this->profile
        ]);
    }

    public function actionIpAccess() {
        $db = htmlspecialchars(Yii::$app->request->get('db'));
        $db_exist = $this->client->baseExist($db);
        if (!$db || !$db_exist) throw new \yii\web\NotFoundHttpException();

        $form = new IpAddressForm();

        $params = [
            'type' => 'list_ip',
            'base' => $db
        ];

        $request = new Request('http://10.10.10.12:81/iis.php?' . http_build_query($params));
        $request->execute();
        $response = $request->getResponse();

        $ip_address = [];
        if ($response != '') {
            $count = 0;
            $ip_address = explode(',', $response);
            $ip_address_result = [];
            foreach ($ip_address as $ip) {
                $ip_address_result[] = [
                    'id' => $count,
                    'ip' => $ip
                ];
                $count++;
            }
        } else {
            $ip_address_result = false;
        }

        if ($form->load(Yii::$app->request->post())) {
            $ip = htmlspecialchars(trim($form->ip));
            $valid = filter_var($ip, FILTER_VALIDATE_IP);

            if (!$ip || !$valid) {
                $alert = [
                    'type' => 'danger',
                    'message' => 'Введен некорректный IP адрес'
                ];
            } elseif (in_array($ip, $ip_address)) {
                $alert = [
                    'type' => 'warning',
                    'message' => 'Данный IP адрес уже добавлен в ограничение'
                ];
            } else {
                $params = [
                    'type' => 'add_ip',
                    'base' => $db,
                    'ip' => $ip
                ];

                $request = new Request('http://10.10.10.12:81/iis.php?' . http_build_query($params));
                $request->execute();

                $this->user->saveAction([
                    'type' => 'Ограничение IP адресов',
                    'details' => 'IP адрес ' . $ip . ' добавлен в ограничение к базе ' . $db
                ]);

                return $this->redirect(['cloud/ip-access', 'db' => $db, 'response' => 'add-success']);
            }

            $form->ip = NULL;
        }

        if (Yii::$app->request->get('response')) {
            switch (Yii::$app->request->get('response')) {
                case 'add-success':
                    $alert = [
                        'type' => 'success',
                        'message' => 'IP адрес успешно добавлен в ограничение'
                    ];
                    break;
                case 'delete-success':
                    $alert = [
                        'type' => 'success',
                        'message' => 'IP адрес успешно удален из ограничения'
                    ];
                    break;
            }
        }

        return $this->render('ip-access', [
            'db' => $db,
            'ip_address' => $ip_address_result,
            'user_ip' => $this->user->getIp(),
            'form' => $form,
            'alert' => $alert
        ]);
    }

    public function actionPrice() {
        return $this->render('price', [
            'profile' => $this->profile
        ]);
    }

    public function actionFaq() {
        return $this->render('faq', [
            'profile' => $this->profile
        ]);
    }

    private function createBaseNav($dbase, $is_franch) {
        return [
            'ul_class' => 'nav-list',
            'set_active' => false,
            'items' => [
                [
                    'label' => 'Активные сеансы',
                    'route' => ['cloud/session', 'db' => $dbase->db]
                ],
                [
                    'label' => 'Изменить количество сеансов',
                    'route' => ['cloud/sessions-change', 'db' => $dbase->db],
                    'hidden' => ($dbase->company_group || $dbase->test_drive)
                ],
                [
                    'label' => 'Заморозить базу',
                    'route' => [
                        'modal' => true,
                        'src' => '#frozen-modal-' . $dbase->id
                    ],
                    'hidden' => $dbase->test_drive
                ],
                [
                    'label' => 'История входов в базу',
                    'route' => ['cloud/history', 'db' => $dbase->db]
                ],
                [
                    'label' => 'Резервные копии',
                    'route' => ['cloud/backup', 'db' => $dbase->db]
                ],
                [
                    'label' => 'Обновления конфигураций',
                    'route' => ['cloud/update', 'db' => $dbase->db]
                ],
                [
                    'label' => 'Ограничения по IP адресам',
                    'route' => ['cloud/ip-access', 'db' => $dbase->db]
                ],
                [
                    'label' => 'Переместить базу данных',
                    'route' => [
                        'modal' => true,
                        'src' => '#movedb-' . $dbase->db,
                    ],
                    'hidden' => !$is_franch
                ],
                [
                    'line' => true,
                ],
                [
                    'label' => 'Удалить базу',
                    'class' => 'text-danger',
                    'route' => [
                        'modal' => true,
                        'src' => '#delete-modal',
                        'data' => [
                            'name' => 'db-delete',
                            'value' => $dbase->db
                        ]
                    ],
                ]
            ]
        ];
    }
}
