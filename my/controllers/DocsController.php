<?php
namespace my\controllers;

use Yii;
use yii\web\Controller;
use yii\db\Query;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\data\Pagination;
use my\components\Client;
use my\models\Rating;
use my\models\forms\PeriodForm;
use my\models\forms\RatingForm;
use common\models\helpers\FormatHelper;

class DocsController extends SiteController {
    public function actionIndex() {
        return $this->render('index', [
            'profile' => $this->profile
        ]);
    }

    public function actionPayment() {
        $result = $this->client->sendRequest(Client::UPP , Client::PAYMENT);

        if (!$result) {
            $response = [
                'type' => 'danger',
                'message' => 'Произошла ошибка при получении счетов'
            ];
        } elseif ($result->ErrorDescription) {
            $response = [
                'type' => 'danger',
                'message' => $result->ErrorDescription
            ];
        } elseif (!$result->list_account) {
            $response = [
                'type' => 'success',
                'message' => 'У вас нет неоплаченных счетов'
            ];
        } else {
            $result = $result->list_account;
            $format_result = [];

            foreach ($result as $row) {
                $format_result[] = [
                    'number' => $row->number,
                    'date' => date('d.m.Y', strtotime($row->date)),
                    'sum' => FormatHelper::money($row->sum),
                    'content' => urldecode($row->content),
                    'card' => $row->card,
                    'payment_url' => 'https://is1c.ru/payment/?account=' . $row->number,
                    'status_edo' => $row->status_edo,
                    'download' => ($row->edo) ? false : Url::to(['handler/handler', 'method' => 'get-account', 'account' => $row->number, 'date' => $row->date]),
                ];
            }
        }

        return $this->render('payment', [
            'result' => $format_result,
            'response' => $response,
            'profile' => $this->profile
        ]);
    }

    public function actionSheet() {
        $form = new PeriodForm();
        $form_rating = new RatingForm();

        if (isset($_GET['period_start']) && isset($_GET['period_end'])) {
            $period_start = date('Ymd', strtotime($_GET['period_start']));
            $period_end = date('Ymd', strtotime($_GET['period_end']));
        } else {
            $date = new \DateTime('-1 month');
            $period_start = $date->format('Ymd');
            $period_end = date('Ymd');
        }

        $result = $this->client->sendRequest(Client::UPP, Client::DOCS, [
            'date_begin' => $period_start,
            'date_end' => $period_end
        ]);

        if (!$result) {
            $response = [
                'type' => 'danger',
                'message' => 'Произошла ошибка при получении документов'
            ];
        } elseif ($result->ErrorDescription) {
            $response = [
                'type' => 'danger',
                'message' => $result->ErrorDescription
            ];
        } elseif (!$result->list_works) {
            $response = [
                'type' => 'warning',
                'message' => 'За указанный период документы не найдены'
            ];
        } else {
            $filtered_result = [];
            $result = $result->list_works;

            $pagination = new Pagination([
                'totalCount' => count($result),
                'defaultPageSize' => 15
            ]);

            $limit = $this->paginationLimit($pagination);

            $result_number = [];
            for ($i = $pagination->offset; $i < $limit; $i++) {
                $filtered_result[$i]['id'] = $i;
                $filtered_result[$i]['date'] = date('d.m.Y', strtotime($result[$i]->date));
                $filtered_result[$i]['number'] = $result[$i]->number;
                $filtered_result[$i]['name'] = $result[$i]->FIO;
                $filtered_result[$i]['sign'] = $result[$i]->sign;
                $result_number[$i] = $result[$i]->number;
                if ($result[$i]->works_array) {
                    for ($j = 0; $j < count($result[$i]->works_array); $j++) {
                        $filtered_result[$i]['works'][$j]['description'] = urldecode($result[$i]->works_array[$j]->description);
                        $filtered_result[$i]['works'][$j]['date_start'] = date('d.m.Y H:i', strtotime($result[$i]->works_array[$j]->time_begin));
                        $filtered_result[$i]['works'][$j]['date_end'] = date('d.m.Y H:i', strtotime($result[$i]->works_array[$j]->time_end));
                        $filtered_result[$i]['works'][$j]['duration'] = $result[$i]->works_array[$j]->count . ' ' . FormatHelper::hour($result[$i]->works_array[$j]->count);
                        $filtered_result[$i]['works'][$j]['type'] = $result[$i]->works_array[$j]->method;
                    }
                }
            }

            $query_rating = Rating::find()
                ->select(['number', 'rating'])
                ->where(['number' => $result_number])
                ->andWhere(['type' => 3])
                ->all();

            $rating_number = [];
            $rating_value = [];
            for ($j = 0; $j < count($query_rating); $j++) {
                $rating_number[$j] = $query_rating[$j]->number;
                $rating_value[$query_rating[$j]->number] = $query_rating[$j]->rating;
            }

            for ($i = $pagination->offset; $i < $limit; $i++) {
                if (in_array($filtered_result[$i]['number'], $result_number)) {
                    $filtered_result[$i]['rating'] = $rating_value[$filtered_result[$i]['number']];
                }
            }

            $result_count = $pagination->totalCount;
        }

        $period_start = date('d.m.Y', strtotime($period_start));
        $period_end = date('d.m.Y', strtotime($period_end));

        if ($form_rating->load(Yii::$app->request->post())) {
            if ($this->user->permission(1)) {
                $query = (new Query())->createCommand()
                    ->insert('rating', [
                        'user_id' => $this->user->getID(),
                        'type' => 3,
                        'number' => $form_rating->number,
                        'rating' => $form_rating->rating,
                        'date' => date('Y-m-d H:i:s', strtotime($form_rating->date)),
                        'message' => $form_rating->message
                    ])
                    ->execute();

                $json['rating'][] = [
                    'number' => $form_rating->number,
                    'rating' => $form_rating->rating,
                    'year' => date('Y', strtotime($form_rating->date)),
                    'comment' => $form_rating->message,
                    'type' => 'specialist'
                ];
                $json = json_encode($json);
                $this->client->sendPostRequest(Client::UPP_IN, Client::RATING, $json);

                if ($query) {
                    $rating_status = 'rating-success';

                    $details = 'Номер: ' . $form_rating->number . ';';
                    $details .= 'Оценка: ' . $form_rating->rating . ';';
                    if ($form_rating->message) $details .= 'Комментарий: ' . $form_rating->message;

                    $this->user->saveAction([
                        'type' => 'Оценка работы специалиста 1С',
                        'details' => $details
                    ]);

                    if ($form_rating->rating != 5) {
                        $text = 'Email: ' . $this->user->getEmail(). '<br>';
                        $text .= 'Код в УПП: ' . $this->user->getCode() . '<br>';
                        $text .= 'Номер документа: ' . $form_rating->number . '<br>';
                        $text .= 'Оценка: ' . $form_rating->rating . '<br>';
                        $text .= 'Комментарий: ' . $form_rating->message;

                        Yii::$app->mailer->compose()
                            ->setFrom(Yii::$app->params['email']['from'])
                            ->setTo(Yii::$app->params['email']['admin'])
                            ->setSubject(Yii::$app->params['title'] . ' - Оценка работы специалиста 1С')
                            ->setHtmlBody($text)
                            ->send();
                    }
                } else {
                    $rating_status = 'rating-fail';
                }
            } else {
                $rating_status = 'rating-fail';
            }

            $this->redirect(Url::to(['docs/index', 'response' => $rating_status]));
        }

        if ($_GET['response']) {
            switch ($_GET['response']) {
                case 'success':
                    $response = [
                        'type' => 'success',
                        'message' => 'Документ успешно подписан',
                        'close' => true
                    ];
                    break;
                case 'fail':
                    $response = [
                        'type' => 'danger',
                        'message' => 'Произошла ошибка при подписании документа',
                        'close' => true
                    ];
                    break;
                case 'rating-success':
                    $response = [
                        'type' => 'success',
                        'message' => 'Оценка успешно сохранена',
                        'close' => true
                    ];
                    break;
                case 'rating-fail':
                    $response = [
                        'type' => 'danger',
                        'message' => 'Произошла ошибка при сохранении оценки',
                        'close' => true
                    ];
                    break;
            }
        }

        return $this->render('sheet', [
            'result' => $filtered_result,
            'form' => $form,
            'form_rating' => $form_rating,
            'period_start' => $period_start,
            'period_end' => $period_end,
            'result_count' => $result_count,
            'pagination' => $pagination,
            'response' => $response,
            'profile' => $this->profile
        ]);
    }

    public function actionAct() {
        $form = new PeriodForm();

        if (isset($_GET['period_start']) && isset($_GET['period_end'])) {
            $period_start = date('Ymd', strtotime($_GET['period_start']));
            $period_end = date('Ymd', strtotime($_GET['period_end']));
        } else {
            $date = new \DateTime('-1 month');
            $period_start = $date->format('Ymd');
            $period_end = date('Ymd');
        }

        $result = $this->client->sendRequest(Client::UPP, Client::ACT, [
            'date_begin' => $period_start,
            'date_end' => $period_end
        ]);

        if (!$result) {
            $response = [
                'type' => 'danger',
                'message' => 'Произошла ошибка при получении актов'
            ];
        } elseif ($result->ErrorDescription) {
            $response = [
                'type' => 'danger',
                'message' => $result->ErrorDescription
            ];
        } elseif (!$result->list_act) {
            $response = [
                'type' => 'warning',
                'message' => 'За указанный период акты не найдены'
            ];
        } else {
            $format_result = [];

            foreach ($result->list_act as $row) {
                $format_result[] = [
                    'number' => $row->number,
                    'date' => date('d.m.Y', strtotime($row->date)),
                    'sum' => FormatHelper::money($row->sum),
                    'download' => ($row->edo) ? false : Url::to(['handler/handler', 'method' => 'get-act', 'number' => $row->number, 'date' => date('Ymd', strtotime($row->date))]),
                    'status_edo' => $row->status_edo,
                    'appendix' => Url::to(['handler/handler', 'method' => 'get-appendix', 'number' => $row->number, 'date' => date('Ymd', strtotime($row->date))])
                ];
            }
        }

        $period_start = date('d.m.Y', strtotime($period_start));
        $period_end = date('d.m.Y', strtotime($period_end));

        return $this->render('act', [
            'result' => $format_result,
            'form' => $form,
            'period_start' => $period_start,
            'period_end' => $period_end,
            'response' => $response,
            'profile' => $this->profile
        ]);
    }
}