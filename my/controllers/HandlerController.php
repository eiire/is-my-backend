<?php
namespace my\controllers;

use Yii;
use yii\web\Controller;
use yii\db\Query;
use yii\base\Security;
use my\components\Client;
use common\models\Request;
use my\components\User;
use my\models\users\Users;
use my\models\users\UsersCode;
use my\models\users\UsersConfirm;
use my\models\users\UsersSession;
use my\models\users\UsersHistory;
use my\models\users\UsersRecovery;
use my\models\news\NewsViewed;
use common\models\helpers\FormatHelper;
use my\models\sms;
use my\models\TwoFactor;

class HandlerController extends Controller {
    private $user;
    private $client;
    private $ip = ['127.0.0.1', '10.10.10.21', '10.10.10.20', '10.10.10.51', '10.10.10.52', '192.168.1.207', '192.168.1.208', '192.168.1.210', '192.168.2.6', '192.168.2.77', '37.192.242.106', '37.192.242.106', '37.192.242.74', '91.195.179.27', '195.211.6.20', '195.211.6.21', '192.168.1.219', '10.10.10.90'];

    public function beforeAction($action) {
        $this->user = new User();
        $this->client = new Client();
        $this->client->id = $this->user->getCode();

        return parent::beforeAction($action);
    }

    public function actionHandler() {
        $this->layout = false;
        $method = Yii::$app->request->get('method');
        $result = false;

        // Удаление сеанса
        if ($method == 'delete-session') {
            $db = Yii::$app->request->get('db');
            $guid = Yii::$app->request->get('guid');
            $ns = Yii::$app->request->get('ns');
            $user = Yii::$app->request->get('user');

            if ($db && $guid) {
                $result = true;

                if ($this->user->permission(1)) {
                    $response = $this->client->sendRequest(Client::SC, Client::REMOVE_SESSION, [
                        'name_db' => $db,
                        'GUID' => $guid
                    ]);

                    if ($response == true) {
                        $response = 'success';

                        $details = 'База: ' . $db . ';';
                        $details .= 'Номер сеанса: ' . $ns . ';';
                        $details .= 'Пользователь: ' . $user;

                        $this->user->saveAction([
                            'type' => 'Удаление сеанса',
                            'details' => $details
                        ]);
                    } else {
                        $response = 'fail';
                    }
                } else {
                    $response = 'fail';
                }

                return $this->redirect(['cloud/session', 'db' => $db, 'response' => $response]);
            }
        // Создание бэкапа
        } elseif ($method == 'create-backup') {
            $db = Yii::$app->request->get('db');

            if ($db) {
                $result = true;

                if ($this->client->backupCount($db) < 2 && $this->user->permission(1)) {
                    $response = $this->client->sendRequest(Client::SC, Client::CREATE_BACKUP, [
                        'name_db' => $db
                    ]);

                    if (!$response->ErrorDescription) {
                        $response = 'create-success';

                        $this->user->saveAction([
                            'type' => 'Создание резервной копии',
                            'details' => 'База: ' . $db
                        ]);
                    } else {
                        $response = 'create-fail';
                    }
                } else {
                    $response = 'create-fail'; 
                }

                return $this->redirect(['cloud/backup', 'db' => $db, 'response' => $response]);
            }
        // Удаление бэкапа
        } elseif ($method == 'delete-backup') {
            $db = Yii::$app->request->get('db');
            $backup = Yii::$app->request->get('backup');

            if ($backup) {
                $result = true;
                if ($this->client->userBackup($backup) && $this->user->permission(1)) {
                    $response = $this->client->sendRequest(Client::SC, Client::DELETE_BACKUP, [
                        'name_db' => $db,
                        'name_backup' => $backup,
                    ]);

                    if (!$response->ErrorDescription) {
                        $response = 'delete-success';

                        $this->user->saveAction([
                            'type' => 'Удаление резервной копии',
                            'details' => 'База: ' . $db
                        ]);
                    } else {
                        $response = 'delete-fail';
                    }
                } else {
                    $response = 'delete-fail';
                }

                return $this->redirect(['cloud/backup', 'db' => $db, 'response' => $response]);
            }
        // Выход на всех устройствах
        } elseif ($method == 'clear-sessions') {
            $result = true;

            if ($this->user->permission(1)) {
                $response = $this->user->clearUserSession();
                $response = ($response) ? 'success' : 'fail';
            } else {
                $response = 'fail';
            }

            return $this->redirect(['site/history', 'response' => $response]);
        // Добавление нового пользователя
        } elseif ($method == 'add-user') {
            $new_user = true;
            $create_code = false;
            $create_recovery = false;
            $email = Yii::$app->request->get('email');
            $code = trim(Yii::$app->request->get('account'));

            if (in_array($this->user->getIp(), $this->ip) && $email && $code) {
                $this->user->login = $email;
                $db = Yii::$app->db;

                $user_exist = Users::find()
                    ->select('id')
                    ->where(['login' => $email, 'confirm' => 1])
                    ->one();

                if (!$user_exist) { // Если пользователь не найден
                    $query = $db->createCommand()
                        ->insert('users', [
                            'login' => $email,
                            'confirm' => 1,
                            'role' => 1
                        ])
                        ->execute();

                    $user_id = $db->getLastInsertID();
                    $create_code = true;
                    $create_recovery = true;
                } else { // Если пользователь найден
                    $user_id = $user_exist->id;

                    $code_exist = UsersCode::find()
                        ->select('id')
                        ->where(['user_id' => $user_id, 'code' => $code])
                        ->one();

                    if ($code_exist) { // Если найден указанный код УПП
                        $query = Users::find()
                            ->select(['id', 'password'])
                            ->where(['login' => $email])
                            ->one();

                        $user_id = $query->id;
                        if ($query->password == NULL) { // Проверяем заходил ли пользователь
                            $create_recovery = true;
                        } else { // Если пользователь заходил
                            $new_user = false;
                        }
                    } else { // Если указанный код УПП не найден
                        $create_code = true;
                    }
                }

                if ($create_code) {
                    $client = new Client();
                    $client->id = $code;
                    $details = $client->sendRequest(Client::UPP, Client::CLIENT);
                    $details->full_name = urldecode($details->full_name);

                    $company = FormatHelper::replace($details->full_name, [
                        'Индивидуальный предприниматель' => 'ИП',
                        'Общество с ограниченной ответственностью' => 'ООО'
                    ]);

                    (new Query())->createCommand()
                        ->insert('users_code', [
                            'user_id' => $user_id,
                            'code' => $code,
                            'name' => $company,
                            'inn' => $details->inn,
                            'cashbox' => $details->kassa,
                            'active' => 0
                        ])
                        ->execute();
                }

                $generated_code = (new Security())->generateRandomString();

                if ($create_recovery) {
                    UsersRecovery::deleteAll(['user_id' => $user_id]);

                    $date = new \DateTime('+6 month');

                    (new Query())->createCommand()
                        ->insert('users_recovery', [
                            'user_id' => $user_id,
                            'code' => $generated_code,
                            'expire' => $date->format('Y-m-d H:i:s')
                        ])
                        ->execute();
                }

                if ($new_user) {
                    $url = 'https://my.is1c.ru/change?code=' . $generated_code;
                    $text = 'Добрый день!<br><br>';
                    $text .= 'Предлагаем Вам воспользоваться Личным кабинетом ИнфоСофт <a href="https://my.is1c.ru/" target="_blank">https://my.is1c.ru/</a><br><br>';
                    $text .= 'В нём вы сможете:<br>';
                    $text .= '- планировать работы специалиста 1С<br>';
                    $text .= '- создать заявку на Линию консультаций<br>';
                    $text .= '- создать заявку на обновление и подключение сервисов<br>';
                    $text .= '- подписать ЛУВР<br>';
                    $text .= '- просмотреть выставленные счета<br>';
                    $text .= '- получить максимальную информацию о Ваших базах в Облаке 1С ИнфоСофт.<br><br>';
                    $text .= 'Ваша учётная запись: ' . $email . '<br>';
                    $text .= 'Перейдя по ссылке ниже Вы сможете задать пароль для своей учётной записи в Личном кабинете.<br>';
                    $text .= '<a href="' . $url . '">' . $url . '</a>';
                } else {
                    $text = 'Добрый день!<br><br>';
                    $text .= 'Для входа в Личный кабинет ИнфоСофт Вы можете перейти по ссылке <a href="https://my.is1c.ru/" target="_blank">https://my.is1c.ru/</a><br>';
                    $text .= 'Ваша учётная запись: ' . $email . '<br><br>';
                    $text .= 'Если Вам не удается войти в аккаунт, вы можете перейти на страницу восстановления пароля <a href="https://my.is1c.ru/recovery?email=' . $email . '" target="_blank">https://my.is1c.ru/recovery</a>';
                }

                $mail = Yii::$app->mailer->compose('layouts/template', ['content' => $text])
                    ->setFrom(Yii::$app->params['email']['from'])
                    ->setTo($email)
                    ->setSubject('Личный кабинет ИнфоСофт')
                    ->send();

                $result = ($mail) ? 'true' : 'false';
            }
        // Выгрузка истории входов в CSV
        } elseif ($method == 'save-activity') {
            $db = Yii::$app->request->get('db');
            $period_start = Yii::$app->request->get('period_start');
            $period_end = Yii::$app->request->get('period_end');

            if ($db && $period_start && $period_end) {
                $result = true;
                $file_name = $db . '_' . $period_start . '_' . $period_end . '.csv';
                $file_path = __DIR__ . '/../web/upload/tmp/';

                $activity = $this->client->sendRequest(Client::SC, Client::HISTORY, [
                    'name_db' => $db, 
                    'date_begin' => date('Ymd', strtotime($period_start)), 
                    'date_end' => date('Ymd', strtotime($period_end))
                ]);

                if (!$activity->ErrorDescription) {
                    $str = "\xEF\xBB\xBF"; // BOM
                    foreach ($activity->InfoDB->login_history as $row) {
                        $str .= $row->user . ';' . date('d.m.Y H:i', strtotime($row->date)) . "\n";
                    }
                    
                    $file = fopen($file_path . $file_name, 'w+');
                    fwrite($file, $str);
                    fclose($file);
                    
                    if (file_exists($file_path . $file_name)) {
                        Yii::$app->response->SendFile($file_path . $file_name);
                        $response = 'success';
                    } else {
                        $response = 'fail';
                    }
                }
            }
        // Получить счет на оплату
        } elseif ($method == 'get-account') {
            $account = Yii::$app->request->get('account');
            $date = Yii::$app->request->get('date');

            if ($account && $date) {
                $result = true;
                $file_name = substr($account, strpos($account, '-') + 1);
                $file_name .= '_' . date('d.m.Y', strtotime($date)) . '.pdf';
                $file_path = __DIR__ . '/../web/upload/tmp/';

                if (file_exists($file_path . $file_name)) {
                    Yii::$app->response->SendFile($file_path . $file_name);
                } else {
                    $date = date('Ymd', strtotime($date));
                    $result = $this->client->sendRequest(Client::UPP, Client::GET_ACCOUNT, [
                        'number_acc' => $account,
                        'date_acc' => $date 
                    ], false);

                    $file = fopen($file_path . $file_name, 'w+');
                    fwrite($file, $result);
                    fclose($file);

                    if (file_exists($file_path . $file_name)) {
                        Yii::$app->response->SendFile($file_path . $file_name);
                    }
                }
            }
        // Получить акт
        } elseif ($method == 'get-act' || $method == 'get-appendix') {
            $number = Yii::$app->request->get('number');
            $date = Yii::$app->request->get('date');

            if ($number && $date) {
                $result = true;
                $file_name = substr($number, strpos($number, '-') + 1);
                $file_name .= '_' . date('d.m.Y', strtotime($date)) . '.pdf';
                $file_path = __DIR__ . '/../web/upload/tmp/';

                if (file_exists($file_path . $file_name)) {
                    Yii::$app->response->SendFile($file_path . $file_name);
                } else {
                    if ($method == 'get-act') {
                        $request_method = Client::GET_ACT;
                        $file_name = '1_' . $file_name;
                    } else {
                        $request_method = Client::GET_APPENDIX;
                        $file_name = '2_' . $file_name;
                    }

                    $result = $this->client->sendRequest(Client::UPP, $request_method, [
                        'number_acc' => $number,
                        'date_acc' => $date 
                    ], false);

                    $file = fopen($file_path . $file_name, 'w+');
                    fwrite($file, $result);
                    fclose($file);

                    if (file_exists($file_path . $file_name)) {
                        Yii::$app->response->SendFile($file_path . $file_name);
                    }
                }
            }
        // Получить список работ для выбранного ПО
        } elseif ($method == 'specialist-works') {
            $soft = Yii::$app->request->get('soft');

            if ($soft) {
                $result = true;

                $response = $this->client->sendRequest(Client::UPP, Client::PLANNING, [
                    'get_oper' => 'get_tw',
                    'get_sp' => $soft
                ], false);

                $result = $response;
            }
        // Подписать документ
        } elseif ($method == 'doc-signature') {
            $number = Yii::$app->request->get('number');
            $date = Yii::$app->request->get('date');

            if ($number && $date) {
                if ($this->user->permission(1)) {
                    $result = true;
                    $response = $this->client->sendRequest(Client::UPP, Client::DOC_SIGNATURE, [
                        'number_doc' => $number,
                        'date_doc' => $date
                    ]);

                    if ($response) {
                        $response = 'success';
                        $this->user->saveAction([
                            'type' => 'Подписание документа',
                            'details' => 'Полписан документ ' . $number
                        ]);
                    } else {
                        $response = 'fail';
                    }

                    return $this->redirect(['docs/index', 'response' => $response]);
                } else {
                    $response = 'fail';
                }
            }
        } elseif ($method == 'sms') {
            $date = new \DateTime('-50 seconds');
            $user_id = Yii::$app->session->get('user_id');

            $query = UsersConfirm::find()
                ->select('id')
                ->where(['user_id' => $user_id])
                ->andWhere(['<', 'date', $date->format('Y-m-d H:i:s')])
                ->one();

            if (!$query) return $this->redirect(['auth/confirm', 'response' => 'fail']);

            $query = Users::find()
                ->select('login')
                ->where(['id' => $user_id])
                ->one();

            if (!$query) return $this->redirect(['auth/confirm', 'response' => 'fail']);

            UsersConfirm::deleteAll(['user_id' => $user_id]);

            $code = $this->user->generateCode(10000, 99999);

            $sms_phone = '7' . $query->login;
            $sms_message = 'Код подтверждения: ' . $code;
            $sms = (new SMS())->sendSms($sms_phone, $sms_message);

            if ($sms) {
                (new Query())->createCommand()
                    ->insert('users_confirm', ['user_id' => $user_id, 'code' => $code, 'date' => date('Y-m-d H:i:s')])
                    ->execute();

                return $this->redirect(['auth/confirm', 'response' => 'success']);
            } else {
                return $this->redirect(['auth/confirm', 'response' => 'fail']);
            }

            $result = true;
        } elseif ($method == 'client-info') {
            $code = Yii::$app->request->get('code');

            if (in_array($this->user->getIp(), $this->ip) && $code) {
                $users_code = UsersCode::find()
                    ->select('user_id')
                    ->where(['code' => $code])
                    ->all();

                if ($users_code) {
                    $users_ids = [];
                    foreach ($users_code as $user) {
                        $users_ids[] = $user->user_id;
                    }

                    $users = Users::find()
                        ->where(['id' => $users_ids])
                        ->all();

                    $logins = [];
                    foreach ($users as $user) {
                        $logins[] = $user->login;
                    }

                    $history = UsersHistory::find()
                        ->where(['login' => $logins])
                        ->all();

                    $history_result = [];
                    foreach ($history as $row) {
                        $history_result[$row->login][] = [
                            'ip' => $row->ip,
                            'browser' => $row->browser,
                            'date' => $row->date,
                            'success' => ($row->success) ? 'true' : 'false'
                        ];
                    }

                    $data = [];
                    foreach ($users as $user) {
                        $temp = [];
                        $temp = [
                            'id' => $user->id,
                            'login' => $user->login,
                            'contact_phone' => $user->contact_phone,
                            'contact_person' => $user->contact_person,
                            'name' => $user->name,
                            'inn' => $user->inn,
                            'cashbox' => ($user->cashbox == 1) ? 'true' : 'false',
                            'entry' => ($user->password == NULL) ? 'false' : 'true',
                        ];

                        if ($history_result[$user->login]) {
                            for ($i = 0; $i < count($history_result[$user->login]); $i++) {
                                $temp['history'][] = $history_result[$user->login][$i];
                            }
                        } else {
                            $temp['history'] = [];
                        }

                        $data['users'][] = $temp;
                    }

                    $result = json_encode($data, JSON_UNESCAPED_UNICODE);
                } else {
                    $result = json_encode(['error' => 'true']);
                }
            }
        } elseif ($method == 'emails-by-code') {
            $code = htmlspecialchars(Yii::$app->request->get('code'));

            if ($code) {
                $users_code = UsersCode::find()
                    ->select('user_id')
                    ->where(['code' => $code])
                    ->all();

                if ($users_code) {
                    $ids = [];
                    foreach ($users_code as $user) {
                        $ids[] = $user->user_id;
                    }

                    $users = Users::find()
                        ->select('login')
                        ->where(['id' => $ids])
                        ->all();

                    $emails = [];
                    foreach ($users as $user) {
                        $emails[] = $user->login;
                    }

                    $result = [
                        'result' => true,
                        'emails' => $emails
                    ];
                } else {
                    $result['result'] = false;
                }

                $result = json_encode($result, JSON_UNESCAPED_UNICODE);
            }
        // Заморозка/разморозка базы
        } elseif ($method == 'frozen') {
            $db = Yii::$app->request->get('db');
            
            if ($this->user && $db) {
                $base = $this->client->baseDetails($db);
                $frozen = ($base->frozen);
                $defrost_date = strtotime('+7 days', strtotime($base->date_frozen));
                $can_request = ($frozen) ? time() > $defrost_date : true;

                if ($can_request) {
                    $response = $this->client->sendRequest(Client::SC, Client::FROZEN, [
                        'name_db' => $db,
                        'frozen' => !$frozen
                    ]);

                    if ($response->result == 'ОК') {
                        $response = ($frozen) ? 'defrost' : 'frozen';

                        $this->user->saveAction([
                            'type' => ($frozen) ? 'База разморожена успешно' : 'База заморожена успешно'
                        ]);
                    } else {
                        $response = 'fail';
                    }
                } else {
                    $response = 'fail';
                }

                return $this->redirect(['cloud/index', 'response' => $response]);
            }
        // Отменить изменение количества сеансов
        } elseif ($method == 'sessions-change-canceled') {
            $db = Yii::$app->request->get('db');

            if ($db) {
                $response = $this->client->sendRequest(Client::SC, Client::SESSIONS_CHANGE, [
                    'name_db' => $db,
                    'cancelled' => 'true'
                ]);

                if ($response->result == 'ОК') {
                    $response = 'success';
                    $this->user->saveAction([
                        'type' => 'Отмена изменений количества сеансов'
                    ]);
                } else {
                    $response = 'fail';
                }

                return $this->redirect(['cloud/sessions-change', 'db' => $db, 'response' => $response]);
            }
        } elseif ($method == 'set-code') {
            $id = (int) htmlspecialchars(Yii::$app->request->get('id'));

            if ($id) {
                UsersCode::updateAll(['active' => 0], ['user_id' => $this->user->getID()]);
                UsersCode::updateAll(['active' => 1], ['id' => $id]);
                return $this->redirect(['site/index']);
            }
        } elseif ($method == 'delete-ip') {
            $db = Yii::$app->request->get('db');
            $ip = Yii::$app->request->get('ip');

            if ($ip && $db) {
                if ($this->client->baseExist($db)) {
                    $params = [
                        'type' => 'list_ip',
                        'base' => $db
                    ];

                    $request = new Request('http://10.10.10.12:81/iis.php?' . http_build_query($params));
                    $request->execute();
                    $response = $request->getResponse();

                    $ip_address = explode(',', $response);

                    if (in_array($ip, $ip_address)) {
                        $params = [
                            'type' => 'del_ip',
                            'base' => $db,
                            'ip' => $ip
                        ];

                        $request = new Request('http://10.10.10.12:81/iis.php?' . http_build_query($params));
                        $request->execute();

                        $this->user->saveAction([
                            'type' => 'Ограничение IP адресов',
                            'details' => 'IP адрес ' . $ip . ' удален из ограничения к базе ' . $db
                        ]);

                        return $this->redirect(['cloud/ip-access', 'db' => $db, 'response' => 'delete-success']);
                    }
                }
            }
        // Удалить базу
        } elseif ($method == 'delete-db') {
            $db = htmlspecialchars(Yii::$app->request->get('delete-db-name'));

            if ($db) {
                $params = [
                    'name_db' => $db,
                    'email' => $this->user->getEmail()
                ];

                $response = $this->client->sendRequest(Client::SC, Client::DELETE_DB, $params);

                if ($response->delete_db) return $this->redirect(['cloud/index', 'response' => 'base-deleted']);
            }

            return $this->redirect(['cloud/index', 'response' => 'fail']);
        } elseif ($method == 'news-viewed') {
            $id = Yii::$app->request->get('id');

            if ($id) {
                NewsViewed::deleteAll(['user_id' => $this->user->getID(), 'news_id' => $id]);

                (new Query())->createCommand()
                    ->insert('news_viewed', [
                        'user_id' => $this->user->getID(),
                        'news_id' => $id
                    ])
                    ->execute();
            }
        } elseif ($method == 'move-db') {
            $current_group_name = htmlspecialchars(trim(Yii::$app->request->get('current_group_name')));
            $name_db = urldecode(htmlspecialchars(trim(Yii::$app->request->get('name_db'))));
            $new_group_name = htmlspecialchars(trim(Yii::$app->request->POST()['CreateCompanyGroupForm']['group_name']));
            $params = [
                'new_group_name' => $new_group_name,
                'group_name' => $current_group_name,
                'name_db' => $name_db,
            ];
            $response = 'fail';

            if ($current_group_name !== $new_group_name) {
                $response_company_group =  $this->client->sendRequest(Client::SC, Client::MOVE_DB, $params);
            } else {
                $response = 'fail-same-group';
            }

            if ($response_company_group->move_db) {
                $this->user->saveAction([
                    'type' => 'Создана заявка на перемещение базы данных группы компаний',
                    'details' => 'Заявка на перемещение базы данных создана успешо'
                ]);

                $response = 'success-move-dbase';
            }

            return $this->redirect(['cloud/', 'response' => $response]);
        } elseif ($method == 'rename-group') {
            $current_group_name = rawurlencode(htmlspecialchars(trim(Yii::$app->request->get('current_group_name'))));
            $new_group_name = rawurlencode(htmlspecialchars(trim(Yii::$app->request->POST()['CreateCompanyGroupForm']['group_name'])));
            $params = [
                'new_group_name' => $new_group_name,
                'group_name' => $current_group_name,
            ];
            $response = 'fail';

            if ($current_group_name !== $new_group_name and $new_group_name !== '') {
                $response_company_group = $this->client->sendRequest(Client::SC, Client::RENAME_COMPANY_GROUP, $params);
            } elseif ($new_group_name === '') {
                $response = 'fail-empty-name';
            } else {
                $response = 'fail-same-name';
            }

            if ($response_company_group->rename_group) {
                $this->user->saveAction([
                    'type' => 'Создана заявка на переименование группы компаний',
                    'details' => 'Заявка на переименование группы компаний создана успешно'
                ]);

                $response = 'success-rename-group';
            }

            return $this->redirect(['cloud/', 'response' => $response]);
        } elseif ($method == 'change-session-group') {
            $current_group_name = rawurlencode(htmlspecialchars(trim(Yii::$app->request->get('current_group_name'))));
            $numb_sessions = htmlspecialchars(trim(Yii::$app->request->POST()['CreateCompanyGroupForm']['group_name']));
            $params = [
                'count_sessions' => $numb_sessions,
                'group_name' => $current_group_name,
            ];
            $response = 'fail-change-session-group';

            if (is_numeric($numb_sessions)) {
                $response_change_sessions = $this->client->sendRequest(Client::SC, Client::CHANGE_SESSIONS_COMPANY_GROUP, $params);
            } else {
                $response = 'fail-incorrect-input-value';
            }

            if ($response_change_sessions->result === 'ОК') {
                $response = 'success-change-session-group';
            }

            return $this->redirect(['cloud/', 'response' => $response, 'numb_sessions' => $numb_sessions, 'changed_group' => rawurldecode($current_group_name)]);
        } else {
            $result = false;
        }

        if ($result) {
            return $this->render('handler', [
                'result' => $result
            ]);
        }

        throw new \yii\web\NotFoundHttpException();
    }

    public function actionSocial() {
        $code = Yii::$app->request->get('code');
        $type = Yii::$app->request->get('type');
        $auth = Yii::$app->request->get('auth');
        $types = ['vk', 'fb'];

        if (!$type || !in_array($type, $types)) throw new \yii\web\NotFoundHttpException();

        if ($auth) {
            $params = [
                'client_id' => Yii::$app->params[$type]['client_id'],
                'redirect_uri' => Yii::$app->params[$type]['redirect_uri'],
                'display' => 'popup',
                'response_type' => 'code',
                'scope' => 'email'
            ];

            if ($type == 'vk') {
                $params['v'] = Yii::$app->params['vk']['version'];
                $url = 'https://oauth.vk.com/authorize?';
            } elseif ($type == 'fb') {
                $url = 'https://www.facebook.com/' . Yii::$app->params['fb']['version'] . '/dialog/oauth?';
            }

            $this->redirect($url . http_build_query($params));
            return;
        }

        $params = [
            'client_id' => Yii::$app->params[$type]['client_id'],
            'client_secret' => Yii::$app->params[$type]['client_secret'],
            'redirect_uri' => Yii::$app->params[$type]['redirect_uri'],
            'code' => $code
        ];

        $url = ($type == 'vk') ? 'https://oauth.vk.com/access_token?' : 'https://graph.facebook.com/' . Yii::$app->params['fb']['version'] . '/oauth/access_token?';
        $json = file_get_contents($url . http_build_query($params));
        $json = json_decode($json);

        if ($type == 'vk') {
            $params = [
                'access_token' => $json->access_token,
                'user_ids' => $json->user_id,
                'v' => Yii::$app->params['vk']['version']
            ];

            $url = 'https://api.vk.com/method/users.get?' . http_build_query($params);
            $json_user = file_get_contents($url);
            $json_user = json_decode($json_user);

            $login = $json_user->response[0]->id;
            $email = $json->email;
            $name = $json_user->response[0]->last_name . ' ' . $json_user->response[0]->first_name;
        } elseif ($type == 'fb') {
            $params = [
                'access_token' => $json->access_token,
                'fields' => 'id,name,email'
            ];

            $url = 'https://graph.facebook.com/' . Yii::$app->params['fb']['version'] . '/me?' . http_build_query($params);

            $json_user = file_get_contents($url);
            $json_user = json_decode($json_user);

            $login = $json_user->id;
            $email = $json_user->email;
            $name = $json_user->name;
        }

        $user = Users::find()
            ->select('id')
            ->where(['AND', ['role' => 2], ['OR', ['login' => $login], ['contact_person' => $email]]])
            ->one();

        if ($user) {
            $user_id = $user->id;
        } else {
            $this->redirect(['auth/signin', 'error' => 1]);
            return;
        }

        if ($this->user->createSession($user_id)) {
            $this->redirect(['site/index']);
            return;
        }

        throw new \yii\web\NotFoundHttpException();
    }

    public function actionAuth() {
        $this->layout = false;
        $method = Yii::$app->request->get('method');
        $result = false;

        if (!$method) throw new \yii\web\NotFoundHttpException();

        $two_factor = new TwoFactor($this->user->getID());

        // Отправить СМС с кодом
        if ($method == 'send-code') {
            $phone = Yii::$app->request->get('phone');
            if (!$phone) throw new \yii\web\NotFoundHttpException();

            $code = $two_factor->createСonfirm();
            $phone = FormatHelper::phone($phone);

            if ($this->user->getPhone() == $phone) {
                $result['success'] = true;
            } elseif ($code) {
                $sms_phone = $phone;
                $sms_message = 'Никому не сообщайте код: ' . $code;
                // $sms = (new SMS())->sendSms($sms_phone, $sms_message);
                $sms = true;

                $result['success'] = ($sms && $query) ? true : false;
            } else {
                $result['success'] = false;
            }
        // Проверяет код
        } elseif ($method == 'check-code') {
            $code = Yii::$app->request->get('code');
            if (!$code) throw new \yii\web\NotFoundHttpException();

            $user_code = $two_factor->createСonfirm();

            if ($code == $user_code) {
                $query = (new Query())->createCommand()
                    ->update('users_auth', ['enable' => 1], ['user_id' => $this->user->getID()])
                    ->execute();

                $result['success'] = ($query) ? true : false;
            } else {
                $result['success'] = false;
            }
        }

        $result = json_encode($result);
        if ($result) {
            return $this->render('handler', [
                'result' => $result
            ]);
        }

        throw new \yii\web\NotFoundHttpException();
    }
}