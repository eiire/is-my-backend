<?php
namespace my\controllers;

use Yii;
use yii\web\Controller;
use my\components\Client;
use common\models\Request;

class PagesController extends Controller {
    public function actionRating() {
        $id = Yii::$app->request->get('id');
        if (!$id) return $this->redirect(['auth/signin']);

        $client = new Client();
        $client->id = 0;

        $details = $client->sendRequest(Client::UPP, Client::DETAILS_JOB_RATING, [
            'guid' => $id
        ]);

        $rating = [];
        if ($details->no_data == true) {
            $alert = [
                'type' => 'warning',
                'message' => 'Для вас нет работ для оценки или срок их действия истек'
            ];
        } else {
            $client->id = trim($details->code_upp);
            $period_start = date('Ymd', strtotime($details->date_start));
            $period_end = date('Ymd', strtotime($details->date_stop));

            // Работы специалиста
            $specialist = $client->sendRequest(Client::UPP, Client::DOCS, [
                'date_begin' => $period_start,
                'date_end' => $period_end
            ]);

            $specialist_rating = [];
            if ($specialist && $specialist->list_works && !$specialist->ErrorDescription) {
                foreach ($specialist->list_works as $row) {
                    if ($row->rating != 0) continue;
                    $_id = str_replace([' ', '-'], '', $row->number);

                    $specialist_rating[$_id] = [
                        'id' => $_id,
                        'title' => 'Оценка работы специалиста 1С',
                        'number' => $row->number,
                        'details' => [
                            'date' => date('d.m.Y', strtotime($row->date)),
                            'specialist' => $row->FIO,
                            'desc' => urldecode($row->works_array[0]->description)
                        ],
                        'year' => date('Y', strtotime($row->date)),
                        'type' => 'specialist'
                    ];
                }
            }

            if (count($specialist_rating) > 1) {
                $_id = 'specialist-group';
                $rating[$_id] = [
                    'id' => $_id,
                    'title' => 'Оценка работы специалиста 1С',
                    'type' => 'specialist',
                    'child' => $specialist_rating
                ];
            } elseif (count($specialist_rating) == 1) {
                $rating = array_merge($rating, $specialist_rating);
            }

            // Сервисы ИТС
            $service = $client->sendRequest(Client::UPP, Client::SERVICE, [
                'date_begin' => $period_start,
                'date_end' => $period_end
            ]);

            $service_rating = [];
            if ($service && $service->list_ss && !$service->ErrorDescription) {
                foreach ($service->list_ss as $row) {
                    if ($row->rating != 0 || $row->result != 'Выполнено') continue;
                    $_id = str_replace([' ', '-'], '', $row->number);

                    $service_rating[$_id] = [
                        'id' => $_id,
                        'title' => 'Оценка обращения по сервисам ИТС',
                        'number' => $row->number,
                        'details' => [
                            'title' => rawurldecode($row->topic),
                            'date' => date('d.m.Y', strtotime($row->date))
                        ],
                        'year' => date('Y', strtotime($row->date)),
                        'type' => 'service'
                    ];
                }
            }

            if (count($service_rating) > 1) {
                $_id = 'service-group';
                $rating[$_id] = [
                    'id' => $_id,
                    'title' => 'Оценка обращений по сервисам ИТС',
                    'type' => 'service',
                    'child' => $service_rating
                ];
            } elseif (count($service_rating) == 1) {
                $rating = array_merge($rating, $service_rating);
            }

            // Линия консультаций
            $support = $client->sendRequest(Client::UPP, Client::CONS, [
                'date_begin' => $period_start,
                'date_end' => $period_end
            ]);

            $support_rating = [];
            if ($support && $support->list_cons && !$support->ErrorDescription) {
                foreach ($support->list_cons as $row) {
                    if ($row->rating != 0 || $row->result != 'Выполнено') continue;
                    $_id = str_replace([' ', '-'], '', $row->number);

                    $support_rating[$_id] = [
                        'id' => $_id,
                        'title' => 'Оценка обращения на линию консультаций',
                        'number' => $row->number,
                        'details' => [
                            'title' => urldecode($row->topic),
                            'date' => date('d.m.Y', strtotime($row->date)),
                        ],
                        'year' => date('Y', strtotime($row->date)),
                        'type' => 'support'
                    ];
                }
            }

            if (count($support_rating) > 1) {
                $_id = 'support-group';
                $rating[$_id] = [
                    'id' => $_id,
                    'title' => 'Оценка обращений на линию консультаций',
                    'type' => 'support',
                    'child' => $support_rating
                ];
            } elseif (count($support_rating) == 1) {
                $rating = array_merge($rating, $support_rating);
            }
        }

        if (Yii::$app->request->post()) {
            $result = Yii::$app->request->post();
            unset($result[Yii::$app->components['request']['csrfParam']]);

            $json['rating'] = [];

            foreach ($result as $key => $value) {
                if (strpos($key, 'group') === false || strpos($key, 'comment') !== false) continue;
                $type = str_replace('-group', '', $key);

                foreach ($rating as $row) {
                    if (isset($row['child']) && $row['type'] == $type) {
                        foreach ($row['child'] as $child) {
                            $json['rating'][$child['id']] = [
                                'number' => $child['number'],
                                'year' => $child['year'],
                                'type' => $child['type'],
                                'rating' => $value,
                                'comment' => trim($result['comment-' . $type . '-group'])
                            ];
                        }
                    }
                }
            }

            $fields = array_merge($specialist_rating, $service_rating, $support_rating);
            foreach ($fields as $key => $value) {
                $_group = $value['type'] . '-group';

                if (isset($result[$key])) {
                    $json['rating'][$value['id']] = [
                        'number' => $value['number'],
                        'year' => $value['year'],
                        'type' => $value['type'],
                        'rating' => isset($result[$key]) ? $result[$key] : $result[$_group],
                        'comment' => isset($result['comment-' . $value['id']]) ? trim($result['comment-' . $value['id']]) : trim($result['comment-' . $value['type'] . '-group']) 
                    ];
                }
            }

            if ($details->nps && isset($result['nps'])) {
                $json['rating']['nps'] = [
                    'number' => NULL,
                    'year' => date('Y'),
                    'type' => 'nps',
                    'rating' => $result['nps'],
                    'comment' => trim($result['comment-nps'])
                ];
            }

            $json['rating'] = array_values($json['rating']);

            if (count($json['rating']) == 0) {
                $alert = [
                    'type' => 'warning',
                    'message' => 'Оцените хотя бы одну работу'
                ];
            } else {
                $json = json_encode($json, JSON_UNESCAPED_UNICODE);
                $response = $client->sendPostRequest(Client::UPP_IN, Client::RATING, $json);

                return $this->redirect(['pages/rating', 'id' => $id, 'response' => $response->ErrorDescription ? 'error' : 'success']);
            }
        }

        if (Yii::$app->request->get('response')) {
            switch (Yii::$app->request->get('response')) {
                case 'success':
                    $alert = [
                        'type' => 'success',
                        'message' => 'Спасибо! Ваши оценки успешно сохранены'
                    ];
                    break;
                case 'error':
                    $alert = [
                        'type' => 'danger',
                        'message' => 'Произошла ошибка, попробуйте еще раз'
                    ];
                    break;
            }
        }

        if (count($rating) == 0 && !$alert) {
            $alert = [
                'type' => 'success',
                'message' => 'Для вас нет работ для оценки'
            ];
        }

        $period = [
            'start' => date('d.m.Y', strtotime($period_start)),
            'end' => date('d.m.Y', strtotime($period_end))
        ];

        return $this->render('rating', [
            'id' => $id,
            'period' => $period,
            'nps' => $details->nps,
            'rating' => $rating,
            'alert' => $alert
        ]);
    }

    public function actionUnsubscribe() {
        $id = Yii::$app->request->get('id');
        $type = Yii::$app->request->get('type');

        if ($id && $type) {
            if ($type == 'rating') {
                $client = new Client();
                $client->id = 0;

                $client->sendRequest(Client::UPP, Client::DETAILS_JOB_RATING, [
                    'guid' => $id,
                    'fy' => 1
                ]);

                return $this->render('unsubscribe');
            }
        }

        return $this->redirect(['auth/signin']);
    }

    public function actionChangelog() {
        $this->layout = false;
        $token = 'PyucbaHQ3jgkGvGTsYHo';
        $project_id = '14565753';
        $url = 'https://gitlab.com/api/v4/projects/' . $project_id . '/repository/commits';

        $commits = new Request($url . '?ref_name=master&per_page=100');
        $commits->setHeaders(['PRIVATE-TOKEN: ' . $token]);
        $commits->execute();
        $commits = json_decode($commits->getResponse());

        foreach ($commits as &$commit) {
            $commit->date = date('d.m.Y H:i', strtotime($commit->created_at));
        }

        return $this->render('changelog', [
            'commits' => $commits
        ]);
    }

    public function actionPrivacy() {
        return $this->render('privacy');
    }
}