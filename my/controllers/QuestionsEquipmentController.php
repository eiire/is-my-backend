<?php
namespace my\controllers;

use Yii;
use yii\web\Controller;
use yii\db\Query;
use yii\data\Pagination;
use yii\helpers\Url;
use my\components\Client;
use my\models\Rating;
use my\models\users\Users;
use my\models\forms\PeriodForm;
use my\models\forms\ConsultantForm;
use my\models\forms\RatingForm;
use common\models\helpers\FormatHelper;

class QuestionsEquipmentController extends SiteController {
    public function actionIndex() {
        $form = new PeriodForm();
        $form_rating = new RatingForm();

        if (isset($_GET['period_start']) && isset($_GET['period_end'])) {
            $period_start = date('Ymd', strtotime($_GET['period_start']));
            $period_end = date('Ymd', strtotime($_GET['period_end']));
        } else {
            $date = new \DateTime('-1 month');
            $period_start = $date->format('Ymd');
            $period_end = date('Ymd');
        }

        $filter_value = (isset($_GET['filter'])) ? $_GET['filter'] : 0;

        $status = [
            0 => 'Все',
            1 => 'Создано',
            2 => 'Выполнено',
            3 => 'Рекомендовано вызвать специалиста',
            4 => 'Отложено',
            5 => 'Отказано',
            6 => 'Отмена'
        ];

        $result = $this->client->sendRequest(Client::UPP, Client::CLIENT);
        if (isset($result) && isset($result->dogovor_to)) {
            $this->profile['dogovor_to'] = $this->client->sendRequest(Client::UPP, Client::CLIENT)->dogovor_to;
        }

        $result = $this->client->sendRequest(Client::UPP, Client::LIST_REQUEST_TO, [
            'date_begin' => $period_start,
            'date_end' => $period_end
        ]);

        if (!$result) {
            $response = [
                'type' => 'danger',
                'message' => 'Произошла ошибка при получении списка обращений'
            ];
        } elseif ($result->ErrorDescription) {
            $response = [
                'type' => 'danger',
                'message' => $result->ErrorDescription
            ];
        } elseif (!$result->list_request_to) {
            $response = [
                'type' => 'warning',
                'message' => 'Список заявок пуст'
            ];
        } else {
            $filtered_result = [];
            $time = $result->min_cons;
            $result = $result->list_request_to;
            $success = ['Выполнено', 'Рекомендовано вызвать специалиста'];

            $pagination = new Pagination([
                'totalCount' => count($result),
                'defaultPageSize' => 25
            ]);

            $limit = $this->paginationLimit($pagination);

            $result_number = [];
            for ($i = $pagination->offset; $i < $limit; $i++) {
                if ($result[$i]->result == $status[$filter_value] || $filter_value == 0) {
                    $filtered_result[$i]['topic'] = urldecode($result[$i]->topic);
                    $filtered_result[$i]['date'] = date('d.m.Y H:i', strtotime($result[$i]->date));
                    $filtered_result[$i]['planned_date'] = ($result[$i]->planned_date != '0001-01-01T00:00:00') ? date('d.m.Y H:i', strtotime($result[$i]->planned_date)) : '';
                    $filtered_result[$i]['date_response'] = ($result[$i]->date_response != '0001-01-01T00:00:00') ? date('d.m.Y H:i', strtotime($result[$i]->date_response)) : '';
                    $filtered_result[$i]['duration'] = $result[$i]->duration_cons . ' минут';
                    $filtered_result[$i]['number'] = $result[$i]->number;
                    $filtered_result[$i]['format_number'] = FormatHelper::number($result[$i]->number, '0');
                    $filtered_result[$i]['result'] = $result[$i]->result;
                    $filtered_result[$i]['question'] = urldecode($result[$i]->question);
                    $filtered_result[$i]['status_style'] = (in_array($result[$i]->result, $success)) ? 'text-success' : '';
                    $result_number[$i] = $result[$i]->number;
                }
            }
            $query_rating = Rating::find()
                ->select(['number', 'rating'])
                ->where(['number' => $result_number])
                ->andWhere(['type' => 1])
                ->all();

            $rating_number = [];
            $rating_value = [];
            for ($j = 0; $j < count($query_rating); $j++) {
                $rating_number[$j] = $query_rating[$j]->number;
                $rating_value[$query_rating[$j]->number] = $query_rating[$j]->rating;
            }

            for ($i = $pagination->offset; $i < $limit; $i++) {
                if (in_array($filtered_result[$i]['number'], $result_number)) {
                    $filtered_result[$i]['rating'] = $rating_value[$filtered_result[$i]['number']];
                }
            }

            if (!$filtered_result) {
                $response = [
                    'type' => 'warning',
                    'message' => 'Обращений не найдено'
                ];
            }

            $result_count = $pagination->totalCount;
        }

        $period_start = date('d.m.Y', strtotime($period_start));
        $period_end = date('d.m.Y', strtotime($period_end));

        if ($form_rating->load(Yii::$app->request->post())) {
            if ($this->user->permission(1)) {
                $query = (new Query())->createCommand()
                    ->insert('rating', [
                        'user_id' => $this->user->getID(),
                        'type' => 1,
                        'number' => $form_rating->number,
                        'rating' => $form_rating->rating,
                        'date' => date('Y-m-d H:i:s', strtotime($form_rating->date)),
                        'message' => $form_rating->message
                    ])
                    ->execute();

                $json['rating'][] = [
                    'number' => $form_rating->number,
                    'rating' => $form_rating->rating,
                    'year' => date('Y', strtotime($form_rating->date)),
                    'comment' => $form_rating->message,
                    'type' => 'support'
                ];
                $json = json_encode($json);
                $this->client->sendPostRequest(Client::UPP_IN, Client::RATING, $json);


                if ($query) {
                    $rating_status = 'success';

                    $details = 'Номер: ' . FormatHelper::number($form_rating->number, '0') . ';';
                    $details .= 'Оценка: ' . $form_rating->rating . ';';
                    if ($form_rating->message) $details .= 'Комментарий: ' . $form_rating->message;

                    $this->user->saveAction([
                        'type' => 'Оценка линии консультаций',
                        'details' => $details
                    ]);

                    if ($form_rating->rating != 5) {
                        $text = 'Email: ' . $this->user->getEmail(). '<br>';
                        $text .= 'Код в УПП: ' . $this->user->getCode() . '<br>';
                        $text .= 'Номер документа: ' . $form_rating->number . '<br>';
                        $text .= 'Оценка: ' . $form_rating->rating . '<br>';
                        $text .= 'Комментарий: ' . $form_rating->message;
        
                        Yii::$app->mailer->compose()
                            ->setFrom(Yii::$app->params['email']['from'])
                            ->setTo(Yii::$app->params['email']['admin'])
                            ->setSubject(Yii::$app->params['title'] . ' - Оценка линии консультаций')
                            ->setHtmlBody($text)
                            ->send();
                    }
                } else {
                    $rating_status = 'fail';
                }
            } else {
                $rating_status = 'fail';
            }

            $this->redirect(Url::to(['support/index', 'response' => $rating_status]));
        }

        if ($_GET['response']) {
            switch ($_GET['response']) {
                case 'success':
                    $response = [
                        'type' => 'success',
                        'message' => 'Оценка успешно сохранена',
                        'close' => true
                    ];
                    break;
                case 'fail':
                    $response = [
                        'type' => 'danger',
                        'message' => 'Произошла ошибка при сохранении оценки',
                        'close' => true
                    ];
                    break;
                case 'no_access':
                    $response = [
                        'type' => 'danger',
                        'message' => 'Для получения услуг по работе с торговым оборудованием, консультациям по маркировке и ЕГАИС требуется заключить договор. Услуги оказываются в рабочее и нерабочее время.',
                        'close' => true
                    ];
                    break;
            }
        }

        return $this->render('index', [
            'result' => $filtered_result,
            'result_count' => $result_count,
            'period_start' => $period_start,
            'period_end' => $period_end,
            'form' => $form,
            'form_rating' => $form_rating,
            'filter' => $status,
            'response' => $response,
            'pagination' => $pagination,
            'time' => $time,
            'profile' => $this->profile
        ]);
    }

    public function actionReport() {
        $form = new ConsultantForm();
        $result = $this->client->sendRequest(Client::UPP, Client::SOFT);
        $show_form = true;

        if ($result->ErrorDescription) {
            $response = [
                'type' => 'danger',
                'message' => urldecode($result->ErrorDescription)
            ];
            $show_form = false;
        } else {
            $result = $result->list_soft;

            $soft = [];
            $soft['false'] = '';
            for ($i = 0; $i < count($result); $i++) {
                $soft[urldecode($result[$i]->name_soft)]= urldecode($result[$i]->name_soft);
            }
            $soft['Другое'] = 'Другое';

            $contact_phone = $this->user->getContactPhone();
            $contact_person = $this->user->getContactPerson();
        }

        if ($form->load(Yii::$app->request->post())) {
            if (!$this->user->permission(1)) {
                $response = [
                    'type' => 'danger',
                    'message' => 'Недостаточно прав для выполнения операции',
                    'close' => true
                ];
            } elseif (!$form->phone || !$form->person || !$form->message || $form->soft == 'false') {
                $response = [
                    'type' => 'danger',
                    'message' => 'Заполните все поля',
                    'close' => true
                ];
            } else {
                $contact_phone = trim($form->phone);
                $contact_person = trim($form->person);

                $json = [
                    'phone' => rawurlencode($contact_phone),
                    'user' => rawurlencode($contact_person),
                    'soft' => rawurlencode($form->soft),
                    'message' => rawurlencode($form->message)
                ];
                $json = json_encode($json);

                $result = $this->client->sendPostRequest(Client::UPP_IN, Client::INS_REQUEST_TO, $json);

                $query = (new Query())->createCommand()
                    ->update('users', ['contact_phone' => $contact_phone, 'contact_person' => $contact_person], ['id' => $this->user->getID()])
                    ->execute();

                if ($result->ErrorDescription) {
                    $response = [
                        'type' => 'danger',
                        'message' => $result->ErrorDescription
                    ];
                } else {
                    $date = date('d.m.Y H:i', strtotime($result->planned_date));
                    $response = [
                        'type' => 'success',
                        'message' => 'Заявка успешно отправлена. Плановая дата ответа ' . $date,
                    ];

                    $details = 'Контактное лицо: ' . $contact_person . ';';
                    $details .= 'Номер телефона: ' . $contact_phone . ';';
                    $details .= 'Конфигурация 1С: ' . $form->soft;

                    $this->user->saveAction([
                        'type' => 'Заявка на линию консультаций',
                        'details' => $details
                    ]);

                    $show_form = false;
                }
            }
            $form->message = NULL;
        }

        return $this->render('report', [
            'soft' => $soft,
            'form' => $form,
            'contact_phone' => $contact_phone,
            'contact_person' => $contact_person,
            'response' => $response,
            'show_form' => $show_form,
            'profile' => $this->profile
        ]);
    }
}