<?php
namespace my\controllers;

use Yii;
use yii\web\Controller;
use yii\db\Query;
use my\components\User;
use my\models\sca\ScaUsers;
use my\models\sca\ScaHistory;
use my\models\sca\ScaTariffs;
use my\models\forms\sca\ScaForm;
use my\models\forms\sca\ScaFeedbackForm;
use common\models\Request;

class ScaController extends Controller {
    public function actionIndex() {
        $form = new ScaForm();
        $form_feedback = new ScaFeedbackForm();

        $types = [
            'Модуль обычного приложения',
            'Модуль управляемого приложения',
            'Модуль внешнего соединения',
            'Модуль сеанса',
            'Общий модуль',
            'Модуль управляемой формы',
            'Модуль обычной формы',
            'Модуль команды',
            'Модуль объекта',
            'Модуль менеджера',
            'Модуль набора записей'
        ];

        if ($form->load(Yii::$app->request->post())) {
            $code_length = strlen(str_replace([' ', '\n', '\t'], '', $form->code));

            if (!$form->email || !$form->code) {
                $this->redirect(['sca/index', 'response' => 'empty']);
            } else {
                $email = trim($form->email);
                $code = $form->code;
                $type = $types[$form->type];

                if ($form->type == 4) {
                    $srv = ($form->srv) ? 1 : 0;
                    $clop = ($form->clop) ? 1 : 0;
                    $clup = ($form->clup) ? 1 : 0;
                    $int = ($form->int) ? 1 : 0;
                }

                $url = 'http://sky.is1c.ru/apk/hs/ConfigurationCheck/quick?';
                $params = [
                    'email' => $email,
                    'type_mod' => $types[$type]
                ];

                $params_query = http_build_query($params);

                $request = new Request($url . $params_query);
                $request->setBasicAuthCredentials('admin', '123');
                $request->setRequestType('POST');
                $request->setPostFields($code);
                $request->execute();
                $response = $request->getResponse();

                $this->redirect(['sca/index', 'response' => 'send', 'message' => $response]);
            }
        }

        if ($form_feedback->load(Yii::$app->request->post())) {
            if (!$form_feedback->message) {
                $alert = [
                    'type' => 'danger',
                    'message' => 'Заполните все поля'
                ];
            } else {
                $message = $this->user->getName() . ' ' . $this->user->getLogin() . '<br><br>';
                $message .= trim($form_feedback->message);

                Yii::$app->mailer->compose('layouts/template', ['content' => $message])
                    ->setFrom(Yii::$app->params['email']['from'])
                    ->setTo(Yii::$app->params['email']['admin'])
                    ->setSubject('Комментарий по SCA ИнфоСофт')
                    ->send();

                $this->redirect(['sca/index', 'response' => 'message']);
            }

            $form_feedback->message = NULL;
        }

        if (isset($_GET['response'])) {
            switch ($_GET['response']) {
                case 'empty':
                    $alert = [
                        'type' => 'danger',
                        'message' => 'Заполните все поля',
                        'close' => true
                    ];
                    break;
                case 'available':
                    $alert = [
                        'type' => 'danger',
                        'message' => 'Превышен лимит проверок за этот месяц',
                        'close' => true
                    ];
                    break;
                case 'limit':
                    $alert = [
                        'type' => 'danger',
                        'message' => 'Количество символов превышает лимит установленный вашим тарифом',
                        'close' => true
                    ];
                    break;
                case 'send':
                    $alert = [
                        'type' => 'primary',
                        'message' => $_GET['message'],
                        'close' => true
                    ];
                    break;
                case 'message':
                    $alert = [
                        'type' => 'success',
                        'message' => 'Ваше сообщение успешно отправлено',
                        'close' => true
                    ];
                    break;
            }
        }

        return $this->render('index', [
            'form' => $form,
            'form_feedback' => $form_feedback,
            'types' => $types,
            'alert' => $alert
        ]);
    }
}