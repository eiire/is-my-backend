<?php
namespace my\controllers;

use Yii;
use yii\web\Controller;
use yii\db\Query;
use yii\data\Pagination;
use yii\helpers\Url;
use my\components\Client;
use my\models\Rating;
use my\models\users\Users;
use my\models\forms\PeriodForm;
use my\models\forms\ServiceForm;
use my\models\forms\RatingForm;
use common\models\helpers\FormatHelper;

class ServiceController extends SiteController {
    public function actionIndex() {
        $form = new PeriodForm();
        $form_rating = new RatingForm();

        if (isset($_GET['period_start']) && isset($_GET['period_end'])) {
            $period_start = date('Ymd', strtotime($_GET['period_start']));
            $period_end = date('Ymd', strtotime($_GET['period_end']));
        } else {
            $date = new \DateTime('-1 month');
            $period_start = $date->format('Ymd');
            $period_end = date('Ymd');
        }

        $filter_value = (isset($_GET['filter'])) ? $_GET['filter'] : 0;

        $status = [
            0 => 'Все',
            1 => 'Создано',
            2 => 'В работе',
            3 => 'Требуется выезд сервис инженера',
            4 => 'Рекомендовано запланировать специалиста',
            5 => 'Рекомендовано обратиться на ЛК',
            6 => 'Отменено',
            7 => 'Выполнено'
        ];

        $result = $this->client->sendRequest(Client::UPP, Client::SERVICE, [
            'date_begin' => $period_start,
            'date_end' => $period_end
        ]);

        if (!$result) {
            $response = [
                'type' => 'danger',
                'message' => 'Произошла ошибка при получении списка обращений'
            ];
        } elseif ($result->ErrorDescription) {
            $response = [
                'type' => 'danger',
                'message' => $result->ErrorDescription
            ];
        } elseif (!$result->list_ss) {
            $response = [
                'type' => 'warning',
                'message' => 'За указанный период обращений не было'
            ];
        } else {
            $filtered_result = [];
            $result = $result->list_ss;
            $success = ['Требуется выезд сервис инженера', 'Рекомендовано запланировать специалиста', 'Рекомендовано обратиться на ЛК', 'Выполнено'];

            $pagination = new Pagination([
                'totalCount' => count($result),
                'defaultPageSize' => 25
            ]);

            $limit = $this->paginationLimit($pagination);

            $result_number = [];
            for ($i = $pagination->offset; $i < $limit; $i++) {
                if ($result[$i]->result == $status[$filter_value] || $filter_value == 0) {
                    $filtered_result[$i]['topic'] = urldecode($result[$i]->topic);
                    $filtered_result[$i]['date'] = date('d.m.Y H:i', strtotime($result[$i]->date));
                    $filtered_result[$i]['planned_date'] = ($result[$i]->planned_date != '0001-01-01T00:00:00') ? date('d.m.Y H:i', strtotime($result[$i]->planned_date)) : '';
                    $filtered_result[$i]['date_response'] = ($result[$i]->date_response != '0001-01-01T00:00:00') ? date('d.m.Y H:i', strtotime($result[$i]->date_response)) : '';
                    $filtered_result[$i]['duration'] = $result[$i]->duration . ' минут';
                    $filtered_result[$i]['number'] = $result[$i]->number;
                    $filtered_result[$i]['format_number'] = FormatHelper::number($result[$i]->number, '0');
                    $filtered_result[$i]['result'] = $result[$i]->result;
                    $filtered_result[$i]['status_style'] = (in_array($result[$i]->result, $success)) ? 'text-success' : '';
                    $result_number[$i] = $result[$i]->number;
                }
            }

            $query_rating = Rating::find()
                ->select(['number', 'rating'])
                ->where(['number' => $result_number])
                ->andWhere(['type' => 2])
                ->all();

            $rating_number = [];
            $rating_value = [];
            for ($j = 0; $j < count($query_rating); $j++) {
                $rating_number[$j] = $query_rating[$j]->number;
                $rating_value[$query_rating[$j]->number] = $query_rating[$j]->rating;
            }

            for ($i = $pagination->offset; $i < $limit; $i++) {
                if (in_array($filtered_result[$i]['number'], $result_number)) {
                    $filtered_result[$i]['rating'] = $rating_value[$filtered_result[$i]['number']];
                }
            }

            if (!$filtered_result) {
                $response = [
                    'type' => 'warning',
                    'message' => 'Обращений не найдено'
                ];
            }

            $result_count = $pagination->totalCount;
        }

        $period_start = date('d.m.Y', strtotime($period_start));
        $period_end = date('d.m.Y', strtotime($period_end));

        if ($form_rating->load(Yii::$app->request->post())) {
            if ($this->user->permission(1)) {
                $query = (new Query())->createCommand()
                    ->insert('rating', [
                        'user_id' => $this->user->getID(),
                        'type' => 2,
                        'number' => $form_rating->number,
                        'rating' => $form_rating->rating,
                        'date' => date('Y-m-d H:i:s', strtotime($form_rating->date)),
                        'message' => $form_rating->message
                    ])
                    ->execute();

                $json['rating'][] = [
                    'number' => $form_rating->number,
                    'rating' => $form_rating->rating,
                    'year' => date('Y', strtotime($form_rating->date)),
                    'comment' => $form_rating->message,
                    'type' => 'service'
                ];
                $json = json_encode($json);
                $this->client->sendPostRequest(Client::UPP_IN, Client::RATING, $json);

                if ($query) {
                    $rating_status = 'success';

                    $details = 'Номер: ' . FormatHelper::number($form_rating->number, '0') . ';';
                    $details .= 'Оценка: ' . $form_rating->rating . ';';
                    if ($form_rating->message) $details .= 'Комментарий: ' . $form_rating->message;

                    $this->user->saveAction([
                        'type' => 'Оценка сервиса ИТС',
                        'details' => $details
                    ]);

                    if ($form_rating->rating != 5) {
                        $text = 'Email: ' . $this->user->getEmail(). '<br>';
                        $text .= 'Код в УПП: ' . $this->user->getCode() . '<br>';
                        $text .= 'Номер документа: ' . $form_rating->number . '<br>';
                        $text .= 'Оценка: ' . $form_rating->rating . '<br>';
                        $text .= 'Комментарий: ' . $form_rating->message;
        
                        Yii::$app->mailer->compose()
                            ->setFrom(Yii::$app->params['email']['from'])
                            ->setTo(Yii::$app->params['email']['admin'])
                            ->setSubject(Yii::$app->params['title'] . ' - Оценка сервиса ИТС')
                            ->setHtmlBody($text)
                            ->send();
                    }
                } else {
                    $rating_status = 'fail';
                }
            } else {
                $rating_status = 'fail';
            }

            $this->redirect(Url::to(['service/index', 'response' => $rating_status]));
        }

        if ($_GET['response']) {
            switch ($_GET['response']) {
                case 'success':
                    $response = [
                        'type' => 'success',
                        'message' => 'Оценка успешно сохранена',
                        'close' => true
                    ];
                    break;
                case 'fail':
                    $response = [
                        'type' => 'danger',
                        'message' => 'Произошла ошибка при сохранении оценки',
                        'close' => true
                    ];
                    break;
            }
        }

        return $this->render('index', [
            'result' => $filtered_result,
            'result_count' => $result_count,
            'period_start' => $period_start,
            'period_end' => $period_end,
            'form' => $form,
            'form_rating' => $form_rating,
            'filter' => $status,
            'response' => $response,
            'pagination' => $pagination,
            'profile' => $this->profile
        ]);
    }

    public function actionReport() {
        $form = new ServiceForm();
        $show_form = true;

        $contact_phone = $this->user->getContactPhone();
        $contact_person = $this->user->getContactPerson();

        if ($form->load(Yii::$app->request->post())) {
            if (!$this->user->permission(1)) {
                $response = [
                    'type' => 'danger',
                    'message' => 'Недостаточно прав для выполнения операции',
                    'close' => true
                ];
            } elseif (!$form->phone || !$form->person || !$form->message) {
                $response = [
                    'type' => 'danger',
                    'message' => 'Заполните все поля',
                    'close' => true
                ];
            } else {
                $contact_phone = trim($form->phone);
                $contact_person = trim($form->person);

                $json = [
                    'phone' => rawurlencode($contact_phone),
                    'user' => rawurlencode($contact_person),
                    'message' => rawurlencode($form->message)
                ];
                $json = json_encode($json);

                $result = $this->client->sendPostRequest(Client::UPP_IN, Client::NEW_SERVICE, $json);

                $query = (new Query())->createCommand()
                    ->update('users', ['contact_phone' => $contact_phone, 'contact_person' => $contact_person], ['id' => $this->user->getID()])
                    ->execute();

                if ($result->ErrorDescription) {
                    $response = [
                        'type' => 'danger',
                        'message' => $result->ErrorDescription
                    ];
                } else {
                    $response = [
                        'type' => 'success',
                        'message' => 'Заявка успешно отправлена',
                    ];

                    $details = 'Контактное лицо: ' . $contact_person . ';';
                    $details .= 'Номер телефона: ' . $contact_phone . ';';

                    $this->user->saveAction([
                        'type' => 'Заявка по сервисам ИТС',
                        'details' => $details
                    ]);
                    $show_form = false;
                }
            }
            $form->message = NULL;
        }

        return $this->render('report', [
            'soft' => $soft,
            'form' => $form,
            'contact_phone' => $contact_phone,
            'contact_person' => $contact_person,
            'response' => $response,
            'show_form' => $show_form,
            'profile' => $this->profile
        ]);
    }
}