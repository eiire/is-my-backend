<?php
namespace my\controllers;

use Yii;
use yii\web\Controller;
use yii\db\Query;
use yii\data\Pagination;
use yii\helpers\Url;
use yii\web\UploadedFile;
use my\components\Client;
use my\components\ActualNews;
use my\components\User;
use my\models\users\Users;
use my\models\users\UsersCode;
use my\models\users\UsersHistory;
use my\models\users\UsersAction;
use my\models\forms\PeriodForm;
use my\models\forms\SettingsForm;
use my\models\forms\NoticeForm;
use my\models\forms\CompaniesForm;
use my\models\forms\FeedbackForm;
use common\models\helpers\FormatHelper;

class SiteController extends Controller {
    protected $user;
    protected $client;
    protected $profile = [];

    protected function paginationLimit($pagination) {
        $per_page = $pagination->totalCount - $pagination->page * $pagination->defaultPageSize;
        $page_limit = ($per_page < $pagination->limit) ? $per_page : $pagination->limit;
        $limit = $pagination->offset + $page_limit;
        $limit = ($pagination->totalCount < $pagination->defaultPageSize) ? $pagination->totalCount : $limit;
        return $limit;
    }

    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    public function beforeAction($action) {
        parent::beforeAction($action);

        $this->user = new User();

        if (isset($_GET['logout'])) {
            $this->user->logOut();
            $this->redirect(['auth/signin']);
            return false;
        } elseif (!$this->user->haveAccess($action->id)) {
            $this->redirect(['auth/signin']);
            return false;
        }

        if ($this->user->permission(1) && ($action->controller->id == 'event' || $action->controller->id == 'sca')) {
            return $this->redirect(['site/index']);
        } elseif ($this->user->permission(2) && $action->controller->id != 'event') {
            return $this->redirect(['event/index']);
        } elseif ($this->user->permission(3) && $action->controller->id != 'sca') {
            return $this->redirect(['sca/index']);
        } elseif ($this->user->permission(9) && $action->controller->id != 'dashboard') {
            return $this->redirect(['dashboard/index']);
        }

        $this->client = new Client();
        $this->client->id = $this->user->getCode();

        $this->profile['name'] = $this->user->getName();
        $this->profile['inn'] = $this->user->getInn();
        $this->profile['is_budget'] = $this->user->getSegment() == Client::SEGMENT_BUDGET;

        return true;
    }

    public function actionIndex() {
        $codes = UsersCode::find()
            ->select(['id', 'name'])
            ->where(['user_id' => $this->user->getID(), 'active' => 1])
            ->all();

        $count = count($codes);
        if ($count > 1) {
            $companies = [];
            foreach ($codes as $code) {
                $companies[] = [
                    'id' => $code->id,
                    'name' => $code->name
                ];
            }
        }

        $news = (new ActualNews($this->user->getID()))->getNews();

        $result = $this->client->sendRequest(Client::UPP, Client::CLIENT);
        if (isset($result) && isset($result->dogovor_to)) {
            $this->profile['dogovor_to'] = $this->client->sendRequest(Client::UPP, Client::CLIENT)->dogovor_to;
        }

        return $this->render('index', [
            'companies' => $companies,
            'news' => $news,
            'profile' => $this->profile
        ]);
    } 

    public function actionHistory() {
        $form = new PeriodForm();

        if (isset($_GET['period_start']) && isset($_GET['period_end'])) {
            $period_start = date('Y-m-d', strtotime($_GET['period_start']));
            $period_end = date('Y-m-d', strtotime($_GET['period_end']));
        } else {
            $date = new \DateTime('-2 days');
            $period_start = $date->format('Y-m-d');
            $period_end = date('Y-m-d');
        }

        $count = UsersHistory::find()
            ->where(['login' => $this->user->getLogin(), 'success' => 1])
            ->andWhere(['>=', 'date', $period_start])
            ->andWhere(['<=', 'date', $period_end . ' 23:59:59'])
            ->count();

        $result = UsersHistory::find()
            ->where(['login' => $this->user->getLogin(), 'success' => 1])
            ->andWhere(['>=', 'date', $period_start])
            ->andWhere(['<=', 'date', $period_end . ' 23:59:59'])
            ->orderBy(['id' => SORT_DESC]);

        $pagination = new Pagination([
            'totalCount' => $count,
            'defaultPageSize' => 25
        ]);

        $result = $result
            ->limit($pagination->limit)
            ->offset($pagination->offset)
            ->all();

        $format_result = [];
        for ($i = 0; $i < count($result); $i++) {
            $format_result[$i] = [
                [
                    'label' => 'Дата',
                    'value' => date('d.m.Y H:i', strtotime($result[$i]->date))
                ],
                [
                    'label' => 'IP адрес',
                    'value' => $result[$i]->ip
                ],
                [
                    'label' => 'Браузер',
                    'value' => $result[$i]->browser
                ]
            ];
        }

        if ($_GET['response']) {
            switch ($_GET['response']) {
                case 'success':
                    $response = [
                        'type' => 'success',
                        'message' => 'Все сеансы, кроме этого, были успешно завершены',
                        'close' => true
                    ];
                    break;
                case 'fail':
                    $response = [
                        'type' => 'danger',
                        'message' => 'Произошла ошибка при завершении сеансов',
                        'close' => true
                    ];
                    break;
            }
        }

        $period_start = date('d.m.Y', strtotime($period_start));
        $period_end = date('d.m.Y', strtotime($period_end));

        return $this->render('history', [
            'result' => $format_result,
            'pagination' => $pagination,
            'form' => $form,
            'period_start' => $period_start,
            'period_end' => $period_end,
            'response' => $response,
            'profile' => $this->profile
        ]);
    }

    public function actionAction() {
        $form = new PeriodForm();

        if (isset($_GET['period_start']) && isset($_GET['period_end'])) {
            $period_start = date('Y-m-d', strtotime($_GET['period_start']));
            $period_end = date('Y-m-d', strtotime($_GET['period_end']));
        } else {
            $date = new \DateTime('-1 month');
            $period_start = $date->format('Y-m-d');
            $period_end = date('Y-m-d');
        }

        $count = UsersAction::find()
            ->where(['user_id' => $this->user->getID()])
            ->andWhere(['>=', 'date', $period_start])
            ->andWhere(['<=', 'date', $period_end . ' 23:59:59'])
            ->count();

        $result = UsersAction::find()
            ->where(['user_id' => $this->user->getID()])
            ->andWhere(['>=', 'date', $period_start])
            ->andWhere(['<=', 'date', $period_end . ' 23:59:59'])
            ->orderBy(['id' => SORT_DESC]);
            

        $pagination = new Pagination([
            'totalCount' => $count,
            'defaultPageSize' => 25
        ]);

        $result = $result
            ->limit($pagination->limit)
            ->offset($pagination->offset)
            ->all();

        $format_result = [];
        for ($i = 0; $i < count($result); $i++) {
            $format_result[$i] = [
                [
                    'label' => 'Действие',
                    'value' => $result[$i]->type
                ],
                [
                    'label' => 'Описание',
                    'value' => $result[$i]->format_details
                ],
                [
                    'label' => 'IP адрес',
                    'value' => $result[$i]->ip
                ],
                [
                    'label' => 'Дата',
                    'value' => date('d.m.Y H:i', strtotime($result[$i]->date))
                ]
            ];
        }

        if (count($format_result) == 0) {
            $response = [
                'type' => 'warning',
                'message' => 'За указанный период активных действий в личном кабинете не осуществлялось'
            ];
        }

        $period_start = date('d.m.Y', strtotime($period_start));
        $period_end = date('d.m.Y', strtotime($period_end)); 

        return $this->render('action', [
            'result' => $format_result,
            'pagination' => $pagination,
            'form' => $form,
            'period_start' => $period_start,
            'period_end' => $period_end,
            'response' => $response,
            'profile' => $this->profile
        ]);
    }

    public function actionSettings() {
        $form = new SettingsForm();
        $form_2 = new NoticeForm();
        $form_3 = new CompaniesForm();
        $this->user->login = $this->user->getLogin();

        if ($form->load(Yii::$app->request->post())) {
            $this->user->password = trim($form->new_password);
            
            if (!$this->user->permission(1)) {
                $alert = [
                    'type' => 'danger',
                    'message' => 'Недостаточно прав для выполнения операции',
                    'close' => true
                ];
            } elseif (!$form->old_password || !$form->new_password || !$form->new_password_repeat) {
                $alert = [
                    'type' => 'danger',
                    'message' => 'Заполните все поля',
                    'close' => true
                ];
            } elseif (!$this->user->passwordExist(trim($form->old_password))) {
                $alert = [
                    'type' => 'danger',
                    'message' => 'Старый пароль введен не верно',
                    'close' => true
                ];
            } elseif ($form->new_password != $form->new_password_repeat) {
                $alert = [
                    'type' => 'danger',
                    'message' => 'Пароли не совпадают',
                    'close' => true
                ];
            } elseif (!$this->user->passwordValidate()) {
                $alert = [
                    'type' => 'danger',
                    'message' => 'Пароль должен быть длиной более 8 символов и содержать хотя бы одну заглавную букву или символ',
                    'close' => true
                ];
            } else {
                if ($this->user->passwordChange(false)) {
                    $alert = [
                        'type' => 'success',
                        'message' => 'Пароль успешно изменен',
                        'close' => true
                    ];
                } else {
                    $alert = [
                        'type' => 'danger',
                        'message' => 'Произошла ошибка при изменении пароля. Попробуйте еще раз',
                        'close' => true
                    ];
                }
            }
            $form->old_password = NULL;
            $form->new_password = NULL;
            $form->new_password_repeat = NULL;
        }

        $codes = UsersCode::find()
            ->select(['id', 'name', 'active'])
            ->where(['user_id' => $this->user->getID()])
            ->all();

        $count = count($codes);
        if ($count > 1) {
            $companies = [];
            foreach ($codes as $code) {
                $companies[$code->id] = $code->name;
                if ($code->active == 1) $form_3->company = $code->id;
            }
        }

        $request_emails = $this->client->sendRequest(Client::SC, Client::NOTICE_EMAILS);
        $notice_emails = [];
        $notice_emails_array = [];
        $id = 0;

        if ($request_emails) {
            foreach ($request_emails->list_email as $email) {
                $notice_emails_array[] = $email;
                $notice_emails[] = [
                    'id' => $id,
                    'email' => $email
                ];
                $id++;
            }
        }

        // Можно ли удалить email
        $email_deleted = (count($notice_emails) > 1) ? true : false;

        if ($form_2->load(Yii::$app->request->post())) {
            $notice_email = htmlspecialchars(trim($form_2->email));

            if (!$notice_email) {
                $alert = [
                    'type' => 'danger',
                    'message' => "Поле с email'om обязательно для заполнения",
                    'close' => true
                ];
            } elseif (!$this->user->emailValidate($notice_email)) {
                $alert = [
                    'type' => 'danger',
                    'message' => 'Email введен некорректно',
                    'close' => true
                ];
            } elseif (in_array($notice_email, $notice_emails_array)) {
                $alert = [
                    'type' => 'warning',
                    'message' => 'Такой email уже добавлен',
                    'close' => true
                ];
            } else {
                $this->client->sendRequest(Client::SC, Client::ADD_NOTICE_EMAIL, ['email' => $notice_email]);

                $this->user->saveAction([
                    'type' => 'Изменение списка адресов для уведомлений',
                    'details' => $notice_email . ' добавлен'
                ]);

                return $this->redirect(['site/settings', 'response' => 'email-add-success']);
            }

            $form_2->email = NULL;
        }

        if ($form_3->load(Yii::$app->request->post())) {
            if (!$form_3->company) {
                $alert = [
                    'type' => 'danger',
                    'message' => 'Произошла ошибка, попробуйте еще раз',
                    'close' => true
                ];
            } else {
                $id = (int) htmlspecialchars($form_3->company);

                UsersCode::updateAll(['active' => 0], ['user_id' => $this->user->getID()]);
                UsersCode::updateAll(['active' => 1], ['id' => $id]);

                $code = UsersCode::find()
                    ->select('code')
                    ->where(['id' => $id])
                    ->one();

                $client = new Client();
                $client->id = $code->code;
                $details = $client->sendRequest(Client::UPP, Client::CLIENT);

                $company = NULL;
                if ($details->full_name) {
                    $details->full_name = urldecode($details->full_name);
                    $company = FormatHelper::replace($details->full_name, [
                        'Индивидуальный предприниматель' => 'ИП',
                        'Общество с ограниченной ответственностью' => 'ООО'
                    ]);
                }

                UsersCode::updateAll([
                    'name' => $company,
                    'segment' => $details->segment,
                    'inn' => $details->inn,
                    'cashbox' => $details->kassa,
                ], [
                    'user_id' => $this->user->getID(),
                    'code' => $code->code
                ]);

                return $this->redirect(['site/settings', 'response' => 'company-change-success']);
            }
        }

        if ($_GET['response']) {
            switch ($_GET['response']) {
                case 'email-add-success':
                    $alert = [
                        'type' => 'success',
                        'message' => 'Email успешно добавлен',
                        'close' => true
                    ];
                break;
                case 'email-delete-success':
                    $alert = [
                        'type' => 'success',
                        'message' => 'Email успешно удален',
                        'close' => true
                    ];
                    break;
                case 'email-delete-error':
                    $alert = [
                        'type' => 'danger',
                        'message' => 'Произошла ошибка, попробуйти еще раз',
                        'close' => true
                    ];
                    break;
                case 'company-change-success':
                    $alert = [
                        'type' => 'success',
                        'message' => 'Юр.лицо успешно изменено',
                        'close' => true
                    ];
                    break;
            }
        }

        return $this->render('settings', [
            'companies' => $companies,
            'selected' => $selected,
            'form' => $form,
            'form_2' => $form_2,
            'form_3' => $form_3,
            'email' => $this->user->getEmail(),
            'emails' => $notice_emails,
            'email_deleted' => $email_deleted,
            'alert' => $alert,
            'profile' => $this->profile
        ]);
    }

    public function actionFeedback() {
        $form = new FeedbackForm();
        if ($form->load(Yii::$app->request->post())) {
            $message = htmlspecialchars(trim($form->message));

            if (!$form->message) {
                $alert = [
                    'type' => 'danger',
                    'message' => 'Заполните все поля'
                ];
            } else {
                $error = false;
                $extensions = ['jpg', 'jpeg', 'png', 'bmp'];

                $text = $this->user->getName();
                $text .= '<br>Email: ' . $this->user->getEmail();
                $text .= '<br>Код УПП: ' . $this->user->getCode();
                $text .= '<br>ИНН: ' . $this->user->getInn();
                $text .= '<br><br>Сообщение:<br>' . $message;

                $mail = Yii::$app->mailer->compose()
                    ->setFrom(Yii::$app->params['email']['from'])
                    ->setTo(Yii::$app->params['email']['admin'])
                    ->setSubject('Личный кабинет ' . Yii::$app->params['title'] . ' - Обратная связь')
                    ->setHtmlBody($text);

                $form->file = UploadedFile::getInstanceByName('file');
                if ($form->file) {
                    $dir = __DIR__ . '/../web/upload/tmp/';

                    if ($form->file->size / pow(10, 6) <= 1) {
                        if (in_array($form->file->extension, $extensions)) {
                            $form->file->saveAs($dir . $form->file->baseName . '.' . $form->file->extension);
                            $mail->attach($dir . $form->file->baseName . '.' . $form->file->extension);
                        } else {
                            $error = 'extension';
                        }
                    } else {
                        $error = 'max-size';
                    }
                }

                if (!$error) {
                    $mail->send();
                    return $this->redirect(['site/feedback', 'response' => 'success']);
                }
            }
        }

        if (Yii::$app->request->get('response') && Yii::$app->request->get('response') == 'success') {
            $alert = [
                'type' => 'success',
                'message' => 'Ваше сообщение успешно отправлено'
            ];
        }

        if ($error) {
            switch ($error) {
                case 'max-size':
                    $alert = [
                        'type' => 'danger',
                        'message' => 'Максимальный размер загружаемого файла 1 МБ'
                    ];
                    break;
                case 'extension':
                    $alert = [
                        'type' => 'danger',
                        'message' => 'Неверный формат загружаемого файла'
                    ];
                    break;
            }
        }

        return $this->render('feedback', [
            'form' => $form,
            'alert' => $alert,
            'profile' => $this->profile
        ]);
    }

    public function actionHandler() {
        $this->layout = false;
        $method = Yii::$app->request->get('method');

        if ($method == 'delete-email') {
            $email = Yii::$app->request->get('email');

            if ($email) {
                $request_emails = $this->client->sendRequest(Client::SC, Client::NOTICE_EMAILS);
                $notice_emails = [];
                foreach ($request_emails->list_email as $email) {
                    $notice_emails[] = $email;
                }

                if (count($notice_emails) > 1 && in_array($email, $notice_emails)) {
                    $this->client->sendRequest(Client::SC, Client::DEL_NOTICE_EMAIL, ['email' => $email]);

                    $this->user->saveAction([
                        'type' => 'Изменение списка адресов для уведомлений',
                        'details' => $email . ' удален'
                    ]);

                    return $this->redirect(['site/settings', 'response' => 'email-delete-success']);
                } else {
                    return $this->redirect(['site/settings', 'response' => 'email-delete-error']);
                }
            }
        } else {
            throw new \yii\web\NotFoundHttpException();
        }
    }
}