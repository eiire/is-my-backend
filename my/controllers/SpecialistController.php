<?php
namespace my\controllers;

use Yii;
use yii\web\Controller;
use yii\helpers\Url;
use yii\db\Query;
use yii\data\Pagination;
use yii\web\UploadedFile;
use my\components\Client;
use my\models\Rating;
use my\models\Holidays;
use my\models\forms\PeriodForm;
use my\models\forms\RatingForm;
use my\models\forms\SpecialistForm;
use my\models\forms\SpecialistFormDefault;
use my\models\forms\ServiceForm;
use my\models\forms\SpecialistTimeForm;
use my\models\forms\ForceSpecialistForm;
use my\models\users\UsersSpecialist;
use common\models\helpers\FormatHelper;

class SpecialistController extends SiteController {
    private function specialistRequest($json) {
        Yii::$app->session->setFlash('success', true);
        if (!isset($json['guid'])) $json['guid'] = '';
        return $this->client->sendPostRequest(Client::UPP_IN, Client::INS_PLANNING, $json);
        return true;
    }

    private function activeForceReport() {
        $now = time();

        if (!$this->user->getCashbox()) return false;

        $hour = date('H', $now);
        if ($hour < 9 || $hour >= 18) return true;

        $day = date('w', $now);
        if ($day == 0 || $day == 6) return true;

        $today = date('Y-m-d', $now);
        $holiday = Holidays::find()
            ->where(['date' => $today])
            ->one();

        if ($holiday) return true;
        return false;
    }

    public function actionIndex() {
        $form = new PeriodForm();
        $form_rating = new RatingForm();
        $segment = [Client::SEGMENT_CORP, Client::SEGMENT_MEDIUM];

        if (isset($_GET['period_start']) && isset($_GET['period_end'])) {
            $period_start = date('Ymd', strtotime($_GET['period_start']));
            $period_end = date('Ymd', strtotime($_GET['period_end']));
        } else {
            $date = new \DateTime('-1 month');
            $period_start = $date->format('Ymd');
            $period_end = date('Ymd');
        }

        if (in_array($this->user->getSegment(), $segment)) {
            $result = $this->client->sendRequest(Client::UA . 'user_tasks/', Client::PLANNING, [
                'date_begin' => $period_start,
                'date_end' => $period_end
            ]);
        } else {
            $result = $this->client->sendRequest(Client::UPP, Client::PLANNING, [
                'get_oper' => 'get_plan',
                'date_begin' => $period_start,
                'date_end' => $period_end
            ]);
        }

        if (!$result) {
            $response = [
                'type' => 'danger',
                'message' => 'Произошла ошибка при получении данных'
            ];
        } elseif ($result->ErrorDescription) {
            $response = [
                'type' => 'danger',
                'message' => $result->ErrorDescription
            ];
        } elseif (!$result->list_plan) {
            $response = [
                'type' => 'warning',
                'message' => 'За указанный период обращений не было'
            ];
        } else {
            $result = $result->list_plan;

            $pagination = new Pagination([
                'totalCount' => count($result),
                'defaultPageSize' => 25
            ]);

            $limit = $this->paginationLimit($pagination);

            $result_number = [];
            $filtered_result = [];
            if (in_array($this->user->getSegment(), $segment)) {
                for ($i = $pagination->offset; $i < $limit; $i++) {
                    $filtered_result[$i] = [
                        'number' => $result[$i]->number,
                        'status' => $result[$i]->status,
                        'date' => date('d.m.Y', strtotime($result[$i]->time_array)),
                        'desc' => trim($result[$i]->description),
                        'name_task' => $result[$i]->name,
                    ];
                }

                $result_number[$i] = $result[$i]->number;
            } else {
                for ($i = $pagination->offset; $i < $limit; $i++) {
                    $filtered_result[$i]['number'] = $result[$i]->number;
                    $filtered_result[$i]['name'] = $result[$i]->FIO;
                    $filtered_result[$i]['type'] = $result[$i]->type_works;
                    $filtered_result[$i]['status'] = $result[$i]->status;
                    $result_number[$i] = $result[$i]->number;
                    if ($result[$i]->time_array) {
                        $filtered_result[$i]['date'] = date('d.m.Y', strtotime($result[$i]->time_array[0]->date));
                        for ($j = 0; $j < count($result[$i]->time_array); $j++) {
                            $filtered_result[$i]['works'][$j]['date'] = date('d.m.Y', strtotime($result[$i]->time_array[$j]->date));
                            $filtered_result[$i]['works'][$j]['time_start'] = date('H:i', strtotime($result[$i]->time_array[$j]->time_begin));
                            $filtered_result[$i]['works'][$j]['time_end'] = date('H:i', strtotime($result[$i]->time_array[$j]->time_end));
                            $filtered_result[$i]['works'][$j]['desc'] = urldecode($result[$i]->time_array[$j]->description);
                            $duration = $filtered_result[$i]['works'][$j]['time_end'] - $filtered_result[$i]['works'][$j]['time_start'];
                            if ($duration > 0) $filtered_result[$i]['works'][$j]['duration'] = $duration . ' ' . FormatHelper::hour($duration);
                        }
                    }
                }
            }

            $query_rating = Rating::find()
                ->select(['number', 'rating'])
                ->where(['number' => $result_number])
                ->andWhere(['type' => 3])
                ->all();

                $rating_number = [];
                $rating_value = [];
                for ($j = 0; $j < count($query_rating); $j++) {
                    $rating_number[$j] = $query_rating[$j]->number;
                    $rating_value[$query_rating[$j]->number] = $query_rating[$j]->rating;
                }

                for ($i = $pagination->offset; $i < $limit; $i++) {
                    if (in_array($filtered_result[$i]['number'], $result_number)) {
                        $filtered_result[$i]['rating'] = $rating_value[$filtered_result[$i]['number']];
                    }
                }

            $result_count = $pagination->totalCount;
        }

        $period_start = date('d.m.Y', strtotime($period_start));
        $period_end = date('d.m.Y', strtotime($period_end));

        if ($form_rating->load(Yii::$app->request->post())) {
            if ($this->user->permission(1)) {
                $query = (new Query())->createCommand()
                    ->insert('rating', [
                        'user_id' => $this->user->getID(),
                        'type' => 3,
                        'number' => $form_rating->number,
                        'rating' => $form_rating->rating,
                        'message' => $form_rating->message
                    ])
                    ->execute();

                if ($query) {
                    $rating_status = 'success';

                    $details = 'Номер: ' . $form_rating->number . ';';
                    $details .= 'Оценка: ' . $form_rating->rating . ';';
                    if ($form_rating->message) $details .= 'Комментарий: ' . $form_rating->message;

                    $this->user->saveAction([
                        'type' => 'Оценка работы специалиста 1С',
                        'details' => $details
                    ]);

                    if ($form_rating->rating != 5) {
                        $text = 'Email: ' . $this->user->getEmail(). '<br>';
                        $text .= 'Код в УПП: ' . $this->user->getCode() . '<br>';
                        $text .= 'Номер документа: ' . $form_rating->number . '<br>';
                        $text .= 'Оценка: ' . $form_rating->rating . '<br>';
                        $text .= 'Комментарий: ' . $form_rating->message;

                        Yii::$app->mailer->compose()
                            ->setFrom(Yii::$app->params['email']['from'])
                            ->setTo(Yii::$app->params['email']['admin'])
                            ->setSubject(Yii::$app->params['title'] . ' - Оценка работы специалиста 1С')
                            ->setHtmlBody($text)
                            ->send();
                    }
                } else {
                    $rating_status = 'fail';
                }
            } else {
                $rating_status = 'fail';
            }

            $this->redirect(Url::to(['specialist/index', 'response' => $rating_status]));
        }

        if ($_GET['response']) {
            switch ($_GET['response']) {
                case 'success':
                    $response = [
                        'type' => 'success',
                        'message' => 'Оценка успешно сохранена',
                        'close' => true
                    ];
                    break;
                case 'fail':
                    $response = [
                        'type' => 'danger',
                        'message' => 'Произошла ошибка при сохранении оценки',
                        'close' => true
                    ];
                    break;
            }
        }

        $force_report = $this->activeForceReport();

        return $this->render('index', [
            'result' => $filtered_result,
            'result_count' => $result_count,
            'pagination' => $pagination,
            'period_start' => $period_start,
            'period_end' => $period_end,
            'form' => $form,
            'form_rating' => $form_rating,
            'force_report' => $force_report,
            'response' => $response,
            'profile' => $this->profile
        ]);
    }

    public function actionReport() {
        $segment = [Client::SEGMENT_CORP, Client::SEGMENT_MEDIUM];
        if (in_array($this->user->getSegment(), $segment)) {
            $form = new SpecialistFormDefault();
            $extended_form = false;

            $remote_access_programs = ['TeamViewer', 'Ammyy Admin', 'AnyDesk', '1С-Коннект', 'RDP + VPN', 'Другое'];
            $remote_access_programs = array_combine($remote_access_programs, $remote_access_programs);

            if ($form->load(Yii::$app->request->post())) {
                if (!$this->user->permission(1)) {
                    $response = [
                        'type' => 'danger',
                        'message' => 'Недостаточно прав для выполнения операции',
                        'close' => true
                    ];
                } elseif (!$form->phone || !$form->person || !$form->message) {
                    $response = [
                        'type' => 'danger',
                        'message' => 'Заполните все поля',
                        'close' => true
                    ];
                } else {
                    $message = htmlspecialchars(trim($form->message));
                    $contact_phone = htmlspecialchars(trim($form->phone));
                    $contact_person = htmlspecialchars(trim($form->person));
                    $configuration = htmlspecialchars(trim($form->configuration));
                    $remote_access = htmlspecialchars(trim($form->remote_access));

                    if ($remote_access) $message = 'Программа удаленного доступа: ' . $remote_access . "\n" . $message;
                    if ($configuration) $message = 'Конфигурация: ' . $configuration . "\n" . $message;

                    $json = [
                        'phone' => rawurlencode($contact_phone),
                        'user' => rawurlencode($contact_person),
                        'message' => rawurlencode($message)
                    ];

                    $form->files = UploadedFile::getInstancesByName('files');
                    $error = false;
                    if ($form->files) {
                        $upload_files = [];
                        $dir = __DIR__ . '/../web/upload/tmp/';
                        $extensions = ['jpg', 'jpeg', 'png', 'bmp', 'doc', 'docx', 'xls', 'xlsx', 'pdf', 'txt', 'xml'];

                        foreach ($form->files as $file) {
                            if ($file->size / pow(10, 6) <= 3) {
                                if (in_array($file->extension, $extensions)) {
                                    $file->saveAs($dir . $file->baseName . '.' . $file->extension);
                                    $file_content = file_get_contents($dir . $file->baseName . '.' . $file->extension);

                                    $upload_files[] = [
                                        'name' => $file->baseName,
                                        'format' => $file->extension,
                                        'content' => base64_encode($file_content)
                                    ];
                                } else {
                                    $error = 'extension';
                                }
                            } else {
                                $error = 'max-size';
                            }
                        }

                        if (!$error) $json['files'] = $upload_files;
                    }

                    if ($error) {
                        switch ($error) {
                            case 'max-size':
                                $response = [
                                    'type' => 'danger',
                                    'message' => 'Максимальный размер загружаемого файла 1 МБ'
                                ];
                                break;
                            case 'extension':
                                $response = [
                                    'type' => 'danger',
                                    'message' => 'Неверный формат загружаемого файла'
                                ];
                                break;
                        }
                    } else {
                        $result = $this->client->sendPostRequest(Client::UA, false, json_encode($json));

                        $query = (new Query())->createCommand()
                            ->update('users', ['contact_phone' => $contact_phone, 'contact_person' => $contact_person], ['id' => $this->user->getID()])
                            ->execute();

                        if ($result->ErrorDescription) {
                            $response = [
                                'type' => 'danger',
                                'message' => $result->ErrorDescription
                            ];
                        } else {
                            $response = [
                                'type' => 'success',
                                'message' => 'Заявка успешно отправлена',
                            ];

                            $details = 'Контактное лицо: ' . $contact_person . ';';
                            $details .= 'Номер телефона: ' . $contact_phone . ';';

                            $this->user->saveAction([
                                'type' => 'Заказ работы специалиста 1С',
                                'details' => $details
                            ]);
                            $show_form = false;
                        }
                    }
                }
                $form->message = NULL;
            }
        } else {
            $extended_form = true;

            $stage = ($_GET['stage']) ? (int) $_GET['stage'] : 1;
            if ($stage < 1 || $stage > 3) $this->redirect(Url::to(['specialist/report']));
            
            if ($stage == 1) {
                $form_1 = new SpecialistForm();
                $result_soft = $this->client->sendRequest(Client::UPP, Client::PLANNING, [
                    'get_oper' => 'get_sp'
                ]);

                if ($result_soft->software_product) {
                    $soft = [];
                    $soft['false'] = '';
                    $result_soft = $result_soft->software_product;
                    for ($i = 0; $i < count($result_soft); $i++) {
                        $soft[$result_soft[$i]->number] = urldecode($result_soft[$i]->description);
                    }
                    $soft['other'] = 'Другое';
                }

                $work = [];
                $type = ['Дистанционно', 'Выезд'];

                $response = [
                    'type' => 'warning',
                    'message' => 'Обратите внимание, что данный вид работ является платным',
                ];

                if ($form_1->load(Yii::$app->request->post())) {
                    if (!$form_1->phone || !$form_1->person || !$form_1->message || ($form_1->soft != 'other' && !$form_1->work)) {
                        $response = [
                            'type' => 'danger',
                            'message' => 'Заполните все поля',
                            'close' => true
                        ];
                        $form_1->soft = null;
                    } elseif ($form_1->soft == 'other' || $form_1->work == '000000000') {
                        $json = [
                            'user' => rawurlencode($form_1->person),
                            'phone' => rawurlencode($form_1->phone),
                            'message' => rawurlencode($form_1->message),
                            'method' => $form_1->type
                        ];
                        if ($form_1->work) $json['type_work'] = $form_1->work;

                        $json = json_encode($json);
                        if ($this->user->permission(1)) $this->specialistRequest($json);
                        $this->redirect(Url::to(['specialist/report', 'stage' => 3]));
                    } else {
                        $contact_phone = trim($form_1->phone);
                        $contact_person = trim($form_1->person);
                        
                        $query = (new Query())->createCommand()
                            ->update('users', ['contact_phone' => $contact_phone, 'contact_person' => $contact_person], ['id' => $this->user->getID()])
                            ->execute();

                        UsersSpecialist::deleteAll(['session_id' => $this->user->getSessionID()]);

                        $query = (new Query())->createCommand()
                            ->insert('users_specialist', [
                                'session_id' => $this->user->getSessionID(),
                                'type' => $form_1->type,
                                'soft' => $form_1->soft,
                                'work' => $form_1->work,
                                'message' => $form_1->message,
                            ])
                            ->execute();

                        $this->redirect(Url::to(['specialist/report', 'stage' => 2]));
                    }
                }
            } elseif ($stage == 2) {
                $form_2 = new SpecialistTimeForm();

                $query = UsersSpecialist::find()
                    ->select(['work', 'message', 'type'])
                    ->where(['session_id' => $this->user->getSessionID()])
                    ->one();

                if (!$query) $this->redirect(Url::to(['specialist/report']));

                $show_result = false;
                $form_btn = true;

                $week = ($_GET['week']) ? (int) $_GET['week'] : 1;
                if ($week > 12) $week = 12;
                switch ($week) {
                    case 1:
                        $date = date('Ymd');
                        break;
                    case 2:
                        $date = date('Ymd', strtotime('next week monday'));
                        break;
                    default:
                        $date = date('Ymd', strtotime('next week monday + ' . $week . ' week'));
                        break;
                }

                $result = $this->client->sendRequest(Client::UPP, Client::PLANNING, [
                    'get_oper' => 'get_spec',
                    'type_work' => $query->work,
                    'date_begin' => $date
                ]);

                $error_result = 'На выбранные даты планирование специалиста невозможно!';

                if (!$result) {
                    $response = [
                        'type' => 'danger',
                        'message' => 'Произошла ошибка при получении данных'
                    ];
                } elseif ($result->ErrorDescription == $error_result) {
                    $response = [
                        'type' => 'warning',
                        'message' => $result->ErrorDescription
                    ];
                    $show_result = true;
                    $form_btn = false;
                    $filtered_result = [];
                } elseif ($result->ErrorDescription) {
                    $response = [
                        'type' => 'danger',
                        'message' => $result->ErrorDescription
                    ];
                } elseif (!$result->list_worker) {
                    $json = [
                        'user' => rawurlencode($form_1->person),
                        'phone' => rawurlencode($form_1->phone),
                        'message' => rawurlencode($form_1->message),
                        'method' => $form_1->type
                    ];
                    if ($form_1->work) $json['type_work'] = $form_1->work;

                    $json = json_encode($json);
                    if ($this->user->permission(1)) $this->specialistRequest($json);
                    $this->redirect(Url::to(['specialist/report', 'stage' => 3]));
                } else {
                    if ($form_2->load(Yii::$app->request->post())) {
                        if (!$this->user->permission(1)) {
                            $response = [
                                'type' => 'danger',
                                'message' => 'Недостаточно прав для выполнения операции',
                                'close' => true
                            ];
                        } elseif (!$form_2->guid || !$form_2->hour || !$form_2->date) {
                            $response = [
                                'type' => 'warning',
                                'message' => 'Произошла ошибка при планировании. Попробуйте еще раз'
                            ];
                        } else {
                            $duration = $result->list_worker[0]->duration_work;
                            $time_start = $form_2->hour + 8;
                            $time_end = $form_2->hour + 8 + $duration;

                            $json = [
                                'user' => rawurlencode($this->user->getContactPerson()),
                                'phone' => rawurlencode($this->user->getContactPhone()),
                                'message' => rawurlencode($query->message),
                                'type_work' => $query->work,
                                'method' => $query->type,
                                'guid' => $form_2->guid,
                                'date_plan' => $form_2->date,
                                'time_begin' => $time_start,
                                'time_end' => $time_end
                            ];

                            $query = (new Query())->createCommand()
                                ->update('users_specialist', [
                                        'time' => $time_start,
                                        'date' => $form_2->date,
                                        'duration' => $duration
                                    ], ['session_id' => $this->user->getSessionID()])
                                ->execute();

                            $json = json_encode($json);
                            if ($this->user->permission(1)) $this->specialistRequest($json);
                            $this->redirect(Url::to(['specialist/report', 'stage' => 3]));
                        }
                    } else {
                        $show_result = true;
                        $result = $result->list_worker;
                        $duration = $result[0]->duration_work;

                        $date_start = date('d.m', strtotime($result[0]->time_array[0]->date));
                        $date_end = date('d.m', strtotime($result[0]->time_array[count($result[0]->time_array) - 1]->date));
                        
                        for ($i = 0; $i < count($result); $i++) {
                            for ($j = 0; $j < count($result[$i]->time_array); $j++) {

                                // Убираем прошедшие часы
                                if (time() >= strtotime($result[$i]->time_array[$j]->date) + 8*3600 + 3600*$result[$i]->time_array[$j]->hour_day) {
                                    $result[$i]->time_array[$j]->free = false;
                                }

                                if ($result[$i]->time_array[$j]->hour_day == 9) { // Вычисляем последние часы дня
                                    for ($k = 0; $k <= $duration - 2; $k++) {
                                        $result[$i]->time_array[$j - $k]->free = false;
                                    }
                                }

                                if ($j <= count($result[$i]->time_array) - $duration) { // Проверяем, что не выходим за пределеы массива с учетом продолжительности работ
                                    if ($result[$i]->time_array[$j]->free) { // Проверям, что текущий час свободен
                                        for ($k = 1; $k <= $duration - 1; $k++) { // Проверям следущие часы 
                                            if ($result[$i]->time_array[$j + $k]->free == false) { // Если следующий час занят 
                                                for ($n = 0; $n <= $duration - 1; $n++) { // То помечаем текущий и последующие часы занятыми
                                                    $result[$i]->time_array[$j + $n]->free = false;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        $tmp_result = [];
                        for ($i = 0; $i < count($result); $i++) {
                            $tmp_result[$i]['name'] = $result[$i]->FIO;
                            $tmp_result[$i]['attached'] = $result[$i]->sc;
                            $tmp_result[$i]['count'] = 0;
                            for ($j = 0; $j < count($result[$i]->time_array); $j++) {
                                $tmp_result[$i]['time'][$j]['guid'] = $result[$i]->GUID;
                                $tmp_result[$i]['time'][$j]['date'] = date('d.m.Y', strtotime($result[$i]->time_array[$j]->date));
                                $tmp_result[$i]['time'][$j]['hour'] = $result[$i]->time_array[$j]->hour_day;
                                $tmp_result[$i]['time'][$j]['free'] = $result[$i]->time_array[$j]->free;
                                if ($tmp_result[$i]['time'][$j]['free']) $tmp_result[$i]['count'] += 1;
                            }
                        }

                        $filtered_result = [];
                        for ($i = 0; $i <= count($tmp_result); $i++) {
                            if ($tmp_result[$i]['count'] != 0) array_push($filtered_result, $tmp_result[$i]);
                        }
                        array_multisort(array_column($filtered_result, 'count'), SORT_DESC, $filtered_result);

                        $duration_desc = FormatHelper::hour($duration);

                        if ($filtered_result) { 
                            $response = [
                                'type' => 'primary',
                                'message' => '<h3 class="m-b-0">Выберите время начала работ и желаемого специалиста.</h3>Приблизительная продолжительность работ ' . $duration . ' ' . $duration_desc . '.'
                            ];
                        } else {
                            $response = [
                                'type' => 'warning',
                                'message' => 'На данный период нет свободных специалистов'
                            ];
                            $form_btn = false;
                        }

                        $days = ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'];
                    }
                }
            } elseif ($stage == 3) {
                $query = UsersSpecialist::find()
                    ->select(['date', 'time', 'duration'])
                    ->where(['session_id' => $this->user->getSessionID()])
                    ->one();

                $access = ($query->date && $query->time && $query->duration) ? true : false;
                if (!$access && !Yii::$app->session->getFlash('success')) $this->redirect(Url::to(['specialist/report']));

                UsersSpecialist::deleteAll(['session_id' => $this->user->getSessionID()]);

                if ($access) {
                    $duration_desc = FormatHelper::hour($query->duration);

                    $date = date('d.m.Y', strtotime($query->date));
                    $time = $query->time . ':00';
                    $title = 'Заявка успешно сохранена';
                    $duration = $query->duration . ' ' . $duration_desc;

                    $details = 'Дата: ' . $date . ';';
                    $details .= 'Время: ' . $time . ';';
                    $details .= 'Приблизительная продолжительность: ' . $query->duration . ' ' . $duration_desc;
                } else {
                    $title = 'Заявка передана менеджеру';
                    $desc = 'С Вами свяжутся в ближайшее время';
                    $details = 'Заявка передана менеджеру';
                }

                $this->user->saveAction([
                    'type' => 'Заказ работы специалиста 1С',
                    'details' => $details
                ]);
            }
        }

        $contact_phone = $this->user->getContactPhone();
        $contact_person = $this->user->getContactPerson();

        $view = ($extended_form) ? 'report-extended' : 'report-default';

        return $this->render($view, [
            'stage' => $stage,
            'form' => $form,
            'form_1' => $form_1,
            'form_2' => $form_2,
            'extended_form' => $extended_form,
            'form_btn' => $form_btn,
            'soft' => $soft,
            'work' => $work,
            'type' => $type,
            'contact_phone' => $contact_phone,
            'contact_person' => $contact_person,
            'result' => $filtered_result,
            'show_result' => $show_result,
            'days' => $days,
            'duration' => $duration,
            'date_start' => $date_start,
            'date_end' => $date_end,
            'title' => $title,
            'desc' => $desc,
            'date' => $date,
            'time' => $time,
            'remote_access_programs' => $remote_access_programs,
            'response' => $response,
            'profile' => $this->profile
        ]);
    }

    public function actionForcereport() {
        if ($this->activeForceReport()) {
            $form = new ForceSpecialistForm();
            $show_form = true;

            $employees = $this->client->sendRequest(Client::UPP, Client::FORCE_PLANNING);

            if (!$employees) {
                $response = [
                    'type' => 'danger',
                    'message' => 'Произошла ошибка при получении списка сотрудников'
                ];
                $show_form = false;
            } elseif ($employees->ErrorDescription) {
                $response = [
                    'type' => 'danger',
                    'message' => $employees->ErrorDescription
                ];
                $show_form = false;
            } elseif (!$employees->list_sotr) {
                $response = [
                    'type' => 'danger',
                    'message' => 'Нет свободных специалистов'
                ];
                $show_form = false;
            } else {
                $employees = $employees->list_sotr;

                $response = [
                    'type' => 'warning',
                    'message' => 'Обращаем ваше внимание на то, что стоимость данных работ будет в 1.5 больше стоимости обычных работ'
                ];

                $employees_format = [];
                $employees_format['false'] = '';
                for ($i = 0; $i < count($employees); $i++) {
                    $employees_format[$employees[$i]->GUID] = urldecode($employees[$i]->FIO);
                }
            }

            if ($form->load(Yii::$app->request->post())) {
                if (!$this->user->permission(1)) {
                    $response = [
                        'type' => 'danger',
                        'message' => 'Недостаточно прав для выполнения операции',
                        'close' => true
                    ];
                } elseif (!$form->phone || !$form->person || !$form->message || $form->employees == 'false') {
                    $response = [
                        'type' => 'danger',
                        'message' => 'Заполните все поля',
                        'close' => true
                    ];
                } else {
                    $contact_phone = trim($form->phone);
                    $contact_person = trim($form->person);

                    $query = (new Query())->createCommand()
                        ->update('users', ['contact_phone' => $contact_phone, 'contact_person' => $contact_person], ['id' => $this->user->getID()])
                        ->execute();

                    $json = [
                        'user' => rawurlencode($contact_person),
                        'phone' => rawurlencode($contact_phone),
                        'message' => rawurlencode($form->message),
                        'guid' => $form->employees
                    ];

                    $json = json_encode($json);
                    $result = $this->client->sendPostRequest(Client::UPP_IN, Client::FORCE_INS_PLANNING, $json);

                    if (!$result->ErrorDescription) {
                        $details = 'Заявка создана';

                        $this->user->saveAction([
                            'type' => 'Срочный заказа работы специалиста 1С',
                            'details' => $details
                        ]);

                        $show_form = false;
                        $response = [
                            'type' => 'success',
                            'message' => 'Заявка успешно создана'
                        ];
                    } else {
                        $response = [
                            'type' => 'danger',
                            'message' => 'Произошла ошибка. Попробуйте еще раз',
                            'close' => true
                        ];
                    }
                }
                $form->message = NULL;
            }

            $contact_phone = $this->user->getContactPhone();
            $contact_person = $this->user->getContactPerson();
        } else {
            $response = [
                'type' => 'warning',
                'message' => 'В настоящее время срочное планирование специалиста невозможно'
            ];
            $show_form = false;
        }

        return $this->render('forcereport', [
            'form' => $form,
            'show_form' => $show_form,
            'contact_person' => $contact_person,
            'contact_phone' => $contact_phone,
            'employees' => $employees_format,
            'response' => $response,
            'profile' => $this->profile
        ]);
    }
}