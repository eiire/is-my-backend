<?php
namespace my\models\forms;

use Yii;
use yii\base\Model;

class AuthForm extends Model {
    public $phone;
    public $auth;

    public function rules() {
        return [
            [['auth'], 'string'],
            [['phone'], 'required'],
        ];
    }
}