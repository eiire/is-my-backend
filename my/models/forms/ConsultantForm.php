<?php
namespace my\models\forms;

use Yii;
use yii\base\Model;

class ConsultantForm extends Model {
    public $message;
    public $soft;
    public $phone;
    public $person;

    public function rules() {
        return [
            [['message', 'soft', 'phone', 'person'], 'required'],
        ];
    }
}