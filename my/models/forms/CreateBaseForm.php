<?php
namespace my\models\forms;

use Yii;
use yii\base\Model;

class CreateBaseForm extends Model {
    public $base;
    public $group_name;

    public function rules() {
        return [
            [['base'], 'required'],
            [['group_name'], 'string'],
        ];
    }
}