<?php
namespace my\models\forms;

use Yii;
use yii\base\Model;


class CreateCompanyGroupForm extends Model {
    public $group_name;

    public function rules() {
        return [
            [['group_name'], 'required'],
        ];
    }
}