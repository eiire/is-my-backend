<?php
namespace my\models\forms;

use Yii;
use yii\base\Model;

class FeedbackForm extends Model {
    public $message;
    public $file;

    public function rules() {
        return [
            [['message'], 'required'],
            [['file'], 'file', 'extensions' => 'jpg, jpeg, png, bmp']
        ];
    }
}