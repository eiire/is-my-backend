<?php
namespace my\models\forms;

use Yii;
use yii\base\Model;

class ForceSpecialistForm extends Model {
    public $message;
    public $phone;
    public $person;
    public $employees;

    public function rules() {
        return [
            [['message', 'phone', 'person', 'employees'], 'required'],
        ];
    }
}