<?php
namespace my\models\forms;

use Yii;
use yii\base\Model;

class IpAddressForm extends Model {
    public $ip;

    public function rules() {
        return [
            [['ip'], 'required'],
        ];
    }
}