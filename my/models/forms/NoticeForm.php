<?php
namespace my\models\forms;

use Yii;
use yii\base\Model;

class NoticeForm extends Model {
    public $email;

    public function rules() {
        return [
            [['email'], 'required'],
        ];
    }
}