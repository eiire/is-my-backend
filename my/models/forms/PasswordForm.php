<?php
namespace my\models\forms;

use Yii;
use yii\base\Model;

class PasswordForm extends Model {
    public $password;
    public $password_repeat;

    public function rules() {
        return [
            [['password', 'password_repeat'], 'required'],
        ];
    }
}