<?php
namespace my\models\forms;

use Yii;
use yii\base\Model;

class PaymentForm extends Model {
    public $sum;
    public $method;
    public $account;

    public function rules() {
        return [
            [['sum', 'method', 'account'], 'string']
        ];
    }
}