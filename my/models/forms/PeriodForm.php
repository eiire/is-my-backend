<?php
namespace my\models\forms;

use Yii;
use yii\base\Model;

class PeriodForm extends Model {
    public $period_start;
    public $period_end;
    public $filter;

    public function rules() {
        return [
            [['period_start', 'period_end'], 'required'],
        ];
    }
}