<?php
namespace my\models\forms;

use Yii;
use yii\base\Model;

class PhoneConfirmForm extends Model {
    public $code;

    public function rules() {
        return [
            [['code'], 'required']
        ];
    }
}