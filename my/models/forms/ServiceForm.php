<?php
namespace my\models\forms;

use Yii;
use yii\base\Model;

class ServiceForm extends Model {
    public $message;
    public $phone;
    public $person;

    public function rules() {
        return [
            [['message', 'phone', 'person'], 'required'],
        ];
    }
}