<?php
namespace my\models\forms;

use Yii;
use yii\base\Model;

class SessionsForm extends Model {
    public $count;

    public function rules() {
        return [
            [['count'], 'required']
        ];
    }
}