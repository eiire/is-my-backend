<?php
namespace my\models\forms;

use Yii;
use yii\base\Model;

class SettingsForm extends Model {
    public $old_password;
    public $new_password;
    public $new_password_repeat;

    public function rules() {
        return [
            [['old_password', 'new_password', 'new_password_repeat'], 'required'],
        ];
    }
}