<?php
namespace my\models\forms;

use Yii;
use yii\base\Model;

class SigninForm extends Model {
    public $login;
    public $password;

    public function rules() {
        return [
            [['login'], 'required'],
            [['password'], 'string']
        ];
    }
}