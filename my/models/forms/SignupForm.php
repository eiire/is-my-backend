<?php
namespace my\models\forms;

use Yii;
use yii\base\Model;

class SignupForm extends Model {
    public $login;
    public $inn;
    public $password;
    public $password_repeat;

    public function rules() {
        return [
            [['login', 'inn', 'password', 'password_repeat'], 'required']
        ];
    }
}