<?php
namespace my\models\forms;

use Yii;
use yii\base\Model;

class SpecialistForm extends Model {
    public $phone;
    public $person;
    public $type;
    public $soft;
    public $work;
    public $message;

    public function rules() {
        return [
            [['message', 'soft', 'work', 'phone', 'person', 'type'], 'required'],
        ];
    }
}