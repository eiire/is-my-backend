<?php
namespace my\models\forms;

use Yii;
use yii\base\Model;

class SpecialistFormDefault extends Model {
    public $message;
    public $phone;
    public $person;
    public $files;
    public $remote_access;
    public $configuration;

    public function rules() {
        return [
            [['message', 'phone', 'person'], 'required'],
            [['remote_access', 'configuration'], 'string'],
            [['files'], 'file', 'extensions' => 'jpg, jpeg, png, bmp', 'maxFiles' => 3]
        ];
    }
}