<?php
namespace my\models\forms;

use Yii;
use yii\base\Model;

class SpecialistTimeForm extends Model {
    public $guid;
    public $hour;
    public $date;

    public function rules() {
        return [
            [['guid', 'hour', 'date'], 'required'],
        ];
    }
}