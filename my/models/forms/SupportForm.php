<?php
namespace my\models\forms;

use Yii;
use yii\base\Model;

class SupportForm extends Model {
    public $message;
    public $db;
    public $email;

    public function rules() {
        return [
            [['message', 'db', 'email'], 'required'],
        ];
    }
}