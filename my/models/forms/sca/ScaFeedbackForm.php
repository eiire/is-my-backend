<?php
namespace my\models\forms\sca;

use Yii;
use yii\base\Model;

class ScaFeedbackForm extends Model {
    public $message;

    public function rules() {
        return [
            [['message'], 'required'],
        ];
    }
}