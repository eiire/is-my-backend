<?php
namespace my\models\forms\sca;

use Yii;
use yii\base\Model;

class ScaForm extends Model {
    public $email;
    public $type;
    public $code;
    public $srv;
    public $clop;
    public $clup;
    public $int;

    public function rules() {
        return [
            [['email', 'type', 'code'], 'required'],
            [['srv', 'clop', 'clup', 'int'], 'string']
        ];
    }
}