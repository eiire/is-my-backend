<?php
namespace my\models\users;

use Yii;
use yii\db\ActiveRecord;

class Users extends ActiveRecord {
    public $code;
    public $name;
    public $inn;
    public $segment;
    public $cashbox;
}