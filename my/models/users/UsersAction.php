<?php
namespace my\models\users;

use Yii;
use yii\db\ActiveRecord;

class UsersAction extends ActiveRecord {
    public $format_details;

    public function afterFind() {
        $this->format_details = str_replace(';', '<br>', $this->details);
    }
}