<?php
use common\widgets\Modal;
?>
<div class="item m-b-10">
    <div class="row m-b-5">
        <div class="col-12 col-lg-2">
            <div class="item-group">
                <div class="item-label">Номер документа</div>
                <div class="item-desc"><?=$result['number']; ?></div>
            </div>
        </div>
        <div class="col-12 col-lg-2">
            <div class="item-group">
                <div class="item-label">Дата</div>
                <div class="item-desc"><?=$result['date']; ?></div>
            </div>
        </div>
        <div class="col-12 col-lg-2">
            <div class="item-group">
                <div class="item-label">Сумма</div>
                <div class="item-desc"><?=$result['sum']; ?></div>
            </div>
        </div>
        <div class="col">
        <?php if ($result['status_edo']) : ?>
            <div class="item-group">
                <div class="item-label">Статус ЭДО</div>
                <div class="item-desc"><?=$result['status_edo']; ?></div>
            </div>
        <?php endif; ?>
        </div>
        <div class="col-auto d-flex">
            <a href="<?=$result['appendix']; ?>" class="btn btn-light btn-small my-auto" data-balloon="Скачать приложение">Приложение</a>
            <?php if ($result['download']) : ?>
                <a href="<?=$result['download']; ?>" class="btn btn-light btn-small ml-3 my-auto" data-balloon="Скачать акт">Акт</a>
            <?php endif; ?>
        </div>
    </div>
</div>