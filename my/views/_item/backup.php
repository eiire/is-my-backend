<?php
use yii\helpers\Url;
use common\widgets\Modal;
?>
<div class="item m-b-10">
    <div class="row">
        <div class="col-12 col-md-auto">
            <div class="item-group">
                <div class="item-label">Имя резервной копии</div>
                <div class="item-desc"><?=$result['name']; ?></div>
            </div>
        </div>
        <div class="col-12 col-md-auto">
            <div class="item-group">
                <div class="item-label">Статус</div>
                <div class="item-desc"><?=$result['condition']; ?></div>
            </div>
        </div>
        <div class="col-12 col-md-auto">
            <div class="item-group">
                <div class="item-label">Дата создания</div>
                <div class="item-desc"><?=$result['date']; ?></div>
            </div>
        </div>
        <div class="col-12 col-md-auto">
            <div class="item-group">
                <div class="item-label">Размер</div>
                <div class="item-desc"><?=$result['size']; ?></div>
            </div>
        </div>
        <?php if ($result['delete']) : ?>
        <div class="col-12 col-md text-right">
            <button data-fancybox data-src="#confirm-modal-<?=$result['id']; ?>" data-modal="true" class="btn btn-danger btn-small m-t-5" data-balloon="Удалить резервную копию" data-balloon-pos="up">Удалить</button>
        </div>
        <?php endif; ?>
    </div>
</div>
<?php 
    if ($result['delete']) echo Modal::widget([
        'id' => 'confirm-modal-' . $result['id'],
        'title' => 'Удаление резервной копии',
        'message' => 'Вы уверены что хотите удалить резервную копию<br>' . $result['name'] . '?',
        'button' => [
            'label' => 'Удалить',
            'route' => ['handler/handler', 'method' => 'delete-backup', 'db' => $result['db'], 'backup' => $result['name']]
        ]
    ]);
?>