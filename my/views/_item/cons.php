<?php
use yii\helpers\Url;
?>
<div class="item m-b-15">
    <div class="row">
        <div class="col-12 col-md-6 col-lg-3">
            <div class="item-group">
                <div class="item-label">Статус</div>
                <div class="item-desc <?=$result['status_style']; ?>"><?=$result['result']; ?></div>
            </div>
        </div>
        <div class="col-12 col-md-6 col-lg-2">
            <div class="item-group">
                <div class="item-label">Дата</div>
                <div class="item-desc"><?=$result['date']; ?></div>
            </div>
        </div>
        <div class="col-12 col-md">
            <div class="item-group">
                <div class="item-label">Содержание</div>
                <div class="item-desc"><?=$result['topic']; ?></div>
            </div>
        </div>
        <div class="col-12 col-md-auto text-right">
            <div class="item-group m-t-5">
            <?php if ($result['rating']) : ?>
                <div class="text-primary" data-balloon="Ваша оценка">
                <?php for ($i = 0; $i < $result['rating']; $i++) : ?>
                <i class="fas fa-star"></i>
                <?php endfor; ?>
                </div>
            <?php else : ?>
                <button data-fancybox data-src="#rating-modal" data-modal="true" data-rating-id="<?=$result['number']; ?>" data-rating-date="<?=$result['date']; ?>" class="btn btn-primary btn-small" data-balloon="Оценить качество услуги"><i class="far fa-star"></i></button>
            <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="item-details">
        <?php if ($result['question']) : ?>
        <div class="row m-b-5">
            <div class="col-12">
                <div class="item-group">
                    <div class="item-label">Вопрос</div>
                    <div class="item-desc"><?=$result['question']; ?></div>
                </div>
            </div>
        </div>
        <?php endif; ?>
        <div class="row">
            <div class="col-12 col-sm-6 col-lg-3">
                <div class="item-group">
                    <div class="item-label">Номер</div>
                    <div class="item-desc"><?=$result['format_number']; ?></div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-lg-3">
                <div class="item-group">
                    <div class="item-label">Длительность</div>
                    <div class="item-desc"><?=$result['duration']; ?></div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-lg-3">
                <div class="item-group">
                    <div class="item-label">Плановое время</div>
                    <div class="item-desc"><?=$result['planned_date']; ?></div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-lg-3">
                <div class="item-group">
                    <div class="item-label">Время ответа</div>
                    <div class="item-desc"><?=$result['date_response']; ?></div>
                </div>
            </div>
        </div>
    </div>
    <button class="item-more"><i class="fas fa-chevron-down"></i></button>
</div>