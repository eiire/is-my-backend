<div class="item m-b-10">
    <div class="row">
    <?php foreach ($items as $item) : ?>
        <div class="col-12 col-md">
            <div class="item-group">
                <div class="item-label"><?=$item['label']; ?></div>
                <div class="item-desc"><?=$item['value']; ?></div>
            </div>
        </div>
    <?php endforeach; ?>
    </div>
</div>