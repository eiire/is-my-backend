<?php
use common\widgets\Modal;
?>
<div class="item m-b-10">
    <div class="row m-b-5">
        <div class="col-3">
            <div class="item-group">
                <div class="item-label">Номер документа</div>
                <div class="item-desc"><?=$result['number']; ?></div>
            </div>
        </div>
        <div class="col-3">
            <div class="item-group">
                <div class="item-label">Дата</div>
                <div class="item-desc"><?=$result['date']; ?></div>
            </div>
        </div>
        <div class="col">
            <div class="item-group">
                <div class="item-label">Специалист</div>
                <div class="item-desc"><?=$result['name']; ?></div>
            </div>
        </div>
        <div class="col-auto p-t-5">
        <?php if ($result['sign']) : ?>
            <?php if ($result['rating']) : ?>
                <div class="text-primary" data-balloon="Ваша оценка">
                <?php for ($i = 0; $i < $result['rating']; $i++) : ?>
                <i class="fas fa-star"></i>
                <?php endfor; ?>
                </div>
            <?php else : ?>
                <button data-fancybox data-src="#rating-modal" data-modal="true" data-rating-id="<?=$result['number']; ?>" data-rating-date="<?=$result['date']; ?>" class="btn btn-primary btn-small" data-balloon="Оценить качество услуги"><i class="far fa-star"></i></button>
            <?php endif; ?>
        <?php else: ?>
            <button data-fancybox data-src="#confirm-modal-<?=$result['id']; ?>" data-modal="true" class="btn btn-primary btn-small">Подписать</button>
        <?php endif; ?>
        </div>
    </div>
    <div class="item-details">
        <?php if ($result['works']) : foreach ($result['works'] as $work) : ?>
        <div class="row">
            <div class="col">
                <div class="item-group">
                    <div class="item-label">Дата начала</div>
                    <div class="item-desc"><?=$work['date_start']; ?></div>
                </div>
            </div>
            <div class="col">
                <div class="item-group">
                    <div class="item-label">Дата завершения</div>
                    <div class="item-desc"><?=$work['date_end']; ?></div>
                </div>
            </div>
            <div class="col">
                <div class="item-group">
                    <div class="item-label">Продолжительность</div>
                    <div class="item-desc"><?=$work['duration']; ?></div>
                </div>
            </div>
            <div class="col">
                <div class="item-group">
                    <div class="item-label">Способ выполнения</div>
                    <div class="item-desc"><?=$work['type']; ?></div>
                </div>
            </div>
        </div>
        <div class="row m-b-10">
            <div class="col">
                <?=$work['description']; ?>
            </div>
        </div>
        <?php endforeach; endif; ?>
    </div>
    <button class="item-more"><i class="fas fa-chevron-down"></i></button>
</div>
<?php
    echo Modal::widget([
        'id' => 'confirm-modal-' . $result['id'],
        'title' => 'Подписать документ',
        'message' => 'Вы уверены что хотите подписать документ ' . $result['number'] . '?',
        'button' => [
            'label' => 'Подписать',
            'route' => ['handler/handler', 'method' => 'doc-signature', 'number' => $result['number'], 'date' => date('Ymd', strtotime($result['date']))]
        ]
    ]);
?>