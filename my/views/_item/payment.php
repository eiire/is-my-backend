<?php
use yii\helpers\Url; 
?>
<div class="item m-b-10">
    <div class="row">
        <div class="col-12 col-md-auto">
            <div class="item-group">
                <div class="item-label">Номер</div>
                <div class="item-desc"><?=$result['number']; ?></div>
            </div>
        </div>
        <div class="col-12 col-md-auto">
            <div class="item-group">
                <div class="item-label">Дата</div>
                <div class="item-desc"><?=$result['date']; ?></div>
            </div>
        </div>
        <div class="col-12 col-md-auto">
            <div class="item-group">
                <div class="item-label">Сумма</div>
                <div class="item-desc"><?=$result['sum']; ?></div>
            </div>
        </div>
        <div class="col-12 col-md">
            <div class="item-group">
                <div class="item-label">Описание</div>
                <div class="item-desc"><?=$result['content']; ?></div>
            </div>
        </div>
        <?php if ($result['status_edo']) : ?>
        <div class="col-12 col-md">
            <div class="item-group">
                <div class="item-label">Статус ЭДО</div>
                <div class="item-desc"><?=$result['status_edo']; ?></div>
            </div>
        </div>
        <?php endif; ?>
        <div class="col-12 col-md-auto text-right">
            <div class="item-group m-t-5">
            <?php if ($result['download']) : ?>
                <a href="<?=$result['download']; ?>" class="btn btn-light btn-small ml-3 my-auto" data-balloon="Скачать счет на оплату">Счет</a>
            <?php endif; ?>
            </div>
        </div>
    </div>
</div>