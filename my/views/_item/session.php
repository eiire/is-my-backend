<?php
use common\widgets\Modal;
?>
<div class="item m-b-10">
    <div class="row">
        <div class="col-12 col-md-1">
            <div class="item-group">
                <div class="item-label">Сеанс</div>
                <div class="item-desc"><?=$result['ns']; ?></div>
            </div>
        </div>
        <div class="col-12 col-md-4">
            <div class="item-group">
                <div class="item-label">Пользователь</div>
                <div class="item-desc"><?=$result['user']; ?></div>
            </div>
        </div>
        <div class="col-12 col-md">
            <div class="item-group">
                <div class="item-label">Время начала</div>
                <div class="item-desc"><?=$result['date']; ?></div>
            </div>
        </div>
        <div class="col-12 col-md-3">
            <div class="item-group">
                <div class="item-label">Приложение</div>
                <div class="item-desc"><?=$result['app']; ?></div>
            </div>
        </div>
        <div class="col-12 col-md text-right">
            <button data-fancybox data-src="#confirm-modal-<?=$result['ns']; ?>" data-modal="true" class="btn btn-danger btn-small m-t-5" data-balloon="Удалить сеанс" data-balloon-pos="up">Удалить</button>
        </div>
    </div>
</div>
<?php
    echo Modal::widget([
        'id' => 'confirm-modal-' . $result['ns'],
        'title' => 'Удаление сеанса',
        'message' => 'Вы уверены что хотите удалить сеанс пользователя ' . $result['user'] . '?',
        'button' => [
            'label' => 'Удалить',
            'route' => ['handler/handler', 'method' => 'delete-session', 'db' => $result['db'], 'ns' => $result['ns'], 'user' => $result['user'], 'guid' => $result['guid']]
        ]
    ]);
?>