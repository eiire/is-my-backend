<div class="item m-b-10">
    <div class="row">
        <div class="col-12 col-md">
            <div class="item-group">
                <div class="item-label">Номер</div>
                <div class="item-desc"><?=$result['number']; ?></div>
            </div>
        </div>
        <?php if ($result['date']) : ?>
        <div class="col-12 col-md">
            <div class="item-group">
                <div class="item-label">Дата</div>
                <div class="item-desc"><?=$result['date']; ?></div>
            </div>
        </div>
        <?php endif; ?>
        <?php if ($result['name']) : ?>
        <div class="col-12 col-md">
            <div class="item-group">
                <div class="item-label">Специалист</div>
                <div class="item-desc"><?=$result['name']; ?></div>
            </div>
        </div>
        <?php endif; ?>
        <?php if ($result['type']) : ?>
        <div class="col-12 col-md">
            <div class="item-group">
                <div class="item-label">Способ выполнения</div>
                <div class="item-desc"><?=$result['type']; ?></div>
            </div>
        </div>
        <?php endif; ?>
        <?php if ($result['work']) : ?>
        <div class="col-12 col-md">
            <div class="item-group">
                <div class="item-label">Вид работы</div>
                <div class="item-desc"><?=$result['work']; ?></div>
            </div>
        </div>
        <?php endif; ?>
        <?php if ($result['status']) : ?>
        <div class="col-12 col-md">
            <div class="item-group">
                <div class="item-label">Статус</div>
                <div class="item-desc"><?=$result['status']; ?></div>
            </div>
        </div>
        <?php endif; ?>
        <?php if ($result['name_task']) : ?>
            <div class="col-12 col-md">
                <div class="item-group">
                    <div class="item-label">Наименование</div>
                    <div class="item-desc"><?=$result['name_task']; ?></div>
                </div>
            </div>
        <?php endif; ?>
    </div>
    <?php if ($result['works'] || $result['desc']) : ?>
    <div class="item-details">
    <?php if ($result['works']) : foreach ($result['works'] as $work) : ?>
        <div class="row m-b-10">
            <div class="col-12 col-md">
            <?php if ($work['date']) : ?>
                <div class="item-group">
                    <div class="item-label">Дата</div>
                    <div class="item-desc"><?=$work['date']; ?></div>
                </div>
            <?php endif; ?>
            </div>
            <div class="col-12 col-md">
            <?php if ($work['time_start']) : ?>
                <div class="item-group">
                    <div class="item-label">Время начала</div>
                    <div class="item-desc"><?=$work['time_start']; ?></div>
                </div>
            <?php endif; ?>
            </div>
            <div class="col-12 col-md">
            <?php if ($work['time_end']) : ?>
                <div class="item-group">
                    <div class="item-label">Время завершения</div>
                    <div class="item-desc"><?=$work['time_end']; ?></div>
                </div>
            <?php endif; ?>
            </div>
            <div class="col-12 col-md">
            <?php if ($work['duration']) : ?>
                <div class="item-group">
                    <div class="item-label">Продолжительность</div>
                    <div class="item-desc"><?=$work['duration']; ?></div>
                </div>
            <?php endif; ?>
            </div>
        </div>
        <?php if ($work['desc']) : ?>
        <div class="row">
            <div class="col-12 col-md">
                <div class="item-group">
                    <div class="item-label">Описание</div>
                    <div class="item-desc"><?=$work['desc']; ?></div>
                </div>
            </div>
        </div>
        <?php endif; ?>
    <?php endforeach; endif; ?>
    <?php if ($result['desc']) : ?>
        <div class="row">
            <div class="col-12 col-md">
                <div class="item-group">
                    <div class="item-label">Описание</div>
                    <div class="item-desc"><?=$result['desc']; ?></div>
                </div>
            </div>
        </div>
        <?php endif; ?>
    </div>
    <button class="item-more"><i class="fas fa-chevron-down"></i></button>
    <?php endif; ?>
</div>