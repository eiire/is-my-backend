<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>
<div id="<?=$id; ?>" class="modal" style="display:none;min-width:500px;"> 
    <div class="modal-title">Ответ</div>
    <div class="modal-desc">
        <p id="questionsanswerform-question" class="p-b-10"></p>
        <?php 
            $active_form = ActiveForm::begin([
                'enableClientValidation' => false,
                'options' => ['autocomplete' => 'off'],
            ]);
        ?>
        <div class="input-group">
            <?=$active_form
                ->field($form, 'answer')
                ->textarea(['rows' => 5])
                ->label(false); ?>
            <?=$active_form
                ->field($form, 'id')
                ->input('hidden')
                ->label(false); ?>
        </div>
        <div class="text-right">
            <?=Html::submitButton('Ответить', ['class' => 'btn btn-primary']); ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>