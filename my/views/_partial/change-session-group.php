<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
?>
<div id="change-session-group-<?=crc32($current_group_name)?>" class="modal" style="display:none;">
    <div class="modal-title">Изменение сеансов группы</div>
    <?php
        $active_form = ActiveForm::begin([
            'enableClientValidation' => false,
            'options' => ['autocomplete' => 'off'],
            'action' => Url::to(['handler/', 'method' => 'change-session-group', 'current_group_name' => $current_group_name]),
        ]);

    ?>
    <div class="modal-desc">
        <div class="input-group">
            <?=$active_form
                ->field($form, 'group_name')
                ->input('number', ['min' => 0, 'max' => 999, 'step' => 1, 'id' => $current_group_name . '-change-session'])
                ->label('Введите количество сеансов'); ?>
        </div>
    </div>
    <div class="modal-control row">
        <div class="col text-left">
            <?=Html::submitButton('Изменить', ['class' => 'btn btn-primary', 'data-disabled' => true]); ?>
        </div>
        <div class="col text-right"><button type="button" class="btn btn-light" data-fancybox-close="">Отмена</button></div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
