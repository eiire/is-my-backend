<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>
<div id="create-base" class="modal" style="display:none;">
    <div class="modal-title">Создание базы</div>
    <?php 
        $active_form = ActiveForm::begin([
            'enableClientValidation' => false,
            'options' => ['autocomplete' => 'off'],
        ]);
    ?>
    <div class="modal-desc">
        <p>Обращаем ваше внимание, что база будет создана в течение 5-10 минут</p>
        <div class="input-group">
            <?=$active_form
                ->field($form, 'base')
                ->dropDownList($base_list)
                ->label('Выберите конфигурацию'); ?>
        </div>

        <?php if ($is_franch) : ?>
            <div class="input-group"> <?=$active_form
                    ->field($form, 'group_name')
                    ->dropDownList($group_names)
                    ->label('Выберите группу компаний'); ?>
            </div>
        <?php endif; ?>

        <?php if (!$group && !$new_client) : ?><div class="alert alert-warning mb-n1">После создания базы, сумма ежедневного списания будет изменена согласно тарифа</div><?php endif; ?>
        <?php if ($new_client) : ?><div class="alert alert-primary mb-n1">После создания базы, вы получите месяц бесплатного использования. Подробности на <a href="https://oblako.is1c.ru" target="_blank">oblako.is1c.ru</a></div><?php endif; ?>
    </div>
    <div class="modal-control row">
        <div class="col text-right">
            <?=Html::submitButton('Создать базу', ['class' => 'btn btn-primary', 'data-disabled' => true]); ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>