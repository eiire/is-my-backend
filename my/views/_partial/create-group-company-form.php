<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>
<div id="create-group" class="modal" style="display:none;">
    <div class="modal-title">Создание группы компаний</div>
    <?php
        $active_form = ActiveForm::begin([
            'enableClientValidation' => false,
            'options' => ['autocomplete' => 'off'],
        ]);
    ?>
    <div class="modal-desc">
        <div class="input-group"> <?=$active_form
                ->field($form, 'group_name')
                ->textInput()
                ->label('Введите название группы компаний'); ?>
        </div>
    </div>
    <div class="modal-control row">
        <div class="col text-right">
            <?=Html::submitButton('Создать группу компаний', ['class' => 'btn btn-primary', 'data-disabled' => true]); ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
