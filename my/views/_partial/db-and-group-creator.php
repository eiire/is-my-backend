<div class="row my-3">
    <div class="col-6" data-fancybox data-src="#create-group">
        <div class="db-add-block d-flex">
            <div class="m-auto text-lighten">Создать группу</div>
        </div>
    </div>
    <div class="col-6" data-fancybox data-src="#create-base">
        <div class="db-add-block d-flex">
            <div class="m-auto text-lighten">Создать базу данных</div>
        </div>
    </div>
</div>
<?=$this->render('@app/views/_partial/create-base-form', ['form' => $form, 'base_list' => $base_list, 'group' => $group, 'new_client' => $new_client, 'group_names' => $group_names, 'is_franch' => $is_franch]); ?>
<?=$this->render('@app/views/_partial/create-group-company-form', ['form' => $group_form]); ?>
