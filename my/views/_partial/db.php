<?php
use yii\helpers\Url;
use common\widgets\Nav;
use common\widgets\Modal;
?>
<?php if (!$base->frozen) : ?>
<div class="item db-block">
    <div class="row">
        <div class="col-9 col-md-11">
            <div class="db-title"><?=$base->name; ?> <span class="db-version" data-balloon="Версия" data-balloon-pos="up"><?=$base->release; ?></span></div>
        </div>
        <div class="db-settings">
            <div class="db-settings-icon nav-list-wrapper">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="22px" height="22px" viewBox="0 0 408 408">
                    <path d="M51,153c-28.05,0-51,22.95-51,51s22.95,51,51,51s51-22.95,51-51S79.05,153,51,153z M357,153c-28.05,0-51,22.95-51,51 s22.95,51,51,51s51-22.95,51-51S385.05,153,357,153z M204,153c-28.05,0-51,22.95-51,51s22.95,51,51,51s51-22.95,51-51 S232.05,153,204,153z"/>
                </svg>
            </div>
            <?=Nav::Widget($dbase_nav_list_configs[$base->db]); ?>
            <?=$this->render('@app/views/_partial/move-dbase',
                [
                    'form' => $group_form,
                    'groups' => $groups,
                    'current_group_name' => rawurlencode($group_company),
                    'name_db' => $base->db,
                    'groups_with_keys' => $groups_with_keys
                ]
            ); ?>
        </div>
    </div>
    <div class="row no-gutters">
        <div class="col-12 col-lg-4">
            <?php if ($base->tariff) : ?><div>Текущий тариф: <?=$base->tariff; ?></div><?php endif; ?>
            <?php if(!$base->group) : ?>
                <div>Дата создания: <?=date('d.m.Y', strtotime($base->date_start)) ?></div>
            <?php endif; ?>
            <div>ИНН: <?=$base->inn; ?></div>
            <?php if ($is_franch) : ?>
                <div>ИНН: <?=urldecode($base->group_name); ?></div>
            <?php else : ?>
                <div>Количество сеансов: <?=$base->count_sessions; ?><?php if ($base->update_date) : ?> (с <?=$base->update_date; ?> — <?=$base->update_count; ?>)<?php endif; ?></div>
            <?php endif; ?>
            <div class="db-link" data-balloon="Ссылка на базу" data-balloon-pos="up"><a href="https://saas.is1c.ru/<?=$base->db; ?>" target="_blank">https://saas.is1c.ru/<?=$base->db; ?></a></div>
        </div>
        <div class="col-12 col-lg-4 my-5 my-lg-0">
            <div class="row no-gutters">
                <div class="col-6 chart-block">
                    <div class="chart-title">Объем</div>
                    <canvas id="chart-size-<?=$base->id;?>"></canvas>
                </div>
                <div class="col-6">
                    <div class="chart-size">
                        <div>Занято</div> 
                        <div class="h2"><?=$base->format_size; ?></div>
                        <div class="m-t-5">Свободно</div>
                        <div class="h2"><?=$base->format_free_size; ?></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-4 chart-block">
            <?php if ($base->activity_count) : ?>
            <div class="chart-title">Количество входов</div>
            <canvas id="chart-activity-<?=$base->id;?>"></canvas>
            <?php endif; ?>
        </div>
    </div>
</div>
<script>
<?php if ($base->activity_count) : ?>
new Chart(document.getElementById('chart-activity-<?=$base->id;?>'), {
    type: 'bar',
    data: {
        labels: [<?=$base->activity_date; ?>],
        datasets: [{
            data: [<?=$base->activity_count; ?>, 0],
            backgroundColor: '#ef731c'
        }]
    },
    options: barOptions
});
<?php endif; ?>
new Chart(document.getElementById('chart-size-<?=$base->id;?>'), {
    type: 'doughnut',
    data: {
        datasets: [{
            data: [<?=$base->free_size; ?>, <?=$base->size; ?>],
            backgroundColor: ['#d9d9d9', '#f5d803']
        }]
    },
    options: doughnutOptions
});
</script>
<?php else : // Если база заморожена ?>
<div class="item db-block pb-2">
    <div class="row">
        <div class="col">
            <div class="db-title"><?=$base->name; ?> <span class="db-version" data-balloon="Версия" data-balloon-pos="up"><?=$base->release; ?></span></div>
            <div class="h3">База заморожена <?=$base->frozen_date; ?></div>
            <?php if ($base->defrost_date) : ?><div>Возможно разморозить с <?=$base->defrost_date; ?></div><?php endif; ?>
        </div>
        <?php if ($base->defrost) : ?>
        <div class="col-auto d-flex">
            <div class="my-auto">
                <button data-fancybox data-modal="true" data-src="#frozen-modal-<?=$base->id; ?>" class="btn btn-light btn-medium">Разморозить</button>
            </div>
        </div>
        <?php endif; ?>
    </div>
</div>
<?php endif; ?>
<?php 
if (!$base->test_drive) :
    echo Modal::widget([
        'id' => 'frozen-modal-' . $base->id,
        'title' => $modal[$base->frozen_type]['title'],
        'message' => $modal[$base->frozen_type]['message'],
        'button' => [
            'label' => $modal[$base->frozen_type]['button'],
            'route' => ['handler/handler', 'method' => 'frozen', 'db' => $base->db]
        ]
    ]);
endif;
