<?php
use common\widgets\Nav;
use yii\widgets\LinkPager;
?>

<?php foreach ($result as $group_company => $dbases_in_group) : ?>
    <?php if ($group_company == $first_empty_group) :?>
        <?php if (empty($is_filter)) : ?>
            <div class="row mb-3">
                <div class="col-12 col-md"><?=LinkPager::widget(['pagination' => $pagination]); ?></div>
            </div>
            <?=$this->render('@app/views/_partial/db-and-group-creator', [
                'form' => $form,
                'group_form' => $group_form,
                'base_list' => $base_list,
                'group' => $group,
                'new_client' => $new_client,
                'group_names' => $group_names,
                'is_franch' => $is_franch
            ]); ?>
            <div class="chart-title mb-3">Список пустых групп:</div>
        <?php endif; ?>
    <?php endif; ?>
    <div class="item mb-3 mt-3">
        <div class="h2 mt-2 mb-n0"><?=$group_company?></div>
        <?php if (!empty($dbases_in_group[0])) : ?>
            <div class="row no-gutters">
                <div class="col-12 col-lg-4">
                    <div>Количество сеансов: <?=$dbases_in_group[0]->count_sessions_group ?></div>
                </div>
            </div>
        <?php endif; ?>
        <div class="db-settings">
            <div class="db-settings-icon nav-list-wrapper">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="22px" height="22px" viewBox="0 0 408 408">
                    <path d="M51,153c-28.05,0-51,22.95-51,51s22.95,51,51,51s51-22.95,51-51S79.05,153,51,153z M357,153c-28.05,0-51,22.95-51,51 s22.95,51,51,51s51-22.95,51-51S385.05,153,357,153z M204,153c-28.05,0-51,22.95-51,51s22.95,51,51,51s51-22.95,51-51 S232.05,153,204,153z"/>
                </svg>
            </div>
            <?=Nav::Widget([
                'ul_class' => 'nav-list',
                'set_active' => false,
                'items' => [
                    [
                        'label' => 'Переименовать группу',
                        'route' => [
                            'modal' => true,
                            'src' => '#rename-group-' . crc32($group_company),
                        ],
                    ],
                    [
                        'label' => 'Изменение сеансов группы',
                        'route' => [
                            'modal' => true,
                            'src' => '#change-session-group-' . crc32($group_company),
                        ],
                    ]
                ]
            ]); ?>
            <?=$this->render('@app/views/_partial/rename-group', ['form' => $group_form, 'current_group_name' => $group_company]); ?>
            <?=$this->render('@app/views/_partial/change-session-group', ['form' => $group_form, 'current_group_name' => $group_company]); ?>
        </div>
        <?php foreach ($dbases_in_group as $dbase) : ?>
            <?=$this->render('@app/views/_partial/db', [
                'group_form' => $group_form,
                'base' => $dbase,
                'modal' => $frozen_modal,
                'groups' => $groups,
                'group_company' => $group_company,
                'groups_with_keys' => $groups_with_keys,
                'is_franch' => $is_franch,
                'dbase_nav_list_configs' => $dbase_nav_list_configs,
            ])?>
        <?php endforeach; ?>
    </div>
<?php endforeach; ?>
<?php if (empty($is_filter) && empty($first_empty_group)) : ?>
    <div class="row mb-3">
        <div class="col-12 col-md"><?=LinkPager::widget(['pagination' => $pagination]); ?></div>
    </div>
    <?=$this->render('@app/views/_partial/db-and-group-creator', [
        'form' => $form,
        'group_form' => $group_form,
        'base_list' => $base_list,
        'group' => $group,
        'new_client' => $new_client,
        'group_names' => $group_names,
        'is_franch' => $is_franch
    ]); ?>
<?php endif; ?>
