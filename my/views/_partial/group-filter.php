<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>
<div id="group-filter" class="modal" style="display:none;">
    <div class="modal-title">Выбор группы для отображения</div>
    <?php
        $active_form = ActiveForm::begin([
            'enableClientValidation' => false,
            'options' => ['autocomplete' => 'off'],
            'method' => 'get',
        ]);
    ?>
    <div class="modal-desc">
        <div class="input-group"> <?=$active_form
                ->field($form, 'group_name')
                ->dropDownList($group_names, ['id' => 'group-filter-dropdown'])
                ->label('Выберите группу компаний'); ?>
        </div>
    </div>
    <div class="modal-control row">
        <div class="col text-right">
            <?=Html::submitButton('Выбрать группу', ['class' => 'btn btn-primary', 'data-disabled' => true]); ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>