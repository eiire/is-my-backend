<?php
use yii\helpers\Url;
$title = ($title) ? $title : $this->title;
$url = ($url) ? $url : 'site/index';
?>
<div class="row no-gutters">
    <div class="col-auto d-none d-lg-block">
        <a href="<?=Url::to([$url]); ?>">
            <div class="home-logo logo m-r-20"></div>
        </a>
    </div>
    <div class="col">
        <div class="title home-title"><?=$title; ?></div>
    </div>
    <div class="col-12 col-md-auto">
        <div class="m-t-15">
            <?=$this->render('@my/views/_partial/profile', ['profile' => $profile, 'nav' => $nav]); ?>
        </div>
    </div>
</div>