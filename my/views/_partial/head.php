<?php
use yii\helpers\Url;
$title = ($title) ? $title : $this->title;
$auto = ($link || $button) ? '-auto' : '';
?>
<div class="row no-gutters">
    <div class="col-12 col-lg<?=$auto; ?>">
        <div><span class="title"><?=$title; ?></span>
        <?php if ($desc) : ?>
            <span class="text-lighten" data-balloon="<?=$desc['balloon']; ?>" data-balloon-pos="up"><?=$desc['label']; ?></span>
        <?php endif; ?>
        </div>
    </div>
    <?php if ($link || $button) : ?>
    <div class="col">
        <?php if ($link) : ?>
            <a href="<?=Url::to($link['route']); ?>" class="btn btn-primary btn-small btn-custom"<?php if ($link['balloon']) : ?> data-balloon="<?=$link['balloon']; ?>" data-balloon-pos="up"<?php endif; ?>><?=$link['label']; ?></a>
        <?php elseif ($button) : ?>
            <button data-fancybox data-src="<?=$button['src']; ?>" data-modal="true" class="btn btn-primary btn-small btn-custom"<?php if ($button['balloon']) : ?> data-balloon="<?=$button['balloon']; ?>" data-balloon-pos="up"<?php endif; ?>><?=$button['label']; ?></button>
        <?php endif; ?>
    </div>
    <?php endif; ?>
    <div class="col-12 col-lg-auto">
        <?=$this->render('@my/views/_partial/profile', ['profile' => $profile]); ?>
    </div>
</div>