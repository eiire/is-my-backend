<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
?>
<div id="movedb-<?=$name_db?>" class="modal" style="display:none;">
    <div class="modal-title">Перемещение базы данных</div>
    <?php
        $active_form = ActiveForm::begin([
            'enableClientValidation' => false,
            'options' => ['autocomplete' => 'off'],
            'action' => Url::to(['handler/', 'method' => 'move-db', 'current_group_name' => $current_group_name, 'name_db' => $name_db]),
        ]);
    ?>
    <div class="modal-desc">
        <div class="input-group">
            <?=$active_form
                ->field($form, 'group_name')
                ->dropDownList($groups_with_keys, ['id' => $name_db])
                ->label('Выберите группу компаний'); ?>
        </div>
    </div>
    <div class="modal-control row">
        <div class="col text-left">
            <?=Html::submitButton('Переместить', ['class' => 'btn btn-primary', 'data-disabled' => true]); ?>
        </div>
        <div class="col text-right"><button type="button" class="btn btn-light" data-fancybox-close="">Отмена</button></div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
