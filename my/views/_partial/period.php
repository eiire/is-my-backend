<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$data_period_start = ($data_period_start) ? $data_period_start : 'data-period-start';
$data_period_end = ($data_period_end) ? $data_period_end : 'data-period-end';
?>
<div class="period">
    <?php
        $active_form = ActiveForm::begin([
            'method' => 'get',
            // 'action' => Url::to([Yii::$app->controller->id . '/' . Yii::$app->controller->action->id]),
            'enableClientValidation' => false,
            'options' => ['autocomplete' => 'off'],
            'fieldConfig' => ['options' => ['tag' => false]]
        ]); 
    ?>
    <div class="row no-gutters m-b-5">
        <?php if ($filter) : ?>
        <div class="col-auto col-sm-2">
            <div class="input-group">
                <?=$active_form
                    ->field($form, 'filter')
                    ->dropDownList($filter['items'], ['class' => 'period-input period-filter', 'name' => 'filter', 'value' => $_GET['filter']])
                    ->label(false);
                ?>
            </div>
        </div>
        <?php endif; ?>
        <div class="col-12 col-sm-auto">
            <i class="far fa-calendar-alt m-r-5"></i>
            <span class="text-lighten">от</span>
            <?=$active_form
                ->field($form, 'period_start')
                ->input('text', ['name' => 'period_start', 'class' => 'period-input', 'value' => $period_start, 'readonly' => true, $data_period_start => true])
                ->label(false); 
            ?>
            <span class="text-lighten">до</span>
            <?=$active_form
                ->field($form, 'period_end')
                ->input('text', ['name' => 'period_end', 'class' => 'period-input', 'value' => $period_end, 'readonly' => true, $data_period_end => true])
                ->label(false); 
            ?>
        </div>
        <div class="col-12 col-sm">
            <?=Html::submitButton('Применить', ['class' => 'btn btn-light btn-small btn-period']); ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>