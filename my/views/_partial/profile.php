<?php
use common\widgets\Nav;
use yii\helpers\Url;
switch ($nav) {
    case 'exit':
        $items = [
            [
                'label' => 'Выход',
                'route' => ['site/index', 'logout' => true],
            ]
        ];
        break;
    default:
        $items = [
            [
                'label' => 'ИНН ' . $profile['inn']
            ],
            [
                'line' => true
            ],
            [
                'label' => 'История входов',
                'route' => ['site/history']
            ],
            [
                'label' => 'История действий',
                'route' => ['site/action']
            ],
            [
                'line' => true
            ],
            [
                'label' => 'Настройки',
                'route' => ['site/settings']
            ]
        ];
        break;
}

$profile_title = $profile['name'] ? $profile['name'] : 'Профиль';
?>
<div class="company">
    <div class="row no-gutters">
        <div class="col">
            <div class="nav-list-wrapper"><?=$profile_title; ?> <i class="fas fa-chevron-down"></i></div>
            <?=Nav::Widget([
                'ul_class' => 'nav-list',
                'set_active' => false,
                'items' => $items
                ]);
            ?>
        </div>
        <div class="col-auto ml-4">
            <a href="<?=Url::to(['site/index', 'logout' => true]); ?>" data-balloon="Выход"><i class="fas fa-sign-out-alt fa-lg"></i></a>
        </div>
    </div>
</div>