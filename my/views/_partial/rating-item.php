<div class="h3 mb-1"><?=$result['title']; ?></div>
<?php if ($result['number'] || $result['details']['specialist'] || $result['details']['date'] || $result['details']['title'] || $result['details']['desc']) : ?>
<div class="item">
    <div class="row pt-2">
        <?php if ($result['details']['specialist']) : ?>
            <div class="col">
                <div class="item-group">
                    <div class="item-label">Специалист</div>
                    <div class="item-desc"><?=$result['details']['specialist']; ?></div>
                </div>
            </div>
        <?php endif; ?>
        <?php if ($result['details']['title']) : ?>
            <div class="col">
                <div class="item-group">
                    <div class="item-label">Содержание</div>
                    <div class="item-desc"><?=$result['details']['title']; ?></div>
                </div>
            </div>
        <?php endif; ?>
        <?php if ($result['details']['date']) : ?>
            <div class="col">
                <div class="item-group">
                    <div class="item-label">Дата</div>
                    <div class="item-desc"><?=$result['details']['date']; ?></div>
                </div>
            </div>
        <?php endif; ?>
    </div>
    <?php if ($result['details']['desc']) : ?>
    <div class="item-details">
        <div class="row m-b-10">
            <div class="col">
                <?=$result['details']['desc']; ?>
            </div>
        </div>
    </div>
    <div class="item-more text-center"><i class="fas fa-chevron-down"></i></div>
    <?php endif; ?>
</div>
<?php endif; ?>
<?=$this->render('@my/views/_partial/stars', ['name' => $result['id'], 'count' => 6, 'legend' => [
    0 => 'Не могу оценить',
    1 => 'Плохо',
    2 => 'Неудовлетворительно',
    3 => 'Удовлетворительно',
    4 => 'Хорошо',
    5 => 'Отлично'
]]); ?>
<div class="row">
    <div class="col-12">
        <div class="input-group">
            <label for="comment-<?=$result['id']; ?>">Комментарий</label>
            <textarea name="comment-<?=$result['id']; ?>" id="comment-<?=$result['id']; ?>" rows="3"></textarea>
        </div>
    </div>
</div>
<?php if ($line) : ?><div class="line"></div><?php endif; ?>