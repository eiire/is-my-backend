<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>
<div id="sessions-modal" class="modal" style="display:none;width:410px;">
    <div class="modal-title">Изменение количества сеансов</div>
    <div class="modal-desc">
        <p id="sessions-message">Вы можете изменить текущее количество сеансов.</p>
        <p id="sessions-status" class="d-none"></p>
        <div class="text-center mt-2 mb-3">
            <button class="counter-btn" data-counter="minus"<?=($disabled) ? ' disabled' : ''; ?>>&minus;</button>
            <input type="text" class="counter-value" value="<?=$sessions; ?>" data-counter-value disabled>
            <button class="counter-btn" data-counter="plus">&plus;</button>
            <input type="hidden" value="<?=$db; ?>" data-counter-db>
        </div>
        <div class="alert alert-warning py-2 px-3">Увеличенное количество сеансов начнет действовать с сегодняшнего дня. Уменьшение количества сеансов начнет действовать с 1 числа следующего месяца.</div>
    </div>
    <div class="row modal-control">
        <div class="col-6">
            <button class="btn btn-light modal-btn" data-sessions>Изменить</button>
        </div>
        <div class="col-6 text-right">
            <a href="javascript:;" class="btn btn-primary modal-btn" data-sessions-close>Закрыть</a>
        </div>
    </div>
</div>