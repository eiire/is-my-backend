<?php
use common\widgets\Nav;
?>
<div class="sidebar">
    <div class="logo-wrapper">
        <a href="/">
            <div class="logo"></div>
        </a>
    </div>
    <?php
        $items = [
            [
                'label' => 'Главная',
                'icon' => 'fas fa-home',
                'route' => ['site/index']
            ],
            [
                'label' => 'Консультации',
                'icon' => 'fas fa-comments',
                'route' => ['support/index']
            ],
            [
                'label' => 'Сервисы ИТС',
                'icon' => 'fas fa-download',
                'route' => ['service/index'],
                'hidden' => $profile['is_budget']
            ],
            [
                'label' => 'Вопросы по ТО',
                'icon' => 'fas fa-comments',
                'route' => ['questions-equipment/index'],

            ],
            [
                'label' => 'Работы по 1С',
                'icon' => 'fas fa-user-cog',
                'route' => ['specialist/index'],
            ],
            [
                'label' => 'Облако',
                'icon' => 'fas fa-cloud',
                'route' => ['cloud/index']
            ],
            [
                'label' => 'Документы',
                'icon' => 'fas fa-file',
                'route' => ['docs/index']
            ]
        ];

        switch ($nav) {
            case 'cloud':
                $cloud_items = [
                    [
                        'line' => true,
                    ],
                    [
                        'label' => 'Тарифы',
                        'icon' => 'fas fa-money-bill',
                        'route' => ['cloud/price']
                    ],
                    [
                        'label' => 'Вопрос-ответ',
                        'icon' => 'fas fa-question-circle',
                        'route' => ['cloud/faq']
                    ],
                    [
                        'label' => 'Поддержка',
                        'icon' => 'fas fa-comment-alt',
                        'route' => ['cloud/support'],
                    ]
                ];

                $items = array_merge($items, $cloud_items);
            break;
            case 'event':
                $items = [
                    [
                        'label' => 'Программа',
                        'icon' => 'fas fa-bullhorn',
                        'route' => ['event/index']
                    ],
                    [
                        'label' => 'Далее',
                        'icon' => 'fas fa-clock',
                        'route' => ['event/timeline']
                    ],
                    [
                        'label' => 'Избранное',
                        'icon' => 'fas fa-star',
                        'route' => ['event/favorites']
                    ],
                    [
                        'label' => 'Анкета',
                        'icon' => 'fas fa-pen',
                        'route' => ['event/feedback']
                    ],
                    [
                        'label' => 'Мои конференции',
                        'icon' => 'fas fa-history',
                        'route' => ['event/history']
                    ],
                    [
                        'label' => 'Выход',
                        'icon' => 'fas fa-sign-out-alt',
                        'route' => ['event/index', 'logout' => true],
                    ]
                ];
            break;
            case 'docs':
                $docs_items = [
                    [
                        'line' => true,
                    ],
                    [
                        'label' => 'ЛУВР',
                        'icon' => 'fas fa-file-contract',
                        'route' => ['docs/sheet']
                    ],
                    [
                        'label' => 'Счета',
                        'icon' => 'fas fa-file-invoice-dollar',
                        'route' => ['docs/payment']
                    ],
                    [
                        'label' => 'Акты',
                        'icon' => 'fas fa-file-alt',
                        'route' => ['docs/act']
                    ]
                ];

                $items = array_merge($items, $docs_items);
            break;
        }

        echo Nav::Widget([
            'ul_class' => 'nav',
            'set_active' => true,
            'items' => $items
        ]);

        echo $this->render('contacts');
    ?>
</div>