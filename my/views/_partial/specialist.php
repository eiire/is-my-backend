<div class="col-12 col-lg-8">
    <div class="schedule m-t-10 m-b-20">
        <div class="row no-gutters">
            <div class="col-1 offset-3 offset-lg-2"><div class="schedule-hour">9:00</div></div>
            <div class="col-1"><div class="schedule-hour">10:00</div></div>
            <div class="col-1"><div class="schedule-hour">11:00</div></div>
            <div class="col-1"><div class="schedule-hour">12:00</div></div>
            <div class="col-1"><div class="schedule-hour">13:00</div></div>
            <div class="col-1"><div class="schedule-hour">14:00</div></div>
            <div class="col-1"><div class="schedule-hour">15:00</div></div>
            <div class="col-1"><div class="schedule-hour">16:00</div></div>
            <div class="col-1"><div class="schedule-hour">17:00</div></div>
        </div>
        <?php for ($i = 0; $i < count($result); $i++) : ?>
        <?php if ($result[$i]['hour'] == 1) : ?>
            <div class="row no-gutters">
            <div class="col-3 col-lg-2"><div class="schedule-day"><?php echo $days[date('w', strtotime($result[$i]['date']))] . ' &ndash; ' . date('d.m', strtotime($result[$i]['date'])); ?></div></div>
        <?php endif; ?>
            <div class="col-1"><div class="schedule-item<?php if (!$result[$i]['free']) echo ' schedule-disable'; ?>" data-guid="<?=$result[$i]['guid']; ?>" data-hour="<?=$result[$i]['hour']; ?>" data-date="<?=date('Ymd', strtotime($result[$i]['date'])); ?>"></div></div>
        <?php if ($result[$i]['hour'] == 9) : ?>
            </div>
        <?php endif; ?>
        <?php endfor; ?>
    </div>
</div>