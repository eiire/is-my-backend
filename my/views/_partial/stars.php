<fieldset class="rating">
    <?php for ($i = $count - 1; $i >= 0; $i--) : ?>
    <input type="radio" id="<?=$name; ?><?=$i; ?>" name="<?=$name; ?>" value="<?=$i; ?>">
    <label class="full" for="<?=$name; ?><?=$i; ?>" title="<?=$legend[$i]; ?>"></label>
    <?php endfor; ?>
</fieldset>