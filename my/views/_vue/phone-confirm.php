<div id="phone-confirm" enable="<?=$enable; ?>" phone="<?=$phone; ?>">
    <p class="mb-1">{{ status }}</p>
    <div class="input-group input-inline">
        <div class="row no-gutters">
            <div class="col-3">
                <label for="phone">Номер телефона</label>
                <div class="row no-gutters">
                    <div class="col-auto d-flex">
                        <div class="h4 my-auto mr-2">+7</div>
                    </div>
                    <div class="col">
                        <input id="phone" type="text" v-model="phone" :disabled="inProgress" autocomplete="off">
                    </div>
                </div>
            </div>
            <div class="col-2 ml-2" v-if="inProgress">
                <label for="code">Код подтверждения</label>
                <input id="code" type="text" v-model="code" :maxlength="max">
            </div>
            <div class="col-auto">
                <?php if (!$enable) : ?>
                <button v-on:click="phoneConfirm" v-if="!inProgress" :disabled="phoneBtnDisabled" class="btn btn-primary">Отправить</button>
                <button v-on:click="codeConfirm" v-if="inProgress" :disabled="codeBtnDisabled" class="btn btn-primary">Подтвердить</button>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <span v-if="timerVisible">Отправить код еще раз через 00:{{ time }}</span>
            <button v-on:click="resendCode" v-if="resendBtnVisible" class="btn btn-link">Отправить код еще раз</button>
        </div>
    </div>
</div>