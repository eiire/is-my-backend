<?php
$this->title = 'Новый пароль';
use yii\widgets\ActiveForm;
use common\widgets\Alert;
use yii\helpers\Url;
use yii\helpers\Html;
?>
<div class="login-page">
    <div class="login-container">
        <div class="logo-wrapper">
            <div class="logo"></div>
        </div>
        <div class="login-block">
            <h2 class="text-center light">Новый пароль</h2>
            <?php if ($alert) echo Alert::Widget(['type' => $alert['type'], 'message' => $alert['message']]); ?>
            <?php if ($show_form) : ?>
            <?php
                $active_form = ActiveForm::begin([
                    'id' => 'login-form',
                    'enableClientValidation' => false,
                    'options' => ['autocomplete' => 'off'],
                ]); 
            ?>
                <div class="input-group">
                    <?=$active_form
                        ->field($form, 'password')
                        ->passwordInput()
                        ->input('password', ['placeholder' => 'Введите ваш пароль', 'autofocus' => 'autofocus'])
                        ->label('Пароль');
                    ?>
                </div>
                <label class="text-lighten">Пароль должен быть длиной более 8 символов и содержать хотя бы одну заглавную букву или символ.</label>
                <div class="input-group">
                    <?=$active_form
                        ->field($form, 'password_repeat')
                        ->passwordInput()
                        ->input('password', ['placeholder' => 'Ваш пароль еще раз'])
                        ->label('Пароль еще раз');
                        ?>
                </div>
                <?=Html::submitButton('Сохранить', ['class' => 'btn btn-primary login-btn']); ?>
            <?php ActiveForm::end(); ?>
            <?php elseif ($alert['type'] == 'success') : ?>
                <a href="<?=Url::to(['auth/signin']); ?>" class="btn btn-primary login-btn">Войти</a>
            <?php endif; ?>
        </div>
    </div>
</div>