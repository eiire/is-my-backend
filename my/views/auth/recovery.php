<?php
$this->title = 'Восстановление пароля';
use yii\widgets\ActiveForm;
use common\widgets\Alert;
use yii\helpers\Html;
use yii\helpers\Url;

$this->registerJsFile('https://www.google.com/recaptcha/api.js');
?>
<div class="login-page">
    <div class="login-container">
        <div class="logo-wrapper">
            <div class="logo"></div>
        </div>
        <div class="login-block">
            <h2 class="text-center light">Восстановление пароля</h2>
            <?php if ($alert) echo Alert::Widget(['type' => $alert['type'], 'message' => $alert['message']]); ?>
            <?php if ($show_form) : ?>
            <?php
                $active_form = ActiveForm::begin([
                    'id' => 'login-form',
                    'enableClientValidation' => false,
                    'options' => ['autocomplete' => 'off'],
                ]); 
            ?>
                <div class="input-group">
                    <?=$active_form
                        ->field($form, 'email')
                        ->input('text', ['placeholder' => 'Введите ваш email', 'autofocus' => 'autofocus', 'value' => $email])
                        ->label('Email');
                    ?>
                </div>
                <div class="input-group">
                    <div class="g-recaptcha" data-sitekey="<?=Yii::$app->params['recaptcha']['public']; ?>"></div>
                </div>
                <?=Html::submitButton('Восстановить пароль', ['class' => 'btn btn-primary login-btn']); ?>
                <div class="divider">
                    <span>или</span>
                </div>
                <a href="<?=Url::to(['auth/signin']); ?>" class="btn btn-light login-btn">Войти</a>
            <?php ActiveForm::end(); ?>
            <?php endif; ?>
        </div>
    </div>
</div>