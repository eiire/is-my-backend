<?php
$this->title = 'Авторизация';
use yii\widgets\ActiveForm;
use common\widgets\Alert;
use yii\helpers\Html;
use yii\helpers\Url;

$this->registerJsFile('https://www.google.com/recaptcha/api.js');
?>
<div class="login-page">
    <div class="login-container">
        <div class="logo-wrapper">
            <div class="logo"></div>
        </div>
        <div class="login-block">
            <h2 class="text-center light">Вход в аккаунт</h2>
            <?php if ($alert) echo Alert::Widget(['type' => $alert['type'], 'message' => $alert['message']]); ?>
            <?php if ($show_form) : ?>
            <?php
                $active_form = ActiveForm::begin([
                    'id' => 'login-form',
                    'enableClientValidation' => false
                ]); 
            ?>
                <div class="input-group">
                    <?=$active_form
                        ->field($form, 'login')
                        ->input('text', ['placeholder' => 'Введите ваш email', 'value' => $email, 'autofocus' => 'autofocus'])
                        ->label('Email');
                    ?>
                </div>
                <div class="input-group">
                    <?=$active_form
                        ->field($form, 'password')
                        ->passwordInput()
                        ->input('password', ['placeholder' => 'Введите ваш пароль'])
                        ->label('Пароль');
                    ?>
                    <div class="login-recovery">
                        <?=Html::a('Восстановить пароль', ['auth/recovery']); ?>
                    </div>
                </div>
                <?php if ($show_captcha) : ?>
                <div class="input-group">
                    <div class="g-recaptcha" data-sitekey="<?=Yii::$app->params['recaptcha']['public']; ?>"></div>
                </div>
                <?php endif; ?>
                <?=Html::submitButton('Войти', ['class' => 'btn btn-primary login-btn']); ?>
            <?php ActiveForm::end(); ?>
            <?php elseif ($alert['type'] == 'success') : ?>
                <?=Html::a('Войти', ['auth/signin'], ['class' => 'btn btn-light login-btn']); ?>
            <?php endif; ?>
            <div class="divider"><span>или</span></div>
            <?=Html::a('Зарегистрироваться', ['auth/signup'], ['class' => 'btn btn-light login-btn']); ?>
        </div>
    </div>
</div>