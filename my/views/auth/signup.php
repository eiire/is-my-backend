<?php
$this->title = 'Регистрация';
use yii\widgets\ActiveForm;
use common\widgets\Alert;
use yii\helpers\Html;
use yii\helpers\Url;

$this->registerJsFile('https://www.google.com/recaptcha/api.js');
?>
<div class="login-page">
    <div class="login-container">
        <div class="logo-wrapper">
            <div class="logo"></div>
        </div>
        <div class="login-block">
            <h2 class="text-center light"><?=$this->title; ?></h2>
            <?php if ($alert) echo Alert::Widget(['type' => $alert['type'], 'message' => $alert['message']]);
                $active_form = ActiveForm::begin([
                    'id' => 'login-form',
                    'enableClientValidation' => false,
                    'options' => ['autocomplete' => 'off']
                ]);
            ?>
                <div class="input-group">
                    <?=$active_form
                        ->field($form, 'login')
                        ->input('text', ['placeholder' => 'Введите ваш email', 'autofocus' => 'autofocus', 'required' => true])
                        ->label('Email');
                    ?>
                </div>
                <div class="input-group">
                    <?=$active_form
                        ->field($form, 'inn')
                        ->input('text', ['placeholder' => 'Введите ваш ИНН', 'required' => true])
                        ->label('ИНН');
                    ?>
                </div>
                <div class="input-group">
                    <?=$active_form
                        ->field($form, 'password')
                        ->passwordInput()
                        ->input('password', ['placeholder' => 'Введите ваш пароль', 'required' => true])
                        ->label('Пароль');
                    ?>
                </div>
                <label class="text-lighten">Пароль должен быть длиной более 8 символов и содержать хотя бы одну заглавную букву или символ.</label>
                <div class="input-group">
                    <?=$active_form
                        ->field($form, 'password_repeat')
                        ->passwordInput()
                        ->input('password', ['placeholder' => 'Ваш пароль еще раз', 'required' => true])
                        ->label('Пароль еще раз');
                        ?>
                </div>
                <div class="input-group mb-0">
                    <div class="g-recaptcha" data-sitekey="<?=Yii::$app->params['recaptcha']['public']; ?>"></div>
                </div>
                <label class="text-lighten">Нажимая на кнопку, вы даете согласие на обработку своих <a href="<?=Url::to(['pages/privacy']); ?>" target="_blank">персональных данных</a>.</label>
                <?=Html::submitButton('Зарегистрироваться', ['class' => 'btn btn-primary login-btn']); ?>
                <div class="divider"><span>или</span></div>
                <?=Html::a('Войти', ['auth/signin'], ['class' => 'btn btn-light login-btn']); ?>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>