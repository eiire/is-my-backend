<?php
$this->title = 'Резервные копии';
use common\widgets\Alert;
use common\widgets\Modal;
use yii\helpers\Html;

$button = [];
if ($create_backup) {
    $button = [
        'label' => 'Создать резервную копию',
        'src' => '#confirm-modal'
    ];
}
?>
<div class="main">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
                <?=$this->render('@app/views/_partial/sidebar', ['profile' => $profile, 'nav' => 'cloud']); ?>
            </div>
            <div class="col-12 col-lg-10">
                <?=$this->render('@app/views/_partial/head', [
                    'button' => $button,
                    'profile' => $profile
                ]); ?>
                <div class="block">
                    <?php 
                        if ($response) echo Alert::Widget(['type' => $response['type'], 'message' => $response['message'], 'close' => $response['close']]);
                        if ($result) :
                            foreach ($result as $row) :
                                echo $this->render('@app/views/_item/backup', ['result' => $row]);
                            endforeach;
                        endif;
                    ?>
                    <?php 
                        if ($create_backup) echo Modal::widget([
                            'id' => 'confirm-modal',
                            'title' => 'Создание резервной копии',
                            'message' => 'Вы уверены что хотите создань новую резервную копию?',
                            'button' => [
                                'label' => 'Создать',
                                'route' => ['handler/handler', 'method' => 'create-backup', 'db' => $db]
                            ]
                        ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>