<?php
$this->title = 'Вопрос-ответ';
use yii\helpers\Url;
?>
<div class="main">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
                <?=$this->render('@app/views/_partial/sidebar', ['profile' => $profile,'nav' => 'cloud']); ?>
            </div>
            <div class="col-12 col-lg-10">
                <?=$this->render('@app/views/_partial/head', ['profile' => $profile]); ?>
                <div class="block">
                    <div class="collapse">
                        <div class="collapse-header">Как обратиться в ТехПоддержку</div>
                        <div class="collapse-body">
                            <p class="m-b-0">Круглосуточно по e-mail: <a href="maito:saas@is1c.ru">saas@is1c.ru</a>, в разделе <a href="<?=Url::to(['cloud/support']);?>">Поддержка</a> личного кабинета.</p>
                            <p>В рабочее время (с 09-00 до 18-00 по Новосибирску) так же и по WhatsApp номер: 8-962-833-89-39</p>
                            <p class="m-b-0">В обращении необходимо указать название Вашей организации и желательно ИНН.</p>
                            <p>Также предоставить снимок экрана с ошибкой.</p>
                        </div>
                    </div>
                    <div class="collapse">
                        <div class="collapse-header">Как начать работу в Облаке когда уже пришло письмо о публикации базы?</div>
                        <div class="collapse-body">Скачайте <a href="/upload/faq/saas.pdf" target="_blank">файл</a> и выполните инструкции внутри него.</div>
                    </div>
                    <div class="collapse">
                        <div class="collapse-header">Программа выдает сообщения с рекомендацией обновить платформу</div>
                        <div class="collapse-body">
                            <img src="/upload/faq/img-1.jpg" style="max-width:100%;" alt="">
                            <p class="m-t-20 m-b-0">Это просто информация с рекомендаций! Вам нужно нажать кнопку «Продолжить».</p>
                            <p>Платформа в Облаке обновляется централизованно.</p>
                        </div>
                    </div>
                    <div class="collapse">
                        <div class="collapse-header">Программа не пускает и выдаёт сообщение: "Идентификация пользователя не выполнена"</div>
                        <div class="collapse-body">
                            <img src="/upload/faq/img-2.jpg" style="max-width:100%;" alt="">
                            <p class="m-t-20">Вы неверно набрали имя пользователя и/ или пароль. Обратите внимание на отсутствие символа «пробел» в начале и в конце имени пользователя и/или пароля. Этот символ не виден глазу, но программа его воспринимает.</p>
                        </div>
                    </div>
                    <div class="collapse">
                        <div class="collapse-header">Программа требует обновить программу доступа</div>
                        <div class="collapse-body">
                            <img src="/upload/faq/img-3.jpg" style="max-width:100%;" alt="">
                            <p class="m-t-20">Нужно нажать "Обновить и запустить"</p>
                        </div>
                    </div>
                    <div class="collapse">
                        <div class="collapse-header">Как выгрузить данные из базы?</div>
                        <div class="collapse-body">
                            <p>1. Открываем информационную базу, данные которой требуется выгрузить. Заходим в Администрирование - Выгрузить данные для перехода в Сервис.</p>
                            <img src="/upload/faq/img-4.jpg" style="max-width:100%;" alt="">
                            <p class="mt-4">2. Будет выведено предупреждение о том, что выгрузка может занять продолжительное время. Следует нажать кнопку Продолжить.</p>
                            <img src="/upload/faq/img-5.jpg" style="max-width:100%;" alt="">
                            <p class="mt-4">3. Если в информационной базе не работают другие пользователи, начнется процесс выгрузки данных. При этом отображается окно, сообщающее о том, что выполняемое действие может быть длительным.</p>
                            <img src="/upload/faq/img-6.jpg" style="max-width:100%;" alt="">
                            <p class="mt-4">4. После того, как архив с данными приложения будет сформирован, будет выведен запрос Сохранить как.</p>
                            <img src="/upload/faq/img-7.jpg" style="max-width:100%;" alt="">
                        </div>
                    </div>
                    <div class="collapse">
                        <div class="collapse-header">Как оплатить действующий договор?</div>
                        <div class="collapse-body">Для оплаты перейдите по ссылке: <a href="https://my.is1c.ru/cloud/payment">https://my.is1c.ru/cloud/payment</a></div>
                    </div>
                    <div class="collapse">
                        <div class="collapse-header">Где можно скачать тонкий клиент?</div>
                        <div class="collapse-body">
                        <p>Скачать тонкий клиент, для своей операционной системы, можно по ссылкам:</p>
                            <div class="row">
                                <div class="col-auto text-center">
                                    <a href="http://1c.is1c.ru/setuptc.exe">
                                        <img src="/upload/os/windows.svg" width="32px" alt="">
                                        <p class="m-t-5">Windows x32</p>
                                    </a>
                                </div>
                                <div class="col-auto text-center">
                                    <a href="http://1c.is1c.ru/setuptc64.exe">
                                        <img src="/upload/os/windows.svg" width="32px" alt="">
                                        <p class="m-t-5">Windows x64</p>
                                    </a>
                                </div>
                                <div class="col-auto text-center">
                                    <a href="http://1c.is1c.ru/thin.client.rpm32.tar.gz">
                                        <img src="/upload/os/linux.svg" width="32px" alt="">
                                        <p class="m-t-5">Linux x32</p>
                                    </a>
                                </div>
                                <div class="col-auto text-center">
                                    <a href="http://1c.is1c.ru/thin.client.rpm64.tar.gz">
                                        <img src="/upload/os/linux.svg" width="32px" alt="">
                                        <p class="m-t-5">Linux x64</p>
                                    </a>
                                </div>
                                <div class="col-auto text-center">
                                    <a href="http://1c.is1c.ru/clientosx.zip">
                                        <img src="/upload/os/mac.svg" width="32px" alt="">
                                        <p class="m-t-5">macOS</p>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>