<?php
$this->title = 'История входов';
use common\widgets\Alert;
use yii\widgets\LinkPager;
use yii\helpers\Url;
?>
<div class="main">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
                <?=$this->render('@app/views/_partial/sidebar', ['profile' => $profile,'nav' => 'cloud']); ?>
            </div>
            <div class="col-12 col-lg-10">
                <?=$this->render('@app/views/_partial/head', [
                    'link' => [
                        'label' => 'Сохранить в Excel',
                        'balloon' => 'Сохранить историю в Excel',
                        'route' => ['handler/handler', 'method' => 'save-activity', 'db' => $db, 'period_start' => $period_start, 'period_end' => $period_end]
                    ],
                    'profile' => $profile
                ]); ?>
                <div class="block">
                <?php
                    echo $this->render('@app/views/_partial/period', [
                        'form' => $form,
                        'period_start' => $period_start,
                        'period_end' => $period_end
                    ]); 
                    if ($response) echo Alert::Widget(['type' => $response['type'], 'message' => $response['message'], 'close' => $response['close']]);
                    if ($result) :
                        foreach ($result as $row) :
                            echo $this->render('@app/views/_item/default', ['items' => $row]);
                        endforeach;
                        echo LinkPager::widget(['pagination' => $pagination]);
                    endif;
                ?>
                </div>
            </div>
        </div>
    </div>
</div>