<?php
$this->title = 'Облако';
use common\widgets\Alert;
use yii\helpers\Url;
use common\widgets\Modal;
use yii\widgets\LinkPager;
$this->registerJsFile('/js/chart.js', ['position' => $this::POS_HEAD]);
?>
<div class="main">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
                <?=$this->render('@app/views/_partial/sidebar', ['profile' => $profile,'nav' => 'cloud']); ?>
            </div>
            <div class="col-12 col-lg-10">
                <?=$this->render('@app/views/_partial/head', ['profile' => $profile]); ?>
                <div class="block">
                    <?php if ($response) echo Alert::Widget(['type' => $response['type'], 'message' => $response['message'], 'close' => $response['close']]); ?>
                    <?php if ($balance) : ?>
                    <div class="row no-gutters">
                        <div class="col-12 col-lg">
                            <div class="h3">Текущий баланс: <?=$balance['current']; ?></div>
                            <?php if ($balance['negative']) : ?>
                            <div class="text-danger">Необходимо пополнить баланс</div>
                            <?php else : ?>
                            <div class="text-lighten">Оставшейся суммы хватит на <?=$balance['days']; ?> <?=$balance['days_title']; ?></div>
                            <?php endif; ?>
                            <?php if ($is_franch) : ?> <div class="h3">Общее количество сеансов по группам: <?=$sum_sessions_groups ?></div> <?php endif; ?>
                        </div>
                        <div class="col-12 col-lg-auto d-flex my-2 my-lg-0 mr-3">
                            <a href="<?=Url::to(['cloud/payment-history']); ?>" class="btn btn-link my-auto">Детализация баланса</a>
                        </div>
                        <div class="col-12 col-lg-auto d-flex">
                            <a href="<?=Url::to(['cloud/payment']); ?>" class="btn btn-primary my-auto">Пополнить баланс</a>
                        </div>
                    </div>
                    <?php endif; ?>
                    <?php if ($result && $is_franch) : ?>
                        <div class="mt-3 ml-1">Выберите <div class="btn btn-link" data-fancybox data-src="#group-filter">группу</div> или <div class="btn btn-link" data-fancybox data-src="#dbase-filter">базу данных</div> для отображения</div>
                        <?=$this->render('@app/views/_partial/franch-dbases-render', [
                            'result' => $result,
                            'first_empty_group' => $first_empty_group,
                            'pagination' => $pagination,
                            'form' => $form,
                            'base_list' => $base_list,
                            'group' => $group,
                            'new_client' => $new_client,
                            'group_names' => $group_names,
                            'group_form' => $group_form,
                            'is_franch' => $is_franch,
                            'dbase_nav_list_configs' => $dbase_nav_list_configs,
                            'modal' => $frozen_modal,
                            'groups' => $groups,
                            'is_filter' => $is_filter,
                            'groups_with_keys' => $groups_with_keys,
                        ]); ?>
                        <?php if ($is_filter) : ?>
                            <a href="<?=Url::to(['cloud/index']); ?>" class="btn btn-link mb-3">Сбросить фильтр</a>
                        <?php endif; ?>
                        <?=$this->render('@app/views/_partial/sessions-form', [
                            'form' => $form,
                            'db' => $base->db,
                            'sessions' => $base->count_sessions,
                            'disabled' => false
                        ]); ?>
                        <?=$this->render('@app/views/_partial/dbase-filter', [
                            'form' => $form,
                            'base_list' => $user_dbases_ids,
                            'group' => $group,
                            'new_client' => $new_client,
                            'group_names' => $group_names,
                            'is_franch' => $is_franch
                        ]); ?>
                        <?=$this->render('@app/views/_partial/group-filter', [
                            'form' => $group_form,
                            'base_list' => $base_list,
                            'group' => $group,
                            'new_client' => $new_client,
                            'group_names' => $group_names,
                            'is_franch' => $is_franch
                        ]); ?>
                    <?php elseif ($result && !$is_franch) : ?>
                        <?php foreach ($result as $dbase) : ?>
                            <?=$this->render('@app/views/_partial/db', [
                                'group_form' => $group_form,
                                'base' => $dbase,
                                'modal' => $frozen_modal,
                                'groups' => $groups,
                                'group_company' => $group_company,
                                'groups_with_keys' => $groups_with_keys,
                                'is_franch' => $is_franch,
                                'dbase_nav_list_configs' => $dbase_nav_list_configs,
                            ])?>
                        <?php endforeach; ?>
                        <div class="db-add mt-3 mb-3 col-12" data-fancybox data-src="#create-base" data-touch="false" data-balloon="Добавить базу">
                            <div class="db-add-block"><span>+</span></div>
                        </div>
                        <?=$this->render('@app/views/_partial/create-base-form', ['form' => $form, 'base_list' => $base_list, 'group' => $group, 'new_client' => $new_client, 'group_names' => $group_names, 'is_franch' => $is_franch]); ?>
                        <div class="row mb-3">
                            <div class="col-12 col-md"><?=LinkPager::widget(['pagination' => $pagination]); ?></div>
                        </div>
                    <?php elseif (empty($result) && $is_franch) : ?>
                        <?=$this->render('@app/views/_partial/db-and-group-creator', [
                            'form' => $form,
                            'group_form' => $group_form,
                            'base_list' => $base_list,
                            'group' => $group,
                            'new_client' => $new_client,
                            'group_names' => $group_names,
                            'is_franch' => $is_franch
                        ]); ?>
                    <?php elseif (empty($result) && !$is_franch) : ?>
                        <div class="db-add mt-3 col-12" data-fancybox data-src="#create-base" data-touch="false" data-balloon="Добавить базу">
                            <div class="db-add-block"><span>+</span></div>
                        </div>
                        <?=$this->render('@app/views/_partial/create-base-form', ['form' => $form, 'base_list' => $base_list, 'group' => $group, 'new_client' => $new_client, 'group_names' => $group_names, 'is_franch' => $is_franch]); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?= Modal::widget([
    'id' => 'delete-modal',
    'title' => 'Удаление базы',
    'message' => '
        Вы уверены что хотите удалить базу <span id="delete-db-name"></span>?
        <div class="alert alert-danger mt-3 mb-n2 px-3 py-2">После удаления базы, восстановить данные будет невозможно!</div>
    ',
    'form' => [
        'action' => Url::to(['handler/handler', 'method' => 'delete-db']),
        'button' => [
            'label' => 'Удалить'
        ],
        'input' => [
            'name' => 'delete-db-name'
        ]
    ]
]); ?>
