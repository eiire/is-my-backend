<?php
$this->title = 'Ограничения по IP адресам';
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use common\widgets\Alert;
use common\widgets\Modal;
?>
<div class="main">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
                <?=$this->render('@app/views/_partial/sidebar', ['profile' => $profile, 'nav' => 'cloud']); ?>
            </div>
            <div class="col-12 col-lg-10">
                <?=$this->render('@app/views/_partial/head', ['profile' => $profile]); ?>
                <div class="block">
                    <?php if ($alert) echo Alert::Widget(['type' => $alert['type'], 'message' => $alert['message'], 'close' => $alert['close']]); ?>
                    <?php if ($ip_address) : ?>
                    <p>IP адреса, с которых разрешен доступ к базе.</p>
                    <?php foreach ($ip_address as $ip) : ?>
                    <div class="mb-1"><?=$ip['ip']; ?>
                        <button data-fancybox data-src="#ip-modal-<?=$ip['id']; ?>" data-modal="true" class="btn-icon btn-icon-trash ml-1" data-balloon="Удалить"><i class="fas fa-times"></i></button>
                        <?php
                            echo Modal::widget([
                                'id' => 'ip-modal-' . $ip['id'],
                                'title' => 'Удалить IP адрес',
                                'message' => 'Вы уверены что хотите удалить доступ к базе с IP адреса ' . $ip['ip'] . '?',
                                'button' => [
                                    'label' => 'Удалить',
                                    'route' => ['handler/handler', 'method' => 'delete-ip', 'db' => $db, 'ip' => $ip['ip']]
                                ]
                            ]);
                        ?>
                    </div>
                    <?php endforeach; else : ?>
                    <p>Для данной базы нет ограничений по IP.</p>
                    <?php endif; ?>
                    <p class="mt-3">Ваш текущий IP адрес <?=$user_ip; ?></p>
                    <p class="mb-n2">Добавить внешний IP адрес</p>
                    <?php
                        $active_form = ActiveForm::begin([
                            'enableClientValidation' => false,
                            'options' => ['autocomplete' => 'off'],
                        ]); 
                    ?>
                    <div class="row no-gutters mt-3">
                        <div class="col-12 col-md-3">
                            <div class="input-group">
                                <?=$active_form
                                    ->field($form, 'ip')
                                    ->input('text', ['value' => $user_ip, 'data-mask-ip' => true])
                                    ->label(false);
                                ?>
                            </div>
                        </div>
                        <div class="col ml-3">
                            <?=Html::submitButton('Добавить', ['class' => 'btn btn-primary btn-medium']); ?>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>