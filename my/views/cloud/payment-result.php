<?php
$this->title = 'Пополнение счета';
?>
<div class="main">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
                <?=$this->render('@app/views/_partial/sidebar', ['profile' => $profile, 'nav' => 'cloud']); ?>
            </div>
            <div class="col-12 col-lg-10">
                <?=$this->render('@app/views/_partial/head', [
                    'button' => $button,
                    'profile' => $profile
                ]); ?>
                <div class="block">
                    <div class="col-12 col-md-8 offset-md-2">
                        <div class="text-center">
                            <i class="fas fa-check-circle fa-10x text-success m-t-10"></i>
                            <div class="h2 m-t-10 m-b-5">Пополнение счета</div>
                            <p>Счет успешно пополнен на <?=$sum; ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>