<?php
$this->title = 'Пополнение баланса';
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use common\widgets\Alert;
?>
<div class="main">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
                <?=$this->render('@app/views/_partial/sidebar', ['profile' => $profile, 'nav' => 'cloud']); ?>
            </div>
            <div class="col-12 col-lg-10">
                <?=$this->render('@app/views/_partial/head', ['profile' => $profile]); ?>
                <div class="block">
                    <?php if ($alert) echo Alert::Widget(['type' => $alert['type'], 'message' => $alert['message'], 'close' => $alert['close']]); ?>
                    <?php if (!$account) : ?>
                    <div class="h3 mb-1">Сумма для пополнения</div>
                    <div class="row">
                        <div class="col-4 col-lg-2">
                            <div class="input-group">
                                <input type="text" class="payment-field" data-payment-sum="<?=$min_sum; ?>">
                                <div class="input-group-postfix payment-icon">&#8381;</div>
                            </div>
                        </div>
                        <div class="col-12 col-lg-auto">
                            <div class="text-lighten mt-2">Минимальная сумма платежа <?=$min_sum; ?></div>
                        </div>
                    </div>
                    <div class="row mt-1">
                        <div class="col-auto">
                        <?php foreach ($quick_pay as $sum) : ?>
                            <button class="btn btn-light btn-small" data-payment-quick="<?=$sum['value']; ?>"><?=$sum['format_value']; ?></button>
                        <?php endforeach; ?>
                        </div>
                    </div>
                    <div class="h3 mt-4 mb-1">Способ оплаты</div>
                    <?php if ($unpaid_account) : ?>
                    <div class="alert alert-primary">
                        <div class="row">
                            <div class="col d-flex">
                                <div class="my-auto">У вас уже есть неоплаченный счет</div>
                            </div>
                            <div class="col-auto">
                                <a href="<?=Url::to(['docs/payment']); ?>" class="btn btn-primary btn-small">Список счетов</a>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>
                    <div class="row">
                        <div class="col-auto">
                            <div class="row mb-2">
                                <div class="col-auto mb-3 mb-lg-0">
                                    <div class="payment-method" data-payment-method="account">
                                        <div class="payment-method-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg" version="1" viewBox="0 0 512 512"><path d="M444.875,109.792L338.208,3.125c-2-2-4.708-3.125-7.542-3.125h-224C83.146,0,64,19.135,64,42.667v426.667 C64,492.865,83.146,512,106.667,512h298.667C428.854,512,448,492.865,448,469.333v-352 C448,114.5,446.875,111.792,444.875,109.792z M341.333,36.417l70.25,70.25h-48.917c-11.771,0-21.333-9.573-21.333-21.333V36.417z M426.667,469.333c0,11.76-9.563,21.333-21.333,21.333H106.667c-11.771,0-21.333-9.573-21.333-21.333V42.667 c0-11.76,9.563-21.333,21.333-21.333H320v64C320,108.865,339.146,128,362.667,128h64V469.333z"/><path d="M373.333 298.667H138.667c-5.896 0-10.667 4.771-10.667 10.667 0 5.896 4.771 10.667 10.667 10.667h234.667c5.896 0 10.667-4.771 10.667-10.667C384 303.438 379.229 298.667 373.333 298.667zM373.333 234.667H138.667c-5.896 0-10.667 4.771-10.667 10.667 0 5.896 4.771 10.667 10.667 10.667h234.667c5.896 0 10.667-4.771 10.667-10.667C384 239.438 379.229 234.667 373.333 234.667zM373.333 362.667H138.667c-5.896 0-10.667 4.771-10.667 10.667 0 5.896 4.771 10.667 10.667 10.667h234.667c5.896 0 10.667-4.771 10.667-10.667C384 367.438 379.229 362.667 373.333 362.667zM266.667 426.667h-128c-5.896 0-10.667 4.771-10.667 10.667 0 5.896 4.771 10.667 10.667 10.667h128c5.896 0 10.667-4.771 10.667-10.667C277.333 431.438 272.563 426.667 266.667 426.667zM234.667 181.333c0 5.896 4.771 10.667 10.667 10.667h128c5.896 0 10.667-4.771 10.667-10.667 0-5.896-4.771-10.667-10.667-10.667h-128C239.438 170.667 234.667 175.438 234.667 181.333zM160 170.667h-21.333c-5.896 0-10.667 4.771-10.667 10.667 0 5.896 4.771 10.667 10.667 10.667h10.667c0 5.896 4.771 10.667 10.667 10.667s10.667-4.771 10.667-10.667v-1.965C183.056 185.617 192 173.888 192 160c0-17.646-14.354-32-32-32-5.875 0-10.667-4.781-10.667-10.667 0-5.885 4.792-10.667 10.667-10.667h21.333c5.896 0 10.667-4.771 10.667-10.667s-4.771-10.667-10.667-10.667h-10.667c0-5.896-4.771-10.667-10.667-10.667s-10.667 4.771-10.667 10.667v1.965C136.944 91.716 128 103.445 128 117.333c0 17.646 14.354 32 32 32 5.875 0 10.667 4.781 10.667 10.667S165.875 170.667 160 170.667z"/></svg>
                                        </div>
                                        <div class="payment-method-title">По счету</div>
                                    </div>
                                </div>
                                <div class="col-auto">
                                    <div class="payment-method" data-payment-method="card">
                                        <div class="payment-method-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg" version="1" viewBox="0 0 512 512"><path d="M273 337h137c5 0 8-3 8-8v-77c0-5-3-9-8-9H273c-5 0-8 4-8 9v77c0 5 3 8 8 8zm9-77h119v60H282v-60zM43 226h68c5 0 8-3 8-8s-3-9-8-9H43c-5 0-9 4-9 9s4 8 9 8zM145 226h68c5 0 9-3 9-8s-4-9-9-9h-68c-5 0-8 4-8 9s3 8 8 8zM43 260h111c5 0 8-3 8-8s-3-9-8-9H43c-5 0-9 4-9 9s4 8 9 8zM213 243h-25c-5 0-9 4-9 9s4 8 9 8h25c5 0 9-3 9-8s-4-9-9-9z"/><path d="M504 137c-6-7-13-11-23-12l-20-3V81c0-19-16-34-34-34H34C15 47 0 62 0 81v273c0 16 10 29 24 33 0 17 13 32 31 34l394 44h3c17 0 33-13 34-29l26-274c1-8-2-18-8-25zM17 107h427v51H17v-51zm17-43h393c9 0 17 8 17 17v9H17v-9c0-9 8-17 17-17zM17 354V175h427v179c0 10-8 17-17 17H34c-9 0-17-7-17-17zm478-195l-26 274c-1 9-9 16-18 15L57 404c-8-1-14-8-15-16h385c18 0 34-15 34-34V138l19 2c5 0 9 3 12 6 2 3 4 9 3 13z"/></svg>
                                        </div>
                                        <div class="payment-method-title">По банковской карте</div>
                                    </div>
                                </div>
                            </div>
                            <?php
                                $active_form = ActiveForm::begin([
                                    'enableClientValidation' => false
                                ]);

                                echo $active_form
                                    ->field($form, 'sum')
                                    ->input('hidden', ['id' => 'payment-sum'])
                                    ->label(false);

                                echo $active_form
                                    ->field($form, 'method')
                                    ->input('hidden', ['id' => 'payment-method'])
                                    ->label(false);
                            ?>
                            <div class="text-right">
                                <?=Html::submitButton('Пополнить', ['class' => 'btn btn-primary mt-3', 'id' => 'payment-pay', 'disabled' => true, 'data-disabled' => true]); ?>
                            </div>
                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                    <?php else : // Если указан номер счета ?>
                    <div class="h3 mb-1">Оплата по счету</div>
                        <p class="mb-1">Ваш счет для пополнения баланса на сумму <?=$account['sum']; ?> сформирован.</p>
                        <a href="<?=Url::to(['handler/handler', 'method' => 'get-account', 'date' => $account['date'], 'account' => $account['number']]); ?>" class="btn btn-primary btn-small">Скачать счет</a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>