<?php
$this->title = 'Тарифы';
?>
<div class="main">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
                <?=$this->render('@app/views/_partial/sidebar', ['profile' => $profile, 'nav' => 'cloud']); ?>
            </div>
            <div class="col-12 col-lg-10">
                <?=$this->render('@app/views/_partial/head', ['profile' => $profile]); ?>
                <div class="block">
                    <table class="price-table">
                        <tr>
                            <th rowspan="3" style="width:33%"></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>2-9</td>
                            <td>10-19</td>
                            <td>20+</td>
                        </tr>
                        <tr>
                            <td>пользователь</td>
                            <td>пользователей</td>
                            <td>пользователей</td>
                            <td>пользователей</td>
                        </tr>
                        <tr>
                            <td>1С: Бухгалтерия 8 ПРОД ред.3</td>
                            <td>1200 р./мес за пользователя<div class="m-t-5"><i>40 р./день</i></div></td>
                            <td>1110 р./мес за пользователя<div class="m-t-5"><i>37 р./день</i></div></td>
                            <td>990 р./мес за пользователя<div class="m-t-5"><i>33 р./день</i></div></td>
                            <td>960 р./мес за пользователя<div class="m-t-5"><i>32 р./день</i></div></td>
                        </tr>
                        <tr>
                            <td>
                                <div class="m-b-5">1С: Комплексная Автоматизация</div>
                                <div class="m-b-5">1С: Комплексная торговлей 11</div>
                                <div class="m-b-5">1С: Комплексная торговлей 11 + 1С:СRM</div>
                                <div>1С: Управление нашей фирмой 8</div>
                            </td>
                            <td>1290 р./мес за пользователя<div class="m-t-5"><i>43 р./день</i></div></td>
                            <td>1200 р./мес за пользователя<div class="m-t-5"><i>40 р./день</i></div></td>
                            <td>1110 р./мес за пользователя<div class="m-t-5"><i>37 р./день</i></div></td>
                            <td>1050 р./мес за пользователя<div class="m-t-5"><i>35 р./день</i></div></td>
                        </tr>
                        <tr>
                            <td>
                                <div>1С: Комплексная Автоматизаяция 2</div>
                            </td>
                            <td><i class="far fa-times-circle"></i></td>
                            <td>1710 р./мес за пользователя<div class="m-t-5"><i>57 р./день</i></div></td>
                            <td>1590 р./мес за пользователя<div class="m-t-5"><i>53 р./день</i></div></td>
                            <td>1500 р./мес за пользователя<div class="m-t-5"><i>50 р./день</i></div></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td colspan="4" class="text-center p-t-10 p-b-10"><h3 class="m-0">Услуги в рамках тарифа</h3></td>
                        </tr>
                        <tr>
                            <td>Безлимитное количество юр.лиц</td>
                            <td><i class="far fa-check-circle"></i></td>
                            <td><i class="far fa-check-circle"></i></td>
                            <td><i class="far fa-check-circle"></i></td>
                            <td><i class="far fa-check-circle"></i></td>
                        </tr>
                        <tr>
                            <td>Обновление типовых ИБ</td>
                            <td><i class="far fa-check-circle"></i></td>
                            <td><i class="far fa-check-circle"></i></td>
                            <td><i class="far fa-check-circle"></i></td>
                            <td><i class="far fa-check-circle"></i></td>
                        </tr>
                        <tr>
                            <td>Резервное копирование ИБ</td>
                            <td><i class="far fa-check-circle"></i></td>
                            <td><i class="far fa-check-circle"></i></td>
                            <td><i class="far fa-check-circle"></i></td>
                            <td><i class="far fa-check-circle"></i></td>
                        </tr>
                        <tr>
                            <td>Подключение стартовых сервисов 1С</td>
                            <td><i class="far fa-check-circle"></i></td>
                            <td><i class="far fa-check-circle"></i></td>
                            <td><i class="far fa-check-circle"></i></td>
                            <td><i class="far fa-check-circle"></i></td>
                        </tr>
                        <tr>
                            <td>Линия консультации</td>
                            <td><div>15 мин/день</div><div>2 раза в месяц</div></td>
                            <td><div>15 мин/день</div><div>2 раза в месяц</div></td>
                            <td><div>15 мин/день</div><div>ежедневно</div></td>
                            <td><div>15 мин/день</div><div>ежедневно</div></td>
                        </tr>
                        <tr>
                            <td>Объём информационной базы</td>
                            <td><div class="h3">5 Гб</div></td>
                            <td><div class="h3">10 Гб</div></td>
                            <td><div class="h3">15 Гб</div></td>
                            <td><div class="h3">20 Гб</div></td>
                        </tr>
                    </table>
                    <p class="m-t-15 m-b-0"><small>Услуга предоставляется с помощью доступа через веб-браузер или тонкий клиент</small></p>
                    <p class="m-t-0"><small>Стоимость указана за один доступ к одной базе. Тарификация ежедневная</small></p>
                </div>
            </div>
        </div>
    </div>
</div>