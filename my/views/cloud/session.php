<?php
$this->title = 'Активные сеансы';
use common\widgets\Alert;
use yii\widgets\LinkPager;
?>
<div class="main">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
                <?=$this->render('@app/views/_partial/sidebar', ['profile' => $profile, 'nav' => 'cloud']); ?>
            </div>
            <div class="col-12 col-lg-10">
                <?=$this->render('@app/views/_partial/head', [
                    'desc' => [
                        'label' => $result_count . ' на ' . date('d.m.Y H:i:s', time()),
                        'balloon' => 'Количество активных сеансов'
                    ],
                    'profile' => $profile
                ]); ?>
                <div class="block">
                    <?php 
                        if ($response) echo Alert::Widget(['type' => $response['type'], 'message' => $response['message'], 'close' => $response['close']]);
                        if ($result) :
                            foreach ($result as $row) :
                                echo $this->render('@app/views/_item/session', ['result' => $row]);
                            endforeach;
                            echo LinkPager::widget(['pagination' => $pagination]);
                        endif;
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>