<?php
$this->title = 'Изменение количества сеансов';
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use common\widgets\Alert;
?>
<div class="main">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
                <?=$this->render('@app/views/_partial/sidebar', ['profile' => $profile, 'nav' => 'cloud']); ?>
            </div>
            <div class="col-12 col-lg-10">
                <?=$this->render('@app/views/_partial/head', ['profile' => $profile]); ?>
                <div class="block">
                    <?php if ($alert) echo Alert::Widget(['type' => $alert['type'], 'message' => $alert['message'], 'close' => $alert['close']]); ?>
                    <?php if ($change_available) : ?>
                        <div class="db-title mb-2"><?=$base->name; ?></div>
                        <div>Текущее максимальное количество сеансов: <?=$base->count_sessions; ?></div>
                        <?php if ($base->update_date) : ?>
                        <div class="alert alert-primary my-3">
                            <div class="row">
                                <div class="col d-flex">
                                    <div class="my-auto">Количество сеансов будет уменьшено <?=$base->update_date; ?> до <?=$base->update_count; ?></div>
                                </div>
                                <div class="col-auto">
                                    <a href="<?=Url::to(['handler/handler', 'method' => 'sessions-change-canceled', 'db' => $base->db]); ?>" class="btn btn-primary btn-small">Отменить</a>
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                        <div class="h3 mt-3 mb-2"><?=!$future_change ? 'Вы можете изменить максимальное количество сеансов' : 'Для изменения количества сеансов необходимо отменить запрос на уменьшение количества сеансов'; ?></div>
                        <?php
                            $active_form = ActiveForm::begin([
                                'enableClientValidation' => false,
                            ]); 
                        ?>
                            <div class="row no-gutters">
                                <div class="col-auto">
                                    <?=$active_form
                                        ->field($form, 'count')
                                        ->input('number', ['value' => $base->count_sessions, 'min' => 1, 'style' => 'width:70px', 'disabled' => $future_change])
                                        ->label(false)
                                    ?>
                                </div>
                                <div class="col-auto ml-3">
                                    <?=Html::submitButton('Сохранить', ['class' => 'btn btn-primary btn-medium', 'disabled' => $future_change]); ?>
                                </div>
                            </div>
                        <?php ActiveForm::end(); ?>
                        <div class="alert alert-warning mt-3">
                            <div>Обращаем ваше внимание, что увеличение количества сеансов происходит мгновенно.</div>
                            <div>При уменьшении количества сеансов, изменения начинают действовать с 1 числа следующего месяца.</div>
                            <div>При изменении количества сеансов на Тест-драйве происходит немедленное переключение на платный тариф.</div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>