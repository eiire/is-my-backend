<?php
$this->title = 'Техническая поддержка';
use yii\widgets\ActiveForm;
use common\widgets\Alert;
use yii\helpers\Url;
use yii\helpers\Html;
?>
<div class="main">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
                <?=$this->render('@app/views/_partial/sidebar', ['profile' => $profile, 'nav' => 'cloud']); ?>
            </div>
            <div class="col-12 col-lg-10">
                <?=$this->render('@app/views/_partial/head', ['profile' => $profile]); ?>
                <div class="block">
                    <?php if ($response) :
                        echo Alert::Widget(['type' => $response['type'], 'message' => $response['message']]); 
                    else: ?>
                        <div class="alert alert-warning">
                            <p class="m-b-0">Техническая поддержка Облака 1С отвечает за работу самого сервиса (не открывается 1С, не подходит пароль и т.п.).</p>
                            <p class="m-b-0">По вопросам работы "внутри 1С" обращайтесь на <a href="<?=Url::to(['support/report']); ?>">Линию консультаций</a> или телефону 211-27-27.</p>
                        </div>
                    <?php endif; ?>
                    <?php if ($show_form) : ?>
                    <?php
                        $active_form = ActiveForm::begin([
                            'enableClientValidation' => false,
                            'options' => ['autocomplete' => 'off', 'enctype' => 'multipart/form-data'],
                        ]); 
                    ?>
                        <div class="input-group">
                        <?=$active_form
                            ->field($form, 'db')
                            ->dropDownList($bases)
                            ->label('Выберите базу');
                        ?>
                        </div>
                        <div class="input-group">
                        <?=$active_form
                            ->field($form, 'message')
                            ->textarea(['rows' => 8])
                            ->label('Сообщение');
                        ?>
                        <?=$active_form
                            ->field($form, 'email')
                            ->hiddenInput(['value' => $user->getEmail()])
                            ->label(false);
                        ?>
                        </div>
                        <div class="row">
                            <div class="col-12 text-right">
                                <?=Html::submitButton('Отправить <i class="fas fa-paper-plane m-l-5"></i>', ['class' => 'btn btn-primary']); ?>
                            </div>
                        </div>
                    <?php ActiveForm::end(); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>