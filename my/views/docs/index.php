<?php
$this->title = 'Документы';
use yii\helpers\Url;
?>
<div class="main">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
            <?=$this->render('@my/views/_partial/sidebar', ['profile' => $profile, 'nav' => 'docs']); ?>
            </div>
            <div class="col-12 col-lg-10">
                <?=$this->render('@my/views/_partial/head', [
                    'profile' => $profile
                ]); ?>
                <div class="row">
                    <div class="col-12 col-md-4">
                        <?=$this->render('@common/views/_partial/card', [
                            'title' => 'Листы учета выполненных работ',
                            'icon' => 'fas fa-file-contract',
                            
                            'route' => ['docs/sheet']
                        ]); ?>
                    </div>
                    <div class="col-12 col-md-4">
                        <?=$this->render('@common/views/_partial/card', [
                            'title' => 'Счета',
                            'icon' => 'fas fa-file-invoice-dollar',
                            'route' => ['docs/payment']
                        ]); ?>
                    </div>
                    <div class="col-12 col-md-4">
                        <?=$this->render('@common/views/_partial/card', [
                            'title' => 'Акты',
                            'icon' => 'fas fa-file-alt',
                            'route' => ['docs/act']
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>