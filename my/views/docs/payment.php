<?php
$this->title = 'Счета';
use common\widgets\Alert;
use yii\helpers\Url;
use yii\helpers\Html;
?>
<div class="main">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
            <?=$this->render('@my/views/_partial/sidebar', ['profile' => $profile, 'nav' => 'docs']); ?>
            </div>
            <div class="col-12 col-lg-10">
                <?=$this->render('@my/views/_partial/head', [
                    'profile' => $profile
                ]); ?>
                <div class="block">
                    <?php 
                    if ($response) echo Alert::Widget(['type' => $response['type'], 'message' => $response['message'], 'close' => $response['close']]);
                    if ($result) :
                        foreach($result as $row) :
                            echo $this->render('@my/views/_item/payment', ['result' => $row]);
                        endforeach;
                    endif;
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>