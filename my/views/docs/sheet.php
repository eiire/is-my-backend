<?php
$this->title = 'Листы учета выполненных работ';
use common\widgets\Alert;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\LinkPager;
?>
<div class="main">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
                <?=$this->render('@my/views/_partial/sidebar', ['profile' => $profile, 'nav' => 'docs']); ?>
            </div>
            <div class="col-12 col-lg-10">
                <?=$this->render('@my/views/_partial/head', [
                    'profile' => $profile
                ]); ?>
                <div class="block">
                <?php
                    echo $this->render('@my/views/_partial/period', [
                        'form' => $form,
                        'period_start' => $period_start,
                        'period_end' => $period_end,
                    ]);
                    if ($response) echo Alert::Widget(['type' => $response['type'], 'message' => $response['message'], 'close' => $response['close']]);
                    if ($result) :
                        foreach($result as $row) :
                            echo $this->render('@my/views/_item/docs', ['result' => $row]);
                        endforeach;
                        echo $this->render('@common/views/_partial/rating-form', ['form' => $form_rating]);
                ?>
                <div class="row">
                    <div class="col-12 col-md"><?=LinkPager::widget(['pagination' => $pagination]); ?></div>
                    <div class="col-12 col-md-auto m-t-5"><span class="text-lighten">Всего найдено</span> <?=$result_count; ?></div>
                </div>
                <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>