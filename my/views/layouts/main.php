<?php
use my\assets\AppAsset;
AppAsset::register($this);
$this->beginPage();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title><?=$this->title; ?></title>
        <?php $this->registerAssetBundle(yii\web\JqueryAsset::className(), $this::POS_HEAD); ?>
        <?php $this->head(); ?>
    </head>
    <body>
    <?php $this->beginBody(); ?>
        <?php if (Yii::$app->controller->id != 'auth') : ?>
        <div class="navbar">
            <button class="navbar-btn"></button>
            <div class="navbar-wrapper">
                <div class="navbar-title">
                    <a href="/">ИнфоСофт</a>
                </div>
            </div>
        </div>
        <?php endif; ?>
        <?=$content; ?>
    <?php $this->endBody(); ?>
    <script>(function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)}; m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)}) (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym"); ym(50799319, "init", { clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true });</script>
    </body>
</html>
<?php $this->endPage(); ?>