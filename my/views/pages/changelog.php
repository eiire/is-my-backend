<h1>Changelog</h1>
<?php foreach ($commits as $commit) : ?>
<p>
    <div>[<span title="<?=$commit->id; ?>"><?=$commit->short_id; ?></span>] <?=$commit->date; ?></div>
    <div><?=$commit->title; ?></div>
</p>
<?php endforeach; ?>