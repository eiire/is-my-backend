<?php
$this->title = 'Оценка качества услуг';
use common\widgets\Alert;
use yii\helpers\Html;
use yii\helpers\Url;
?>
<div class="container">
    <div class="row">
        <div class="col-12 col-lg-8 offset-lg-2">
            <div class="logo-in-page">
                <a href="<?=Url::base(true); ?>">
                    <div class="logo"></div>
                </a>
            </div>
            <div class="block">
                <?php if ($alert) echo Alert::Widget(['type' => $alert['type'], 'message' => $alert['message'], 'close' => $alert['close']]); ?>
                <?php if ($rating) : ?>
                <h1 class="mb-n1"><?=$this->title; ?></h1>
                <div class="mb-4">В период с <?=$period['start']; ?> по <?=$period['end']; ?></div>
                <?=Html::beginForm(['pages/rating', 'id' => $id], 'post', ['enctype' => 'multipart/form-data']) ?>
                    <?php foreach ($rating as $row) : ?>
                        <?=$this->render('@my/views/_partial/rating-item', ['result' => $row, 'line' => $row['child'] ? false : true]); ?>
                        <?php if ($row['child']) : ?>
                            <div class="collapse">
                                <div class="collapse-header collapse-header-extend">Смотреть все обращения</div>
                                <div class="collapse-body">
                                    <?php foreach ($row['child'] as $child) : ?>
                                        <?=$this->render('@my/views/_partial/rating-item', ['result' => $child, 'line' => true]); ?>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="line"></div>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <?php if ($nps) : ?>
                        <div class="pt-3 mb-n2">Насколько вероятно, что вы порекомендуете нашу Компанию своим знакомым?</div>
                        <?=$this->render('@my/views/_partial/stars', ['name' => 'nps', 'count' => 11, 'legend' => [
                            0 => 'Не могу оценить',
                            1 => 'Никогда',
                            2 => 'Вы шутите?! Нет',
                            3 => 'Маловероятно',
                            4 => 'Скорее Нет, чем Да',
                            5 => 'Возможно',
                            6 => 'Скорее Да, чем Нет',
                            7 => 'Вероятно порекомендую',
                            8 => 'Конечно',
                            9 => 'Я уже рекомендую',
                            10 => 'К вам уже приходили от меня',
                        ]]); ?>
                        <div class="clearfix"></div>
                        <div class="row">
                            <div class="col-12">
                                <div class="input-group">
                                    <label for="comment-nps">Что бы вы изменили в работе с нами? </label>
                                    <textarea name="comment-nps" id="comment-nps" rows="3"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="line"></div>
                    <?php endif; ?>
                    <div class="row mb-n2">
                        <div class="col">
                            <p>Более детальную информацию о проделанных работах вы можете посмотреть в <a href="<?=Url::to(['site/index']); ?>">личном кабинет</a>.</p>
                        </div>
                        <div class="col-auto">
                            <?=Html::submitButton('Оценить', ['class' => 'btn btn-primary']); ?>
                        </div>
                    </div>
                <?=Html::endForm(); ?>
                <?php endif; ?>
                <div class="clearfix"></div>
            </div>
            <?=$this->render('@common/views/_partial/contacts', ['home' => true]); ?>
        </div>
    </div>
</div>