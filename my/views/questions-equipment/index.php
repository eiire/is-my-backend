<?php
$this->title = 'Вопросы по ТО, маркировке и ЕГАИС';
use yii\widgets\ActiveForm;
use common\widgets\Alert;
use common\widgets\RatingModal;
use yii\widgets\LinkPager;
?>
<div class="main">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
                <?=$this->render('@my/views/_partial/sidebar', ['profile' => $profile]); ?>
            </div>
            <div class="col-12 col-lg-10">
                <?=$this->render('@my/views/_partial/head', [
                    'link' => [
                        'label' => 'Новая заявка',
                        'route' => $profile['dogovor_to'] ? ['questions-equipment/report'] : ['?response=no_access']
                    ],
                    'profile' => $profile
                ]); ?>
                <div class="block">
                    <div class="row">
                        <div class="col-12 col-md">
                            <?=$this->render('@my/views/_partial/period', [
                                'form' => $form,
                                'period_start' => $period_start,
                                'period_end' => $period_end,
                                'filter' => ['items' => $filter]
                            ]); ?>
                        </div>
                        <div class="col-12 col-md-auto d-flex">
                            <?php if ($time) : ?><div class="h3">Оставшееся время консультаций: <?=$time; ?></div><?php endif; ?>
                        </div>
                    </div>
                    <?php
                        if ($response) echo Alert::Widget(['type' => $response['type'], 'message' => $response['message'], 'close' => $response['close']]);
                        if ($result) :
                            foreach ($result as $row) :
                                echo $this->render('@my/views/_item/cons', ['result' => $row]);
                            endforeach;
                            echo $this->render('@common/views/_partial/rating-form', ['form' => $form_rating]);
                    ?>
                    <div class="row">
                        <div class="col-12 col-md"><?=LinkPager::widget(['pagination' => $pagination]); ?></div>
                        <div class="col-12 col-md-auto m-t-5"><span class="text-lighten">Всего найдено</span> <?=$result_count; ?></div>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>