<?php
$this->title = 'Статическая проверка кода 1С';
use yii\widgets\ActiveForm;
use common\widgets\Alert;
use yii\helpers\Html;
use yii\helpers\Url;
?>
<div class="main">
    <div class="container">
        <div class="col-12 col-md-10 offset-md-1">
            <?=$this->render('@app/views/_partial/head-extend', ['profile' => $profile, 'title' => $this->title, 'url' => 'sca/index', 'nav' => 'exit']); ?>
            <div class="block">
                <?php if ($alert) echo Alert::Widget(['type' => $alert['type'], 'message' => $alert['message'], 'close' => $alert['close']]); ?>
                <?php
                    $active_form = ActiveForm::begin([
                        'enableClientValidation' => false,
                        'options' => ['autocomplete' => 'off'],
                    ]);
                ?>
                <div class="row">
                    <div class="col-12 col-md-4">
                        <div class="input-group">
                            <?=$active_form
                                ->field($form, 'email')
                                ->input('text')
                                ->label('Email');
                                ?>
                        </div>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="input-group">
                            <?=$active_form
                                ->field($form, 'type')
                                ->dropDownList($types)
                                ->label('Тип модуля');
                                ?>
                        </div>
                    </div>
                </div>
                <div class="input-group">
                    <?=$active_form
                        ->field($form, 'code')
                        ->textarea(['rows' => 12])
                        ->label('Код');
                    ?>
                </div>
                <div id="params" class="input-group d-none">
                    <div class="row">
                        <div class="col-auto">
                            <?=$active_form
                                ->field($form, 'srv', [
                                    'template' =>
                                        '<input type="checkbox" id="srv" name="ScaForm[srv]" value="1" class="checkbox">
                                        <label for="srv">Сервер</label>'
                                ])
                                ->checkbox();
                            ?>
                        </div>
                        <div class="col-auto">
                            <?=$active_form
                                ->field($form, 'clop', [
                                    'template' =>
                                        '<input type="checkbox" id="clop" name="ScaForm[clop]" value="1" class="checkbox">
                                        <label for="clop">Клиент обычное приложение</label>'
                                ])
                                ->checkbox();
                            ?>
                        </div>
                        <div class="col-auto">
                            <?=$active_form
                                ->field($form, 'clup', [
                                    'template' =>
                                        '<input type="checkbox" id="clup" name="ScaForm[clup]" value="1" class="checkbox">
                                        <label for="clup">Клиент управляемое приложение</label>'
                                ])
                                ->checkbox();
                            ?>
                        </div>
                        <div class="col-auto">
                            <?=$active_form
                                ->field($form, 'int', [
                                    'template' =>
                                        '<input type="checkbox" id="int" name="ScaForm[int]" value="1" class="checkbox">
                                        <label for="int">Внешнее cоединение</label>'
                                ])
                                ->checkbox();
                            ?>
                        </div>
                    </div>
                </div>
                <div class="input-group text-right">
                    <?=Html::submitButton('Проверить', ['class' => 'btn btn-primary']); ?>
                </div>
                <?php ActiveForm::end(); ?>
                <?php
                    $active_form = ActiveForm::begin([
                        'enableClientValidation' => false,
                        'options' => ['autocomplete' => 'off'],
                    ]);
                ?>
                <div class="input-group">
                    <?=$active_form
                        ->field($form_feedback, 'message')
                        ->textarea(['rows' => 4])
                        ->label('Комментарий');
                    ?>
                </div>
                <div class="input-group text-right">
                    <?=Html::submitButton('Отправить', ['class' => 'btn btn-primary']); ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
            <?=$this->render('@app/views/_partial/contacts', ['home' => true]); ?>
        </div>
    </div>
</div>