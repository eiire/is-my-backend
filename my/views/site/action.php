<?php
$this->title = 'История действий';
use yii\widgets\LinkPager;
use common\widgets\Alert;
use yii\helpers\Url;
?>
<div class="main">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
                <?=$this->render('@my/views/_partial/sidebar', ['profile' => $profile]); ?>
            </div>
            <div class="col-12 col-lg-10">
                <?=$this->render('@my/views/_partial/head', [
                    'profile' => $profile
                ]); ?>
                <div class="block">
                    <?php
                        echo $this->render('@my/views/_partial/period', [
                            'form' => $form,
                            'period_start' => $period_start,
                            'period_end' => $period_end,
                        ]); 
                        if ($response) : 
                            echo Alert::Widget(['type' => $response['type'], 'message' => $response['message'], 'close' => $response['close']]);
                        else:
                            foreach($result as $row) :
                                echo $this->render('@my/views/_item/default', ['items' => $row]);
                            endforeach;
                            echo LinkPager::widget(['pagination' => $pagination]);
                        endif;
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>