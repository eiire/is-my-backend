<?php
$this->title = 'Обратная связь';
use yii\widgets\ActiveForm;
use common\widgets\Alert;
use yii\helpers\Url;
use yii\helpers\Html;
?>
<div class="main">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
                <?=$this->render('@my/views/_partial/sidebar', ['profile' => $profile]); ?>
            </div>
            <div class="col-12 col-lg-10">
                <?=$this->render('@my/views/_partial/head', [
                    'profile' => $profile
                ]); ?>
                <div class="block">
                <?php if ($alert) echo Alert::Widget(['type' => $alert['type'], 'message' => $alert['message'], 'close' => $alert['close']]); ?>
                <div class="alert alert-primary">
                    <div class="h3">Обращаем ваше внимание</div> 
                    <span>Через данную форму принимаются только вопросы и предложения касательно работы Личного кабинета</span>
                </div>
                <?php
                    $active_form = ActiveForm::begin([
                        'enableClientValidation' => false,
                        'options' => ['autocomplete' => 'off', 'enctype' => 'multipart/form-data'],
                    ]); 
                ?>
                    <div class="row">
                        <div class="col-12">
                            <div class="input-group">
                                <?=$active_form
                                    ->field($form, 'message')
                                    ->textarea(['rows' => 8])
                                    ->label('Сообщение');
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <?=$active_form
                                ->field($form, 'file', [
                                    'template' =>
                                    "<div class='file-upload mb-n2'>
                                        <label>
                                            <input type='file' name='file'>
                                            <span>Выберите файл</span>
                                        </label>
                                        <input type='text' readonly>
                                    </div>"
                                ])
                                ->fileInput(['multiple' => false])
                                ->label(false);
                            ?>
                            <small>Допустимые форматы файла: <i>jpg</i>, <i>jpeg</i>, <i>png</i>, <i>bmp</i>. Максимальный размер файла 1 МБ.</small>
                        </div>
                        <div class="col-auto d-flex">
                            <?=Html::submitButton('Отправить', ['class' => 'btn btn-primary my-auto']); ?>
                        </div>
                    </div>
                <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>