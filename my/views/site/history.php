<?php
$this->title = 'История входов';
use yii\widgets\LinkPager;
use common\widgets\Alert;
use common\widgets\Modal;
?>
<div class="main">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
                <?=$this->render('@my/views/_partial/sidebar', ['profile' => $profile]); ?>
            </div>
            <div class="col-12 col-lg-10">
                <?=$this->render('@my/views/_partial/head', [
                    'button' => [
                        'label' => 'Завершить сеансы',
                        'balloon' => 'Завершить все активные сеансы',
                        'src' => '#confirm-modal'
                    ],
                    'profile' => $profile
                ]); ?>
                <div class="block">
                    <?php
                        echo $this->render('@my/views/_partial/period', [
                            'form' => $form,
                            'period_start' => $period_start,
                            'period_end' => $period_end,
                        ]); 
                        if ($response) echo Alert::Widget(['type' => $response['type'], 'message' => $response['message'], 'close' => $response['close']]); 
                    ?>
                    <p>История входов показывает информацию о том, в какое время вы входили в личный кабинет.</p>
                    <?php
                        foreach($result as $row) :
                            echo $this->render('@my/views/_item/default', ['items' => $row]);
                        endforeach;
                        echo LinkPager::widget(['pagination' => $pagination]);
                        echo Modal::widget([
                            'id' => 'confirm-modal',
                            'title' => 'Завершить все активные сеансы',
                            'message' => 'Вы уверены что хотите завершить все активные сеансы?',
                            'button' => [
                                'label' => 'Завершить',
                                'route' => ['handler/handler', 'method' => 'clear-sessions']
                            ]
                        ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>