<?php
$this->title = 'Личный кабинет';
use common\widgets\Alert;
use my\widgets\Toast;
use yii\helpers\Url;
use yii\helpers\Html;
?>
<div class="main">
    <div class="container">
        <div class="d-block d-lg-none">
            <?=$this->render('@my/views/_partial/sidebar', ['profile' => $profile]); ?>
        </div>
        <div class="row">
            <div class="col-12 col-md-10 offset-md-1">
                <?=$this->render('@my/views/_partial/head-extend', ['profile' => $profile, 'title' => 'Личный кабинет', 'url' => 'site/index', 'nav' => 'my']); ?>
                <div class="row">
                    <div class="col-12 col-md-6 col-lg-4">
                        <?=$this->render('@common/views/_partial/card', [
                            'title' => 'Линия консультаций',
                            'icon' => 'fas fa-question',
                            'route' => ['support/index']
                        ]); ?>
                    </div>
                    <?php if(!$profile['is_budget']) : ?>
                        <div class="col-12 col-md-6 col-lg-4">
                            <?=$this->render('@common/views/_partial/card', [
                                'title' => 'Обновление и сервисы ИТС',
                                'icon' => 'fas fa-download',
                                'route' => ['service/index']
                            ]); ?>
                        </div>
                    <?php endif; ?>
                    <div class="col-12 col-md-6 col-lg-4">
                        <?=$this->render('@common/views/_partial/card', [
                            'title' => 'Заказать работы Специалиста 1С',
                            'icon' => 'fas fa-user-cog',
                            'route' => ['specialist/index']
                        ]); ?>
                    </div>
                    <div class="col-12 col-md-6 col-lg-4">
                        <?=$this->render('@common/views/_partial/card', [
                            'title' => 'Облако',
                            'icon' => 'fas fa-cloud',
                            'route' => ['cloud/index']
                        ]); ?>
                    </div>
                    <div class="col-12 col-md-6 col-lg-4">
                        <?=$this->render('@common/views/_partial/card', [
                            'title' => 'Документы, счета, ЛУВР',
                            'icon' => 'fas fa-file',
                            'route' => ['docs/index']
                        ]); ?>
                    </div>
                    <div class="col-12 col-md-6 col-lg-4">
                        <?=$this->render('@common/views/_partial/card', [
                            'title' => 'Вопросы по ТО, маркировке и ЕГАИС',
                            'icon' => 'fas fa-cash-register',
                            'route' => ['questions-equipment/index']
                        ]); ?>
                    </div>
                </div>
                <?=$this->render('@my/views/_partial/contacts', ['home' => true]); ?>
                <?php if ($companies) : ?>
                <div id="company" class="modal" style="width:400px;display:none;">
                    <div class="modal-title">Выбор юр.лица</div>
                    <div class="modal-desc py-2">
                        <p>Вы сможете изменить юр.лицо в Настройках.</p>
                        <?php foreach ($companies as $company) : ?>
                            <div class="row my-2">
                                <div class="col"><?=$company['name']; ?></div>
                                <div class="col-auto">
                                    <a href="<?=Url::to(['handler/handler', 'method' => 'set-code', 'id' => $company['id']]); ?>" class="btn btn-primary btn-small">Выбрать</a>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <?php if ($news) : ?>
    <div class="toast">
        <?php 
            foreach ($news as $row) :
                echo Toast::widget(['id' => $row['id'], 'title' => $row['title'], 'content' => $row['content']]);
            endforeach;
        ?>
    </div>
    <?php endif; ?>
</div>