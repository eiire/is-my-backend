<?php
$this->title = 'Настройки';
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use common\widgets\Alert;
use common\widgets\Modal;

// $this->registerJsFile('js/vue.min.js', ['position'=>\yii\web\View::POS_HEAD]);
$this->registerJsFile('https://cdn.jsdelivr.net/npm/vue/dist/vue.js', ['position'=>\yii\web\View::POS_HEAD]);
$this->registerJsFile('js/components.js');
?>
<div class="main">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
                <?=$this->render('@my/views/_partial/sidebar', ['profile' => $profile]); ?>
            </div>
            <div class="col-12 col-lg-10">
                <?=$this->render('@my/views/_partial/head', [
                    'profile' => $profile
                ]); ?>
                <div class="block">
                <?php if ($alert) echo Alert::Widget(['type' => $alert['type'], 'message' => $alert['message'], 'close' => $alert['close']]); ?>
                <?php
                    $active_form = ActiveForm::begin([
                        'enableClientValidation' => false,
                        'options' => ['autocomplete' => 'off'],
                    ]); 
                ?>
                    <h3 class="line">Аккаунт</h3>
                    <div class="row">
                        <div class="col-12 col-md-5">
                            <div class="input-group">
                            <?=$active_form
                                ->field($form, 'email')
                                ->input('email', ['value' => $email, 'disabled' => true])
                                ->label('Email');
                            ?>
                            </div>
                        </div>
                    </div>
                    <h3 class="line m-t-20">Изменить пароль</h3>
                    <div class="row">
                        <div class="col-12 col-md-5">
                            <div class="input-group">
                            <?=$active_form
                                ->field($form, 'old_password')
                                ->passwordInput()
                                ->input('password')
                                ->label('Старый пароль'); 
                            ?>
                            </div>
                            <div class="input-group">
                            <?=$active_form
                                ->field($form, 'new_password')
                                ->passwordInput()
                                ->input('password')
                                ->label('Новый пароль'); 
                            ?>
                            </div>
                            <div class="input-group">
                            <?=$active_form
                                ->field($form, 'new_password_repeat')
                                ->passwordInput()
                                ->input('password')
                                ->label('Новый пароль еще раз'); 
                            ?>
                            </div>
                            <div class="text-right">
                                <?=Html::submitButton('Сохранить', ['class' => 'btn btn-primary']); ?>
                            </div>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
                <div class="block">
                    <h3 class="line">Уведомления о балансе на счете Облака</h3>
                    <p class="mb-n2">Добавить email для уведомлений</p>
                    <?php
                        $active_form_2 = ActiveForm::begin([
                            'enableClientValidation' => false,
                            'options' => ['autocomplete' => 'off'],
                        ]); 
                    ?>
                    <div class="row no-gutters mt-3">
                        <div class="col-12 col-md-3">
                            <div class="input-group">
                                <?=$active_form_2
                                    ->field($form_2, 'email')
                                    ->input('text', ['data-mask-email' => true])
                                    ->label(false);
                                ?>
                            </div>
                        </div>
                        <div class="col ml-3">
                            <?=Html::submitButton('Добавить', ['class' => 'btn btn-primary btn-medium']); ?>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                    <?php if ($emails) : ?>
                    <p class="mb-1">Адреса, на которые будут приходить уведомления</p>
                    <?php foreach ($emails as $e) : ?>
                        <div class="mb-1"><?=$e['email']; ?> 
                        <?php if ($email_deleted) : ?>
                        <button data-fancybox data-src="#email-modal-<?=$e['id']; ?>" data-modal="true" class="btn-icon btn-icon-trash ml-1" data-balloon="Удалить"><i class="fas fa-times"></i></button>
                        <?php
                            echo Modal::widget([
                                'id' => 'email-modal-' . $e['id'],
                                'title' => 'Удалить email',
                                'message' => 'Вы уверены что хотите удалить email ' . $e['email'] . '?',
                                'button' => [
                                    'label' => 'Удалить',
                                    'route' => ['site/handler', 'method' => 'delete-email', 'email' => $e['email']]
                                ]
                            ]);
                        ?>
                        <?php endif; // $email_deleted ?>
                        </div>
                    <?php endforeach; ?>
                    <?php endif; // $emails ?>
                </div>
                <?php if ($companies) : ?>
                <div class="block">
                    <h3 class="line">Выбор юр.лица</h3>
                    <?php
                        $active_form_3 = ActiveForm::begin([
                            'enableClientValidation' => false,
                            'options' => ['autocomplete' => 'off'],
                        ]); 
                    ?>
                    <div class="row no-gutters mt-3">
                        <div class="col-12 col-md-3">
                            <div class="input-group">
                                <?=$active_form_3
                                    ->field($form_3, 'company')
                                    ->dropDownList($companies)
                                    ->label(false);
                                ?>
                            </div>
                        </div>
                        <div class="col ml-3">
                            <?=Html::submitButton('Выбрать', ['class' => 'btn btn-primary btn-medium']); ?>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
                <?php endif; ?>
                <?php /*
                <div class="block">
                    <h3 class="line">Двухфакторная аутентификация</h3>
                    <p>При включенной двухфакторной аутентификации, перед входом в аккаунт, вам будет оптправлено СМС-сообщение с одноразовым кодом. После успешного ввода кода из СМС вы получите доступ к своему аккаунту. Это гарантирует, что доступ к вашему аккаунт сможете получить только вы.</p>
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <div class="input-group">
                                <div class="row no-gutters">
                                    <div class="col-auto">
                                        <div class="switcher">
                                            <input type="checkbox" name="switcher" class="switcher-checkbox" id="switcher">
                                            <label class="switcher-label" for="switcher"></label>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="p-l-10">Включить двухфакторную аутентификацию</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <p class="text-danger">Для включения двухфакторной аутентификации необходимо указать и подтвердить номер телефона.</p>
                        </div>
                        <div class="col-12">
                            <?=$this->render('@my/views/_vue/phone-confirm', [
                                'phone' => $phone,
                                'enable' => $enable
                            ]); ?>
                        </div>
                    </div>
                </div>
                */ ?>
            </div>
        </div>
    </div>
</div>
