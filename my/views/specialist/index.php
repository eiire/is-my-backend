<?php
$this->title = 'Работы специалиста 1С';
use yii\widgets\ActiveForm;
use common\widgets\Alert;
use yii\widgets\LinkPager;
use yii\helpers\Url;
?>
<div class="main">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
                <?=$this->render('@my/views/_partial/sidebar', ['profile' => $profile]); ?>
            </div>
            <div class="col-12 col-lg-10">
                <?=$this->render('@my/views/_partial/head', [
                    'link' => [
                        'label' => 'Новая заявка',
                        'route' => ['specialist/report']
                    ],
                    'profile' => $profile
                ]); ?>
                <div class="block">
                    <?php if ($force_report) : ?>
                    <div class="alert alert-primary m-b-20">
                        <div class="row">
                            <div class="col">
                                <span>Создать срочную заявку на установку, настройку или консультацию по подключаемому оборудованию. Стоимость данных работ в 1.5 раза больше стоимости обычных работ.</span>
                            </div>
                            <div class="col-auto">
                                <a href="<?=Url::to(['specialist/forcereport']); ?>" class="btn btn-primary">Срочная заявка</a>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>
                    <?php
                        echo $this->render('@my/views/_partial/period', [
                            'form' => $form,
                            'period_start' => $period_start,
                            'period_end' => $period_end,
                            'data_period_end' => 'data-period'
                        ]);
                        if ($response) echo Alert::Widget(['type' => $response['type'], 'message' => $response['message'], 'close' => $response['close']]);
                        if ($result) :
                            foreach ($result as $row) :
                                echo $this->render('@my/views/_item/specialist', ['result' => $row]);
                            endforeach;
                            echo $this->render('@common/views/_partial/rating-form', ['form' => $form_rating]);
                    ?>
                    <div class="row">
                        <div class="col-12 col-md"><?=LinkPager::widget(['pagination' => $pagination]); ?></div>
                        <div class="col-12 col-md-auto m-t-5"><span class="text-lighten">Всего найдено</span> <?=$result_count; ?></div>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>