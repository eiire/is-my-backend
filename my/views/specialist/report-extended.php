<?php
$this->title = 'Заказать работы Специалиста 1С';
use yii\widgets\ActiveForm;
use common\widgets\Alert;
use common\widgets\WeekNav;
use yii\helpers\Url;
use yii\helpers\Html;
?>
<div class="main">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
                <?=$this->render('@my/views/_partial/sidebar', ['profile' => $profile]); ?>
            </div>
            <div class="col-12 col-lg-10">
                <?=$this->render('@my/views/_partial/head', [
                    'profile' => $profile
                ]); ?>
                <div class="block">
                <?php 
                if ($response) echo Alert::Widget(['type' => $response['type'], 'message' => $response['message'], 'close' => $response['close']]);
                if ($stage == 1) :
                    $active_form = ActiveForm::begin([
                        'enableClientValidation' => false,
                        'options' => ['autocomplete' => 'off', 'enctype' => 'multipart/form-data'],
                    ]); 
                ?>
                    <div class="row">
                        <div class="col-12 col-md-6 col-lg-3">
                            <div class="input-group">
                                <?=$active_form
                                    ->field($form_1, 'phone')
                                    ->input('text', ['value' => $contact_phone, 'data-mask-phone' => true])
                                    ->label('Номер телефона');
                                ?>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-3">
                            <div class="input-group">
                                <?=$active_form
                                    ->field($form_1, 'person')
                                    ->input('text', ['value' => $contact_person])
                                    ->label('Контактное лицо');
                                ?>
                            </div>
                        </div>
                        <div class="col-12 col-lg-3">
                            <div class="input-group">
                                <?=$active_form
                                    ->field($form_1, 'type')
                                    ->dropDownList($type)
                                    ->label('Способ выполнения');
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-lg-6">
                            <div class="input-group">
                                <?=$active_form
                                    ->field($form_1, 'soft')
                                    ->dropDownList($soft)
                                    ->label('Конфигурация 1С');
                                ?>
                            </div>
                        </div>
                        <div class="col-12 col-lg-6">
                            <div class="input-group">
                                <?=$active_form
                                    ->field($form_1, 'work')
                                    ->dropDownList($work)
                                    ->label('Вид работы');
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="input-group">
                        <?=$active_form
                            ->field($form_1, 'message')
                            ->textarea(['rows' => 8])
                            ->label('Описание работ');
                        ?>
                    </div>
                    <div class="row">
                        <div class="col-12 text-right">
                            <?=Html::submitButton('Продолжить', ['class' => 'btn btn-primary']); ?>
                        </div>
                    </div>
                <?php 
                    ActiveForm::end();
                    elseif ($stage == 2) :
                    if ($show_result) :
                ?>
                <?=WeekNav::Widget(); ?>
                <?php for ($i = 0; $i < count($result); $i++) : ?>
                    <div class="line m-b-5">
                        <?=$result[$i]['name']; ?>
                        <?php if ($result[$i]['attached']) : ?>
                        <span class="text-primary" data-balloon="Закрепленный специалист"><i class="fas fa-star"></i></span>
                        <?php endif; ?>
                    </div>
                    <div class="row">
                        <?=$this->render('@my/views/_partial/specialist', ['result' => $result[$i]['time'], 'days' => $days]); ?>
                    </div>
                <?php 
                    endfor;
                    $active_form = ActiveForm::begin([
                        'enableClientValidation' => false,
                        'options' => ['autocomplete' => 'off', 'enctype' => 'multipart/form-data'],
                    ]);
                        echo $active_form
                            ->field($form_2, 'guid')
                            ->input('text', ['hidden' => true])
                            ->label(false);
                        echo $active_form
                            ->field($form_2, 'hour')
                            ->input('text', ['hidden' => true])
                            ->label(false);
                        echo $active_form
                            ->field($form_2, 'date')
                            ->input('text', ['hidden' => true])
                            ->label(false);
                ?>
                <?php if ($form_btn) : ?>
                <div class="text-right">
                    <?=Html::submitButton('Запланировать', ['id' => 'specialisttimeform-btn', 'class' => 'btn btn-primary', 'disabled' => true]); ?>
                </div>
                <?php endif; ?>
                <?php
                    ActiveForm::end();
                    endif;
                    elseif ($stage == 3) :
                ?>
                    <div class="col-12 col-md-8 offset-md-2">
                        <div class="text-center">
                            <i class="fas fa-check-circle fa-10x text-success m-t-10"></i>
                            <div class="h2 m-t-10 m-b-5"><?=$title; ?></div>
                            <?php if ($desc) : ?><p><?=$desc; ?></p><?php endif; ?>
                            <?php if ($date) : ?><p class="m-b-0">Дата: <?=$date; ?></p><?php endif; ?>
                            <?php if ($time) : ?><p class="m-b-0">Время: <?=$time; ?></p><?php endif; ?>
                            <?php if ($duration) : ?><p>Приблизительная продолжительность: <?=$duration; ?></p><?php endif; ?>
                        </div>
                    </div>
                <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>