<?php
$this->title = 'Обращение на линию консультации';
use yii\widgets\ActiveForm;
use common\widgets\Alert;
use yii\helpers\Url;
use yii\helpers\Html;
?>
<div class="main">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
                <?=$this->render('@my/views/_partial/sidebar', ['profile' => $profile]); ?>
            </div>
            <div class="col-12 col-lg-10">
                <?=$this->render('@my/views/_partial/head', [
                    'profile' => $profile
                ]); ?>
                <div class="block">
                <?php if ($response) echo Alert::Widget(['type' => $response['type'], 'message' => $response['message'], 'close' => $response['close']]); ?>
                <?php if ($show_form) : ?>
                <?php
                    $active_form = ActiveForm::begin([
                        'enableClientValidation' => false,
                        'options' => ['autocomplete' => 'off', 'enctype' => 'multipart/form-data'],
                    ]); 
                ?>
                    <div class="row">
                        <div class="col-12 col-md-6 col-lg-3">
                            <div class="input-group">
                                <?=$active_form
                                    ->field($form, 'phone')
                                    ->input('text', ['value' => $contact_phone, 'data-mask-phone' => true])
                                    ->label('Номер телефона');
                                ?>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-3">
                            <div class="input-group">
                                <?=$active_form
                                    ->field($form, 'person')
                                    ->input('text', ['value' => $contact_person])
                                    ->label('Контактное лицо');
                                ?>
                            </div>
                        </div>
                        <div class="col-12 col-lg-6">
                            <div class="input-group">
                                <?=$active_form
                                    ->field($form, 'soft')
                                    ->dropDownList($soft)
                                    ->label('Конфигурация 1С');
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="input-group">
                        <?=$active_form
                            ->field($form, 'message')
                            ->textarea(['rows' => 8])
                            ->label('Сообщение');
                        ?>
                    </div>
                    <div class="row">
                        <div class="col-12 text-right">
                            <?=Html::submitButton('Отправить <i class="fas fa-paper-plane m-l-5"></i>', ['class' => 'btn btn-primary']); ?>
                        </div>
                    </div>
                <?php ActiveForm::end(); ?>
                <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>