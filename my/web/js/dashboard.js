$(document).ready(function() {
    $('#autoupdate').click(function() {
        $.ajax({
            type: 'GET',
            url: '/handler/event',
            data: {
                method: 'questions-autoupdate',
            },
            success: function(data) {
                if (data == 'on') {
                    $('#autoupdate').prop('checked', true);
                } else {
                    $('#autoupdate').prop('checked', false);
                }
            }
        });
    });

    $('#schedule').change(function() {
        $.ajax({
            type: 'GET',
            url: '/handler/event',
            data: {
                method: 'questions-current',
                id: $(this).val()
            }
        });
    });

    $('#questionsform-event').change(function() {
        $('#questionsform-event option[value="false"]').remove();
        $.ajax({
            type: 'GET',
            url: '/handler/event',
            dataType: 'json',
            data: {
                method: 'get-questions',
                event: $(this).val()
            },
            success: function(data) {
                var select, options, option;
                select = document.getElementById('questionsform-speaker');
                select.options.length = 0;
                options = data;
                for (var i = 0; i < options.length; i++) {
                    option = options[i];
                    select.options.add(new Option(option.title, option.id));
                }
            }
        })
    });
});