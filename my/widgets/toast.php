<?php
namespace my\widgets;

use yii\base\Widget;
use yii\helpers\Html;

class Toast extends Widget {
    public $id;
    public $title;
    public $content;

    public function run() {
        $inner = Html::tag('div', $this->title, ['class' => 'toast-title']);
        $inner .= Html::tag('div', $this->content, ['class' => 'toast-content']);

        $button = Html::button('Понятно', ['class' => 'btn btn-primary btn-small', 'data-news' => $this->id, 'data-toast-close' => true]);
        $inner .= Html::tag('div', $button, ['class' => 'toast-control']);

        $toast = Html::tag('div', $inner, ['class' => 'toast-wrap']);

        return $toast;
    }
}